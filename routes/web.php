<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Home;
use App\Http\Controllers\Auth;
use App\Http\Controllers\Admin;
use App\Http\Controllers\Asesor;
use App\Http\Controllers\Asesi;
use App\Http\Controllers\Penyelia;
use App\Http\Controllers\Alamat;
use App\Http\Controllers\AsesmenController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Monitoring;
use App\Http\Controllers\MasterData;
use App\Http\Controllers\ManajemenJadwal;
use App\Http\Controllers\UjiKompetensi;
use App\Http\Controllers\apl02Controller;
use App\Http\Controllers\ak01Controller;
use App\Http\Controllers\Ia11Controller;
use App\Http\Controllers\ak05Controller;
use App\Http\Controllers\blankoController;
use App\Http\Controllers\Berita;
use App\Http\Controllers\Galeri;
use App\Http\Controllers\Artikel;
use App\Http\Controllers\Informasi;
use App\Http\Controllers\Skema;
use App\Http\Controllers\Asesmen;
use App\Http\Controllers\Mapa_mkva;

route::get('/wilcim', function () {
    return view('welcome');
});


Route::controller(Home::class)->group(function () {
    Route::get('/', 'home');
    Route::get('/galeri', 'galeri');
    Route::get('/artikel', 'artikel');
    Route::get('/berita', 'berita');
    Route::get('/detail_skema/{id}', 'detail_skema');
});

Route::controller(Berita::class)->group(function () {
    Route::get('/data_berita', 'data_berita');
    Route::get('/getDataBerita', 'getDataBerita');
    Route::post('/saveDataBerita', 'saveDataBerita');
    Route::post('/hapusDataBerita', 'hapusDataBerita');
    Route::post('/getDataBeritaById', 'getDataBeritaById');
    Route::post('/updateDataBerita', 'updateDataBerita');
});

Route::controller(Galeri::class)->group(function () {
    Route::get('/data_galeri', 'data_galeri');
    Route::get('/getDataGaleri', 'getDataGaleri');
    Route::post('/saveDataGaleri', 'saveDataGaleri');
    Route::post('/hapusDataGaleri', 'hapusDataGaleri');
    Route::post('/getDataGaleriById', 'getDataGaleriById');
    Route::post('/updateDataGaleri', 'updateDataGaleri');
});

Route::controller(Artikel::class)->group(function () {
    Route::get('/data_artikel', 'data_artikel');
    Route::get('/getDataArtikel', 'getDataArtikel');
    Route::post('/saveDataArtikel', 'saveDataArtikel');
    Route::post('/hapusDataArtikel', 'hapusDataArtikel');
    Route::post('/getDataArtikelById', 'getDataArtikelById');
    Route::post('/updateDataArtikel', 'updateDataArtikel');
});

Route::controller(Informasi::class)->group(function () {
    Route::get('/data_informasi', 'data_informasi');
    Route::get('/getDataInformasi', 'getDataInformasi');
    Route::post('/saveDataInformasi', 'saveDataInformasi');
    Route::post('/hapusDataInformasi', 'hapusDataInformasi');
    Route::post('/getDataInformasiById', 'getDataInformasiById');
    Route::post('/updateDataInformasi', 'updateDataInformasi');
});

Route::controller(Skema::class)->group(function () {
    Route::get('/data_skema', 'data_skema');
    Route::get('/getDataSkema', 'getDataSkema');
    Route::get('/getSubSkema', 'getSubSkema');
    Route::post('/saveDataSkema', 'saveDataSkema');
    Route::post('/hapusDataSkema', 'hapusDataSkema');
    Route::post('/getDataSkemaById', 'getDataSkemaById');
    Route::post('/updateDataSkema', 'updateDataSkema');
});

Route::controller(Auth::class)->group(function () {
    Route::get('/login', 'login');
    Route::post('/validasiLogin', 'validasiLogin');
    Route::get('/logout', 'logout');
    Route::get('/daftar', 'daftar');
    Route::post('/savedaftar', 'store');
    Route::get('/ubahPassword', 'ubahPassword');
    Route::post('/saveUbahPassword', 'saveUbahPassword');
});

Route::controller(Admin::class)->group(function () {
    Route::get('/admin', 'admin');
    Route::get('/profile_lsp', 'profile_lsp');
    Route::post('/saveProfileLsp', 'saveProfileLsp');
    Route::get('/profile_admin', 'profiladmin');
    Route::post('/saveProfileAdmin', 'saveProfileadmin');
});

Route::controller(Asesor::class)->group(function () {
    Route::get('/asesor', 'asesor');
    Route::get('/profile_asesor', 'profile_asesor');
    Route::post('/saveProfileAsesor', 'saveProfileAsesor');
});

Route::controller(Asesi::class)->group(function () {
    Route::get('/asesi', 'asesi');
    Route::get('/profile_asesi', 'profile_asesi');
    Route::post('/saveProfileAsesi', 'saveProfileAsesi');
});

Route::controller(Penyelia::class)->group(function () {
    Route::get('/penyelia', 'penyelia');
    Route::get('/profile_penyelia', 'profile_penyelia');
    Route::post('/saveProfilePenyelia', 'saveProfilePenyelia');
});

Route::controller(AsesmenController::class)->group(function () {
    Route::get('/jadwal_ia011', 'index');
    Route::get('/mia011/{id}/{id_jadwal_asesmen}', 'cobi');
    Route::post('/savemia11', 'saveia11');
});

Route::controller(Alamat::class)->group(function () {
    Route::post('/getKabKota', 'getKabKota');
});
// monitoring
Route::controller(Monitoring::class)->group(function () {
    Route::get('/monitoring_apl01', 'JadwalAsesmen_monitoringapl01');
    Route::get('/getJadwalAsesmen_monitoring_apl01', 'getJadwalAsesmen');
    Route::get('/jadwalLanjut_apl01_monitoring/{id}', 'jadwalLanjut_monitoring_apl01');
    Route::get('/pilih_asesi_monitoring_apl01/{no_reg}/{id_jadwal}', 'asesi_monitoring_apl01');
    Route::get('/detail_apl01/{id}/{id_jadwal_asesmen}', 'PrintApl01');
    Route::get('/Edit_apl01/{id}/{id_jadwal_asesmen}', 'EditApl01');
    Route::post('/UpdateApl01/{id}', 'UpdateApl01');
});

Route::controller(apl02Controller::class)->group(function () {
    Route::get('/monitoring_apl02', 'JadwalAsesmen_monitoringapl02');
    Route::get('/getJadwalAsesmen_apl02', 'getJadwalAsesmen');
    Route::get('/jadwalLanjut_apl02_monitoring/{id}', 'jadwalLanjut_monitoring_apl02');
    Route::get('/pilih_asesi_monitoring_apl02/{no_reg}/{id_jadwal}', 'asesi_monitoring_apl02');
    Route::get('/detail_apl02/{id}', 'detail_print_apl02');
    Route::get('/GetSkema_full/{id_jadwal_asesmen}', 'GetSkema_full');
});

Route::controller(ak01Controller::class)->group(function () {
    Route::get('/monitoring_ak01', 'indexak01');
    Route::get('/getJadwalMonitoring_ak01', 'getJadwalAsesmen');
    Route::get('/jadwalLanjut_ak01_monitoring/{id}', 'jadwalLanjut_monitoring_ak01');
    Route::get('/pilih_asesi_monitoring_ak01/{no_reg}/{id_jadwal}', 'asesi_monitoring_ak01');
    Route::get('/detail_ak01/{id}', 'detail_print_ak01');
});

Route::controller(Ia11Controller::class)->group(function () {
    Route::get('/monitoring_ia11', 'indexia11');
    Route::get('/getJadwalMonitoring_ia11', 'getJadwalAsesmen');
    Route::get('/jadwalLanjut_ia11_monitoring/{id}', 'jadwalLanjut_monitoring_ia11');
    Route::get('/pilih_asesi_monitoring_ia11/{no_reg}/{id_jadwal}', 'asesi_monitoring_ia11');
    Route::get('/detail_ia11/{id}/{id_jadwal_asesmen}', 'detail_print_ia11');
});

Route::controller(ak05Controller::class)->group(function () {
    Route::get('/monitoring_ak05', 'indexak05');
    Route::get('/pilihjadwallanjut/{id}', 'Getjadwal_ak05');
});

Route::controller(blankoController::class)->group(function () {
    Route::get('/monitoring_blanko', 'indexblanko');
});
// end monitoring

Route::controller(MasterData::class)->group(function () {
    Route::get('/getSrukturJabatanLsp', 'getSrukturJabatanLsp');
    Route::get('/struktur_jabatan', 'struktur_jabatan');
    Route::post('/saveStrukturJabatanLsp', 'saveStrukturJabatanLsp');
    Route::post('/hapusStrukturJabatanLsp', 'hapusStrukturJabatanLsp');
    Route::post('/getDataStrukturJabatanLsp', 'getDataStrukturJabatanLsp');
    Route::post('/updateStrukturJabatanLsp', 'updateStrukturJabatanLsp');
    //daftar_admin
    Route::get('/daftar_admin', 'daftar_admin');
    Route::get('/getDaftarAdmin', 'getDaftarAdmin');
    Route::post('/saveDaftarAdmin', 'saveDaftarAdmin');
    Route::post('/hapusDaftarAdmin', 'hapusDaftarAdmin');
    Route::post('/getDataDaftarAdmin', 'getDataDaftarAdmin');
    Route::post('/updateDataDaftarAdmin', 'updateDataDaftarAdmin');
    //daftar_penyelia
    Route::get('/daftar_penyelia', 'daftar_penyelia');
    Route::get('/getDaftarPenyelia', 'getDaftarPenyelia');
    Route::post('/saveDaftarPenyelia', 'saveDaftarPenyelia');
    Route::post('/hapusDaftarPenyelia', 'hapusDaftarPenyelia');
    Route::post('/getDataDaftarPenyelia', 'getDataDaftarPenyelia');
    Route::post('/updateDataDaftarPenyelia', 'updateDataDaftarPenyelia');
    //daftar_tuk
    Route::get('/daftar_tuk', 'daftar_tuk');
    Route::get('/getDaftarTuk', 'getDaftarTuk');
    Route::post('/saveDaftarTuk', 'saveDaftarTuk');
    Route::post('/hapusDaftarTuk', 'hapusDaftarTuk');
    Route::get('/getDataDaftarTuk', 'getDataDaftarTuk');
    Route::post('/updateDataDaftarTuk', 'updateDataDaftarTuk');
    //daftar_asesor
    Route::get('/daftar_asesor', 'daftar_asesor');
    Route::get('/getDaftarAsesor', 'getDaftarAsesor');
    Route::post('/saveDaftarAsesor', 'saveDaftarAsesor');
    Route::post('/hapusDaftarAsesor', 'hapusDaftarAsesor');
    Route::post('/getDataDaftarAsesor', 'getDataDaftarAsesor');
    Route::post('/updateDataDaftarAsesor', 'updateDataDaftarAsesor');
    // template
    Route::get('/print-ak01', 'printak01');
    Route::get('/print-ak02', 'printak02');
    Route::get('/print-ak03', 'printak03');
    Route::get('/print-ak05', 'printak05');
});

Route::controller(ManajemenJadwal::class)->group(function () {
    Route::get('/jadwal_asesmen', 'jadwal_asesmen');
    Route::get('/getJadwalAsesmen', 'getJadwalAsesmen');
    Route::post('/getSubTuk', 'getSubTuk');
    Route::post('/getSubSkema', 'getSubSkema');
    Route::post('/saveJadwalAsesmen', 'saveJadwalAsesmen');
    Route::post('/hapusJadwalAsesmen', 'hapusJadwalAsesmen');
    Route::get('/jadwalLanjutTuk/{id}', 'jadwalLanjutTuk');

    Route::get('/penugasan_asesor', 'penugasan_asesor');
    Route::post('/getTanggalUjiJadwalAsesmen', 'getTanggalUjiJadwalAsesmen');
    Route::post('/getNoregAsesor', 'getNoregAsesor');
    Route::post('/saveJadwalAsesor', 'saveJadwalAsesor');

    Route::get('/surat_tugas', 'surat_tugas');
    Route::get('/getSuratTugas', 'getSuratTugas');
    Route::post('/saveSuratTugas', 'saveSuratTugas');
    Route::get('/view_surat_tugas/{id}/{id_jadwal}', 'view_surat_tugas');
    Route::get('/getDataSuratTugas', 'getDataSuratTugas');
});

Route::controller(UjiKompetensi::class)->group(function () {
    Route::get('/pskapl01', 'pskapl01');
    Route::get('/getJadwalAsesmen_pskapl01', 'getJadwalAsesmen_pskapl01');
    Route::get('/jadwalLanjut/{id}', 'jadwalLanjut');
    Route::get('/apl01lsp/{id_jadwal}/{no_reg}', 'apl01lsp');
    Route::post('/saveApl01Lsp', 'saveApl01Lsp');

    //MENU BANDING ASESMEN (AK 04)
    Route::get('/ba04', 'ba04');
    Route::post('/saveBa04', 'saveBa04');
    //MENU UMPAN BALI DAN CATATAN ASESMEN (AK 03)
    Route::get('/ubdcaak03', 'ubdcaak03');
    Route::post('/saveUbdcaak03', 'saveUbdcaak03');
    //MENU ASESMEN MANDIRI (APL 02)
    Route::get('/am02', 'am02');
    Route::get('/getJadwalAsesmen_am02', 'getJadwalAsesmen_am02');
    Route::post('/saveAm02', 'saveAm02');
    Route::get('/formAm02/{id}', 'formAm02');
    //MENU PERSETUJUAN ASESMEN DAN KERAHASIAAN (AK 01)
    Route::get('/padkak01', 'padkak01');
    Route::get('/getJadwalAsesmen_ak01', 'getJadwalAsesmen_ak01');
    Route::post('/savePadkak01', 'savePadkak01');
    Route::get('/formPadkak01/{id}', 'formPadkak01');
    //MENU FORMULIR REKAMAN ASESMEN KOMPETENSI (AK 02)
    Route::get('/frakak02', 'frakak02');
    Route::get('/getJadwalAsesmen_ak02', 'getJadwalAsesmen_ak02');
    Route::get('/getAsesi_ak02', 'getAsesi_ak02');
    Route::post('/saveFrakak02', 'saveFrakak02');
    Route::get('/frakak02_lanjut/{id_jadwal}', 'frakak02_lanjut');
    Route::get('/formFrakak02/{id_jadwal}/{id}', 'formFrakak02');
    Route::get('/asesiak02', 'asesiak02');
    Route::post('saveFrakak02_asesi', 'saveFrakak02_asesi');

    Route::get('/pppp', 'pppp');
});

Route::controller(Asesmen::class)->group(function () {
    //MENU UPLOAD SOAL PRAKTIK (IA 02)
    Route::get('/upload_ia02', 'upload_ia02');
    Route::post('/saveUpload_ia02', 'saveUpload_ia02');

    //MENU UPLOAD SOAL ESSAI (IA 04)
    Route::get('/upload_ia04', 'upload_ia04');
    Route::post('/saveUpload_ia04', 'saveUpload_ia04');

    //MENU UPLOAD SOAL PG (IA 05)
    Route::get('/upload_ia05', 'upload_ia05');
    Route::post('/saveUpload_ia05', 'saveUpload_ia05');

    //MENU VIEW SOAL PRAKTIK (IA 02)
    Route::get('/view_ia02', 'view_ia02');
    Route::post('/saveView_ia02', 'saveView_ia02');

    //MENU VIEW SOAL ESSAI (IA 04)
    Route::get('/view_ia04', 'view_ia04');
    Route::post('/saveView_ia04', 'saveView_ia04');

    //MENU VIEW SOAL PG (IA 05)
    Route::get('/view_ia05', 'view_ia05');
    Route::post('/saveView_ia05', 'saveView_ia05');
});

Route::controller(Mapa_mkva::class)->group(function () {
    //MENU UPLOAD MAPA01
    Route::get('/upload_mapa01', 'upload_mapa01');
    Route::post('/saveMapa01', 'saveMapa01');

    //MENU UPLOAD MAPA02
    Route::get('/upload_mapa02', 'upload_mapa02');
    Route::post('/saveMapa02', 'saveMapa02');

    //MENU UPLOAD MKVA
    Route::get('/upload_mkva', 'upload_mkva');
    Route::post('/saveMkva', 'saveMkva');
});
