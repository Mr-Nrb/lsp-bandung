<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListKebangsaan extends Model
{
    use HasFactory;

    protected $table = 'list_kebangsaan';

    public function users()
    {
        return $this->hasOne(User::class, 'kebangsaan', 'id');
    }
}