<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class apl01Model extends Model
{
    use HasFactory;
    protected $table = 'apl01';
    protected $softDelete = false;
    protected $dates = ['tanggallahir'];
    public $timestamps = false;
    protected $fillable ;
}
