<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class skema extends Model
{
    use HasFactory;
    protected $table = 'skema';
    protected $primaryKey = 'id_skema';
    protected $softDelete = false;
    public $timestamps = false;
    protected $fillable = ['nama_skema'];

    public function unit()
    {
        return $this->hasMany(unit::class,'id_skema', 'id_skema');
    }
}
