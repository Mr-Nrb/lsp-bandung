<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WilayahProvinsi extends Model
{
    use HasFactory;

    protected $table = 'wilayah_2020';

    public function users()
    {
        return $this->hasOne(User::class, 'provinsi', 'kode');
    }
}