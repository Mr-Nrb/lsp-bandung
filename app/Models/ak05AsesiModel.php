<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ak05AsesiModel extends Model
{
    use HasFactory;
    protected $table = 'apl01';
    protected $softDelete = false;
    protected $dates = ['tanggallahir'];
    public $timestamps = false;
    protected $fillable ;
}
