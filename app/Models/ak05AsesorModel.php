<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ak05AsesorModel extends Model
{
    use HasFactory;
    protected $table = 'ak05_asesor';
    protected $primaryKey = 'id';
    protected $softDelete = false;
    public $timestamps = false;
    protected $fillable = [
                            'nama_skema',
                            'nomor_skema',
                            'tuk',
                            'nama_asesor',
                            'nomor_registrasi',
                            'tanggal_uji',
                            'judul_jadwal',
                            'aspek_negatif_positif',
                            'penolakan_hasil_asesmen',
                            'saran_perbaikan'
                          ];
}
