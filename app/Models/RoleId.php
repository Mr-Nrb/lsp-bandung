<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoleId extends Model
{
    use HasFactory;

    protected $table = "master_users_role";

    public function users() 
    {
        return $this->hasOne(User::class,'role_id','id');
    }
}
