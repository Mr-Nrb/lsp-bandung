<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $table = 'users';
    
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function asesor()
    {
        return $this->hasOne(ListAsesor::class, 'id_akun', 'id');
    }

    public function role()
    {
        return $this->belongsTo(RoleLevel::class, 'role_id', 'id');
    }

    public function listkebangsaan()
    {
        return $this->belongsTo(ListKebangsaan::class, 'kebangsaan', 'id');
    }

    public function listkelamin() 
    {
        return $this->belongsTo(ListKelamin::class, 'jenis_kelamin', 'id');
    }

    public function listpendidikan() 
    {
        return $this->belongsTo(ListPendidikan::class, 'pendidikan', 'id');
    }

    public function listprofesi() 
    {
        return $this->belongsTo(ListPekerjaan::class, 'pekerjaan', 'id');
    }

    public function listprovinsi()
    {
        return $this->belongsTo(WilayahProvinsi::class, 'provinsi', 'kode');
    }

    public function listkabupatenkota()
    {
        return $this->belongsTo(WilayahKabupatenkota::class, 'kabupaten_kota', 'kode');
    }
}
