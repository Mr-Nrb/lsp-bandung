<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListAsesor extends Model
{
    use HasFactory;
    protected $table = 'list_asesor';

    public function users()
    {
        return $this->belongsTo(User::class, 'id_akun', 'id');
    }
}