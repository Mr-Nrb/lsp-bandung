<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class unit extends Model
{
    use HasFactory;
    protected $table = 'unit';
    protected $primaryKey = 'id_unit';
    protected $softDelete = false;
    public $timestamps = false;
    protected $fillable = ['id_skema','nomor_unit'];

    public function skema()
    {
        return $this->belongsTo(skema::class, 'id_skema', 'id_skema');
    }
}
