<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListPekerjaan extends Model
{
    use HasFactory;

    protected $table = 'list_pekerjaan';

    public function users()
    {
        return $this->hasOne(User::class, 'pekerjaan','id');
    }
}