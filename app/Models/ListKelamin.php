<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListKelamin extends Model
{
    use HasFactory;

    protected $table = 'list_kelamin';

    public function users() 
    {
        return $this->hasOne(User::class, 'jenis_kelamin', 'id');
    }
}