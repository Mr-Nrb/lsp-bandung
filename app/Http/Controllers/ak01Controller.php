<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\skema;
use App\Models\list_unitModel;
use App\Models\Viewlistps;
use App\Models\Viewprakerin;
use Illuminate\Support\Facades\DB;

class ak01Controller extends Controller
{
    protected $skema;
    
    public function __construct()
    {
        // $this->skema = skema::all();
    }

    public function indexak01(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Monitoring";
        $data['title_sub_menu'] = "Monitoring AK01";
        $token = $data_session['token'];
        $data['jadwal_asesmen'] = DB::table("jadwal_asesmen")->get();
       
        return view('monitoring.jadwal_asesmen_monitoring_ak01', $data);
       
    }

    public function getJadwalAsesmen(Request $request)
    {
        $data = DB::select("SELECT
                            `a`.`id` AS `id`,
                            `a`.`tanggal_uji` AS `tanggal_uji`,
                            `a`.`nama_jadwal` AS `nama_jadwal`,
                            `b`.`nama` AS `nama_tuk`,
                            `a`.`tema` AS `tema`,
                            `c`.`nama` AS `nama_skema`,
                            `c`.`nomor` AS `nomor_skema`,
                            `c`.`id_skema` AS `id_skema`,
                            `a`.`target_peserta` AS `target_peserta`,
                            `a`.`jam_uji` AS `jam_uji`,
                            `a`.`lokasi_uji` AS `lokasi_uji`,
                            `d`.`nama` AS `nama_sumber_anggaran`,
                            `a`.`anggaran_perasesi` AS `anggaran_perasesi`,
                            `a`.`nomor_spk` AS `nomor_spk`,
                            `a`.`tanggal_spk` AS `tanggal_spk`,
                            `a`.`tgl_mulai_pelatihan` AS `tgl_mulai_pelatihan`,
                            `a`.`tgl_akhir_pelatihan` AS `tgl_akhir_pelatihan`,
                            `a`.`durasi_pelatihan` AS `durasi_pelatihan`,
                            `a`.`nama_pelatihan` AS `nama_pelatihan`,
                            `a`.`nama_instansi` AS `nama_instansi`,
                            `a`.`nama_lsp` AS `nama_lsp`
                            FROM `jadwal_asesmen` `a`
                            JOIN `list_tuk` `b`
                            ON `a`.`tuk` = `b`.`id`
                            JOIN `list_skema` `c`
                            ON `a`.`skema` = `c`.`kode`
                            JOIN `list_sumber_anggaran` `d`
                            ON `a`.`sumber_anggaran` = `d`.`id`
                            WHERE `a`.`tanggal_uji`>=CURRENT_DATE - INTERVAL 1 WEEK ORDER BY `a`.`tanggal_uji` DESC");
        echo json_encode($data);
    }

    public function jadwalLanjut_monitoring_ak01(Request $request, $id)
    {
        $id_jadwal_asesmen = $id;
        $jadwal = DB::table('jadwal_asesor')
        ->where('id_jadwal','=',$id_jadwal_asesmen)->get();

        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Manajemen Jadwal";
        $data['title_sub_menu'] = "Jadwal Asesor";
        // dd($id_jadwal_asesmen);
        return view('monitoring.pilih_asesor_monitoring_ak01', $data, ['jadwal' => $jadwal]);
    }

    public function asesi_monitoring_ak01(Request $request, $no_reg, $id_jadwal_asesmen)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Monitoring";
        $data['title_sub_menu'] = "Monitoring Apl01";
        $token = $data_session['token'];
        // $data['data_ak01'] = DB::table("ak_ak01")->where('nama_asesor', '=', $no_reg, 'AND', 'id_jadwal', '=', $id_jadwal_asesmen)->get();
        $data['data_ak01'] = DB::select(" SELECT 
                                            `ak_ak01`. *,
                                            `users`. `name`,
                                            `jadwal_asesmen`.`nama_jadwal`

                                            FROM `list_asesor`
                                            JOIN `users`
                                            ON `list_asesor`.`id_akun` = `users`.`id`
                                            JOIN `ak_ak01`
                                            ON `list_asesor`.`met_asesor` = `ak_ak01`.`nama_asesor`
                                            JOIN `jadwal_asesmen`
                                            ON `ak_ak01`.`id_jadwal` = `jadwal_asesmen`.`id`
                                            WHERE `ak_ak01`.`nama_asesor` = '$no_reg' 
                                            AND `ak_ak01`.`id_jadwal` = $id_jadwal_asesmen;
                                                 ");
        // dd($data['data_ak01']);
        return view('monitoring.monitoring_ak01', $data);
    }
    public $bukti_mantap;
    public function detail_print_ak01(Request $request, $id )
    {
        //ambil tanggal dari jadwal asesmen
        $res_jadwal_asesmen = DB::table('jadwal_asesmen')->where('id', $id)->first();
        $id_jadwal_asesmen = $id;
        // $data['res_jadwal_asesmen'] = DB::table('jadwal_asesmen')->where('id', $id_jadwal_asesmen)->first();
        $data['res_jadwal_asesmen'] = DB::select("SELECT
                                        `a`.`nama_jadwal` AS `nama_jadwal`,
                                        `a`.`tanggal_spk` AS `tanggal_spk`,
                                        `a`.`lokasi_uji` AS `lokasi_uji`,
                                        `b`.`nama` AS `nama_skema`,
                                        `b`.`nomor` AS `nomor_skema`,
                                        `b`.`id_skema` AS `id_skema`,
                                        `c`.`nama` AS `nama_tuk`
                                        FROM `jadwal_asesmen` `a`
                                        JOIN `list_skema` `b`
                                        ON `a`.`skema` = `b`.`kode`
                                        JOIN `list_tuk` `c`
                                        ON `a`.`tuk` = `c`.`id`
                                        WHERE `a`.`id` = $id_jadwal_asesmen");
        $data['list_tujuan_asesmen'] = DB::select("SELECT * from list_tujuan_asesmen");
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $email = $data_session['email'];
        $data['data_ak01'] = DB::select(" SELECT
                                                    `ak_ak01`. *,
                                                    `users`. `name`,
                                                    `jadwal_asesmen`.`nama_jadwal`,
                                                    `apl01`.*
                                                    FROM `list_asesor`
                                                    JOIN `users`
                                                    ON `list_asesor`.`id_akun` = `users`.`id`
                                                    JOIN `ak_ak01`
                                                    ON `list_asesor`.`met_asesor` = `ak_ak01`.`nama_asesor`
                                                    JOIN `jadwal_asesmen`
                                                    ON `ak_ak01`.`id_jadwal` = `jadwal_asesmen`.`id`
                                                    JOIN `apl01`
                                                    ON `ak_ak01`.`id_jadwal` = `jadwal_asesmen`.`id`
                                                    WHERE `ak_ak01`.`id` = $id 
                                                   ");
        $data['ttd_asesor'] = DB::select("SELECT 
                                          users.tanda_tangan
                                          FROM users
                                          JOIN list_asesor 
                                          ON users.id = list_asesor.id_akun
                                          JOIN ak_ak01 
                                          ON list_asesor.met_asesor = ak_ak01.nama_asesor
                                          WHERE ak_ak01.id = $id   
                                        ");
        // $data['ttd_asesi'] = DB::select("SELECT 
        //                                 users.tanda_tangan
        //                                 FROM users
        //                                 JOIN list_asesor 
        //                                 ON users.id = list_asesor.id_akun
        //                                 JOIN ak_ak01 
        //                                 ON list_asesor.met_asesor = ak_ak01.nama_asesor
        //                                 WHERE ak_ak01.id = $id   
        //                                 ");
                                                
        $data['asesiak01bukti'] = DB::select("SELECT 
        `a`.`bukti`
        FROM `ak_ak01` `a`
        WHERE `id` = $id
        ");
        // dd($data['asesiak01bukti']);
        $bukti = $data['asesiak01bukti'];
        // dd($bukti);
        $bukti2 = $bukti[0]->bukti;
        // dd($bukti2);
        $bukti3 = explode(";", $bukti2);

        $data['explode_bukti'] = $bukti3;

        // dd($data['explode_bukti']);

        $data['list_bukti_ak01'] = DB::select(" SELECT * FROM list_bukti ");
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Persetujuan Asesmen dan Kerahasiaan";
        $token = $data_session['token'];
        return view('monitoring.view_print_ak01', $data,
        [
        'bukti2' => $bukti2,
        'buktimantap' => $this->bukti_mantap
        ]);
    }
}
