<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;


class AsesmenController extends Controller
{
        public function index(Request $request)
        {
            $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $email = $data_session['email'];
        $data['data_apl01'] = DB::select(" SELECT * FROM apl01");
      //  @dd($data['data_apl01']);
        $data['list_bukti'] = DB::select(" SELECT * FROM list_bukti ");
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Meninjau Instrumen Asesmen (IA11)";
        $token = $data_session['token'];
        return view('asesmen.jadwal_ia11', $data);
        }

        public function cobi(Request $request,string $id, string $id_jadwal_asesmen)
        {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $email = $data_session['email'];
        $data['data_apl01'] = DB::table('apl01')->where('id', $id)->first();
        $data['data_apl01'] = DB::table('apl01')->where('id_jadwal_asesmen', $id_jadwal_asesmen)->first();
       // @dd($data['data_apl01']);
        // $data['res_jadwal_asesmen'] = DB::table('jadwalasesmen')->where('juduljadwal', $id_jadwal_asesmen)->first();
        //  @dd( $data['res_jadwal_asesmen'] );
        $data['res_jadwal_asesmen'] = DB::select("SELECT
                                                  `a`.`nama_jadwal` AS `nama_jadwal`,
                                                  `a`.`tanggal_spk` AS `tanggal_spk`,
                                                  `a`.`lokasi_uji` AS `lokasi_uji`,
                                                  `b`.`nama` AS `nama_skema`,
                                                  `b`.`nomor` AS `nomor_skema`,
                                                  `b`.`id_skema` AS `id_skema`,
                                                  `c`.`nama` AS `nama_tuk`
                                                  FROM `jadwal_asesmen` `a`
                                                  JOIN `list_skema` `b`
                                                  ON `a`.`skema` = `b`.`kode`
                                                  JOIN `list_tuk` `c`
                                                  ON `a`.`tuk` = `c`.`id`
                                                  WHERE `a`.`id` = $id_jadwal_asesmen");
        $id_skema = $data['res_jadwal_asesmen'][0]->id_skema;
        $data['get_skema'] = DB::select("SELECT id,induk,nama,
                                        max(case when (nama='1') then isi else '-' end) as kode,
                                        max(case when (nama='2') then isi else '-' end) as nama

                                        FROM (SELECT * FROM list_skema_full ORDER BY id ASC) a,
                                        (SELECT @pv := $id_skema) b
                                        WHERE find_in_set(induk, @pv)
                                        group by grup,induk order by id ASC");


        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Meninjau Instrumen Asesmen (IA11)";
        return view('asesmen.mia011', $data);
        }

        public function saveia11 (Request $request) {
          $data_session = $request->session()->get('dataUser');
          $token = $data_session['token'];
          $user = DB::table('users')->where('token', $token)->first();
          //jika ada tanda_tangan asesi
          if ($request->hasFile('tanda_tangan')) {
              $filenameWithExt = $request->file('tanda_tangan')->getClientOriginalName();
              $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
              $extension = $request->file('tanda_tangan')->getClientOriginalExtension(); // Get just Extension
              $fileName_tanda_tangan = 'ttd_penyelia' . '_' . $token . '.' . $extension; // Filename To store
              $request->tanda_tangan->move(public_path('assets/document/tanda_tangan/penyelai'), $fileName_tanda_tangan);
          } else {
              $fileName_tanda_tangan = "";
          }

          $idJadwalAsesmen = $request->input('idJadwalAsesmen');
          $juduljadwal = $request->input('juduljadwal');
          $nomor = $request->input('nomor');
          $nama_asesi = $request->input('nama_asesi');
          $namaasesor = $request->input('namaasesor');
          $no_reg = $request->input('no_reg');
          $tanggalasesmen = $request->input('tanggalasesmen');
          $instruksi_jelas = $request->input('instruksi_jelas');
          $informasi_tepat = $request->input('informasi_tepat');
          $kegiatan_asesmen = $request->input('kegiatan_asesmen');
          $kesulitan_bahasa = $request->input('kesulitan_bahasa');
          $kesulitan_kegiatan = $request->input('kesulitan_kegiatan');
          $ceklis_asesmen = $request->input('ceklis_asesmen');
          $modifikasi = $request->input('modifikasi');
          $tugas_asesmen = $request->input('tugas_asesmen');
          $peninjau = $request->input('peninjau');
          $ttdpenyelia = $request->input('ttdpenyelia');
          $komentar = $request->input('komentar');

          $data = array(
            'id_jadwal_asesmen' => $idJadwalAsesmen,
            'juduljadwal' => $juduljadwal,
            'nomor' => $nomor,
            'nama_asesi' => $nama_asesi,
            'namaasesor' => $namaasesor,
            'no_reg' => $no_reg,
            'tanggalasesmen' => $tanggalasesmen,
            'instruksi_jelas' => $instruksi_jelas,
            'informasi_tepat' => "$informasi_tepat",
            'kegiatan_asesmen' => "$kegiatan_asesmen",
            'kesulitan_bahasa' => "$kesulitan_bahasa",
            'kesulitan_kegiatan' => "$kesulitan_kegiatan",
            'ceklis_asesmen' => "$ceklis_asesmen",
            'modifikasi' => "$modifikasi",
            'tugas_asesmen' => "$tugas_asesmen",
            'peninjau' => $peninjau,
            'ttdpenyelia' => $ttdpenyelia,
            'komentar' => $komentar,
            
          );
          DB::table('ia11')->insert($data);
          Alert::success('Berhasil', 'Data Berhasil di Simpan');
          return redirect("/jadwal_ia011");
        }
}

