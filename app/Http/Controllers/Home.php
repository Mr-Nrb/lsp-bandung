<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class Home extends Controller
{

    public function home()
    {
        $data['data_informasi'] = DB::select('SELECT * FROM `list_informasi` `a` ORDER BY `id` DESC limit 5');
        $data['data_galeri'] = DB::select('SELECT * FROM `list_galeri` `a` ORDER BY `id` DESC limit 3');
        $data['data_kejuruan'] = DB::select("SELECT * FROM list_skema WHERE LENGTH(kode) = 2");
        return view('home.index', $data);
    }

    public function galeri()
    {
        $data['data_galeri'] = DB::select('SELECT * FROM `list_galeri` `a` ORDER BY `id` DESC');
        return view('home.galeri', $data);
    }

    public function artikel()
    {
        return view('home.artikel');
    }

    public function berita()
    {
        $data['data_berita'] = DB::select('SELECT * FROM `list_berita` WHERE `is_aktif` = 1 ORDER BY `id` DESC limit 3 ');
        return view('home.berita', $data);
    }

    public function detail_skema(Request $request, string $id)
    {
        //ambil tanggal dari jadwal asesmen
        $res_kejuruan = DB::table('list_skema')->where('id', $id)->first();
        $kode_kejuruan = $res_kejuruan->kode;
        $data['nama_kejuruan'] = $res_kejuruan->nama;

        $n = strlen($kode_kejuruan);
        $data['data_skema'] = DB::select("SELECT * FROM list_skema WHERE LEFT(kode,$n)='$kode_kejuruan' AND CHAR_LENGTH(kode)=5 ORDER BY nama");

        return view('home.detail_skema', $data);
    }
}