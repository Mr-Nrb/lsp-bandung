<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class Berita extends Controller
{
    public function data_berita(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Data Home Page";
        $data['title_sub_menu'] = "Data Berita";
        $data['list_status_aktif'] = DB::select('SELECT * FROM `list_status_aktif` ORDER BY id DESC');
        return view('data_home_page.data_berita', $data);
    }

    public function getDataBerita(Request $request)
    {
        $data = DB::select('SELECT *
                            FROM `list_berita` `a`');
        echo json_encode($data);
    }

    public function saveDataBerita(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        $judul_berita = $request->input('judul_berita');
        $tanggal_berita = $request->input('tanggal_berita');
        $keterangan = $request->input('keterangan');
        $status_aktif = $request->input('status_aktif');
        //jika ada gambar
        if ($request->hasFile('gambar')) {
            $filenameWithExt = $request->file('gambar')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('gambar')->getClientOriginalExtension(); // Get just Extension
            $fileName_gambar = 'gambar' . '_' . md5($filename) . '.' . $extension; // Filename To store
            $request->gambar->move(public_path('assets/document/home_page/berita'), $fileName_gambar);
        } else {
            $fileName_gambar = "";
        }

        $data = array(
            'judul_berita' => $judul_berita,
            'tanggal_berita' => $tanggal_berita,
            'keterangan' => $keterangan,
            'gambar' => $fileName_gambar,
            'is_aktif' => $status_aktif
        );

        DB::table('list_berita')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([Berita::class, 'data_berita']);
    }

    public function hapusDataBerita(Request $request)
    {
        $id = $request->input('id');

        DB::delete('DELETE FROM list_berita WHERE id = ?', [$id]);
        echo json_encode($id);
    }

    public function getDataBeritaById(Request $request)
    {
        $id = $request->input('id');
        $data = DB::select("SELECT * FROM list_berita WHERE id = '$id'");
        echo json_encode($data);
    }

    public function updateDataBerita(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];

        $id = $request->input('id');
        $data = DB::table('list_berita')->where('id', $id)->first();

        $judul_berita = $request->input('judul_berita');
        $tanggal_berita = $request->input('tanggal_berita');
        $keterangan = $request->input('keterangan');
        $status_aktif = $request->input('status_aktif');
        //jika ada tanda_tangan
        if ($request->hasFile('gambar')) {
            $filenameWithExt = $request->file('gambar')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('gambar')->getClientOriginalExtension(); // Get just Extension
            $fileName_gambar = 'gambar' . '_' . md5($filename) . '.' . $extension; // Filename To store
            $request->gambar->move(public_path('assets/document/home_page/berita'), $fileName_gambar);
        } else {
            $fileName_gambar = $data->gambar;
        }

        $update =  DB::table('list_berita')
            ->where('id', $id)
            ->update([
                'judul_berita' => $judul_berita,
                'tanggal_berita' => $tanggal_berita,
                'keterangan' => $keterangan,
                'gambar' => $fileName_gambar,
                'is_aktif' => $status_aktif
            ]);

        echo json_encode($update);
    }
}
