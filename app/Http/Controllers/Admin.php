<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;



class Admin extends Controller
{
    public function admin(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['token'] = $data_session['token'];
        $data['user'] = DB::table('users')->where('token',  $data['token'])->first();
        $data['title_menu'] = "Dashboard";
        return view('dashboard.admin', $data);
    }

    public function profile_lsp(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['token'] = $data_session['token'];
        $data['user'] = DB::table('users')->where('token',  $data['token'])->first();
        $data['title_menu'] = "Profile";
        $data['title_sub_menu'] = "Profile LSP";
        return view('admin.profile_lsp', $data);
    }

    public function saveProfileLsp(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();
        //jika ada dokument SK_LSP
        if ($request->hasFile('dokumen_sk_lsp')) {
            $filenameWithExt = $request->file('dokumen_sk_lsp')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_sk_lsp')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_sk_lsp = 'sk_lsp' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_sk_lsp->move(public_path('assets/document/sk_lsp'), $fileName_dokumen_sk_lsp);
        } else {
            $fileName_dokumen_sk_lsp = $user->dokumen_sk_lsp;
        }
        //jika ada dokument LISENSI_LSP
        if ($request->hasFile('dokumen_lisensi_lsp')) {
            $filenameWithExt = $request->file('dokumen_lisensi_lsp')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_lisensi_lsp')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_lisensi_lsp = 'lisensi_lsp' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_lisensi_lsp->move(public_path('assets/document/lisensi_lsp'), $fileName_dokumen_lisensi_lsp);
        } else {
            $fileName_dokumen_lisensi_lsp = $user->dokumen_lisensi_lsp;
        }
        //jika ada dokument NPWP
        if ($request->hasFile('dokumen_npwp')) {
            $filenameWithExt = $request->file('dokumen_npwp')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_npwp')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_npwp = 'npwp' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_npwp->move(public_path('assets/document/npwp'), $fileName_dokumen_npwp);
        } else {
            $fileName_dokumen_npwp = $user->dokumen_npwp;
        }
        //jika ada dokument dokumen_rek_bank
        if ($request->hasFile('dokumen_rek_bank')) {
            $filenameWithExt = $request->file('dokumen_rek_bank')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_rek_bank')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_rek_bank = 'rek_bank' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_rek_bank->move(public_path('assets/document/rek_bank'), $fileName_dokumen_rek_bank);
        } else {
            $fileName_dokumen_rek_bank = $user->dokumen_rek_bank;
        }

        $jenis_lsp = $request->input('jenis_lsp');
        $no_hp = $request->input('no_hp');
        $no_hp_kantor = $request->input('no_hp_kantor');
        $no_fax = $request->input('no_fax');
        $email = $request->input('email');
        $password = $request->input('password');
        $website = $request->input('website');
        $kode_pos = $request->input('kode_pos');
        $alamat_kantor = $request->input('alamat_kantor');
        $no_sk_lsp = $request->input('no_sk_lsp');
        $no_lisensi_lsp = $request->input('no_lisensi_lsp');
        $npwp = $request->input('npwp');
        $rekening_bank = $request->input('rekening_bank');
        $waktu_sekarang     = Date('Y-m-d');

        $update_users =  DB::table('users')
            ->where('token', $token)
            ->update([
                'jenis_lsp' => $jenis_lsp,
                'no_hp' => $no_hp,
                'no_hp_kantor' => $no_hp_kantor,
                'fax_kantor' => $no_fax,
                'email' => $email,
                'alamat_kantor' => $alamat_kantor,
                'password' => $password,
                'website' => $website,
                'kodepos' => $kode_pos,
                'no_sk_lsp' => $no_sk_lsp,
                'dokumen_sk_lsp' => $fileName_dokumen_sk_lsp,
                'no_lisensi_lsp' => $no_lisensi_lsp,
                'dokumen_lisensi_lsp' => $fileName_dokumen_lisensi_lsp,
                'npwp' => $npwp,
                'dokumen_npwp' => $fileName_dokumen_npwp,
                'rekening_bank' => $rekening_bank,
                'dokumen_rek_bank' => $fileName_dokumen_rek_bank,
                'updated_at' => $waktu_sekarang,
            ]);
        Alert::success('Berhasil', 'Data Berhasil di Update');
        return redirect()->action([Admin::class, 'profile_lsp']);
    }

    public function profiladmin(Request $request){
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['token'] = $data_session['token'];
        $data['user'] = DB::table('users')->where('token',  $data['token'])->first();

        $data['jenis_kelamin'] = DB::select('SELECT * FROM list_kelamin');
        $data['kebangsaan'] = DB::select('SELECT * FROM list_kebangsaan');
        $data['pendidikan'] = DB::select('SELECT * FROM list_pendidikan');
        $data['pekerjaan'] = DB::select('SELECT * FROM list_pekerjaan');
        $data['golongan'] = DB::select('SELECT * FROM list_pangkatgolongan');
        $data['jabatan'] = DB::select('SELECT * FROM list_jabatan');
        $data['provinsi'] = DB::select('SELECT * FROM wilayah_2020 WHERE CHAR_LENGTH(kode)=2 ORDER BY nama');
       
        $kode_kab_kota = $data['user']->kabupaten_kota;
        $data['kabupaten_kota'] = DB::select("SELECT * FROM wilayah_2020 WHERE kode = '$kode_kab_kota'");

        // @dd($data['provinsi']);

        $data['title_menu'] = "Profile";
        $data['title_sub_menu'] = "Profile Admin";
        return view('admin.profile_admin', $data);
    }

    public function saveProfileadmin(Request $request){
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();
        //jika ada tanda_tangan asesi
        if ($request->hasFile('tanda_tangan')) {
            $filenameWithExt = $request->file('tanda_tangan')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('tanda_tangan')->getClientOriginalExtension(); // Get just Extension
            $fileName_tanda_tangan = 'ttd_admin' . '_' . $token . '.' . $extension; // Filename To store
            $request->tanda_tangan->move(public_path('assets/document/tanda_tangan/admin'), $fileName_tanda_tangan);
        } else {
            $fileName_tanda_tangan = $user->tanda_tangan;
        }


        $email = $request->input('email');
        $password = $request->input('password');
        $name = $request->input('name');
        $nik = $request->input('nik');
        $tempat_lahir = $request->input('tempat_lahir');
        $tanggal_lahir = $request->input('tanggal_lahir');
        $jenis_kelamin = $request->input('jenis_kelamin');
        $kebangsaan = $request->input('kebangsaan');
        $alamat = $request->input('alamat');
        $kode_pos = $request->input('kode_pos');
        $pekerjaan = $request->input('pekerjaan');
        $provinsi = $request->input('provinsi');
        $kab_kota = $request->input('kab_kota');
        $no_hp = $request->input('no_hp');
        $pendidikan = $request->input('pendidikan');
        $institusi_perusahaan = $request->input('institusi_perusahaan');
        $panggol = $request->input('panggol');
        $jabatan = $request->input('jabatan');
        $nip = $request->input('nip');
        $waktu_sekarang     = Date('d-m-Y');

        $update_users =  DB::table('users')
            ->where('token', $token)
            ->update([
                'email' => $email,
                'password' => $password,
                'name' => $name,
                'nik' => $nik,
                'tempat_lahir' => $tempat_lahir,
                'tanggal_lahir' => $tanggal_lahir,
                'jenis_kelamin' => $jenis_kelamin,
                'kebangsaan' => $kebangsaan,
                'alamat' => $alamat,
                'kodepos' => $kode_pos,
                'provinsi' => $provinsi,
                'kabupaten_kota' => $kab_kota,
                'no_hp' => $no_hp,
                'kualifikasi_pendidikan' => $pendidikan,
                'pekerjaan' => $pekerjaan,
                'tanda_tangan' => $fileName_tanda_tangan,
                'institusi_perusahaan' => $institusi_perusahaan,
                'pangkat_golongan' => $panggol,
                'jabatan' => $jabatan,
                'nip' => $nip,
                'updated_at' => $waktu_sekarang
            ]);
        Alert::success('Berhasil', 'Data Berhasil di Update');
        return back();
    }

}