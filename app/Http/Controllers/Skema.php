<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class Skema extends Controller
{
    public function data_skema(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Data Home Page";
        $data['title_sub_menu'] = "Data Skema";
        $data['list_status_aktif'] = DB::select('SELECT * FROM `list_status_aktif` ORDER BY id DESC');
        $data['list_kejuruan'] = DB::select('SELECT * FROM list_kejuruan ORDER BY nama_kejuruan ASC');
        return view('data_home_page.data_skema', $data);
    }

    public function getDataSkema(Request $request)
    {
        $data = DB::select('SELECT 
                            `a`.*,
                            `b`.`nama_kejuruan` AS `kejuruan`,
                            `c`.`nama` AS `skema`
                            FROM `list_dokumen_skema` `a`
                            LEFT JOIN `list_kejuruan` `b`
                            ON `a`.`id_kejuruan` = `b`.`kode`
                            LEFT JOIN `list_skema` `c`
                            ON `a`.`id_skema` = `c`.`kode`
                            ');
        echo json_encode($data);
    }

    public function saveDataSkema(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        $skema_kejuruan = $request->input('skema_kejuruan');
        $skema = $request->input('skema');
        $status_aktif = $request->input('status_aktif');
        //jika ada gambar
        if ($request->hasFile('gambar')) {
            $filenameWithExt = $request->file('gambar')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('gambar')->getClientOriginalExtension(); // Get just Extension
            $fileName_gambar = 'gambar' . '_' . md5($filename) . '.' . $extension; // Filename To store
            $request->gambar->move(public_path('assets/document/home_page/skema'), $fileName_gambar);
        } else {
            $fileName_gambar = "";
        }

        $data = array(
            'id_kejuruan' => $skema_kejuruan,
            'id_skema' => $skema,
            'dokumen' => $fileName_gambar,
            'is_aktif' => $status_aktif
        );

        DB::table('list_dokumen_skema')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([Skema::class, 'data_skema']);
    }

    public function hapusDataSkema(Request $request)
    {
        $id = $request->input('id');
        DB::delete('DELETE FROM list_dokumen_skema WHERE id = ?', [$id]);
        echo json_encode($id);
    }

    public function getDataSkemaById(Request $request)
    {
        $id = $request->input('id');
        $data = DB::select("SELECT * FROM list_dokumen_skema WHERE id = '$id'");
        echo json_encode($data);
    }

    public function getSubSkema(Request $request)
    {
        $valTukKejuruan = $request->input('kode');
        $n = strlen($valTukKejuruan);
        $data = DB::select("SELECT * FROM list_skema WHERE LEFT(kode,$n)='$valTukKejuruan' AND CHAR_LENGTH(kode)=6 ORDER BY nama");
        echo json_encode($data);
    }

    public function updateDataSkema(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];

        $id = $request->input('id');
        $data = DB::table('list_dokumen_skema')->where('id', $id)->first();

        $skema_kejuruan = $request->input('skema_kejuruan');
        $skema = $request->input('skema');
        $status_aktif = $request->input('status_aktif');

        if ($request->hasFile('gambar')) {
            $filenameWithExt = $request->file('gambar')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('gambar')->getClientOriginalExtension(); // Get just Extension
            $fileName_gambar = 'gambar' . '_' . md5($filename) . '.' . $extension; // Filename To store
            $request->gambar->move(public_path('assets/document/home_page/skema'), $fileName_gambar);
        } else {
            $fileName_gambar = $data->dokumen;
        }

        if ($skema == "" || $skema == null) {
            $skema = $data->id_skema;
        }

        $update =  DB::table('list_dokumen_skema')
            ->where('id', $id)
            ->update([
                'id_kejuruan' => $skema_kejuruan,
                'id_skema' => $skema,
                'dokumen' => $fileName_gambar,
                'is_aktif' => $status_aktif
            ]);

        echo json_encode($update);
    }
}