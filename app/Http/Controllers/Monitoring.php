<?php

namespace App\Http\Controllers;

use GuzzleHttp\Psr7\Message;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class Monitoring extends Controller
{
    public function JadwalAsesmen_monitoringapl01(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Monitoring";
        $data['title_sub_menu'] = "Monitoring APL 01";
        $token = $data_session['token'];
        $data['jadwal_asesmen'] = DB::table("jadwal_asesmen")->get();
      
        // dd($data['data_apl01']);
        return view('monitoring.jadwal_asesmen_monitoring', $data);
    }

    public function getJadwalAsesmen(Request $request)
    {
        $data = DB::select("SELECT
                            `a`.`id` AS `id`,
                            `a`.`tanggal_uji` AS `tanggal_uji`,
                            `a`.`nama_jadwal` AS `nama_jadwal`,
                            `b`.`nama` AS `nama_tuk`,
                            `a`.`tema` AS `tema`,
                            `c`.`nama` AS `nama_skema`,
                            `c`.`nomor` AS `nomor_skema`,
                            `c`.`id_skema` AS `id_skema`,
                            `a`.`target_peserta` AS `target_peserta`,
                            `a`.`jam_uji` AS `jam_uji`,
                            `a`.`lokasi_uji` AS `lokasi_uji`,
                            `d`.`nama` AS `nama_sumber_anggaran`,
                            `a`.`anggaran_perasesi` AS `anggaran_perasesi`,
                            `a`.`nomor_spk` AS `nomor_spk`,
                            `a`.`tanggal_spk` AS `tanggal_spk`,
                            `a`.`tgl_mulai_pelatihan` AS `tgl_mulai_pelatihan`,
                            `a`.`tgl_akhir_pelatihan` AS `tgl_akhir_pelatihan`,
                            `a`.`durasi_pelatihan` AS `durasi_pelatihan`,
                            `a`.`nama_pelatihan` AS `nama_pelatihan`,
                            `a`.`nama_instansi` AS `nama_instansi`,
                            `a`.`nama_lsp` AS `nama_lsp`
                            FROM `jadwal_asesmen` `a`
                            JOIN `list_tuk` `b`
                            ON `a`.`tuk` = `b`.`id`
                            JOIN `list_skema` `c`
                            ON `a`.`skema` = `c`.`kode`
                            JOIN `list_sumber_anggaran` `d`
                            ON `a`.`sumber_anggaran` = `d`.`id`
                            WHERE `a`.`tanggal_uji`>=CURRENT_DATE - INTERVAL 1 WEEK ORDER BY `a`.`tanggal_uji` DESC");
        echo json_encode($data);
    }

    public function jadwalLanjut_monitoring_apl01(Request $request, $id)
    {
        $id_jadwal_asesmen = $id;
        $jadwal = DB::table('jadwal_asesor')
        ->where('id_jadwal','=',$id_jadwal_asesmen)->get();

        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Manajemen Jadwal";
        $data['title_sub_menu'] = "Jadwal Asesor";
        // dd($id_jadwal_asesmen);
        return view('monitoring.pilih_asesor_monitoring_apl01', $data, ['jadwal' => $jadwal]);
    }

    public function asesi_monitoring_apl01(Request $request, $no_reg, $id_jadwal_asesmen)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        // dd($data['user']);
        $data['title_menu'] = "Monitoring";
        $data['title_sub_menu'] = "Monitoring Apl01";
        $token = $data_session['token'];
        $data['data_apl01'] = DB::select("SELECT * FROM `apl01` WHERE no_Reg = '$no_reg' AND `id_jadwal_asesmen` = $id_jadwal_asesmen");
      
        // dd($data['data_apl01']);
        return view('monitoring.monitoring_apl01', $data);
    }

    public function PrintApl01(Request $request,  $id_apl01, $id_jadwal_asesmen)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $email = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        // dd($data['user']);
        $data['data_apl01'] = DB::table("apl01")->where('id', '=', $id_apl01)->first();
        $data['res_jadwal_asesmen'] = DB::select("SELECT
                                        `a`.`nama_jadwal` AS `nama_jadwal`,
                                        `a`.`tanggal_spk` AS `tanggal_spk`,
                                        `a`.`lokasi_uji` AS `lokasi_uji`,
                                        `b`.`nama` AS `nama_skema`,
                                        `b`.`nomor` AS `nomor_skema`,
                                        `b`.`id_skema` AS `id_skema`,
                                        `c`.`nama` AS `nama_tuk`
                                        FROM `jadwal_asesmen` `a`
                                        JOIN `list_skema` `b`
                                        ON `a`.`skema` = `b`.`kode`
                                        JOIN `list_tuk` `c`
                                        ON `a`.`tuk` = `c`.`id`
                                        WHERE `a`.`id` = $id_jadwal_asesmen");
        $data['list_tujuan_asesmen'] = DB::select("SELECT * from list_tujuan_asesmen");
        $id_skema = $data['res_jadwal_asesmen'][0]->id_skema;
        $data['get_skema'] = DB::select("SELECT id,induk,nama,
                                        max(case when (nama='1') then isi else '-' end) as kode,
                                        max(case when (nama='2') then isi else '-' end) as nama

                                        FROM (SELECT * FROM list_skema_full ORDER BY id ASC) a,
                                        (SELECT @pv := $id_skema) b
                                        WHERE find_in_set(induk, @pv)
                                        group by grup,induk order by id ASC");
        //KALO DATANYA KOSONG KAISH KONDISI
        // dd($data['data_apl01']);
  
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Asesmen Mandiri";
        $token = $data_session['token'];
        return view('monitoring.view_print_apl01', $data);
}
    
    public function EditApl01(Request $request,  $id_apl01, $id_jadwal_asesmen)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $email = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        // dd($data['user']);
        $data['data_apl01'] = DB::table("apl01")->where('id', '=', $id_apl01)->first();
        $data['res_jadwal_asesmen'] = DB::select("SELECT
                                        `a`.`nama_jadwal` AS `nama_jadwal`,
                                        `a`.`tanggal_spk` AS `tanggal_spk`,
                                        `a`.`lokasi_uji` AS `lokasi_uji`,
                                        `b`.`nama` AS `nama_skema`,
                                        `b`.`nomor` AS `nomor_skema`,
                                        `b`.`id_skema` AS `id_skema`,
                                        `c`.`nama` AS `nama_tuk`
                                        FROM `jadwal_asesmen` `a`
                                        JOIN `list_skema` `b`
                                        ON `a`.`skema` = `b`.`kode`
                                        JOIN `list_tuk` `c`
                                        ON `a`.`tuk` = `c`.`id`
                                        WHERE `a`.`id` = $id_jadwal_asesmen");
        // $data['ttd_asesor'] = DB::select("SELECT 
        //                                     users.tanda_tangan
        //                                     FROM users
        //                                     JOIN list_asesor 
        //                                     ON users.id = list_asesor.id_akun
        //                                     JOIN ak_ak01 
        //                                     ON list_asesor.met_asesor = ak_ak01.nama_asesor
        //                                     WHERE ak_ak01.id = $id_apl01   
        //                                 ");
        $data['list_tujuan_asesmen'] = DB::select("SELECT * from list_tujuan_asesmen");
        $id_skema = $data['res_jadwal_asesmen'][0]->id_skema;
        $data['get_skema'] = DB::select("SELECT id,induk,nama,
                                        max(case when (nama='1') then isi else '-' end) as kode,
                                        max(case when (nama='2') then isi else '-' end) as nama

                                        FROM (SELECT * FROM list_skema_full ORDER BY id ASC) a,
                                        (SELECT @pv := $id_skema) b
                                        WHERE find_in_set(induk, @pv)
                                        group by grup,induk order by id ASC");
        //KALO DATANYA KOSONG KAISH KONDISI
        // dd($data['data_apl01']);
  
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Asesmen Mandiri";
        $token = $data_session['token'];
        return view('monitoring.update_apl01_monitoring', $data);
}

public function UpdateApl01(Request $request, $id_apl01)
{

    $data_session = $request->session()->get('dataUser');
    $token = $data_session['token'];
    $data['user'] = DB::table('users')->where('token', $token)->first();
    if ($request->hasFile('ttd_admin')) {
        $filenameWithExt = $request->file('ttd_admin')->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
        $extension = $request->file('tanda_tangan')->getClientOriginalExtension(); // Get just Extension
        $fileName_tanda_tangan = 'ttd_admin' . '_' . $token . '.' . $extension; // Filename To store
       
    } else {
        $fileName_tanda_tangan =  $data['user']->tanda_tangan;
        
    }
    $name = $request->input('namaadmin');
    $rekomendasi = $request->input('rekomendasi');
    $no_admin = $request->input('no_pegawai');
    $tanggal_admin = $request->input('tanggaladmin');
    // dd($name);
    $upd = DB::table('apl01')
    ->where('id','=',$id_apl01)    
    ->update([
        'ttdadmin' =>$fileName_tanda_tangan,
        'namaadmin'=>$name,
        'rekomendasi'=>$rekomendasi,
        'noreg'=>$no_admin,
        'tanggaladmin'=>$tanggal_admin
    ]);
    // dd($upd);
if ($upd) {
  return back();
  Alert::success('Berhasil', 'Data Berhasil di Update');
} else {
    Alert::success('Berhasil', 'Data Berhasil di Simpan');
    return back();
}
  
}

}