<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class UjiKompetensi extends Controller
{
    public $bukti_mantap;
    
    public function pskapl01(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Permohonan Sertifikasi Kompetensi";
        $token = $data_session['token'];
        return view('uji_kompetensi.pskapl01', $data);
    }

    public function getJadwalAsesmen_pskapl01(Request $request)
    {
        $data = DB::select("SELECT
                            `a`.`id` AS `id`,
                            `a`.`tanggal_uji` AS `tanggal_uji`,
                            `a`.`nama_jadwal` AS `nama_jadwal`,
                            `b`.`nama` AS `nama_tuk`,
                            `e`.`nama_kejuruan` AS `tema`,
                            `c`.`nama` AS `nama_skema`,
                            `c`.`nomor` AS `nomor_skema`,
                            `c`.`id_skema` AS `id_skema`,
                            `a`.`target_peserta` AS `target_peserta`,
                            `a`.`jam_uji` AS `jam_uji`,
                            `a`.`lokasi_uji` AS `lokasi_uji`,
                            `d`.`nama` AS `nama_sumber_anggaran`,
                            `a`.`anggaran_perasesi` AS `anggaran_perasesi`,
                            `a`.`nomor_spk` AS `nomor_spk`,
                            `a`.`tanggal_spk` AS `tanggal_spk`,
                            `a`.`tgl_mulai_pelatihan` AS `tgl_mulai_pelatihan`,
                            `a`.`tgl_akhir_pelatihan` AS `tgl_akhir_pelatihan`,
                            `a`.`durasi_pelatihan` AS `durasi_pelatihan`,
                            `a`.`nama_pelatihan` AS `nama_pelatihan`,
                            `a`.`nama_instansi` AS `nama_instansi`,
                            `a`.`nama_lsp` AS `nama_lsp`
                            FROM `jadwal_asesmen` `a`
                            JOIN `list_tuk` `b`
                            ON `a`.`tuk` = `b`.`id`
                            JOIN `list_skema` `c`
                            ON `a`.`skema` = `c`.`kode`
                            JOIN `list_sumber_anggaran` `d`
                            ON `a`.`sumber_anggaran` = `d`.`id`
                            JOIN `list_kejuruan` `e`
                            ON `a`.`tema` = `e`.`id_kejuruan`
                            WHERE `a`.`tanggal_uji`>=CURRENT_DATE - INTERVAL 1 WEEK ORDER BY `a`.`tanggal_uji` DESC");
        echo json_encode($data);
    }

    public function jadwalLanjut(Request $request, string $id)
    {
        $id_jadwal_asesmen = $id;
        //ambil tanggal dari jadwal asesmen
       
        //ambil tanggal uji dan nama_jadwal dari jadwal asesmen yg dipilih untuk nge group siapa aja asesornya
        $jadwal = DB::table('jadwal_asesor')
        ->where('id_jadwal','=',$id_jadwal_asesmen)->get();
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Pilih Asesor";

        return view('uji_kompetensi.jadwal_lanjut', $data, ['jadwal_asesor' => $jadwal]);
    }

    public function apl01lsp(Request $request, string $id_asesor, string $id_jadwal_asesmen)
    {
        //ambil tanggal dari jadwal asesor
        // dd($id_jadwal_asesmen);
        // $data['res_jadwal_asesor'] = DB::table('jadwal_asesor')->where('id', $id_asesor)->first();
        $data['res_jadwal_asesor'] =  DB::select("SELECT 
                                            `jadwal_asesor`.*,
                                            `list_asesor`.`token`
                                            FROM `list_asesor`
                                            JOIN `jadwal_asesor`
                                            ON `list_asesor`.`met_asesor` = `jadwal_asesor`.`no_Reg`
                                            WHERE `jadwal_asesor`.`id` = $id_asesor");
        //ambil tanggal dari jadwal asesmen
        // $data['res_jadwal_asesmen'] = DB::table('jadwal_asesmen')->where('id', $id_jadwal_asesmen)->first();
        $data['res_jadwal_asesmen'] = DB::select("SELECT
                                        `a`.`nama_jadwal` AS `nama_jadwal`,
                                        `a`.`tanggal_spk` AS `tanggal_spk`,
                                        `a`.`lokasi_uji` AS `lokasi_uji`,
                                        `b`.`nama` AS `nama_skema`,
                                        `b`.`nomor` AS `nomor_skema`,
                                        `b`.`id_skema` AS `id_skema`,
                                        `c`.`nama` AS `nama_tuk`
                                        FROM `jadwal_asesmen` `a`
                                        JOIN `list_skema` `b`
                                        ON `a`.`skema` = `b`.`kode`
                                        JOIN `list_tuk` `c`
                                        ON `a`.`tuk` = `c`.`id`
                                        WHERE `a`.`id` = $id_jadwal_asesmen");
        $data['list_tujuan_asesmen'] = DB::select("SELECT * from list_tujuan_asesmen");
        $id_skema = $data['res_jadwal_asesmen'][0]->id_skema;
        $data['get_skema'] = DB::select("SELECT id,induk,nama,
                                        max(case when (nama='1') then isi else '-' end) as kode,
                                        max(case when (nama='2') then isi else '-' end) as nama

                                        FROM (SELECT * FROM list_skema_full ORDER BY id ASC) a,
                                        (SELECT @pv := $id_skema) b
                                        WHERE find_in_set(induk, @pv)
                                        group by grup,induk order by id ASC");
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $email = $data_session['email'];
        // $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['user'] = DB::select("SELECT
                                    `a`.`name` AS `nama_lengkap`,
                                    `a`.`nik` AS `nik`,
                                    `a`.`tempat_lahir` AS `tempat_lahir`,
                                    `a`.`tanggal_lahir` AS `tanggal_lahir`,
                                    `b`.`nama` AS `nama_jenis_kelamin`,
                                    `c`.`nama` AS `nama_kebangsaan`,
                                    `a`.`alamat` AS `alamat`,
                                    `a`.`kodepos` AS `kodepos`,
                                    `d`.`nama` AS `nama_provinsi`,
                                    `a`.`kabupaten_kota` AS `kabupaten_kota`,
                                    `a`.`no_hp` AS `no_hp`,
                                    `a`.`email` AS `email`,
                                    `e`.`nama` AS `nama_kualifikasi_pendidikan`,
                                    `f`.`nama` AS `nama_pekerjaan`,
                                    `a`.`institusi_perusahaan` AS `institusi_perusahaan`,
                                    `a`.`jabatan` AS `jabatan`,
                                    `a`.`alamat_kantor` AS `alamat_kantor`,
                                    `a`.`kodepos_kantor` AS `kodepos_kantor`,
                                    `a`.`no_hp_kantor` AS `no_hp_kantor`,
                                    `a`.`fax_kantor` AS `fax_kantor`,
                                    `a`.`email_kantor` AS `email_kantor`,
                                    `a`.`tanda_tangan` AS `tanda_tangan`
                                    FROM `users` `a`
                                    JOIN `list_kelamin` `b`
                                    ON `a`.`jenis_kelamin` = `b`.`id`
                                    JOIN `list_kebangsaan` `c`
                                    ON `a`.`kebangsaan` = `c`.`id`
                                    JOIN `wilayah_2020` `d`
                                    ON `a`.`provinsi` = `d`.`kode`
                                    JOIN `list_pendidikan` `e`
                                    ON `a`.`kualifikasi_pendidikan` = `e`.`id`
                                    JOIN `list_pekerjaan` `f`
                                    ON `a`.`pekerjaan` = `f`.`id`
                                    WHERE `a`.`email` = '$email'
                                    ");
        $data['kab_kota_user'] = DB::select("SELECT
                                    `b`.`nama` AS `nama_kab_kota`
                                    FROM `users` `a`
                                    JOIN `wilayah_2020` `b`
                                    ON `a`.`kabupaten_kota` = `b`.`kode`
                                    WHERE `a`.`email` = '$email' ");
        // @dd($data['user']);
        // die;
        $data['id_jadwal_asesmen'] = $id_jadwal_asesmen;
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Form Apl 01";

        return view('uji_kompetensi.apl01lsp', $data);
    }


    public function saveApl01Lsp(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        //jika ada pasFoto asesi
        if ($request->hasFile('pasFoto')) {
            $filenameWithExt = $request->file('pasFoto')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('pasFoto')->getClientOriginalExtension(); // Get just Extension
            $fileName_pasFoto = 'pas_foto' . '_' . $token . '.' . $extension; // Filename To store
            $request->pasFoto->move(public_path('assets/document/pas_foto/asesi'), $fileName_pasFoto);
        } else {
            $fileName_pasFoto = "";
        }

        //jika ada ktp asesi
        if ($request->hasFile('ktp')) {
            $filenameWithExt = $request->file('ktp')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('ktp')->getClientOriginalExtension(); // Get just Extension
            $fileName_ktp = 'ktp' . '_' . $token . '.' . $extension; // Filename To store
            $request->ktp->move(public_path('assets/document/ktp/asesi'), $fileName_ktp);
        } else {
            $fileName_ktp = "";
        }

        //jika ada ijazah asesi
        if ($request->hasFile('ijazah')) {
            $filenameWithExt = $request->file('ijazah')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('ijazah')->getClientOriginalExtension(); // Get just Extension
            $fileName_ijazah = 'ijazah' . '_' . $token . '.' . $extension; // Filename To store
            $request->ijazah->move(public_path('assets/document/ijazah/asesi'), $fileName_ijazah);
        } else {
            $fileName_ijazah = "";
        }

        //jika ada sertifikasi_pelatihan asesi
        if ($request->hasFile('sertifikasi_pelatihan')) {
            $filenameWithExt = $request->file('sertifikasi_pelatihan')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('sertifikasi_pelatihan')->getClientOriginalExtension(); // Get just Extension
            $fileName_sertifikasi_pelatihan = 'sertifikasi_pelatihan' . '_' . $token . '.' . $extension; // Filename To store
            $request->sertifikasi_pelatihan->move(public_path('assets/document/sertifikasi_pelatihan/asesi'), $fileName_sertifikasi_pelatihan);
        } else {
            $fileName_sertifikasi_pelatihan = "";
        }

        //jika ada cv asesi
        if ($request->hasFile('cv')) {
            $filenameWithExt = $request->file('cv')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('cv')->getClientOriginalExtension(); // Get just Extension
            $fileName_cv = 'cv' . '_' . $token . '.' . $extension; // Filename To store
            $request->cv->move(public_path('assets/document/cv/asesi'), $fileName_cv);
        } else {
            $fileName_cv = "";
        }
        //jika ada portofolio asesi
        if ($request->hasFile('portofolio')) {
            $filenameWithExt = $request->file('portofolio')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('portofolio')->getClientOriginalExtension(); // Get just Extension
            $fileName_portofolio = 'portofolio' . '_' . $token . '.' . $extension; // Filename To store
            $request->portofolio->move(public_path('assets/document/portofolio/asesi'), $fileName_portofolio);
        } else {
            $fileName_portofolio = "";
        }

        $idJadwalAsesmen = $request->input('idJadwalAsesmen');
        $lokasi_uji = $request->input('lokasi_uji');
        $nama_asesi = $request->input('nama_asesi');
        $nik = $request->input('nik');
        $tempat_lahir = $request->input('tempat_lahir');
        $tanggal_lahir = $request->input('tanggal_lahir');
        $nama_jenis_kelamin = $request->input('nama_jenis_kelamin');
        $nama_kebangsaan = $request->input('nama_kebangsaan');

        $alamat = $request->input('alamat');
        $kodepos = $request->input('kodepos');
        $nama_provinsi = $request->input('nama_provinsi');
        $nama_kab_kota = $request->input('nama_kab_kota');
        $no_hp = $request->input('no_hp');
        $email = $request->input('email');
        $nama_kualifikasi_pendidikan = $request->input('nama_kualifikasi_pendidikan');
        $nama_pekerjaan = $request->input('nama_pekerjaan');
        $institusi_perusahaan = $request->input('institusi_perusahaan');
        $jabatan = $request->input('jabatan');
        $alamat_kantor = $request->input('alamat_kantor');
        $kodepos_kantor = $request->input('kodepos_kantor');
        $no_hp_kantor = $request->input('no_hp_kantor');
        $fax_kantor = $request->input('fax_kantor');
        $email_kantor = $request->input('email_kantor');
        $tuk = $request->input('tuk');
        $judul_jadwal = $request->input('judul_jadwal');
        $skema = $request->input('skema');
        $nomor = $request->input('nomor');
        $asesor = $request->input('asesor');
        $no_reg = $request->input('no_reg');
        $token_asesor = $request->input('token_asesor');
        $tujuan_asesmen = $request->input('tujuan_asesmen');
        $tanggal_asesmen = $request->input('tanggal_asesmen');
        $ttdasesi = $request->input('ttdasesi');
        $nama_pemohon = $request->input('nama_pemohon');
        $tanggal_pemohon = $request->input('tanggal_pemohon');


        $waktu_sekarang     = Date('Y-m-d');
        $induk_lsp = "BBPVP_Bandung";
        $data = array(
            'id_apl01_rand' => "",
            'id_jadwal_asesmen' => $idJadwalAsesmen,
            'lokasi_uji' => $lokasi_uji,
            'tanggalasesmen' => $tanggal_asesmen,
            'juduljadwal' => $judul_jadwal,
            'nama_asesi' => $nama_asesi,
            'nik' => $nik,
            'tempatlahir' => $tempat_lahir,
            'tanggallahir' => $tanggal_lahir,
            'jeniskelamin' => $nama_jenis_kelamin,
            'kebangsaan' => $nama_kebangsaan,
            'alamat' => $alamat,
            'kodepos' => $kodepos,
            'provinsi' => $nama_provinsi,
            'kabupatenkota' => $nama_kab_kota,
            'telepon' => $no_hp,
            'email' => $email,
            'kualifikasipendidikan' => $nama_kualifikasi_pendidikan,
            'pekerjaan' => $nama_pekerjaan,
            'namainstitusiperusahaan' => $institusi_perusahaan,
            'jabatan' => $jabatan,
            'alamatkantor' => $alamat_kantor,
            'kodepos2' => $kodepos_kantor,
            'telepon2' => $no_hp_kantor,
            'fax2' => $fax_kantor,
            'email2' => $email_kantor,
            'tuk' => $tuk,
            'skema' => $skema,
            'nomor' => $nomor,
            'namaasesor' => $asesor,
            'token_asesor' => "",
            'no_Reg' => $no_reg,
            'token_asesor' => $token_asesor,
            'tujuanasesmen' => $tujuan_asesmen,
            'pasFoto' => $fileName_pasFoto,
            'pasFoto_ada' => "",
            'ktp' => $fileName_ktp,
            'ktp_ada' => "",
            'ijazah' => $fileName_ijazah,
            'ijazah_ada' => "",
            'sertifikasipelatihan' => $fileName_sertifikasi_pelatihan,
            'sertifikasipelatihan_ada' => "",
            'cv' => $fileName_cv,
            'cv_ada' => "",
            'portofolio' => $fileName_portofolio,
            'portofolio_ada' => "",
            'rekomendasi' => "",
            'namapemohon' => $nama_pemohon,
            'ttdpemohon' => $ttdasesi,
            'tanggalpemohon' => $tanggal_pemohon,
            'catatanadmin' => "",
            'namaadmin' => "",
            'noreg' => "",
            'ttdadmin' => "",
            'tanggaladmin' => "",
            'status_permohonan' => "permohonan",
        );
        DB::table('apl01')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Simpan');
        return redirect()->action([UjiKompetensi::class, 'pskapl01']);
    }

    // MENU BANDING ASESMEN (AK 04)
    public function ba04(Request $request)
    {

        Alert::warning('Warning', 'Yakin Ingin Melakukan Banding ?');
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $email = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Banding Asesmen (AK 04)";
        $token = $data_session['token'];
        $data['data_apl01'] = DB::select(" SELECT * FROM apl01 WHERE email = '$email' AND tanggalasesmen >= CURRENT_DATE - INTERVAL 1 WEEK ");
        return view('uji_kompetensi.ba04', $data);
    }

    public function saveBa04(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        //jika ada tanda_tangan asesi
        if ($request->hasFile('ttdasesi')) {
            $filenameWithExt = $request->file('ttdasesi')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('ttdasesi')->getClientOriginalExtension(); // Get just Extension
            $fileName_ttdasesi = 'ttd_asesi' . '_' . $token . '.' . $extension; // Filename To store
            $request->ttdasesi->move(public_path('assets/document/tanda_tangan/asesi'), $fileName_ttdasesi);
        } else {
            $fileName_ttdasesi = "";
        }

        $namaasesi = $request->input('namaasesi');
        $namaasesor = $request->input('namaasesor');
        $tanggal = $request->input('tanggal');
        $prosesbanding = $request->input('prosesbanding');
        $diskusibanding = $request->input('diskusibanding');
        $melibatkan = $request->input('melibatkan');

        $skemasertif = $request->input('skemasertif');
        $noskema = $request->input('noskema');
        $alasanbanding = $request->input('alasanbanding');
        $tanggal2 = $request->input('tanggal2');
        $waktu_sekarang     = Date('Y-m-d');
        $induk_lsp = "BBPVP_Bandung";
        $data = array(
            'induk' => $induk_lsp,
            'nama_asesi' => $namaasesi,
            'ttd_asesi' => $fileName_ttdasesi,
            'nama_asesor' => $namaasesor,
            'tanggal' => $tanggal2,
            'prosesbanding' => $prosesbanding,
            'diskusibanding' => $diskusibanding,
            'melibatkan' => $melibatkan,
            'skemasertif' => $skemasertif,
            'noskema' => $noskema,
            'alasanbanding' => $alasanbanding,
            'created_at' => $waktu_sekarang,
        );
        DB::table('ak_ak04')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([UjiKompetensi::class, 'ba04']);
    }


    //MENU UMPAN BALI DAN CATATAN ASESMEN (AK 03)
    public function ubdcaak03(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $email = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Umpan Balik dan Catatan Asesmen (AK 03)";
        $token = $data_session['token'];
        $data['data_apl01'] = DB::select(" SELECT * FROM apl01 WHERE email = '$email' AND tanggalasesmen >= CURRENT_DATE - INTERVAL 1 WEEK ");
        return view('uji_kompetensi.ubdcaak03', $data);
    }

    public function saveUbdcaak03(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();



        $namaasesi = $request->input('namaasesi');
        $namaasesor = $request->input('namaasesor');
        $tanggal = $request->input('tanggal');
        $soal1 = $request->input('yata1');
        $ket1 = $request->input('ket1');
        $ket2 = $request->input('ket2');
        $soal2 = $request->input('yata2');
        $ket2 = $request->input('ket2');
        $soal3 = $request->input('yata3');
        $ket3 = $request->input('ket3');
        $soal4 = $request->input('yata4');
        $ket4 = $request->input('ket4');
        $soal5 = $request->input('yata5');
        $ket5 = $request->input('ket5');
        $soal6 = $request->input('yata6');
        $ket6 = $request->input('ket6');
        $soal7 = $request->input('yata7');
        $ket7 = $request->input('ket7');
        $soal8 = $request->input('yata8');
        $ket8 = $request->input('ket8');
        $soal9 = $request->input('yata9');
        $ket9 = $request->input('ket9');
        $soal10 = $request->input('yata10');
        $ket10 = $request->input('ket10');
        $keterangan = $request->input('keterangan');
        $waktu_sekarang     = Date('Y-m-d');
        $induk_lsp = "BBPVP_Bandung";
        $data = array(
            'induk' => $induk_lsp,
            'tanggal' => $tanggal,
            'nama' => $namaasesi,
            'namaasesor' => $namaasesor,
            'soal1' => $soal1,
            'ket1' => $ket1,
            'soal2' => $soal2,
            'ket2' => $ket2,
            'soal3' => $soal3,
            'ket3' => $ket3,
            'soal4' => $soal4,
            'ket4' => $ket4,
            'soal5' => $soal5,
            'ket5' => $ket5,
            'soal6' => $soal6,
            'ket6' => $ket6,
            'soal7' => $soal7,
            'ket7' => $ket7,
            'soal8' => $soal8,
            'ket8' => $ket8,
            'soal9' => $soal9,
            'ket9' => $ket9,
            'soal10' => $soal10,
            'ket10' => $ket10,
            'keterangan' => $keterangan,
            'created_at' => $waktu_sekarang,
            'status' => 1
        );
        DB::table('ak_ak03')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([UjiKompetensi::class, 'ubdcaak03']);
    }

    public function am02(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $email = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['data_apl01'] = DB::select(" SELECT * FROM apl01 WHERE email = '$email'");
        //KALO DATANYA KOSONG KAISH KONDISI
        // dd($data['data_apl01']);
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Asesmen Mandiri";
        $token = $data_session['token'];
        return view('uji_kompetensi.am02', $data);
    }

    public function getJadwalAsesmen_am02(Request $request)
    {
        $id_jadwal_asesmen = $request->input('idJadwalAsesmen');
        $data = DB::select("SELECT
                            `a`.`id` AS `id`,
                            `a`.`tanggal_uji` AS `tanggal_uji`,
                            `a`.`nama_jadwal` AS `nama_jadwal`,
                            `b`.`nama` AS `nama_tuk`,
                            `e`.`nama_kejuruan` AS `tema`,
                            `c`.`nama` AS `nama_skema`,
                            `c`.`nomor` AS `nomor_skema`,
                            `c`.`id_skema` AS `id_skema`,
                            `a`.`target_peserta` AS `target_peserta`,
                            `a`.`jam_uji` AS `jam_uji`,
                            `a`.`lokasi_uji` AS `lokasi_uji`,
                            `d`.`nama` AS `nama_sumber_anggaran`,
                            `a`.`anggaran_perasesi` AS `anggaran_perasesi`,
                            `a`.`nomor_spk` AS `nomor_spk`,
                            `a`.`tanggal_spk` AS `tanggal_spk`,
                            `a`.`tgl_mulai_pelatihan` AS `tgl_mulai_pelatihan`,
                            `a`.`tgl_akhir_pelatihan` AS `tgl_akhir_pelatihan`,
                            `a`.`durasi_pelatihan` AS `durasi_pelatihan`,
                            `a`.`nama_pelatihan` AS `nama_pelatihan`,
                            `a`.`nama_instansi` AS `nama_instansi`,
                            `a`.`nama_lsp` AS `nama_lsp`
                            FROM `jadwal_asesmen` `a`
                            JOIN `list_tuk` `b`
                            ON `a`.`tuk` = `b`.`id`
                            JOIN `list_skema` `c`
                            ON `a`.`skema` = `c`.`kode`
                            JOIN `list_sumber_anggaran` `d`
                            ON `a`.`sumber_anggaran` = `d`.`id`
                            JOIN `list_kejuruan` `e`
                            ON `a`.`tema` = `e`.`id_kejuruan`
                            WHERE `a`.`id` =  $id_jadwal_asesmen
                            AND `a`.`tanggal_uji`>=CURRENT_DATE - INTERVAL 1 WEEK ORDER BY `a`.`tanggal_uji` DESC");
        echo json_encode($data);
    }

    public function formAm02(Request $request, string $id)
    {
        //ambil tanggal dari jadwal asesmen
        $res_jadwal_asesmen = DB::table('jadwal_asesmen')->where('id', $id)->first();
        $id_jadwal_asesmen = $id;
        //ambil tanggal dari jadwal asesmen
        // $data['res_jadwal_asesmen'] = DB::table('jadwal_asesmen')->where('id', $id_jadwal_asesmen)->first();
        $data['res_jadwal_asesmen'] = DB::select("SELECT
                                        `a`.`nama_jadwal` AS `nama_jadwal`,
                                        `a`.`tanggal_spk` AS `tanggal_spk`,
                                        `a`.`lokasi_uji` AS `lokasi_uji`,
                                        `b`.`nama` AS `nama_skema`,
                                        `b`.`nomor` AS `nomor_skema`,
                                        `b`.`id_skema` AS `id_skema`,
                                        `c`.`nama` AS `nama_tuk`
                                        FROM `jadwal_asesmen` `a`
                                        JOIN `list_skema` `b`
                                        ON `a`.`skema` = `b`.`kode`
                                        JOIN `list_tuk` `c`
                                        ON `a`.`tuk` = `c`.`id`
                                        WHERE `a`.`id` = $id_jadwal_asesmen");
        $data['list_tujuan_asesmen'] = DB::select("SELECT * from list_tujuan_asesmen");
        $id_skema = $data['res_jadwal_asesmen'][0]->id_skema;
        $data['id_skema'] = $id_skema;

        $request->session()->put('id_skema', $data['id_skema']);
        $request->session()->put('data_jadwal_asesmen', $data['res_jadwal_asesmen']);

        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $email = $data_session['email'];
        $data['data_apl01'] = DB::select(" SELECT * FROM apl01 WHERE email = '$email'");
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Asesmen Mandiri";

        return view('uji_kompetensi.formAm02', $data);
    }

    public function pppp(Request $request)
    {
        $id_skema = $request->session()->get('id_skema');
        $data_jadwal_asesmen = $request->session()->get('data_jadwal_asesmen');

        $data_skema = DB::select("SELECT id,induk,nama,
                            max(case when (nama='1') then isi else '-' end) as kode,
                            max(case when (nama='2') then isi else '-' end) as nama,
                            max(case when (nama='3') then isi else '-' end) as elemen,
                            max(case when (nama='4') then isi else '-' end) as kuk

                            FROM (SELECT * FROM list_skema_full ORDER BY id ASC) a,
                            (SELECT @pv := $id_skema) b
                            WHERE find_in_set(induk, @pv) AND length(@pv := concat(@pv, ',', id))
                            group by grup,induk order by id ASC");
        $data = [
            "id_skema" => $id_skema,
            "data_skema" => $data_skema,
            "data_jadwal_asesmen" => $data_jadwal_asesmen,
        ];
        echo json_encode($data);
    }

    public function saveAm02(Request $request)
    {

        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        //jika ada tanda_tangan asesi
        if ($request->hasFile('ttdasesi')) {
            $filenameWithExt = $request->file('ttdasesi')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('ttdasesi')->getClientOriginalExtension(); // Get just Extension
            $fileName_ttdasesi = 'ttd_asesi' . '_' . $token . '.' . $extension; // Filename To store
            $request->ttdasesi->move(public_path('assets/document/tanda_tangan/asesi'), $fileName_ttdasesi);
        } else {
            $fileName_ttdasesi = "";
        }

        $skema = $request->input('skema');
        $nomor = $request->input('nomor');
        $nama_asesi = $request->input('nama_asesi');
        $ttd_asesi = $request->input('ttd_asesi');
        $tanggal_asesi = $request->input('tanggal_asesi');
        $nama_asesor = $request->input('nama_asesor');
        $id_jadwal_asesmen = $request->input('id_jadwal_asesmen');
        $no_Reg = $request->input('no_Reg');
        $ttd_asesor = $request->input('ttd_asesor');
        $tanggal_asesor = $request->input('tanggal_asesor');

        $waktu_sekarang     = Date('Y-m-d');
        $induk_lsp = "BBPVP_Bandung";
        $data = array(
            'kode' => "",
            'skema' => $skema,
            'nomor' => $nomor,
            'nama_asesi' => $nama_asesi,
            'ttd_asesi' => $ttd_asesi,
            'tanggal_asesi' => $tanggal_asesi,
            'nama_asesor' => $nama_asesor,
            'rekomendasi' => "",
            'ttd_asesor' => $ttd_asesor,
            'id_jadwal' => $id_jadwal_asesmen,
            'no_Reg' => $no_Reg,
            'tanggal_asesor' => $tanggal_asesor,
            'nama_lsp' => 'BBPVP Bandung'
        );
        DB::table('apl02')->insert($data);

        $query_last_id =  DB::select("SELECT id FROM apl02 ORDER BY id DESC LIMIT 1");
        $last_id = $query_last_id[0]->id;
        $unit_kompetensi =  $request->input('unit_kompetensi');
        $no_unit_kompetensi =  $request->input('no_unit_kompetensi');
        for ($i = 0; $i < count($unit_kompetensi); $i++) {
            //insert ke table apl02_sb
            $per_unit_kompetensi = $unit_kompetensi[$i];
            $per_no_unit_kompetensi = $no_unit_kompetensi[$i];
            $data2 = array(
                'id_apl02' => $last_id,
                'unit_kompetensi' => $per_unit_kompetensi,
                'no_unit_kompetensi' => $per_no_unit_kompetensi,

            );
            DB::table('apl02_sb')->insert($data2);
            //insert ke table apl02_sb_elemen
            $query_last_id_apl02_sb =  DB::select("SELECT id FROM apl02_sb ORDER BY id DESC LIMIT 1");
            $last_id_apl02_sb = $query_last_id_apl02_sb[0]->id;
            $no_urut = (int)$i + 1;
            $elemen = $request->input('elemen' . $no_urut);
            // @dd(count($elemen));
            for ($e = 0; $e < count($elemen); $e++) {
                $no_urut2 = (int)$e + 1;
                $per_elemen = $elemen[$e];
                $data3 = array(
                    'id_apl02' => $last_id,
                    'id_apl02_sb' => $last_id_apl02_sb,
                    'elemen' => $per_elemen
                );
                DB::table('apl02_sb_elemen')->insert($data3);
                //insert ke table apl02_sb_elemen_child
                $query_last_id_apl02_sb_elemen =  DB::select("SELECT id FROM apl02_sb_elemen ORDER BY id DESC LIMIT 1");
                $last_id_apl02_sb_elemen = $query_last_id_apl02_sb_elemen[0]->id;
                $kbk = $request->input('kbk' . $no_urut . $no_urut2);
                // @dd($kbk);
                if ($kbk == null) {
                    $val_kbk = "";
                } else {
                    $val_kbk = $kbk[0];
                }
                $bukti = $request->input('bukti' . $no_urut . $no_urut2);
                if ($bukti == null) {
                    $val_bukti = "";
                } else {
                    $val_bukti = $bukti[0];
                }
                // for ($c = 0; $c < count($bukti); $c++) {
                //     $per_kbk = $kbk[$c];
                //     $per_bukti = $bukti[$c];
                //     var_dump($per_kbk);
                //     var_dump($per_bukti);
                $data4 = array(
                    'id_apl02' => $last_id,
                    'id_apl02_sb' => $last_id_apl02_sb,
                    'id_apl02_sb_elemen' => $last_id_apl02_sb_elemen,
                    'kbk' => $val_kbk,
                    'bukti' => $val_bukti
                );
                DB::table('apl02_sb_elemen_child')->insert($data4);
                // }
            }
        }

        // @dd(count($unit_kompetensi));
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([UjiKompetensi::class, 'am02']);
    }

    public function padkak01(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $email = $data_session['email'];
        $data['data_apl01'] = DB::select(" SELECT * FROM apl01 WHERE email = '$email' AND tanggalasesmen >= CURRENT_DATE - INTERVAL 1 WEEK ");
        $data['list_bukti'] = DB::select(" SELECT * FROM list_bukti ");
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Persetujuan Asesmen dan Kerahasiaan";
        $token = $data_session['token'];
        return view('uji_kompetensi.padkak01', $data);
    }

    public function getJadwalAsesmen_ak01(Request $request)
    {
        $id_jadwal_asesmen = $request->input('idJadwalAsesmen');
        $data = DB::select("SELECT
                            `a`.`id` AS `id`,
                            `a`.`tanggal_uji` AS `tanggal_uji`,
                            `a`.`nama_jadwal` AS `nama_jadwal`,
                            `b`.`nama` AS `nama_tuk`,
                            `e`.`nama_kejuruan` AS `tema`,
                            `c`.`nama` AS `nama_skema`,
                            `c`.`nomor` AS `nomor_skema`,
                            `c`.`id_skema` AS `id_skema`,
                            `a`.`target_peserta` AS `target_peserta`,
                            `a`.`jam_uji` AS `jam_uji`,
                            `a`.`lokasi_uji` AS `lokasi_uji`,
                            `d`.`nama` AS `nama_sumber_anggaran`,
                            `a`.`anggaran_perasesi` AS `anggaran_perasesi`,
                            `a`.`nomor_spk` AS `nomor_spk`,
                            `a`.`tanggal_spk` AS `tanggal_spk`,
                            `a`.`tgl_mulai_pelatihan` AS `tgl_mulai_pelatihan`,
                            `a`.`tgl_akhir_pelatihan` AS `tgl_akhir_pelatihan`,
                            `a`.`durasi_pelatihan` AS `durasi_pelatihan`,
                            `a`.`nama_pelatihan` AS `nama_pelatihan`,
                            `a`.`nama_instansi` AS `nama_instansi`,
                            `a`.`nama_lsp` AS `nama_lsp`
                            FROM `jadwal_asesmen` `a`
                            JOIN `list_tuk` `b`
                            ON `a`.`tuk` = `b`.`id`
                            JOIN `list_skema` `c`
                            ON `a`.`skema` = `c`.`kode`
                            JOIN `list_sumber_anggaran` `d`
                            ON `a`.`sumber_anggaran` = `d`.`id`
                            JOIN `list_kejuruan` `e`
                            ON `a`.`tema` = `e`.`id_kejuruan`
                            WHERE `a`.`id` =  $id_jadwal_asesmen
                            AND `a`.`tanggal_uji`>=CURRENT_DATE - INTERVAL 1 WEEK ORDER BY `a`.`tanggal_uji` DESC");
        echo json_encode($data);
    }

    public function formPadkak01(Request $request, string $id)
    {
        //ambil tanggal dari jadwal asesmen
        $res_jadwal_asesmen = DB::table('jadwal_asesmen')->where('id', $id)->first();
        $id_jadwal_asesmen = $id;
        //ambil tanggal dari jadwal asesmen
        // $data['res_jadwal_asesmen'] = DB::table('jadwal_asesmen')->where('id', $id_jadwal_asesmen)->first();
        $data['res_jadwal_asesmen'] = DB::select("SELECT
                                        `a`.`nama_jadwal` AS `nama_jadwal`,
                                        `a`.`tanggal_spk` AS `tanggal_spk`,
                                        `a`.`lokasi_uji` AS `lokasi_uji`,
                                        `b`.`nama` AS `nama_skema`,
                                        `b`.`nomor` AS `nomor_skema`,
                                        `b`.`id_skema` AS `id_skema`,
                                        `c`.`nama` AS `nama_tuk`
                                        FROM `jadwal_asesmen` `a`
                                        JOIN `list_skema` `b`
                                        ON `a`.`skema` = `b`.`kode`
                                        JOIN `list_tuk` `c`
                                        ON `a`.`tuk` = `c`.`id`
                                        WHERE `a`.`id` = $id_jadwal_asesmen");
        $data['list_tujuan_asesmen'] = DB::select("SELECT * from list_tujuan_asesmen");
        $id_skema = $data['res_jadwal_asesmen'][0]->id_skema;
        $data['id_skema'] = $id_skema;

        $request->session()->put('id_skema', $data['id_skema']);
        $request->session()->put('data_jadwal_asesmen', $data['res_jadwal_asesmen']);

        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $email = $data_session['email'];
        $data['data_apl01'] = DB::select(" SELECT 
                                                    `apl01`. `nomor`,
                                                    `apl01`. `tuk`,
                                                    `apl01`. `namaasesor`,
                                                    `apl01`. `nama_asesi`,
                                                    `apl01`. `tanggalasesmen`,
                                                    `apl01`. `nomor`,
                                                    `apl01`. `ttdpemohon`,
                                                    `apl01`. `skema`,
                                                    `apl01`. `lokasi_uji`,
                                                    `jadwal_asesor`.`no_Reg`,
                                                    `jadwal_asesor`.`id_jadwal`
                                                    FROM `apl01`
                                                    JOIN `jadwal_asesor`
                                                    ON `apl01`.`no_Reg` = `jadwal_asesor`.`no_Reg`
                                                    WHERE email = '$email' 
                                                    AND tanggalasesmen >= CURRENT_DATE - INTERVAL 1 WEEK 
                                                    AND id_jadwal_asesmen = $id_jadwal_asesmen ");
        $data['list_bukti'] = DB::select(" SELECT * FROM list_bukti ");
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Persetujuan Asesmen dan Kerahasiaan";
        $token = $data_session['token'];
        return view('uji_kompetensi.formPadkak01', $data);
    }

    public function savePadkak01(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        //jika ada tanda_tangan asesi
        if ($request->hasFile('ttdasesi')) {
            $filenameWithExt = $request->file('ttdasesi')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('ttdasesi')->getClientOriginalExtension(); // Get just Extension
            $fileName_ttdasesi = 'ttd_asesi' . '_' . $token . '.' . $extension; // Filename To store
            $request->ttdasesi->move(public_path('assets/document/tanda_tangan/asesi'), $fileName_ttdasesi);
        } else {
            $fileName_ttdasesi = "";
        }

        $judul = $request->input('judul');
        $nomor = $request->input('nomor');
        $tuk = $request->input('tuk');
        $id_jadwal = $request->input('id_jadwal');
        $nama_asesor = $request->input('nama_asesor');
        $nama_asesi = $request->input('nama_asesi');
        $ttd_asesor = $request->input('ttd_asesor');
        $tanggal = $request->input('tanggal');
        $bukti = $request->input('bukti');
        $bukti_arr = [];
        foreach ($bukti as $value) {
            array_push($bukti_arr, $value);
            // echo "Chosen colour : " . $value . '<br/>';
        }
        $bukti_oke = implode(";", $bukti_arr);
        $tuk2 = $request->input('lokasi_uji');
        $ttd_asesor = $request->input('ttd_asesor');
        $tgl_ttd_asesor = $request->input('tgl_ttd_asesor');
        $ttd_asesi = $request->input('ttd_asesi');
        $tgl_ttd_asesi = $request->input('tgl_ttd_asesi');

        $waktu_sekarang     = Date('Y-m-d');
        $induk_lsp = "BBPVP_Bandung";
        $data = array(
            'skema' => $judul,
            'nomor' => $nomor,
            'tuk' => $tuk,
            'id_jadwal' => $id_jadwal,
            'nama_asesor' => $nama_asesor,
            'nama_asesi' => $nama_asesi,
            'tanggal' => $tanggal,
            'bukti' => $bukti_oke,
            'tuk2' => $tuk2,
            'ttd_asesor' => $ttd_asesor,
            'tgl_ttd_asesor' => $tgl_ttd_asesor,
            'ttd_asesi' => $ttd_asesi,
            'tgl_ttd_asesi' => $tgl_ttd_asesi
        );
        DB::table('ak_ak01')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([UjiKompetensi::class, 'padkak01']);
    }
    
    public function frakak02(Request $request)
    {
        //ambil tanggal dari jadwal asesmen
        //ambil tanggal dari jadwal asesmen
        // $data['res_jadwal_asesmen'] = DB::table('jadwal_asesmen')->where('id', $id_jadwal_asesmen)->first();
        $data['res_jadwal_asesmen'] = DB::select("SELECT
                                        `a`.`nama_jadwal` AS `nama_jadwal`,
                                        `a`.`tanggal_spk` AS `tanggal_spk`,
                                        `a`.`lokasi_uji` AS `lokasi_uji`,
                                        `b`.`nama` AS `nama_skema`,
                                        `b`.`nomor` AS `nomor_skema`,
                                        `b`.`id_skema` AS `id_skema`,
                                        `c`.`nama` AS `nama_tuk`
                                        FROM `jadwal_asesmen` `a`
                                        JOIN `list_skema` `b`
                                        ON `a`.`skema` = `b`.`kode`
                                        JOIN `list_tuk` `c`
                                        ON `a`.`tuk` = `c`.`id`");
        $data['list_tujuan_asesmen'] = DB::select("SELECT * from list_tujuan_asesmen");
        $id_skema = $data['res_jadwal_asesmen'][0]->id_skema;
        $data['id_skema'] = $id_skema;

        $request->session()->put('id_skema', $data['id_skema']);
        $request->session()->put('data_jadwal_asesmen', $data['res_jadwal_asesmen']);

        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $token_asesor = $data_session['token'];
        $data['data_apl01'] = DB::select(" SELECT * FROM apl01 WHERE token_asesor = '$token_asesor' AND tanggalasesmen >= CURRENT_DATE - INTERVAL 1 WEEK ");
        $data['list_buktiak02'] = DB::select(" SELECT * FROM list_buktiak02 ");
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Formulir Rekaman Asesmen Kompetensi";
        return view('uji_kompetensi.frakak02', $data);
    }

    public function frakak02_lanjut(Request $request, string $id_jadwal)
    {
        $id = $request->input('id');
        //ambil tanggal dari jadwal asesmen
        //ambil tanggal dari jadwal asesmen
        // $data['res_jadwal_asesmen'] = DB::table('jadwal_asesmen')->where('id', $id_jadwal_asesmen)->first();
        $data['res_jadwal_asesmen'] = DB::select("SELECT
                                        `a`.`nama_jadwal` AS `nama_jadwal`,
                                        `a`.`tanggal_spk` AS `tanggal_spk`,
                                        `a`.`lokasi_uji` AS `lokasi_uji`,
                                        `b`.`nama` AS `nama_skema`,
                                        `b`.`nomor` AS `nomor_skema`,
                                        `b`.`id_skema` AS `id_skema`,
                                        `c`.`nama` AS `nama_tuk`
                                        FROM `jadwal_asesmen` `a`
                                        JOIN `list_skema` `b`
                                        ON `a`.`skema` = `b`.`kode`
                                        JOIN `list_tuk` `c`
                                        ON `a`.`tuk` = `c`.`id`");
        $data['list_tujuan_asesmen'] = DB::select("SELECT * from list_tujuan_asesmen");
        $id_skema = $data['res_jadwal_asesmen'][0]->id_skema;
        $data['id_skema'] = $id_skema;

        $request->session()->put('id_skema', $data['id_skema']);
        $request->session()->put('data_jadwal_asesmen', $data['res_jadwal_asesmen']);

        $data_session = $request->session()->get('dataUser');
        $data['token'] = $data_session['token'];
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $token_asesor = $data_session['token'];
        $data['data_apl01'] = DB::select(" SELECT * FROM apl01 WHERE token_asesor = '$token_asesor' AND tanggalasesmen >= CURRENT_DATE - INTERVAL 1 WEEK ");
        $data['list_buktiak02'] = DB::select(" SELECT * FROM list_buktiak02 ");
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Formulir Rekaman Asesmen Kompetensi";
        $data['id_jadwal'] = $id_jadwal;
        $asesor = DB::table('list_asesor')->where('token', $data['token'])->first();
        $data['token_asesor'] = $asesor->token;

        $res_apl01 = DB::table('apl01')->where('id_jadwal_asesmen', $id_jadwal)->first();
        $token_asesor = $asesor->token;
        $data['ambil_asesi'] = DB::select("SELECT
                            *
                            FROM apl01
                            WHERE `token_asesor` = '$token_asesor' 
                            AND `id_jadwal_asesmen` =  $id_jadwal
                            ORDER BY  `nama_asesi` ASC
                            ");
        return view('uji_kompetensi.frakak02_lanjut', $data);
    }

    public function formFrakak02(Request $request, string $id_jadwal, string $id)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $email = $data_session['email'];
        $data['data_apl01'] = DB::select(" SELECT * FROM apl01 WHERE id_jadwal_asesmen = $id_jadwal AND id = $id ");
        $data['data_jadwal_asesmen'] = DB::select(" SELECT * FROM jadwal_asesmen WHERE id = $id_jadwal");
        $data['list_buktiak02'] = DB::select(" SELECT * FROM list_buktiak02 ");
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Persetujuan Asesmen dan Kerahasiaan";
        $token = $data_session['token'];

        $data['res_jadwal_asesmen'] = DB::select("SELECT
                                        `a`.`nama_jadwal` AS `nama_jadwal`,
                                        `a`.`tanggal_spk` AS `tanggal_spk`,
                                        `a`.`lokasi_uji` AS `lokasi_uji`,
                                        `b`.`nama` AS `nama_skema`,
                                        `b`.`nomor` AS `nomor_skema`,
                                        `b`.`id_skema` AS `id_skema`,
                                        `c`.`nama` AS `nama_tuk`
                                        FROM `jadwal_asesmen` `a`
                                        JOIN `list_skema` `b`
                                        ON `a`.`skema` = `b`.`kode`
                                        JOIN `list_tuk` `c`
                                        ON `a`.`tuk` = `c`.`id`");
        $data['list_tujuan_asesmen'] = DB::select("SELECT * from list_tujuan_asesmen");
        $id_skema = $data['res_jadwal_asesmen'][0]->id_skema;
        $data['id_skema'] = $id_skema;
        $data['data_skema'] = DB::select("SELECT id,induk,nama,
                max(case when (nama='1') then isi else '-' end) as kode,
                max(case when (nama='2') then isi else '-' end) as nama

                FROM (SELECT * FROM list_skema_full ORDER BY id ASC) a,
                (SELECT @pv := $id_skema) b
                WHERE find_in_set(induk, @pv)
                group by grup,induk order by id ASC");
        return view('uji_kompetensi.formFrakak02', $data);
    }

    public function asesiak02(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['cek'] = DB::table('ak_ak02')->where('email_asesi', $data['email'])->first();
        $email = $data_session['email'];
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Persetujuan Asesmen dan Kerahasiaan";
        $token = $data_session['token'];

        if ($data['cek'] == true)
        {
            $data['res_jadwal_asesmen'] = DB::select("SELECT
            `a`.`nama_jadwal` AS `nama_jadwal`,
            `a`.`tanggal_spk` AS `tanggal_spk`,
            `a`.`lokasi_uji` AS `lokasi_uji`,
            `b`.`nama` AS `nama_skema`,
            `b`.`nomor` AS `nomor_skema`,
            `b`.`id_skema` AS `id_skema`,
            `c`.`nama` AS `nama_tuk`
            FROM `jadwal_asesmen` `a`
            JOIN `list_skema` `b`
            ON `a`.`skema` = `b`.`kode`
            JOIN `list_tuk` `c`
            ON `a`.`tuk` = `c`.`id`");
            $data['list_tujuan_asesmen'] = DB::select("SELECT * from list_tujuan_asesmen");
            $id_skema = $data['res_jadwal_asesmen'][0]->id_skema;
            $data['id_skema'] = $id_skema;
            $data['data_skema'] = DB::select("SELECT id,induk,nama,
            max(case when (nama='1') then isi else '-' end) as kode,
            max(case when (nama='2') then isi else '-' end) as nama

            FROM (SELECT * FROM list_skema_full ORDER BY id ASC) a,
            (SELECT @pv := $id_skema) b
            WHERE find_in_set(induk, @pv)
            group by grup,induk order by id ASC");

            $data['asesiak02'] = DB::select("SELECT 
                        `a`.*
                        FROM `ak_ak02` `a`
                        WHERE email_asesi = '$email'
                        ");
            // dd($data['asesiak02']);
            $data['asesiak02bukti'] = DB::select("SELECT 
                    `a`.`bukti`
                    FROM `ak_ak02` `a`
                    WHERE email_asesi = '$email'
                    ");

            $bukti = $data['asesiak02bukti'];
            // $bukti_arr = [];
            // foreach ($bukti as $value) {
            //     array_push($bukti_arr, $value);
            //     // echo "Chosen colour : " . $value . '<br/>';
            // }
            // $bukti_oke = implode(";", $bukti);
            $bukti2 = $bukti[0]->bukti;
            // dd($bukti2);
            $bukti3 = explode(";", $bukti2);

            $data['explode_bukti'] = $bukti3;

            // dd($data['explode_bukti']);

            $data['list_buktiak02'] = DB::select('SELECT * FROM `list_buktiak02`');
            return view('uji_kompetensi.formFrakak02_asesi', $data, [
                'bukti2' => $bukti2,
                'buktimantap' => $this->bukti_mantap
            ]);
        }
        else 
        {
            Alert::warning('Gagal', 'Maaf, Data Formulir Rekaman Asesmen Kompetensi (AK 02) Anda Belum Diproses Asesor');
            return redirect('/asesi');
        }
    }

    public function saveFrakak02(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        //jika ada tanda_tangan asesi
        $id_jadwal = $request->input('id_jadwal');
        $nama_asesi = $request->input('nama_asesi');
        $email_asesi = $request->input('email_asesi');
        $nama_asesor = $request->input('nama_asesor');
        $met_asesor = $request->input('met_asesor');
        $skema = $request->input('skema');
        $tanggal_mulai_asesmen = $request->input('tanggal_mulai_asesmen');
        $tanggal_akhir_asesmen = $request->input('tanggal_akhir_asesmen');
        $bukti = $request->input('bukti');
        $bukti_arr = [];
        foreach ($bukti as $value) {
            array_push($bukti_arr, $value);
            // echo "Chosen colour : " . $value . '<br/>';
        }
        $bukti_oke = implode(";", $bukti_arr);
        $rekomendasi = $request->input('rekomendasi');
        $tindak_lanjut = $request->input('tindak_lanjut');
        $komentar_asesor = $request->input('komentar_asesor');
        $ttdasesor = $request->input('ttdasesor');
        $tanggal_asesor = $request->input('tanggal_asesor');
        $ttdasesi = $request->input('ttdasesi');
        $tanggal_asesi = $request->input('tanggal_asesi');

        $waktu_sekarang     = Date('Y-m-d');
        $induk_lsp = "BBPVP_Bandung";
        $data = array(
            'id_jadwal' => $id_jadwal,
            'nama_asesi' => $nama_asesi,
            'email_asesi' => $email_asesi,
            'nama_asesor' => $nama_asesor,
            'met_asesor' => $met_asesor,
            'skema' => $skema,
            'tanggal_mulai_asesmen' => $tanggal_mulai_asesmen,
            'tanggal_akhir_asesmen' => $tanggal_akhir_asesmen,
            'bukti' => $bukti_oke,
            'rekomendasi' => $rekomendasi,
            'tindak_lanjut' => $tindak_lanjut,
            'komentar_asesor' => $komentar_asesor,
            'ttd_asesor' => $ttdasesor,
            'tanggal_asesor' => $tanggal_asesor,
            'ttd_asesi' => $ttdasesi,
            'tanggal_asesi' => $tanggal_asesi
        );
        DB::table('ak_ak02')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([UjiKompetensi::class, 'frakak02']);
    }

    public function saveFrakak02_asesi(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        //jika ada tanda_tangan asesi
        $id_ak02 = $request->input('id_ak02');
        $ttdasesi = $request->input('ttdasesi');
        $tanggal_asesi = $request->input('tanggal_asesi');

        $waktu_sekarang     = Date('Y-m-d');
        $induk_lsp = "BBPVP_Bandung";
        DB::table('ak_ak02')
        ->where('id_ak02', $id_ak02)
        ->update([
            'ttd_asesi' => $ttdasesi,
            'tanggal_asesi' => $tanggal_asesi
        ]);

        Alert::success('Berhasil', 'Data Berhasil Ditambahkan');
        return redirect()->action([UjiKompetensi::class, 'asesiak02']);
    }

    public function getJadwalAsesmen_ak02(Request $request)
    {
        $id_jadwal_asesmen = $request->input('idJadwalAsesmen');
        $data = DB::select("SELECT
                            `a`.`id` AS `id`,
                            `a`.`tanggal_uji` AS `tanggal_uji`,
                            `a`.`nama_jadwal` AS `nama_jadwal`,
                            `b`.`nama` AS `nama_tuk`,
                            `e`.`nama_kejuruan` AS `tema`,
                            `c`.`nama` AS `nama_skema`,
                            `c`.`nomor` AS `nomor_skema`,
                            `c`.`id_skema` AS `id_skema`,
                            `a`.`target_peserta` AS `target_peserta`,
                            `a`.`jam_uji` AS `jam_uji`,
                            `a`.`lokasi_uji` AS `lokasi_uji`,
                            `d`.`nama` AS `nama_sumber_anggaran`,
                            `a`.`anggaran_perasesi` AS `anggaran_perasesi`,
                            `a`.`nomor_spk` AS `nomor_spk`,
                            `a`.`tanggal_spk` AS `tanggal_spk`,
                            `a`.`tgl_mulai_pelatihan` AS `tgl_mulai_pelatihan`,
                            `a`.`tgl_akhir_pelatihan` AS `tgl_akhir_pelatihan`,
                            `a`.`durasi_pelatihan` AS `durasi_pelatihan`,
                            `a`.`nama_pelatihan` AS `nama_pelatihan`,
                            `a`.`nama_instansi` AS `nama_instansi`,
                            `a`.`nama_lsp` AS `nama_lsp`
                            FROM `jadwal_asesmen` `a`
                            JOIN `list_tuk` `b`
                            ON `a`.`tuk` = `b`.`id`
                            JOIN `list_skema` `c`
                            ON `a`.`skema` = `c`.`kode`
                            JOIN `list_sumber_anggaran` `d`
                            ON `a`.`sumber_anggaran` = `d`.`id`
                            JOIN `list_kejuruan` `e`
                            ON `a`.`tema` = `e`.`id_kejuruan`
                            WHERE `a`.`tanggal_uji`>=CURRENT_DATE - INTERVAL 1 WEEK ORDER BY `a`.`tanggal_uji` DESC");
        echo json_encode($data);
    }

    public function getAsesi_ak02(Request $request, string $id_jadwal)
    {
        // $id = $request->input('id');
        $res_apl01 = DB::table('apl01')->where('id_jadwal_asesmen', $id_jadwal)->first();
        $id_jadwal_asesmen = $res_apl01->id_jadwal_asesmen;
        $token_asesor = $res_apl01->token_asesor;
        $data = DB::select("SELECT
                            *
                            FROM apl01
                            WHERE `token_asesor` = '$token_asesor' 
                            AND `id_jadwal_asesmen` =  $id_jadwal_asesmen
                            ORDER BY  `nama_asesi` ASC
                            ");
        // $data = DB::select("SELECT
        //                     *
        //                     FROM apl01
        //                     WHERE `token_asesor` = '$token_asesor' 
        //                     AND `id_jadwal_asesmen` =  $id_jadwal_asesmen
        //                     ORDER BY  `nama_asesi` ASC");
        echo json_encode($data);
    }
}