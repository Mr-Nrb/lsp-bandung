<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class Informasi extends Controller
{
    public function data_informasi(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Data Home Page";
        $data['title_sub_menu'] = "Data Informasi";
        $data['list_status_aktif'] = DB::select('SELECT * FROM `list_status_aktif` ORDER BY id DESC');
        return view('data_home_page.data_informasi', $data);
    }

    public function getDataInformasi(Request $request)
    {
        $data = DB::select('SELECT *
                            FROM `list_informasi` `a`');
        echo json_encode($data);
    }

    public function saveDataInformasi(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        $judul_informasi = $request->input('judul_informasi');
        $keterangan = $request->input('keterangan');
        $status_aktif = $request->input('status_aktif');

        $data = array(
            'judul_informasi' => $judul_informasi,
            'keterangan' => $keterangan,
            'is_aktif' => $status_aktif
        );

        DB::table('list_informasi')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([Informasi::class, 'data_informasi']);
    }

    public function hapusDataInformasi(Request $request)
    {
        $id = $request->input('id');

        DB::delete('DELETE FROM list_informasi WHERE id = ?', [$id]);
        echo json_encode($id);
    }

    public function getDataInformasiById(Request $request)
    {
        $id = $request->input('id');
        $data = DB::select("SELECT * FROM list_informasi WHERE id = '$id'");
        echo json_encode($data);
    }

    public function updateDataInformasi(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];

        $id = $request->input('id');
        $data = DB::table('list_informasi')->where('id', $id)->first();

        $judul_informasi = $request->input('judul_informasi');
        $keterangan = $request->input('keterangan');
        $status_aktif = $request->input('status_aktif');

        $update =  DB::table('list_informasi')
            ->where('id', $id)
            ->update([
                'judul_informasi' => $judul_informasi,
                'keterangan' => $keterangan,
                'is_aktif' => $status_aktif
            ]);
        echo json_encode($update);
    }
}
