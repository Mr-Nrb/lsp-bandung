<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Str;


class Auth extends Controller
{

    public function login()
    {
        
        return view('auth.login');
    }

    public function validasiLogin(Request $request)
    {
        $user = DB::table('users')->where('email', $request->input('email'))->first();
        if ($user) {
            //cek pin
            if ($user->password == $request->input('password')) {
                $request->session()->put('pesan', 'Berhasil login');
                $dataUser = [
                    'name'     => $user->name,
                    'email'     => $user->email,
                    'token'     => $user->token,
                    'foto_profile'     => $user->foto_profile,
                    'password'  => $user->password,
                    'role_id'  => $user->role_id
                ];
                $request->session()->put('dataUser', $dataUser);
                if ($dataUser['role_id'] == 1) {
                    return redirect()->action([Sa::class, 'sa']);
                } else if ($dataUser['role_id'] == 2) {
                    return redirect()->action([Admin::class, 'admin']);
                } else if ($dataUser['role_id'] == 3) {
                    return redirect()->action([Asesor::class, 'asesor']);
                } else if ($dataUser['role_id'] == 4) {
                    return redirect()->action([Asesi::class, 'asesi']);
                } else if ($dataUser['role_id'] == 5) {
                    return redirect()->action([Penyelia::class, 'penyelia']);
                }

            } else {
                Alert::error('Gagal', 'Password Anda Salah');
                return redirect()->action([Auth::class, 'login']);
            }
        } else {
            Alert::error('Gagal', 'Email Anda tidak Terdaftar');
            return redirect()->action([Auth::class, 'login']);
        }
    }


    public function logout(Request $request)
    {
        $request->session()->forget('dataUser');

        return redirect('/login');
    }


    public function daftar()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        $waktu_sekarang     = Date('Y-m-d');
        $email = $request->input('email');
        $str =  str::upper($request->input('name'));
        $cek =  DB::select("SELECT * FROM users WHERE email = '$email'");
        if ($cek == []) {

        $data = array(
            'role_id' => '4',
            'name' => $str ,
            'email' => $request->input('email'),
            'email_verified_at' => $waktu_sekarang,
            'password' => $request->input('password'),
            'token' => md5($email),
            'created_at' => $waktu_sekarang,
            'updated_at' => '',
            'deleted_at' => ''
        );
    
        DB::table('users')->insert($data);

            Alert::success('Berhasil', 'Register Akun Berhasil ' . $str);
            return redirect('/login');
        } else {
            Alert::error('Duplikat', 'Email Duplikat');
            return back();
        }
       
    }

    public function ubahPassword(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['token'] = $data_session['token'];
        $data['user'] = DB::table('users')->where('token',  $data['token'])->first();
        $data['title_menu'] = "Profile";
        $data['title_sub_menu'] = "Ubah Password";
        return view('auth.ubahPassword', $data);
    }

    public function saveUbahPassword(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();
        
        $password = $request->input('password');
        $update_users = DB::table('users')
            ->where('token', $token)
            ->update([
                'password' => $password
            ]);
            // return redirect('/profile_lsp');
            // dd($user);
        if ($user->role_id == 2) {
            Alert::success('Berhasil', 'Data Berhasil di Update');
            return redirect('/profile_lsp');
        } else if ($user->role_id == 3) {
            Alert::success('Berhasil', 'Data Berhasil di Update');
            return redirect('/profile_asesor');
        } else if ($user->role_id == 4) {
            Alert::success('Berhasil', 'Data Berhasil di Update');
            return redirect('/profile_asesi');
        } else if ($user->role_id == 5) {
            Alert::success('Berhasil', 'Data Berhasil di Update');
            return redirect('/profile_penyelia');
        }
    }

    public function lupaPassword()
    {
        return view('auth.lupaPassword');
    }
    
}
