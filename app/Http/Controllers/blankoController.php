<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\skema;
use App\Models\list_unitModel;
use App\Models\Viewlistps;
use App\Models\Viewprakerin;
use Illuminate\Support\Facades\DB;

class blankoController extends Controller
{
    protected $skema;
    
    public function __construct()
    {
        // $this->skema = skema::all();
    }

    public function indexblanko(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Monitoring";
        $data['title_sub_menu'] = "Monitoring Blanko";
       
        return view('monitoring.monitoring_blanko', $data);
       
    }
   
}
