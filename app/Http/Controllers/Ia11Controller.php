<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\skema;
use App\Models\list_unitModel;
use App\Models\Viewlistps;
use App\Models\Viewprakerin;
use Illuminate\Support\Facades\DB;

class Ia11Controller extends Controller
{
    protected $skema;

    public function __construct()
    {
        // $this->skema = skema::all();
    }

    public function indexia11(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Monitoring";
        $data['title_sub_menu'] = "Monitoring IA11";
        $token = $data_session['token'];
        $data['jadwal_asesmen'] = DB::table("jadwal_asesmen")->get();

        return view('monitoring.jadwal_asesmen_monitoring_ia11', $data);
    }

    public function getJadwalAsesmen(Request $request)
    {
        $data = DB::select("SELECT
                            `a`.`id` AS `id`,
                            `a`.`tanggal_uji` AS `tanggal_uji`,
                            `a`.`nama_jadwal` AS `nama_jadwal`,
                            `b`.`nama` AS `nama_tuk`,
                            `a`.`tema` AS `tema`,
                            `c`.`nama` AS `nama_skema`,
                            `c`.`nomor` AS `nomor_skema`,
                            `c`.`id_skema` AS `id_skema`,
                            `a`.`target_peserta` AS `target_peserta`,
                            `a`.`jam_uji` AS `jam_uji`,
                            `a`.`lokasi_uji` AS `lokasi_uji`,
                            `d`.`nama` AS `nama_sumber_anggaran`,
                            `a`.`anggaran_perasesi` AS `anggaran_perasesi`,
                            `a`.`nomor_spk` AS `nomor_spk`,
                            `a`.`tanggal_spk` AS `tanggal_spk`,
                            `a`.`tgl_mulai_pelatihan` AS `tgl_mulai_pelatihan`,
                            `a`.`tgl_akhir_pelatihan` AS `tgl_akhir_pelatihan`,
                            `a`.`durasi_pelatihan` AS `durasi_pelatihan`,
                            `a`.`nama_pelatihan` AS `nama_pelatihan`,
                            `a`.`nama_instansi` AS `nama_instansi`,
                            `a`.`nama_lsp` AS `nama_lsp`
                            FROM `jadwal_asesmen` `a`
                            JOIN `list_tuk` `b`
                            ON `a`.`tuk` = `b`.`id`
                            JOIN `list_skema` `c`
                            ON `a`.`skema` = `c`.`kode`
                            JOIN `list_sumber_anggaran` `d`
                            ON `a`.`sumber_anggaran` = `d`.`id`
                            WHERE `a`.`tanggal_uji`>=CURRENT_DATE - INTERVAL 1 WEEK ORDER BY `a`.`tanggal_uji` DESC");
        echo json_encode($data);
    }

    public function jadwalLanjut_monitoring_ia11(Request $request, $id)
    {
        $id_jadwal_asesmen = $id;
        $jadwal = DB::table('jadwal_asesor')
            ->where('id_jadwal', '=', $id_jadwal_asesmen)->get();

        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Manajemen Jadwal";
        $data['title_sub_menu '] = "Jadwal Asesor";
        // dd($id_jadwal_asesmen);
        return view('monitoring.pilih_asesor_monitoring_ia11', $data, ['jadwal' => $jadwal]);
    }

    public function asesi_monitoring_ia11(Request $request, $no_reg, $id_jadwal_asesmen)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Monitoring";
        $data['title_sub_menu'] = "Monitoring Apl01";
        $token = $data_session['token'];
        $data['data_ia11'] = DB::select(" SELECT * FROM ia11 WHERE no_reg = '$no_reg' AND id_jadwal_asesmen = $id_jadwal_asesmen");
        // dd($data['data_ia11']);
        return view('monitoring.monitoring_ia11', $data);
    }
    public function detail_print_ia11(Request $request, $id, $id_jadwal_asesmen)
    {
        $id_jadwal = $id_jadwal_asesmen;
        // dd($id_jadwal);
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $email = $data_session['email'];
        // $data['data_ia11'] = DB::table('apl01')->where('id', $id)->first();
        $data['data_ia11'] = DB::select(" SELECT
                                        ia11.*,
                                        apl01.id AS id_apl01,
                                        apl01.skema,
                                        apl01.nomor,
                                        apl01.nama_asesi,
                                        apl01.namaasesor,
                                        apl01.tanggalasesmen
                                        FROM ia11
                                        JOIN apl01
                                        ON ia11.no_reg = apl01.no_Reg
                                        WHERE ia11.id = $id

                                 ");
        // @dd($data['data_ia11']);

        $data['ket1'] = DB::select(" SELECT instruksi_jelas FROM ia11 WHERE id = $id");
        $data['ket2'] = DB::select(" SELECT informasi_tepat FROM ia11 WHERE id = $id");
        // dd($data['data_ia11']);
        // $data['res_jadwal_asesmen'] = DB::table('jadwalasesmen')->where('juduljadwal', $id_jadwal_asesmen)->first();

        $data['res_jadwal_asesmen'] = DB::select("SELECT
                                              `a`.`nama_jadwal` AS `nama_jadwal`,
                                              `a`.`tanggal_spk` AS `tanggal_spk`,
                                              `a`.`lokasi_uji` AS `lokasi_uji`,
                                              `b`.`nama` AS `nama_skema`,
                                              `b`.`nomor` AS `nomor_skema`,
                                              `b`.`id_skema` AS `id_skema`,
                                              `c`.`nama` AS `nama_tuk`
                                              FROM `jadwal_asesmen` `a`
                                              JOIN `list_skema` `b`
                                              ON `a`.`skema` = `b`.`kode`
                                              JOIN `list_tuk` `c`
                                              ON `a`.`tuk` = `c`.`id`
                                              WHERE `a`.`id` = $id_jadwal");
        // dd( $data['res_jadwal_asesmen'] );
        $id_skema = $data['res_jadwal_asesmen'][0]->id_skema;
        $data['get_skema'] = DB::select("SELECT id,induk,nama,
                                    max(case when (nama='1') then isi else '-' end) as kode,
                                    max(case when (nama='2') then isi else '-' end) as nama

                                    FROM (SELECT * FROM list_skema_full ORDER BY id ASC) a,
                                    (SELECT @pv := $id_skema) b
                                    WHERE find_in_set(induk, @pv)
                                    group by grup,induk order by id ASC");

        // dd($data['get_skema']);
        $data['title_menu'] = "Uji Kompetensi";
        $data['title_sub_menu'] = "Meninjau Instrumen Asesmen (IA11)";
        return view('monitoring.view_print_ia11', $data);
    }
}
