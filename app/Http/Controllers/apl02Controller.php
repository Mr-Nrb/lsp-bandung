<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\skema;
use App\Models\unit;
use App\Models\Viewlistps;
use App\Models\Viewprakerin;
use Illuminate\Support\Facades\DB;

class apl02Controller extends Controller
{

    public function JadwalAsesmen_monitoringapl02(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Monitoring";
        $data['title_sub_menu'] = "Monitoring Apl02";
        $token = $data_session['token'];
        $data['jadwal_asesmen'] = DB::table("jadwal_asesmen")->get();
      
        // dd($data['data_apl01']);
        return view('monitoring.jadwal_asesmen_monitoring_apl02', $data);
    }

    public function getJadwalAsesmen(Request $request)
    {
        $data = DB::select("SELECT
                            `a`.`id` AS `id`,
                            `a`.`tanggal_uji` AS `tanggal_uji`,
                            `a`.`nama_jadwal` AS `nama_jadwal`,
                            `b`.`nama` AS `nama_tuk`,
                            `a`.`tema` AS `tema`,
                            `c`.`nama` AS `nama_skema`,
                            `c`.`nomor` AS `nomor_skema`,
                            `c`.`id_skema` AS `id_skema`,
                            `a`.`target_peserta` AS `target_peserta`,
                            `a`.`jam_uji` AS `jam_uji`,
                            `a`.`lokasi_uji` AS `lokasi_uji`,
                            `d`.`nama` AS `nama_sumber_anggaran`,
                            `a`.`anggaran_perasesi` AS `anggaran_perasesi`,
                            `a`.`nomor_spk` AS `nomor_spk`,
                            `a`.`tanggal_spk` AS `tanggal_spk`,
                            `a`.`tgl_mulai_pelatihan` AS `tgl_mulai_pelatihan`,
                            `a`.`tgl_akhir_pelatihan` AS `tgl_akhir_pelatihan`,
                            `a`.`durasi_pelatihan` AS `durasi_pelatihan`,
                            `a`.`nama_pelatihan` AS `nama_pelatihan`,
                            `a`.`nama_instansi` AS `nama_instansi`,
                            `a`.`nama_lsp` AS `nama_lsp`
                            FROM `jadwal_asesmen` `a`
                            JOIN `list_tuk` `b`
                            ON `a`.`tuk` = `b`.`id`
                            JOIN `list_skema` `c`
                            ON `a`.`skema` = `c`.`kode`
                            JOIN `list_sumber_anggaran` `d`
                            ON `a`.`sumber_anggaran` = `d`.`id`
                            WHERE `a`.`tanggal_uji`>=CURRENT_DATE - INTERVAL 1 WEEK ORDER BY `a`.`tanggal_uji` DESC");
        echo json_encode($data);
    }
    public function getJadwalAsesmen_apl02(Request $request)
    {
        $id_jadwal_asesmen = $request->input('idJadwalAsesmen');
        $data = DB::select("SELECT
                            `a`.`id` AS `id`,
                            `a`.`tanggal_uji` AS `tanggal_uji`,
                            `a`.`nama_jadwal` AS `nama_jadwal`,
                            `b`.`nama` AS `nama_tuk`,
                            `e`.`nama_kejuruan` AS `tema`,
                            `c`.`nama` AS `nama_skema`,
                            `c`.`nomor` AS `nomor_skema`,
                            `c`.`id_skema` AS `id_skema`,
                            `a`.`target_peserta` AS `target_peserta`,
                            `a`.`jam_uji` AS `jam_uji`,
                            `a`.`lokasi_uji` AS `lokasi_uji`,
                            `d`.`nama` AS `nama_sumber_anggaran`,
                            `a`.`anggaran_perasesi` AS `anggaran_perasesi`,
                            `a`.`nomor_spk` AS `nomor_spk`,
                            `a`.`tanggal_spk` AS `tanggal_spk`,
                            `a`.`tgl_mulai_pelatihan` AS `tgl_mulai_pelatihan`,
                            `a`.`tgl_akhir_pelatihan` AS `tgl_akhir_pelatihan`,
                            `a`.`durasi_pelatihan` AS `durasi_pelatihan`,
                            `a`.`nama_pelatihan` AS `nama_pelatihan`,
                            `a`.`nama_instansi` AS `nama_instansi`,
                            `a`.`nama_lsp` AS `nama_lsp`
                            FROM `jadwal_asesmen` `a`
                            JOIN `list_tuk` `b`
                            ON `a`.`tuk` = `b`.`id`
                            JOIN `list_skema` `c`
                            ON `a`.`skema` = `c`.`kode`
                            JOIN `list_sumber_anggaran` `d`
                            ON `a`.`sumber_anggaran` = `d`.`id`
                            JOIN `list_kejuruan` `e`
                            ON `a`.`tema` = `e`.`id_kejuruan`
                            WHERE `a`.`id` =  $id_jadwal_asesmen
                           ");
        echo json_encode($data);
    }



    public function jadwalLanjut_monitoring_apl02(Request $request, $id)
    {
        $id_jadwal_asesmen = $id;
        $jadwal = DB::table('jadwal_asesor')
        ->where('id_jadwal','=',$id_jadwal_asesmen)->get();

        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Manajemen Jadwal";
        $data['title_sub_menu'] = "Jadwal Asesor";
        // dd($id_jadwal_asesmen);
        return view('monitoring.pilih_asesor_monitoring_apl02', $data, ['jadwal' => $jadwal]);
    }

    public function asesi_monitoring_apl02(Request $request, $no_reg, $id_jadwal_asesmen)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Monitoring";
        $data['title_sub_menu'] = "Monitoring Apl02";
        $token = $data_session['token'];
        $data['data_apl02'] = DB::table("apl02")->where('no_Reg', '=', $no_reg, 'AND', 'id_jadwal', '=', $id_jadwal_asesmen)->get();
        // $data['data_apl02'] = DB::select("SELECT 
        //                                     `jadwal_asesmen`.`nama_jadwal`,
        //                                     `apl02`.*
        //                                     FROM `apl02`
        //                                     JOIN `jadwal_asesmen` 
        //                                     ON `apl02`.`id_jadwal` = `jadwal_asesmen`.`id`
        //                                     WHERE `apl02`.`id_jadwal` = $id_jadwal_asesmen
        //                                     AND `no_Reg` = '$no_reg'
        //                             ");
        // dd($data['data_apl02']);
      
        return view('monitoring.monitoring_apl02', $data);
    }

    public function detail_print_apl02(Request $request, string $id)
    {
         //ambil tanggal dari jadwal asesmen
         $res_jadwal_asesmen = DB::table('jadwal_asesmen')->where('id', $id)->first();
         $id_jadwal_asesmen = $id;
         //ambil tanggal dari jadwal asesmen
         // $data['res_jadwal_asesmen'] = DB::table('jadwal_asesmen')->where('id', $id_jadwal_asesmen)->first();
         $data['res_jadwal_asesmen'] = DB::select("SELECT
                                         `a`.`nama_jadwal` AS `nama_jadwal`,
                                         `a`.`tanggal_spk` AS `tanggal_spk`,
                                         `a`.`lokasi_uji` AS `lokasi_uji`,
                                         `a`.`id` AS `id`,
                                         `b`.`nama` AS `nama_skema`,
                                         `b`.`nomor` AS `nomor_skema`,
                                         `b`.`id_skema` AS `id_skema`,
                                         `c`.`nama` AS `nama_tuk`
                                         FROM `jadwal_asesmen` `a`
                                         JOIN `list_skema` `b`
                                         ON `a`.`skema` = `b`.`kode`
                                         JOIN `list_tuk` `c`
                                         ON `a`.`tuk` = `c`.`id`
                                         WHERE `a`.`id` = $id_jadwal_asesmen");
         $data['list_tujuan_asesmen'] = DB::select("SELECT * from list_tujuan_asesmen");
        //  dd($data['res_jadwal_asesmen']);
         $id_skema = $data['res_jadwal_asesmen'][0]->id_skema;
         $data['id_skema'] = $id_skema;
 
         $request->session()->put('id_skema', $data['id_skema']);
         $request->session()->put('data_jadwal_asesmen', $data['res_jadwal_asesmen']);
 
         $data_session = $request->session()->get('dataUser');
         $data['role_id'] = $data_session['role_id'];
         $data['email'] = $data_session['email'];
         $email = $data_session['email'];
         $data['data_apl02'] = DB::select(" SELECT * FROM apl01 WHERE id = '$id'");
        //  dd($data['data_apl02']);
         $data['user'] = DB::table('users')->where('email', $data['email'])->first();
         $data['title_menu'] = "Uji Kompetensi";
         $data['title_sub_menu'] = "Asesmen Mandiri";

        return view('monitoring.view_print_apl02', $data);
    }

    public function GetSkema_full(Request $request, $id_jadwal_asesmen)
    {
        $id_skema = $request->session()->get('id_skema');
        // $data_jadwal_asesmen = $request->session()->get('data_jadwal_asesmen');
        $data_jadwal_asesmen = DB::select("SELECT
                                         `a`.`nama_jadwal` AS `nama_jadwal`,
                                         `a`.`tanggal_spk` AS `tanggal_spk`,
                                         `a`.`lokasi_uji` AS `lokasi_uji`,
                                         `a`.`id` AS `id`,
                                         `b`.`nama` AS `nama_skema`,
                                         `b`.`nomor` AS `nomor_skema`,
                                         `b`.`id_skema` AS `id_skema`,
                                         `c`.`nama` AS `nama_tuk`
                                         FROM `jadwal_asesmen` `a`
                                         JOIN `list_skema` `b`
                                         ON `a`.`skema` = `b`.`kode`
                                         JOIN `list_tuk` `c`
                                         ON `a`.`tuk` = `c`.`id`
                                         WHERE `a`.`id` = $id_jadwal_asesmen");
        $data_skema = DB::select("SELECT id,induk,nama,
                            max(case when (nama='1') then isi else '-' end) as kode,
                            max(case when (nama='2') then isi else '-' end) as nama,
                            max(case when (nama='3') then isi else '-' end) as elemen,
                            max(case when (nama='4') then isi else '-' end) as kuk

                            FROM (SELECT * FROM list_skema_full ORDER BY id ASC) a,
                            (SELECT @pv := $id_skema) b
                            WHERE find_in_set(induk, @pv) AND length(@pv := concat(@pv, ',', id))
                            group by grup,induk order by id ASC");
        $data = [
            "id_skema" => $id_skema,
            "data_skema" => $data_skema,
            "data_jadwal_asesmen" => $data_jadwal_asesmen,
        ];
        echo json_encode($data);
    }
   
                            // $data = DB::select("SELECT 
                            //                          `a`.`nama_jadwal`AS nama_jadwal,
                            //                          `b`. `id` AS `id`,
                            //                          `b`. `id_jadwal` AS `id_jadwal`,
                            //                          `b`. `no_Reg` AS `no_Reg`,
                            //                          `b`. `skema` AS `skema`,
                            //                          `b`. `nomor` AS `nomor`,
                            //                          `b`. `nama_asesi` AS `nama_asesi`,
                            //                          `b`. `tanggal_asesi` AS `tanggal_asesi`,
                            //                          `c`. `id_skema` AS `id_skema`
                            //                          FROM `apl02` `b`
                            //                          JOIN `jadwal_asesmen` `a` 
                            //                          ON `b`.`id_jadwal` = `a`.`id`
                            //                          JOIN `list_skema` `c`
                            //                          ON `a`.`skema` = `c`.`kode`
                            //                          WHERE `b`.`id_jadwal` = $id_jadwal_asesmen
                            //                          AND `no_Reg` = '$no_reg'
                            //                  ");
}
