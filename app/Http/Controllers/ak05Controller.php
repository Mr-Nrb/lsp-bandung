<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\skema;
use App\Models\list_unitModel;
use App\Models\Viewlistps;
use App\Models\Viewprakerin;
use Illuminate\Support\Facades\DB;

class ak05Controller extends Controller
{
    protected $skema;
    
    public function __construct()
    {
        // $this->skema = skema::all();
    }

    public function indexak05(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Laporan";
        $data['title_sub_menu'] = "Laporan Asesmen AK05";
       
        return view('asesor.ak05_pilih_jadwal', $data);
       
    }

    public function Getjadwal_ak05(Request $request, $id)
    {
        $id_jadwal_asesmen = $id;
        //ambil tanggal dari jadwal asesmen
        // $res_jadwal_asesmen = DB::table('jadwal_asesmen')->where('id', $id)->first();
        // //ambil tanggal uji dan nama_jadwal dari jadwal asesmen yg dipilih untuk nge group siapa aja asesornya
        // $tanggal_uji = $res_jadwal_asesmen->tanggal_uji;
        // $nama_jadwal = $res_jadwal_asesmen->nama_jadwal;
        $jadwal = DB::table('jadwal_asesor')
        ->where('id_jadwal','=',$id_jadwal_asesmen)->get();

        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Manajemen Jadwal";
        $data['title_sub_menu'] = "Jadwal Asesor";
        // dd($id_jadwal_asesmen);
        return view('asesor.pilih_jadwal_lanjut', $data, ['jadwal' => $jadwal]);
    }
   
}
