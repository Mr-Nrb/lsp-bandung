<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Alamat extends Controller
{
    public function getKabKota(Request $request)
    {
        $valProvinsi = $request->input('valProvinsi');
        // @dd($valProvinsi);
        $panjang = $request->input('panjang');
        $n = strlen($valProvinsi);
        $data = DB::select("SELECT * FROM wilayah_2020 WHERE LEFT(kode,$n)='$valProvinsi' AND CHAR_LENGTH(kode)='$panjang' ORDER BY nama");
        echo json_encode($data);
    }
}
