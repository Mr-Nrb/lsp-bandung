<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class Mapa_mkva extends Controller
{
    //Menu MAPA01
    public function getUpload_mapa01(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('mapa01')->where('email', $data['email'])->first();

        $data = DB::select("SELECT *  FROM mapa01 ORDER BY tanggal_uji DESC");
        echo json_encode($data);
    }
    
    public function upload_mapa01(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['users'] = DB::table('users')->where('email', $data['email'])->first();
        $data['mapa01'] = DB::table('mapa01')->get();
        $data['title_menu'] = "MAPA dan MK.VA";
        $data['title_sub_menu'] = "Merencanakan Aktivitas dan Proses Asesmen (FR.MAPA.01)";
        $token = $data_session['token'];

        $data['nama_jadwal'] = DB::select("SELECT
                                            id, nama_jadwal, tanggal_uji, tuk
                                            FROM jadwal_asesmen");
                                            // jadwal_asesmen.id,
                                            // jadwal_asesmen.nama_jadwal,
                                            // jadwal_asesmen.tanggal_uji,
                                            // jadwal_asesmen.tuk,
                                            // jadwal_asesmen.skema,
                                            // jadwal_asesmen.nomor,
                                            // jadwal_asesor.id
                                            // FROM jadwal_asesmen
                                            // INNER JOIN jadwal_asesor
                                            // ON jadwal_asesmen.id = jadwal_asesor.id_jadwal");
        // $data['user'] = DB::table('view_mapa01')->where('email', '=', $data['email'] )->get();
        return view('mapa_mkva.upload_mapa01', $data);
    }

    public function saveUpload_mapa01(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();
        $nama_dokumen = $request->input('nama_dokumen');
        $namaFile = md5($nama_dokumen);
        $cek =  DB::select("SELECT * FROM upload_mapa01 WHERE nama_dokumen = '$nama_dokumen'");
        if ($cek == []) {
            if ($request->hasFile('nama_dokumen')) {
                $filenameWithExt = $request->file('nama_dokumen')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('nama_dokumen')->getClientOriginalExtension(); // Get just Extension
                $fileName_nama_dokumen = 'mapa01' . '_' . $token . '_' . $namaFile . '.' . $extension; // Filename To store
                $request->nama_dokumen->move(public_path('assets/document/upload_mapa01/asesor'), $fileName_nama_dokumen);
            } else {
                $fileName_nama_dokumen = $user->nama_dokumen;
            }

            $nama_jadwal = $request->input('nama_jadwal');
            $id_jadwal_asesor = $request->input('id_jadwal_asesor');
            $tanggal_uji = $request->input('tanggal_uji');
            $tuk = $request->input('tuk');
            $skema = $request->input('skema');
            $nomor = $request->input('nomor');
            $nama_asesor = $request->input('nama_asesor');
            $no_reg = $request->input('no_reg');
            $nama_dokumen = $request->input('nama_dokumen');

            $data = array(
                'nama_jadwal' => $nama_jadwal,
                'id_jadwal_asesor' => $id_jadwal_asesor,
                'tanggal_uji' => $tanggal_uji,
                'tuk' => $tuk,
                'skema' => $skema,
                'nomor' => $nomor,
                'nama_asesor' => $nama_asesor,
                'no_reg' => $no_reg,
                'nama_dokumen' => $nama_dokumen
            );
            DB::table('mapa01')->insert($data);
            Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
            return redirect()->action([Mapa_mkva::class, 'upload_mapa01']);
        } else {
            //jika duplikat maka redirect dan kasih pesan duplikat
            Alert::error('Duplikat', 'Dokumen Sudah Ada');
            return redirect()->action([Mapa_mkva::class, 'upload_mapa01']);
        }
    }
    
    //Menu MAPA02
    public function getUpload_mapa02(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('mapa02')->where('email', $data['email'])->first();

        $data = DB::select("SELECT *  FROM mapa02 ORDER BY tanggal_uji DESC");
        echo json_encode($data);
    }
    
    public function upload_mapa02(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['users'] = DB::table('users')->where('email', $data['email'])->first();
        $data['mapa02'] = DB::table('mapa02')->get();
        $data['title_menu'] = "MAPA dan MK.VA";
        $data['title_sub_menu'] = "Peta Instrumen Asesmen, Hasil Pendekatan Asesmen, dan Perencanaan Asesmen (FR.MAPA.02)";
        $token = $data_session['token'];

        $data['nama_jadwal'] = DB::select("SELECT
                                            id, nama_jadwal, tanggal_uji, tuk
                                            FROM jadwal_asesmen");
                                            // jadwal_asesmen.id,
                                            // jadwal_asesmen.nama_jadwal,
                                            // jadwal_asesmen.tanggal_uji,
                                            // jadwal_asesmen.tuk,
                                            // jadwal_asesmen.skema,
                                            // jadwal_asesmen.nomor,
                                            // jadwal_asesor.id
                                            // FROM jadwal_asesmen
                                            // INNER JOIN jadwal_asesor
                                            // ON jadwal_asesmen.id = jadwal_asesor.id_jadwal");
        // $data['user'] = DB::table('view_mapa02')->where('email', '=', $data['email'] )->get();
        return view('mapa_mkva.upload_mapa02', $data);
    }

    public function saveUpload_mapa02(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();
        $nama_dokumen = $request->input('nama_dokumen');
        $namaFile = md5($nama_dokumen);
        $cek =  DB::select("SELECT * FROM upload_mapa02 WHERE nama_dokumen = '$nama_dokumen'");
        if ($cek == []) {
            if ($request->hasFile('nama_dokumen')) {
                $filenameWithExt = $request->file('nama_dokumen')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('nama_dokumen')->getClientOriginalExtension(); // Get just Extension
                $fileName_nama_dokumen = 'mapa02' . '_' . $token . '_' . $namaFile . '.' . $extension; // Filename To store
                $request->nama_dokumen->move(public_path('assets/document/upload_mapa02/asesor'), $fileName_nama_dokumen);
            } else {
                $fileName_nama_dokumen = $user->nama_dokumen;
            }

            $nama_jadwal = $request->input('nama_jadwal');
            $id_jadwal_asesor = $request->input('id_jadwal_asesor');
            $tanggal_uji = $request->input('tanggal_uji');
            $tuk = $request->input('tuk');
            $skema = $request->input('skema');
            $nomor = $request->input('nomor');
            $nama_asesor = $request->input('nama_asesor');
            $no_reg = $request->input('no_reg');
            $nama_dokumen = $request->input('nama_dokumen');

            $data = array(
                'nama_jadwal' => $nama_jadwal,
                'id_jadwal_asesor' => $id_jadwal_asesor,
                'tanggal_uji' => $tanggal_uji,
                'tuk' => $tuk,
                'skema' => $skema,
                'nomor' => $nomor,
                'nama_asesor' => $nama_asesor,
                'no_reg' => $no_reg,
                'nama_dokumen' => $nama_dokumen
            );
            DB::table('mapa02')->insert($data);
            Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
            return redirect()->action([Mapa_mkva::class, 'upload_mapa02']);
        } else {
            //jika duplikat maka redirect dan kasih pesan duplikat
            Alert::error('Duplikat', 'Dokumen Sudah Ada');
            return redirect()->action([Mapa_mkva::class, 'upload_mapa02']);
        }
    }

    //Menu MKVA
    public function getUpload_mkva(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('mkva')->where('email', $data['email'])->first();

        $data = DB::select("SELECT *  FROM mkva ORDER BY tanggal_uji DESC");
        echo json_encode($data);
    }
    
    public function upload_mkva(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['users'] = DB::table('users')->where('email', $data['email'])->first();
        $data['mkva'] = DB::table('mkva')->get();
        $data['title_menu'] = "MAPA dan MK.VA";
        $data['title_sub_menu'] = "Memberikan Kontribusi Dalam Validasi Asesmen (MK.VA)";
        $token = $data_session['token'];

        $data['nama_jadwal'] = DB::select("SELECT
                                            id, nama_jadwal, tanggal_uji, tuk
                                            FROM jadwal_asesmen");
                                            // jadwal_asesmen.id,
                                            // jadwal_asesmen.nama_jadwal,
                                            // jadwal_asesmen.tanggal_uji,
                                            // jadwal_asesmen.tuk,
                                            // jadwal_asesmen.skema,
                                            // jadwal_asesmen.nomor,
                                            // jadwal_asesor.id
                                            // FROM jadwal_asesmen
                                            // INNER JOIN jadwal_asesor
                                            // ON jadwal_asesmen.id = jadwal_asesor.id_jadwal");
        // $data['user'] = DB::table('view_mkva')->where('email', '=', $data['email'] )->get();
        return view('mapa_mkva.upload_mkva', $data);
    }

    public function saveUpload_mkva(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();
        $nama_dokumen = $request->input('nama_dokumen');
        $namaFile = md5($nama_dokumen);
        $cek =  DB::select("SELECT * FROM upload_mkva WHERE nama_dokumen = '$nama_dokumen'");
        if ($cek == []) {
            if ($request->hasFile('nama_dokumen')) {
                $filenameWithExt = $request->file('nama_dokumen')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('nama_dokumen')->getClientOriginalExtension(); // Get just Extension
                $fileName_nama_dokumen = 'mkva' . '_' . $token . '_' . $namaFile . '.' . $extension; // Filename To store
                $request->nama_dokumen->move(public_path('assets/document/upload_mkva/asesor'), $fileName_nama_dokumen);
            } else {
                $fileName_nama_dokumen = $user->nama_dokumen;
            }

            $nama_jadwal = $request->input('nama_jadwal');
            $id_jadwal_asesor = $request->input('id_jadwal_asesor');
            $tanggal_uji = $request->input('tanggal_uji');
            $tuk = $request->input('tuk');
            $skema = $request->input('skema');
            $nomor = $request->input('nomor');
            $nama_asesor = $request->input('nama_asesor');
            $no_reg = $request->input('no_reg');
            $nama_dokumen = $request->input('nama_dokumen');

            $data = array(
                'nama_jadwal' => $nama_jadwal,
                'id_jadwal_asesor' => $id_jadwal_asesor,
                'tanggal_uji' => $tanggal_uji,
                'tuk' => $tuk,
                'skema' => $skema,
                'nomor' => $nomor,
                'nama_asesor' => $nama_asesor,
                'no_reg' => $no_reg,
                'nama_dokumen' => $nama_dokumen
            );
            DB::table('mkva')->insert($data);
            Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
            return redirect()->action([Mapa_mkva::class, 'upload_mkva']);
        } else {
            //jika duplikat maka redirect dan kasih pesan duplikat
            Alert::error('Duplikat', 'Dokumen Sudah Ada');
            return redirect()->action([Mapa_mkva::class, 'upload_mkva']);
        }
    }

    //Menu MAPA01
    public function view_mapa01(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "MAPA dan MK.VA";
        $data['title_sub_menu'] = "Merencanakan Aktivitas dan Proses Asesmen (MAPA.01)";
        $token = $data_session['token'];
        return view('mapa_mkva.view_mapa01', $data);
    }
    
    //Menu MAPA02
    public function view_mapa02(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "MAPA dan MK.VA";
        $data['title_sub_menu'] = "Peta Instrumen Asesmen, Pendekatan Asesmen, dan Perencanaan Asesmen (MAPA.02)";
        $token = $data_session['token'];
        return view('mapa_mkva.view_mapa02', $data);
    }

    //Menu MAPA01
    public function view_mkva(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "MAPA dan MKVA";
        $data['title_sub_menu'] = "Memberikan Kontribusi Dalam Validasi Asesmen (MK.VA)";
        $token = $data_session['token'];
        return view('mapa_mkva.view_mkva', $data);
    }
}