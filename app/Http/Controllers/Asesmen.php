<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class Asesmen extends Controller
{
    //MENU UPLOAD SOAL PRAKTIK
    public function getUpload_ia02(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('ia02')->where('email', $data['email'])->first();

        $data = DB::select("SELECT *  FROM ia02 ORDER BY tanggal_uji DESC");
        echo json_encode($data);
    }

    public function upload_ia02(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['users'] = DB::table('users')->where('email', $data['email'])->first();
        $data['ia02'] = DB::table('ia02')->get();
        $data['title_menu'] = "Asesmen";
        $data['title_sub_menu'] = "Tugas Praktik Demonstrasi (FR.IA.02)";
        $token = $data_session['token'];

        $data['nama_jadwal'] = DB::select("SELECT
                                            id, nama_jadwal, tanggal_uji, tuk
                                            FROM jadwal_asesmen");
                                            // jadwal_asesmen.id,
                                            // jadwal_asesmen.nama_jadwal,
                                            // jadwal_asesmen.tanggal_uji,
                                            // jadwal_asesmen.tuk,
                                            // jadwal_asesmen.skema,
                                            // jadwal_asesmen.nomor,
                                            // jadwal_asesor.id
                                            // FROM jadwal_asesmen
                                            // INNER JOIN jadwal_asesor
                                            // ON jadwal_asesmen.id = jadwal_asesor.id_jadwal");
        // $data['user'] = DB::table('view_ia04')->where('email', '=', $data['email'] )->get();
        return view('asesmen.upload_ia02', $data);
    }

    public function saveUpload_ia02(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();
        $nama_dokumen = $request->input('nama_dokumen');
        $namaFile = md5($nama_dokumen);
        $cek =  DB::select("SELECT * FROM upload_ia02 WHERE nama_dokumen = '$nama_dokumen'");
        if ($cek == []) {
            if ($request->hasFile('nama_dokumen')) {
                $filenameWithExt = $request->file('nama_dokumen')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('nama_dokumen')->getClientOriginalExtension(); // Get just Extension
                $fileName_nama_dokumen = 'soalPraktik02' . '_' . $token . '_' . $namaFile . '.' . $extension; // Filename To store
                $request->nama_dokumen->move(public_path('assets/document/upload_ia02/asesor'), $fileName_nama_dokumen);
            } else {
                $fileName_nama_dokumen = $user->nama_dokumen;
            }

            $nama_jadwal = $request->input('nama_jadwal');
            $id_jadwal_asesor = $request->input('id_jadwal_asesor');
            $tanggal_uji = $request->input('tanggal_uji');
            $tuk = $request->input('tuk');
            $skema = $request->input('skema');
            $nomor = $request->input('nomor');
            $nama_asesor = $request->input('nama_asesor');
            $no_reg = $request->input('no_reg');
            $nama_dokumen = $request->input('nama_dokumen');

            $data = array(
                'nama_jadwal' => $nama_jadwal,
                'id_jadwal_asesor' => $id_jadwal_asesor,
                'tanggal_uji' => $tanggal_uji,
                'tuk' => $tuk,
                'skema' => $skema,
                'nomor' => $nomor,
                'nama_asesor' => $nama_asesor,
                'no_reg' => $no_reg,
                'nama_dokumen' => $nama_dokumen
            );
            DB::table('ia02')->insert($data);
            Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
            return redirect()->action([Asesmen::class, 'upload_ia02']);
        } else {
            //jika duplikat maka redirect dan kasih pesan duplikat
            Alert::error('Duplikat', 'Dokumen Sudah Ada');
            return redirect()->action([Asesmen::class, 'upload_ia02']);
        }
    }

    //MENU UPLOAD SOAL ESAI
    public function getUpload_ia04(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('ia04')->where('email', $data['email'])->first();

        $data = DB::select("SELECT *  FROM ia04 ORDER BY tanggal_uji DESC");
        echo json_encode($data);
    }
    
    public function upload_ia04(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['users'] = DB::table('users')->where('email', $data['email'])->first();
        $data['ia04'] = DB::table('ia04')->get();
        $data['title_menu'] = "Asesmen";
        $data['title_sub_menu'] = "Pertanyaan Tertulis Esai (FR.IA.04)";
        $token = $data_session['token'];

        $data['nama_jadwal'] = DB::select("SELECT
                                            id, nama_jadwal, tanggal_uji, tuk
                                            FROM jadwal_asesmen");
                                            // jadwal_asesmen.id,
                                            // jadwal_asesmen.nama_jadwal,
                                            // jadwal_asesmen.tanggal_uji,
                                            // jadwal_asesmen.tuk,
                                            // jadwal_asesmen.skema,
                                            // jadwal_asesmen.nomor,
                                            // jadwal_asesor.id
                                            // FROM jadwal_asesmen
                                            // INNER JOIN jadwal_asesor
                                            // ON jadwal_asesmen.id = jadwal_asesor.id_jadwal");
        // $data['user'] = DB::table('view_ia04')->where('email', '=', $data['email'] )->get();
        return view('asesmen.upload_ia04', $data);
    }

    public function saveUpload_ia04(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();
        $nama_dokumen = $request->input('nama_dokumen');
        $namaFile = md5($nama_dokumen);
        $cek =  DB::select("SELECT * FROM upload_ia04 WHERE nama_dokumen = '$nama_dokumen'");
        if ($cek == []) {
            if ($request->hasFile('nama_dokumen')) {
                $filenameWithExt = $request->file('nama_dokumen')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('nama_dokumen')->getClientOriginalExtension(); // Get just Extension
                $fileName_nama_dokumen = 'soalPraktik02' . '_' . $token . '_' . $namaFile . '.' . $extension; // Filename To store
                $request->nama_dokumen->move(public_path('assets/document/upload_ia04/asesor'), $fileName_nama_dokumen);
            } else {
                $fileName_nama_dokumen = $user->nama_dokumen;
            }

            $nama_jadwal = $request->input('nama_jadwal');
            $id_jadwal_asesor = $request->input('id_jadwal_asesor');
            $tanggal_uji = $request->input('tanggal_uji');
            $tuk = $request->input('tuk');
            $skema = $request->input('skema');
            $nomor = $request->input('nomor');
            $nama_asesor = $request->input('nama_asesor');
            $no_reg = $request->input('no_reg');
            $nama_dokumen = $request->input('nama_dokumen');

            $data = array(
                'nama_jadwal' => $nama_jadwal,
                'id_jadwal_asesor' => $id_jadwal_asesor,
                'tanggal_uji' => $tanggal_uji,
                'tuk' => $tuk,
                'skema' => $skema,
                'nomor' => $nomor,
                'nama_asesor' => $nama_asesor,
                'no_reg' => $no_reg,
                'nama_dokumen' => $nama_dokumen
            );
            DB::table('ia04')->insert($data);
            Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
            return redirect()->action([Asesmen::class, 'upload_ia04']);
        } else {
            //jika duplikat maka redirect dan kasih pesan duplikat
            Alert::error('Duplikat', 'Dokumen Sudah Ada');
            return redirect()->action([Asesmen::class, 'upload_ia04']);
        }
    }

    //MENU UPLOAD SOAL PG
    public function getUpload_ia05(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('ia05')->where('email', $data['email'])->first();

        $data = DB::select("SELECT *  FROM ia05 ORDER BY tanggal_uji DESC");
        echo json_encode($data);
    }
    
    public function upload_ia05(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['users'] = DB::table('users')->where('email', $data['email'])->first();
        $data['ia05'] = DB::table('ia05')->get();
        $data['title_menu'] = "Asesmen";
        $data['title_sub_menu'] = "Pertanyaan Tertulis Pilihan Ganda (FR.IA.05)";
        $token = $data_session['token'];

        $data['nama_jadwal'] = DB::select("SELECT
                                            id, nama_jadwal, tanggal_uji, tuk
                                            FROM jadwal_asesmen");
                                            // jadwal_asesmen.id,
                                            // jadwal_asesmen.nama_jadwal,
                                            // jadwal_asesmen.tanggal_uji,
                                            // jadwal_asesmen.tuk,
                                            // jadwal_asesmen.skema,
                                            // jadwal_asesmen.nomor,
                                            // jadwal_asesor.id
                                            // FROM jadwal_asesmen
                                            // INNER JOIN jadwal_asesor
                                            // ON jadwal_asesmen.id = jadwal_asesor.id_jadwal");
        // $data['user'] = DB::table('view_ia05')->where('email', '=', $data['email'] )->get();
        return view('asesmen.upload_ia05', $data);
    }

    public function saveUpload_ia05(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();
        $nama_dokumen = $request->input('nama_dokumen');
        $namaFile = md5($nama_dokumen);
        $cek =  DB::select("SELECT * FROM upload_ia05 WHERE nama_dokumen = '$nama_dokumen'");
        if ($cek == []) {
            if ($request->hasFile('nama_dokumen')) {
                $filenameWithExt = $request->file('nama_dokumen')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('nama_dokumen')->getClientOriginalExtension(); // Get just Extension
                $fileName_nama_dokumen = 'soalPilihanGanda05' . '_' . $token . '_' . $namaFile . '.' . $extension; // Filename To store
                $request->nama_dokumen->move(public_path('assets/document/upload_ia05/asesor'), $fileName_nama_dokumen);
            } else {
                $fileName_nama_dokumen = $user->nama_dokumen;
            }

            $nama_jadwal = $request->input('nama_jadwal');
            $id_jadwal_asesor = $request->input('id_jadwal_asesor');
            $tanggal_uji = $request->input('tanggal_uji');
            $tuk = $request->input('tuk');
            $skema = $request->input('skema');
            $nomor = $request->input('nomor');
            $nama_asesor = $request->input('nama_asesor');
            $no_reg = $request->input('no_reg');
            $nama_dokumen = $request->input('nama_dokumen');

            $data = array(
                'nama_jadwal' => $nama_jadwal,
                'id_jadwal_asesor' => $id_jadwal_asesor,
                'tanggal_uji' => $tanggal_uji,
                'tuk' => $tuk,
                'skema' => $skema,
                'nomor' => $nomor,
                'nama_asesor' => $nama_asesor,
                'no_reg' => $no_reg,
                'nama_dokumen' => $nama_dokumen
            );
            DB::table('ia05')->insert($data);
            Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
            return redirect()->action([Asesmen::class, 'upload_ia05']);
        } else {
            //jika duplikat maka redirect dan kasih pesan duplikat
            Alert::error('Duplikat', 'Dokumen Sudah Ada');
            return redirect()->action([Asesmen::class, 'upload_ia05']);
        }
    }


    //MENU DOWNLOAD SOAL PRAKTIK
    public function download_ia02(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Asesmen";
        $data['title_sub_menu'] = "Tugas Praktik Demonstrasi (FR.IA.02)";
        $token = $data_session['token'];
        return view('asesmen.download_ia02', $data);
    }

    //MENU DOWNLOAD SOAL ESAI
    public function download_ia04(Request $request)
    {
        
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Asesmen";
        $data['title_sub_menu'] = "Pertanyaan Tertulis Esai (FR.IA.04)";
        $token = $data_session['token'];
        return view('asesmen.download_ia04', $data);
    }
    
    //MENU DOWNLOAD SOAL PG
    public function download_ia05(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Asesmen";
        $data['title_sub_menu'] = "Pertanyaan Tertulis Pilihan Ganda (FR.IA.05)";
        $token = $data_session['token'];
        return view('asesmen.download_ia05', $data);
    }
}