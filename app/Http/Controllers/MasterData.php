<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use App\Models\ListAsesor;
use App\Models\ListKelamin;
use Illuminate\Http\Request;
use App\Models\ListKebangsaan;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class MasterData extends Controller
{
    protected $list_asesor;
    protected $list_kelamin;
    protected $list_kebangsaan;
    protected $user;
    
    public function __construct()
    {
        $this->list_asesor = ListAsesor::all();
        $this->list_kelamin = ListKelamin::all();
        $this->list_kebangsaan = ListKebangsaan::all();
        $this->user = User::all();
    }
    public function struktur_jabatan(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Master Data LSP";
        $data['title_sub_menu'] = "Struktur Jabatan LSP";
        $data['list_jabatan'] = DB::select('SELECT * FROM list_jabatan');

        $token = $data_session['token'];
       
        return view('master_data.struktur_jabatan_lsp', $data);
    }

    public function getSrukturJabatanLsp(Request $request)
    {
        $data = DB::select('SELECT
                            `a`.`id` AS `id`,
                            `b`.`nama_jabatan` AS `nama_jabatan`,
                            `a`.`nama` AS `nama`,
                            `a`.`nip` AS `nip`,
                            `a`.`ttd` AS `ttd`
                            FROM `struktur_jabatan_lsp` `a`
                            JOIN `list_jabatan` `b`
                            ON `a`.`jabatan` = `b`.`id`
                            ');
        echo json_encode($data);
    }

    public function saveStrukturJabatanLsp(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        $jabatan = $request->input('jabatan');
        $nama = $request->input('nama');
        $nip = $request->input('nip');
        $waktu_sekarang     = Date('Y-m-d');
        //jika ada tanda_tangan
        if ($request->hasFile('ttd')) {
            $filenameWithExt = $request->file('ttd')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('ttd')->getClientOriginalExtension(); // Get just Extension
            $fileName_ttd = 'ttd' . '_' . $nip . '.' . $extension; // Filename To store
            $request->ttd->move(public_path('assets/document/tanda_tangan/struktur_jabatan'), $fileName_ttd);
        } else {
            $fileName_ttd = "";
        }

        $data = array(
            'jabatan' => $jabatan,
            'nama' => $nama,
            'nip' => $nip,
            'ttd' => $fileName_ttd,
            'created_at' => $waktu_sekarang,
            'updated_at' => ""
        );

        DB::table('struktur_jabatan_lsp')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([MasterData::class, 'struktur_jabatan']);
    }

    public function hapusStrukturJabatanLsp(Request $request)
    {
        $id = $request->input('id');
        $waktu_sekarang     = Date('Y-m-d');

        DB::delete('DELETE FROM struktur_jabatan_lsp WHERE id = ?', [$id]);
        echo json_encode($id);
    }

    public function getDataStrukturJabatanLsp(Request $request)
    {
        $id = $request->input('id');
        $data = DB::select("SELECT * FROM struktur_jabatan_lsp WHERE id = '$id'");
        echo json_encode($data);
    }

    public function updateStrukturJabatanLsp(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];

        $id = $request->input('id');
        $data = DB::table('struktur_jabatan_lsp')->where('id', $id)->first();

        $jabatan = $request->input('jabatan');
        $nama = $request->input('nama');
        $nip = $request->input('nip');
        $waktu_sekarang     = Date('Y-m-d');
        //jika ada tanda_tangan
        if ($request->hasFile('ttd')) {
            $filenameWithExt = $request->file('ttd')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('ttd')->getClientOriginalExtension(); // Get just Extension
            $fileName_ttd = 'ttd' . '_' . $nip . '.' . $extension; // Filename To store
            $request->ttd->move(public_path('assets/document/tanda_tangan/struktur_jabatan'), $fileName_ttd);
        } else {
            $fileName_ttd = $data->ttd;
        }

        $update =  DB::table('struktur_jabatan_lsp')
            ->where('id', $id)
            ->update([
                'jabatan' => $jabatan,
                'nama' => $nama,
                'nip' => $nip,
                'ttd' => $fileName_ttd,
                'updated_at' => $waktu_sekarang
            ]);

        echo json_encode($update);
    }

    //DAFTAR_ADMIN
    public function daftar_admin(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Master Data LSP";
        $data['title_sub_menu'] = "Daftar Admin";
        $data['list_jenis_lsp'] = DB::select('SELECT * FROM list_jenis_lsp WHERE is_aktif = 1');

        $token = $data_session['token'];
        $data['data_struktur_jabatan_lsp'] = DB::select("SELECT * FROM `struktur_jabatan_lsp`");
        return view('master_data.daftar_admin', $data);
    }

    public function getDaftarAdmin(Request $request)
    {
        $data = DB::select('SELECT 
                            `a`.*,
                            `b`.`nama_jenis_lsp`
                            FROM `users` `a`
                            JOIN `list_jenis_lsp` `b`
                            ON `a`.`jenis_lsp` = `b`.`id`
                            WHERE role_id = 2'); //2 adalah hak akses admin
        echo json_encode($data);
    }

    public function saveDaftarAdmin(Request $request)
    {
        //cek duplikat email
        $email = $request->input('email');
        $cek =  DB::select("SELECT * FROM users WHERE email = '$email'");
        // @dd($cek);
        if ($cek == []) {
            //jika nggk duplikat maka insert
            $token = md5($email);
            //jika ada dokument SK_LSP
            if ($request->hasFile('dokumen_sk_lsp')) {
                $filenameWithExt = $request->file('dokumen_sk_lsp')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('dokumen_sk_lsp')->getClientOriginalExtension(); // Get just Extension
                $fileName_dokumen_sk_lsp = 'sk_lsp' . '_' . $token . '.' . $extension; // Filename To store
                $request->dokumen_sk_lsp->move(public_path('assets/document/sk_lsp'), $fileName_dokumen_sk_lsp);
            } else {
                $fileName_dokumen_sk_lsp = "";
            }
            //jika ada dokument LISENSI_LSP
            if ($request->hasFile('dokumen_lisensi_lsp')) {
                $filenameWithExt = $request->file('dokumen_lisensi_lsp')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('dokumen_lisensi_lsp')->getClientOriginalExtension(); // Get just Extension
                $fileName_dokumen_lisensi_lsp = 'lisensi_lsp' . '_' . $token . '.' . $extension; // Filename To store
                $request->dokumen_lisensi_lsp->move(public_path('assets/document/lisensi_lsp'), $fileName_dokumen_lisensi_lsp);
            } else {
                $fileName_dokumen_lisensi_lsp = "";
            }
            //jika ada dokument NPWP
            if ($request->hasFile('dokumen_npwp')) {
                $filenameWithExt = $request->file('dokumen_npwp')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('dokumen_npwp')->getClientOriginalExtension(); // Get just Extension
                $fileName_dokumen_npwp = 'npwp' . '_' . $token . '.' . $extension; // Filename To store
                $request->dokumen_npwp->move(public_path('assets/document/npwp'), $fileName_dokumen_npwp);
            } else {
                $fileName_dokumen_npwp = "";
            }
            //jika ada dokument dokumen_rek_bank
            if ($request->hasFile('dokumen_rek_bank')) {
                $filenameWithExt = $request->file('dokumen_rek_bank')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('dokumen_rek_bank')->getClientOriginalExtension(); // Get just Extension
                $fileName_dokumen_rek_bank = 'rek_bank' . '_' . $token . '.' . $extension; // Filename To store
                $request->dokumen_rek_bank->move(public_path('assets/document/rek_bank'), $fileName_dokumen_rek_bank);
            } else {
                $fileName_dokumen_rek_bank = "";
            }

            $jenis_lsp = $request->input('jenis_lsp');
            $no_hp = $request->input('no_hp');
            $nama = $request->input('nama');
            $no_hp_kantor = $request->input('no_hp_kantor');
            $no_fax = $request->input('no_fax');
            $email = $request->input('email');
            $password = $request->input('password');
            $website = $request->input('website');
            $kode_pos = $request->input('kode_pos');
            $alamat_kantor = $request->input('alamat_kantor');
            $no_sk_lsp = $request->input('no_sk_lsp');
            $no_lisensi_lsp = $request->input('no_lisensi_lsp');
            $npwp = $request->input('npwp');
            $rekening_bank = $request->input('rekening_bank');
            $waktu_sekarang     = Date('Y-m-d');

            $data = array(
                'role_id' => 2,
                'name' => $nama,
                'jenis_lsp' => $jenis_lsp,
                'no_hp' => $no_hp,
                'token' => $token,
                'no_hp_kantor' => $no_hp_kantor,
                'fax_kantor' => $no_fax,
                'alamat_kantor' => $alamat_kantor,
                'email' => $email,
                'password' => $password,
                'website' => $website,
                'kodepos' => $kode_pos,
                'no_sk_lsp' => $no_sk_lsp,
                'dokumen_sk_lsp' => $fileName_dokumen_sk_lsp,
                'no_lisensi_lsp' => $no_lisensi_lsp,
                'dokumen_lisensi_lsp' => $fileName_dokumen_lisensi_lsp,
                'npwp' => $npwp,
                'dokumen_npwp' => $fileName_dokumen_npwp,
                'rekening_bank' => $rekening_bank,
                'dokumen_rek_bank' => $fileName_dokumen_rek_bank,
                'created_at' => $waktu_sekarang,
            );
            
            DB::table('users')->insert($data);

            Alert::success('Berhasil', 'Data Berhasil di Tambahkan');
            return redirect()->action([MasterData::class, 'daftar_admin']);
        } else {
            //jika duplikat maka redirect dan kasih pesan duplikat
            Alert::error('Duplikat', 'Email Sudah terdaftar');
            return redirect()->action([MasterData::class, 'daftar_admin']);
        }
    }

    public function hapusDaftarAdmin(Request $request)
    {
        $id = $request->input('id');
        $waktu_sekarang     = Date('Y-m-d');

        DB::delete('DELETE FROM users WHERE id = ?', [$id]);
        echo json_encode($id);
    }

    public function getDataDaftarAdmin(Request $request)
    {
        $id = $request->input('id');
        $data = DB::select("SELECT * FROM users WHERE id = '$id'");
        echo json_encode($data);
    }

    public function updateDataDaftarAdmin(Request $request)
    {

        $id = $request->input('id');
        $data = DB::table('users')->where('id', $id)->first();

        $data_session = $request->session()->get('dataUser');
        $token = $data->token;

        //jika ada dokument SK_LSP
        if ($request->hasFile('dokumen_sk_lsp')) {
            $filenameWithExt = $request->file('dokumen_sk_lsp')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_sk_lsp')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_sk_lsp = 'sk_lsp' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_sk_lsp->move(public_path('assets/document/sk_lsp'), $fileName_dokumen_sk_lsp);
        } else {
            $fileName_dokumen_sk_lsp = $data->dokumen_sk_lsp;
        }
        //jika ada dokument LISENSI_LSP
        if ($request->hasFile('dokumen_lisensi_lsp')) {
            $filenameWithExt = $request->file('dokumen_lisensi_lsp')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_lisensi_lsp')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_lisensi_lsp = 'lisensi_lsp' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_lisensi_lsp->move(public_path('assets/document/lisensi_lsp'), $fileName_dokumen_lisensi_lsp);
        } else {
            $fileName_dokumen_lisensi_lsp = $data->dokumen_lisensi_lsp;
        }
        //jika ada dokument NPWP
        if ($request->hasFile('dokumen_npwp')) {
            $filenameWithExt = $request->file('dokumen_npwp')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_npwp')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_npwp = 'npwp' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_npwp->move(public_path('assets/document/npwp'), $fileName_dokumen_npwp);
        } else {
            $fileName_dokumen_npwp = $data->dokumen_npwp;
        }
        //jika ada dokument dokumen_rek_bank
        if ($request->hasFile('dokumen_rek_bank')) {
            $filenameWithExt = $request->file('dokumen_rek_bank')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('dokumen_rek_bank')->getClientOriginalExtension(); // Get just Extension
            $fileName_dokumen_rek_bank = 'rek_bank' . '_' . $token . '.' . $extension; // Filename To store
            $request->dokumen_rek_bank->move(public_path('assets/document/rek_bank'), $fileName_dokumen_rek_bank);
        } else {
            $fileName_dokumen_rek_bank = $data->dokumen_rek_bank;
        }

        $nama = $request->input('nama');
        $jenis_lsp = $request->input('jenis_lsp');
        $no_hp = $request->input('no_hp');
        $no_hp_kantor = $request->input('no_hp_kantor');
        $no_fax = $request->input('no_fax');
        $email = $request->input('email');
        $password = $request->input('password');
        $website = $request->input('website');
        $kode_pos = $request->input('kode_pos');
        $alamat_kantor = $request->input('alamat_kantor');
        $no_sk_lsp = $request->input('no_sk_lsp');
        $no_lisensi_lsp = $request->input('no_lisensi_lsp');
        $npwp = $request->input('npwp');
        $rekening_bank = $request->input('rekening_bank');
        $waktu_sekarang     = Date('Y-m-d');

        $update =  DB::table('users')
            ->where('token', $token)
            ->update([
                'name' => $nama,
                'jenis_lsp' => $jenis_lsp,
                'no_hp' => $no_hp,
                'no_hp_kantor' => $no_hp_kantor,
                'fax_kantor' => $no_fax,
                'email' => $email,
                'alamat_kantor' => $alamat_kantor,
                'password' => $password,
                'website' => $website,
                'kodepos' => $kode_pos,
                'no_sk_lsp' => $no_sk_lsp,
                'dokumen_sk_lsp' => $fileName_dokumen_sk_lsp,
                'no_lisensi_lsp' => $no_lisensi_lsp,
                'dokumen_lisensi_lsp' => $fileName_dokumen_lisensi_lsp,
                'npwp' => $npwp,
                'dokumen_npwp' => $fileName_dokumen_npwp,
                'rekening_bank' => $rekening_bank,
                'dokumen_rek_bank' => $fileName_dokumen_rek_bank,
                'updated_at' => $waktu_sekarang,
            ]);
        echo json_encode($update);
    }

    //daftar_penyelia
    public function daftar_penyelia(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Master Data LSP";
        $data['title_sub_menu'] = "Daftar Penyelia";
        $data['list_akses'] = DB::select('SELECT * FROM master_users_role WHERE id = 5');
        $data['provinsi'] = DB::select('SELECT * FROM wilayah_2020 WHERE CHAR_LENGTH(kode)=2 ORDER BY nama');

        $token = $data_session['token'];
        return view('master_data.daftar_penyelia', $data);
    }

    public function getDaftarPenyelia(Request $request)
    {
        $data = DB::select('SELECT
                            `a`.`id` as `id`,
                            `a`.`email` as `email`,
                            `a`.`password` as `password`,
                            `a`.`name` as `name`,
                            `b`.`nama_role` as `nama_role`,
                            `a`.`alamat` as `alamat`,
                            `a`.`kodepos` as `kodepos`,
                            `c`.`nama` as `nama_provinsi`
                            FROM `users` `a`
                            JOIN `master_users_role` `b`
                            ON `a`.`role_id` = `b`.`id`
                            JOIN `wilayah_2020` `c`
                            ON `c`.`kode` = `a`.`provinsi`
                            WHERE `a`.`role_id` = 5'); //5 adalah hak akses penyelia
        echo json_encode($data);
    }

    public function saveDaftarPenyelia(Request $request)
    {
        //cek duplikat email
        $email = $request->input('email');
        $cek =  DB::select("SELECT * FROM users WHERE email = '$email'");
        // @dd($cek);
        if ($cek == []) {
            //jika nggk duplikat maka insert
            $token = md5($email);
            $email = $request->input('email');
            $password = $request->input('password');
            $nama = $request->input('nama');
            $akses = $request->input('akses');
            $alamat = $request->input('alamat');
            $kode_pos = $request->input('kode_pos');
            $provinsi = $request->input('provinsi');
            $kab_kota = $request->input('kab_kota');
            $waktu_sekarang     = Date('Y-m-d');

            $data = array(
                'token' => $token,
                'email' => $email,
                'password' => $password,
                'name' => $nama,
                'role_id' => $akses,
                'alamat' => $alamat,
                'kodepos' => $kode_pos,
                'provinsi' => $provinsi,
                'kabupaten_kota' => $kab_kota,
                'created_at' => $waktu_sekarang,
            );

            DB::table('users')->insert($data);

            Alert::success('Berhasil', 'Data Berhasil di Tambahkan');
            return redirect()->action([MasterData::class, 'daftar_penyelia']);
        } else {
            //jika duplikat maka redirect dan kasih pesan duplikat
            Alert::error('Duplikat', 'Email Sudah terdaftar');
            return redirect()->action([MasterData::class, 'daftar_penyelia']);
        }
    }

    public function hapusDaftarPenyelia(Request $request)
    {
        $id = $request->input('id');
        DB::delete('DELETE FROM users WHERE id = ?', [$id]);
        echo json_encode($id);
    }

    public function getDataDaftarPenyelia(Request $request)
    {
        $id = $request->input('id');
        $dataPenyelia = DB::table('users')->where('id', $id)->first();
        $kab_kota_id = $dataPenyelia->kabupaten_kota;
        $dataKabKota = DB::table('wilayah_2020')->where('kode', $kab_kota_id)->first();
        $data = [
            'dataPenyelia' => $dataPenyelia,
            'dataKabKota' => $dataKabKota
        ];
        echo json_encode($data);
    }

    public function updateDataDaftarPenyelia(Request $request)
    {
        //jika nggk duplikat maka insert
        $id = $request->input('id');
        $email = $request->input('email');
        $password = $request->input('password');
        $nama = $request->input('nama');
        $akses = $request->input('akses');
        $alamat = $request->input('alamat');
        $kode_pos = $request->input('kode_pos');
        $provinsi = $request->input('provinsi');
        $kab_kota = $request->input('kab_kota');
        $waktu_sekarang     = Date('Y-m-d');

        $update =  DB::table('users')
            ->where('id', $id)
            ->update([
                'email' => $email,
                'password' => $password,
                'name' => $nama,
                'role_id' => $akses,
                'alamat' => $alamat,
                'kodepos' => $kode_pos,
                'provinsi' => $provinsi,
                'kabupaten_kota' => $kab_kota,
                'updated_at' => $waktu_sekarang
            ]);
        echo json_encode($update);
    }

    //daftar_tuk
    public function daftar_tuk(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Master Data LSP";
        $data['title_sub_menu'] = "Daftar TUK";
        $data['list_kejuruan'] = DB::select('SELECT * FROM list_kejuruan ORDER BY nama_kejuruan ASC');

        $token = $data_session['token'];
        return view('master_data.daftar_tuk', $data);
    }

    public function getDaftarTuk(Request $request)
    {
        $data = DB::select('SELECT
                            *
                            FROM `list_tuk` `a`
                            JOIN `list_kejuruan` `b`
                            ON `a`.`kejuruan` = `b`.`id_kejuruan`');
        echo json_encode($data);
    }

    public function saveDaftarTuk(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $nama = $request->input('nama');
        $namaFile = md5($nama);
        $cek =  DB::select("SELECT * FROM list_tuk WHERE nama = '$nama'");
        if ($cek == []) {
            if ($request->hasFile('persyaratan_teknis')) {
                $filenameWithExt = $request->file('persyaratan_teknis')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('persyaratan_teknis')->getClientOriginalExtension(); // Get just Extension
                $fileName_persyaratan_teknis = 'persyaratan_teknis' . '_' . $token . '_' . $namaFile . '.' . $extension; // Filename To store
                $request->persyaratan_teknis->move(public_path('assets/document/persyaratan_teknis'), $fileName_persyaratan_teknis);
            } else {
                $fileName_persyaratan_teknis = "";
            }
            // masukan ruang teori
            if ($request->hasFile('ruang_teori')) {
                $filenameWithExt = $request->file('ruang_teori')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('ruang_teori')->getClientOriginalExtension(); // Get just Extension
                $fileName_ruang_teori = 'ruang_teori' . '_' . $token . '_' . $namaFile . '.' . $extension; // Filename To store
                $request->ruang_teori->move(public_path('assets/document/ruang_teori'), $fileName_ruang_teori);
            } else {
                $fileName_ruang_teori = "";
            }
            // masukan ruang praktek
            if ($request->hasFile('ruang_praktek')) {
                $filenameWithExt = $request->file('ruang_praktek')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('ruang_praktek')->getClientOriginalExtension(); // Get just Extension
                $fileName_ruang_praktek = 'ruang_praktek' . '_' . $token . '_' . $namaFile . '.' . $extension; // Filename To store
                $request->ruang_praktek->move(public_path('assets/document/ruang_praktek'), $fileName_ruang_praktek);
            } else {
                $fileName_ruang_praktek = "";
            }
            // masukan perlengkapan
            if ($request->hasFile('perlengkapan_i')) {
                $filenameWithExt = $request->file('perlengkapan_i')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('perlengkapan_i')->getClientOriginalExtension(); // Get just Extension
                $fileName_perlengkapan_i = 'perlengkapan_i' . '_' . $token . '_' . $namaFile . '.' . $extension; // Filename To store
                $request->perlengkapan_i->move(public_path('assets/document/perlengkapan'), $fileName_perlengkapan_i);
            } else {
                $fileName_perlengkapan_i = "";
            }
    
            if ($request->hasFile('perlengkapan_ii')) {
                $filenameWithExt = $request->file('perlengkapan_ii')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('perlengkapan_ii')->getClientOriginalExtension(); // Get just Extension
                $fileName_perlengkapan_ii = 'perlengkapan_ii' . '_' . $token . '_' . $namaFile . '.' . $extension; // Filename To store
                $request->perlengkapan_ii->move(public_path('assets/document/perlengkapan'), $fileName_perlengkapan_ii);
            } else {
                $fileName_perlengkapan_ii = "";
            }
    
            if ($request->hasFile('perlengkapan_iii')) {
                $filenameWithExt = $request->file('perlengkapan_iii')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('perlengkapan_iii')->getClientOriginalExtension(); // Get just Extension
                $fileName_perlengkapan_iii = 'perlengkapan_iii' . '_' . $token . '_' . $namaFile . '.' . $extension; // Filename To store
                $request->perlengkapan_iii->move(public_path('assets/document/perlengkapan'), $fileName_perlengkapan_iii);
            } else {
                $fileName_perlengkapan_iii = "";
            }
    
            // insert data TUK
                $nama = $request->input('nama');
                $kejuruan = $request->input('kejuruan');
                $instansi_satker = $request->input('instansi_satker');
                $alamat = $request->input('alamat');
                $kota = $request->input('kota');
                $email = $request->input('email');
                $nama_ketua = $request->input('nama_ketua');
                $nomor_ketua = $request->input('nomor_ketua');
                $nama_pengelola = $request->input('nama_pengelola');
                $nomor_pengelola = $request->input('nomor_pengelola');
    
                $data = array(
                    'nama' => $nama,
                    'kejuruan' => $kejuruan,
                    'instansi_satker' => $instansi_satker,
                    'alamat' => $alamat,
                    'kota' => $kota,
                    'email' => $email,
                    'nama_ketua' => $nama_ketua,
                    'nomor_ketua' => $nomor_ketua,
                    'nama_pengelola' => $nama_pengelola,
                    'nomor_pengelola' => $nomor_pengelola,
                    'persyaratan_teknis' => $fileName_persyaratan_teknis,
                    'ruang_teori' => $fileName_ruang_teori,
                    'ruang_praktek' => $fileName_ruang_praktek,
                    'perlengkapan_i' => $fileName_perlengkapan_i,
                    'perlengkapan_ii' => $fileName_perlengkapan_ii,
                    'perlengkapan_iii' => $fileName_perlengkapan_iii
                );
    
                DB::table('list_tuk')->insert($data);
    
                Alert::success('Berhasil', 'Data Berhasil di Tambahkan');
                return redirect()->action([MasterData::class, 'daftar_tuk']);
        } else {
            //jika duplikat maka redirect dan kasih pesan duplikat
            Alert::error('Duplikat', 'TUK Sudah terdaftar');
            return redirect()->action([MasterData::class, 'daftar_tuk']);
        }
    }

    public function hapusDaftarTuk(Request $request)
    {
        $id = $request->input('id');
        DB::delete('DELETE FROM list_tuk WHERE id = ?', [$id]);
        echo json_encode($id);
    }

    public function getDataDaftarTuk(Request $request)
    {
        $id = $request->input('id');
        $dataTuk = DB::table('list_tuk')->where('id', $id)->first();
        $kejuruan = $dataTuk->kejuruan;
        $dataKejuruan = DB::table('list_kejuruan')->where('id_kejuruan', $kejuruan)->first();
        $data = [
            'dataTuk' => $dataTuk,
            'dataKejuruan' => $dataKejuruan
        ];
        echo json_encode($data);
    }

    public function updateDataDaftarTuk(Request $request)
    {
        //jika nggk duplikat maka insert
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];

        $id = $request->input('id');
        $data = DB::table('list_tuk')->where('id', $id)->first();

        $nama = $request->input('nama');
        $kejuruan = $request->input('kejuruan');
        $instansi_satker = $request->input('instansi_satker');
        $alamat = $request->input('alamat');
        $kota = $request->input('kota');
        $email = $request->input('email');
        $nama_ketua = $request->input('nama_ketua');
        $nomor_ketua = $request->input('nomor_ketua');
        $nama_pengelola = $request->input('nama_pengelola');
        $nomor_pengelola = $request->input('nomor_pengelola');

        // masukan persyaratan teknis
        if ($request->hasFile('persyaratan_teknis')) {
            $filenameWithExt = $request->file('persyaratan_teknis')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('persyaratan_teknis')->getClientOriginalExtension(); // Get just Extension
            $fileName_persyaratan_teknis = 'persyaratan_teknis' . '_' . $token . '_' . $nama . '.' . $extension; // Filename To store
            $request->persyaratan_teknis->move(public_path('assets/document/persyaratan_teknis'), $fileName_persyaratan_teknis);
        } else {
            $fileName_persyaratan_teknis = $data->persyaratan_teknis;
        }
        // masukan ruang teori
        if ($request->hasFile('ruang_teori')) {
            $filenameWithExt = $request->file('ruang_teori')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('ruang_teori')->getClientOriginalExtension(); // Get just Extension
            $fileName_ruang_teori = 'ruang_teori' . '_' . $token . '_' . $nama . '.' . $extension; // Filename To store
            $request->ruang_teori->move(public_path('assets/document/ruang_teori'), $fileName_ruang_teori);
        } else {
            $fileName_ruang_teori = $data->ruang_teori;
        }
        // masukan ruang praktek
        if ($request->hasFile('ruang_praktek')) {
            $filenameWithExt = $request->file('ruang_praktek')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('ruang_praktek')->getClientOriginalExtension(); // Get just Extension
            $fileName_ruang_praktek = 'ruang_praktek' . '_' . $token . '_' . $nama . '.' . $extension; // Filename To store
            $request->ruang_praktek->move(public_path('assets/document/ruang_praktek'), $fileName_ruang_praktek);
        } else {
            $fileName_ruang_praktek = $data->ruang_praktek;
        }
        // masukan perlengkapan
        if ($request->hasFile('perlengkapan_i')) {
            $filenameWithExt = $request->file('perlengkapan_i')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('perlengkapan_i')->getClientOriginalExtension(); // Get just Extension
            $fileName_perlengkapan_i = 'perlengkapan_i' . '_' . $token . '_' . $nama . '.' . $extension; // Filename To store
            $request->perlengkapan_i->move(public_path('assets/document/perlengkapan'), $fileName_perlengkapan_i);
        } else {
            $fileName_perlengkapan_i = $data->perlengkapan_i;
        }

        if ($request->hasFile('perlengkapan_ii')) {
            $filenameWithExt = $request->file('perlengkapan_ii')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('perlengkapan_ii')->getClientOriginalExtension(); // Get just Extension
            $fileName_perlengkapan_ii = 'perlengkapan_ii' . '_' . $token . '_' . $nama . '.' . $extension; // Filename To store
            $request->perlengkapan_ii->move(public_path('assets/document/perlengkapan'), $fileName_perlengkapan_ii);
        } else {
            $fileName_perlengkapan_ii = $data->perlengkapan_ii;
        }

        if ($request->hasFile('perlengkapan_iii')) {
            $filenameWithExt = $request->file('perlengkapan_iii')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('perlengkapan_iii')->getClientOriginalExtension(); // Get just Extension
            $fileName_perlengkapan_iii = 'perlengkapan_iii' . '_' . $token . '_' . $nama . '.' . $extension; // Filename To store
            $request->perlengkapan_iii->move(public_path('assets/document/perlengkapan'), $fileName_perlengkapan_iii);
        } else {
            $fileName_perlengkapan_iii = $data->perlengkapan_iii;
        }

        $update =  DB::table('list_tuk')
            ->where('id', $id)
            ->update([
                'nama' => $nama,
                'kejuruan' => $kejuruan,
                'instansi_satker' => $instansi_satker,
                'alamat' => $alamat,
                'kota' => $kota,
                'email' => $email,
                'nama_ketua' => $nama_ketua,
                'nomor_ketua' => $nomor_ketua,
                'nama_pengelola' => $nama_pengelola,
                'nomor_pengelola' => $nomor_pengelola,
                'persyaratan_teknis' => $fileName_persyaratan_teknis,
                'ruang_teori' => $fileName_ruang_teori,
                'ruang_praktek' => $fileName_ruang_praktek,
                'perlengkapan_i' => $fileName_perlengkapan_i,
                'perlengkapan_ii' => $fileName_perlengkapan_ii,
                'perlengkapan_iii' => $fileName_perlengkapan_iii
            ]);

        echo json_encode($update);
    }

    //daftar asesor
    public function daftar_asesor(Request $request)
    {   
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Master Data LSP";
        $data['title_sub_menu'] = "Daftar Admin";
        $data['list_asesor'] = DB::table('users')->where('id',$request->id)->first();
        $data['list_kelamin'] = $this->list_kelamin;
        $data['list_kebangsaan'] = $this->list_kebangsaan;

        $token = $data_session['token'];
        // $data['data_struktur_jabatan_lsp'] = DB::select("SELECT * FROM `struktur_jabatan_lsp`");
        return view('master_data.daftar_asesor',$data);
    }

    public function getDaftarAsesor(Request $request)
    {
        $data = DB::select('SELECT
                            `users`.*,
                            `a`.nama as nama_provinsi,
                            `b`.nama as nama_kabupatenkota,
                            `c`.nama as nama_kelamin,
                            `d`.nama as nama_kebangsaan,
                            `e`.nama as nama_pekerjaan,
                            `f`.nama as nama_pendidikan,
                            `g`.nama_role as nama_role,
                            `list_asesor`.*,
                            `list_kelamin`.nama as jenis_kelamin
                            FROM `users` 
                            LEFT JOIN `wilayah_2020` `a`
                            ON `users`.`provinsi` = `a`.`kode`
                            LEFT JOIN `wilayah_2020` `b`
                            ON `users`.`kabupaten_kota` = `b`.`kode`
                            LEFT JOIN `list_kelamin` `c`
                            ON `users`.`jenis_kelamin` = `c`.`id`
                            LEFT JOIN `list_kebangsaan` `d`
                            ON `users`.`kebangsaan` = `d`.`id`
                            LEFT JOIN `list_pekerjaan` `e`
                            ON `users`.`pekerjaan` = `e`.`id`
                            LEFT JOIN `list_pendidikan` `f`
                            ON `users`.`kualifikasi_pendidikan` = `f`.`id`
                            LEFT JOIN `master_users_role` `g`
                            ON `users`.`role_id` = `g`.`id`
                            LEFT JOIN `list_asesor`
                            ON `users`.`id` = `list_asesor`.`id_akun`
                            LEFT JOIN `list_kelamin`
                            ON `users`.`jenis_kelamin` = `list_kelamin`.`id`
                            WHERE `role_id` = 3'); //3 adalah hak akses asesor
        // dd($data);
        echo json_encode($data);
    }

    public function hapusDaftarAsesor(Request $request)
    {
        $id = $request->input('id');
        DB::delete('DELETE FROM list_asesor WHERE id_akun = ?', [$id]);
        echo json_encode($id);
    }

    public function saveDaftarAsesor(Request $request)
    {
        //cek duplikat email
        $email = $request->input('email');
        $cek =  DB::select("SELECT * FROM users WHERE email = '$email'");
        // @dd($cek);
        if ($cek == []) {
            //jika nggk duplikat maka insert

            $id = $request->input('id');
            $id_asesor = $request->input('id_asesor');
            $role_id = 3;
            $nama = $request->input('nama');
            $met_asesor = $request->input('met_asesor');
            $nik = $request->input('nik');
            $tempat_lahir = $request->input('tempat_lahir');
            $tanggal_lahir = $request->input('tanggal_lahir');
            $jenis_kelamin = $request->input('jenis_kelamin');
            $kebangsaan = $request->input('kebangsaan');
            $email = $request->input('email');
            $password = $request->input('password');
            $alamat = $request->input('alamat');
            $kode_pos = $request->input('kode_pos');
            $nomor_sertif = $request->input('nomor_sertif');
            $tanggal_sertif = $request->input('tanggal_sertif');
            $tanggal_sertif_exp = $request->input('tanggal_sertif_exp');
            $nomor_blanko = $request->input('nomor_blanko');
            $token = md5($email);
            $waktu_sekarang = now();
            
            if ($request->hasFile('foto_sertif_asesor')) {
                $filenameWithExt = $request->file('foto_sertif_asesor')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('foto_sertif_asesor')->getClientOriginalExtension(); // Get just Extension
                $fileName_foto_sertif_asesor = 'foto_sertif_asesor' . '_' . $token . '.' . $extension; // Filename To store
                $request->foto_sertif_asesor->move(public_path('assets/document/foto_sertif_asesor'), $fileName_foto_sertif_asesor);
            } else {
                $fileName_foto_sertif_asesor = "";
            }
            // masukan perlengkapan
            if ($request->hasFile('foto_sertif_teknik')) {
                $filenameWithExt = $request->file('foto_sertif_teknik')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('foto_sertif_teknik')->getClientOriginalExtension(); // Get just Extension
                $fileName_foto_sertif_teknik = 'foto_sertif_teknik' . '_' . $token . '.' . $extension; // Filename To store
                $request->foto_sertif_teknik->move(public_path('assets/document/foto_sertif_teknik'), $fileName_foto_sertif_teknik);
            } else {
                $fileName_foto_sertif_teknik = "";
            }
            
            // $provinsi = $request->input('provinsi');
            // $kab_kota = $request->input('kab_kota');
            

            $data = array(
                'id' => $id,
                'id_asesor' => $id_asesor,
                'role_id' => $role_id,
                'name' => $nama,
                'met_asesor' => $met_asesor,
                'nik' => $nik,
                'tempat_lahir' => $tempat_lahir,
                'tanggal_lahir' => $tanggal_lahir,
                'jenis_kelamin' => $jenis_kelamin,
                'kebangsaan' => $kebangsaan,
                'email' => $email,
                'password' => $password,
                'alamat' => $alamat,
                'kodepos' => $kode_pos,
                'nomor_sertif' => $nomor_sertif,
                'tanggal_sertif' => $tanggal_sertif,
                'tanggal_sertif_exp' => $tanggal_sertif_exp,
                'foto_sertif_asesor' => $fileName_foto_sertif_asesor,
                'foto_sertif_teknik' => $fileName_foto_sertif_teknik,
                'nomor_blanko' => $nomor_blanko,
                'token' => $token,
                'created_at' => $waktu_sekarang
            );

                DB::select('CALL tambah_asesor(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [
                    $data['id'],
                    $data['id_asesor'],
                    $data['role_id'],
                    $data['name'],
                    $data['met_asesor'],
                    $data['nik'],
                    $data['tempat_lahir'],
                    $data['tanggal_lahir'],
                    $data['jenis_kelamin'],
                    $data['kebangsaan'],
                    $data['email'],
                    $data['password'],
                    $data['alamat'],
                    $data['kodepos'],
                    $data['nomor_sertif'],
                    $data['tanggal_sertif'],
                    $data['tanggal_sertif_exp'],
                    $data['foto_sertif_asesor'],
                    $data['foto_sertif_teknik'],
                    $data['nomor_blanko'],
                    $data['token'],
                    $data['created_at']
                ]);
                Alert::success('Berhasil', 'Data Berhasil di Tambahkan');
                return redirect()->action([MasterData::class, 'daftar_asesor']);
        } else {
            //jika duplikat maka redirect dan kasih pesan duplikat
            Alert::error('Duplikat', 'Email Sudah terdaftar');
            return redirect()->action([MasterData::class, 'daftar_asesor']);
        }
    }

    public function getDataDaftarAsesor(Request $request)
    {
        $id = $request->input('id');
        $dataAsesor = DB::table('list_asesor')->where('id_akun', $id)->first();
        $dataUsers = DB::table('users')->where('id', $id)->first();
        $data = [
            'dataAsesor' => $dataAsesor,
            'dataUsers' => $dataUsers
        ];
        echo json_encode($data);
    }

    public function updateDataDaftarAsesor(Request $request)
    {
            $id = $request->input('id');
            $dataAsesor = DB::table('list_asesor')->where('id_akun', $id)->first();

            $role_id = $request->input('role_id');
            $nama = $request->input('nama');
            $met_asesor = $request->input('met_asesor');
            $nik = $request->input('nik');
            $tempat_lahir = $request->input('tempat_lahir');
            $tanggal_lahir = $request->input('tanggal_lahir');
            $jenis_kelamin = $request->input('jenis_kelamin');
            $kebangsaan = $request->input('kebangsaan');
            $email = $request->input('email');
            $password = $request->input('password');
            $alamat = $request->input('alamat');
            $kode_pos = $request->input('kode_pos');
            $nomor_sertif = $request->input('nomor_sertif');
            $tanggal_sertif = $request->input('tanggal_sertif');
            $tanggal_sertif_exp = $request->input('tanggal_sertif_exp');
            $nomor_blanko = $request->input('nomor_blanko');
            $token = md5($email);
            
            if ($request->hasFile('foto_sertif_asesor')) {
                $filenameWithExt = $request->file('foto_sertif_asesor')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('foto_sertif_asesor')->getClientOriginalExtension(); // Get just Extension
                $fileName_foto_sertif_asesor = 'foto_sertif_asesor' . '_' . $token . '.' . $extension; // Filename To store
                $request->foto_sertif_asesor->move(public_path('assets/document/foto_sertif_asesor'), $fileName_foto_sertif_asesor);
            } else {
                $fileName_foto_sertif_asesor = $dataAsesor->foto_sertif_asesor;
            }
            // masukan perlengkapan
            if ($request->hasFile('foto_sertif_teknik')) {
                $filenameWithExt = $request->file('foto_sertif_teknik')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
                $extension = $request->file('foto_sertif_teknik')->getClientOriginalExtension(); // Get just Extension
                $fileName_foto_sertif_teknik = 'foto_sertif_teknik' . '_' . $token . '.' . $extension; // Filename To store
                $request->foto_sertif_teknik->move(public_path('assets/document/foto_sertif_teknik'), $fileName_foto_sertif_teknik);
            } else {
                $fileName_foto_sertif_teknik = $dataAsesor->foto_sertif_teknik;
            }
            
            // $provinsi = $request->input('provinsi');
            // $kab_kota = $request->input('kab_kota');

            $data = [
                'id' => $id,
                'role_id' => $role_id,
                'name' => $nama,
                'nik' => $nik,
                'tempat_lahir' => $tempat_lahir,
                'tanggal_lahir' => $tanggal_lahir,
                'jenis_kelamin' => $jenis_kelamin,
                'kebangsaan' => $kebangsaan,
                'email' => $email,
                'password' => $password,
                'alamat' => $alamat,
                'kodepos' => $kode_pos,
                'met_asesor' => $met_asesor,
                'nomor_sertif' => $nomor_sertif,
                'tanggal_sertif' => $tanggal_sertif,
                'tanggal_sertif_exp' => $tanggal_sertif_exp,
                'foto_sertif_asesor' => $fileName_foto_sertif_asesor,
                'foto_sertif_teknik' => $fileName_foto_sertif_teknik,
                'nomor_blanko' => $nomor_blanko,
                'token' => $token
            ];

            DB::select('CALL update_asesor(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [
                $data['id'],
                $data['role_id'],
                $data['name'],
                $data['met_asesor'],
                $data['nik'],
                $data['tempat_lahir'],
                $data['tanggal_lahir'],
                $data['jenis_kelamin'],
                $data['kebangsaan'],
                $data['email'],
                $data['password'],
                $data['alamat'],
                $data['kodepos'],
                $data['nomor_sertif'],
                $data['tanggal_sertif'],
                $data['tanggal_sertif_exp'],
                $data['foto_sertif_asesor'],
                $data['foto_sertif_teknik'],
                $data['nomor_blanko'],
                $data['token']
            ]);
            echo json_encode($data);
            // $updateUsers = [
            //     'id' => $id,
            //     'role_id' => $role_id,
            //     'name' => $nama,
            //     'nik' => $nik,
            //     'tempat_lahir' => $tempat_lahir,
            //     'tanggal_lahir' => $tanggal_lahir,
            //     'jenis_kelamin' => $jenis_kelamin,
            //     'kebangsaan' => $kebangsaan,
            //     'email' => $email,
            //     'password' => $password,
            //     'alamat' => $alamat,
            //     'kodepos' => $kode_pos,
            //     'token' => $token
            // ];

            // $updateAsesor = [
            //     'id_akun' => $id,
            //     'met_asesor' => $met_asesor,
            //     'nomor_sertif' => $nomor_sertif,
            //     'tanggal_sertif' => $tanggal_sertif,
            //     'tanggal_sertif_exp' => $tanggal_sertif_exp,
            //     'foto_sertif_asesor' => $fileName_foto_sertif_asesor,
            //     'foto_sertif_teknik' => $fileName_foto_sertif_teknik,
            //     'nomor_blanko' => $nomor_blanko,
            //     'token' => $token
            // ];

            // DB::table('users')->where('id',$request->id)->update($updateUsers);
            // DB::table('list_asesor')->where('id_akun',$request->id)->update($updateAsesor);
            // DB::commit();

          
           
    }
    
    public function printak01() {
        return view('template_print.printak01');
    }

    public function printak02() {
        return view('template_print.printak02');
    }

    public function printak03() {
        return view('template_print.printak03');
    }

    public function printak05() {
        return view('template_print.printak05');
    }
}