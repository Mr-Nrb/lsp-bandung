<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class Artikel extends Controller
{
    public function data_artikel(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Data Home Page";
        $data['title_sub_menu'] = "Data Artikel";
        $data['list_status_aktif'] = DB::select('SELECT * FROM `list_status_aktif` ORDER BY id DESC');
        return view('data_home_page.data_artikel', $data);
    }

    public function getDataArtikel(Request $request)
    {
        $data = DB::select('SELECT *
                            FROM `list_artikel` `a`');
        echo json_encode($data);
    }

    public function saveDataArtikel(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        $judul_artikel = $request->input('judul_artikel');
        $tanggal_artikel = $request->input('tanggal_artikel');
        $keterangan = $request->input('keterangan');
        $status_aktif = $request->input('status_aktif');
        //jika ada gambar
        if ($request->hasFile('gambar')) {
            $filenameWithExt = $request->file('gambar')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('gambar')->getClientOriginalExtension(); // Get just Extension
            $fileName_gambar = 'gambar' . '_' . md5($filename) . '.' . $extension; // Filename To store
            $request->gambar->move(public_path('assets/document/home_page/artikel'), $fileName_gambar);
        } else {
            $fileName_gambar = "";
        }

        $data = array(
            'judul_artikel' => $judul_artikel,
            'tanggal_artikel' => $tanggal_artikel,
            'keterangan' => $keterangan,
            'gambar' => $fileName_gambar,
            'is_aktif' => $status_aktif
        );

        DB::table('list_artikel')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([Artikel::class, 'data_artikel']);
    }

    public function hapusDataArtikel(Request $request)
    {
        $id = $request->input('id');

        DB::delete('DELETE FROM list_artikel WHERE id = ?', [$id]);
        echo json_encode($id);
    }

    public function getDataArtikelById(Request $request)
    {
        $id = $request->input('id');
        $data = DB::select("SELECT * FROM list_artikel WHERE id = '$id'");
        echo json_encode($data);
    }

    public function updateDataArtikel(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];

        $id = $request->input('id');
        $data = DB::table('list_artikel')->where('id', $id)->first();

        $judul_artikel = $request->input('judul_artikel');
        $tanggal_artikel = $request->input('tanggal_artikel');
        $keterangan = $request->input('keterangan');
        $status_aktif = $request->input('status_aktif');
        //jika ada tanda_tangan
        if ($request->hasFile('gambar')) {
            $filenameWithExt = $request->file('gambar')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); // Get Filename
            $extension = $request->file('gambar')->getClientOriginalExtension(); // Get just Extension
            $fileName_gambar = 'gambar' . '_' . md5($filename) . '.' . $extension; // Filename To store
            $request->gambar->move(public_path('assets/document/home_page/artikel'), $fileName_gambar);
        } else {
            $fileName_gambar = $data->gambar;
        }

        $update =  DB::table('list_artikel')
            ->where('id', $id)
            ->update([
                'judul_artikel' => $judul_artikel,
                'tanggal_artikel' => $tanggal_artikel,
                'keterangan' => $keterangan,
                'gambar' => $fileName_gambar,
                'is_aktif' => $status_aktif
            ]);

        echo json_encode($update);
    }
}
