<?php

namespace App\Http\Controllers;

use App\Models\ListAsesor;
use App\Http\Requests\StoreListAsesorRequest;
use App\Http\Requests\UpdateListAsesorRequest;

class ListAsesorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreListAsesorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreListAsesorRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ListAsesor  $listAsesor
     * @return \Illuminate\Http\Response
     */
    public function show(ListAsesor $listAsesor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ListAsesor  $listAsesor
     * @return \Illuminate\Http\Response
     */
    public function edit(ListAsesor $listAsesor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateListAsesorRequest  $request
     * @param  \App\Models\ListAsesor  $listAsesor
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateListAsesorRequest $request, ListAsesor $listAsesor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ListAsesor  $listAsesor
     * @return \Illuminate\Http\Response
     */
    public function destroy(ListAsesor $listAsesor)
    {
        //
    }
}
