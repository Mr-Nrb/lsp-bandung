<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class ManajemenJadwal extends Controller
{
    public function jadwal_asesmen(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Manajemen Jadwal";
        $data['title_sub_menu'] = "Jadwal Asesmen";
        $token = $data_session['token'];

        $data['list_kejuruan'] = DB::select('SELECT * FROM list_kejuruan ORDER BY nama_kejuruan ASC');
        $data['list_skema'] = DB::select('SELECT * FROM list_skema WHERE CHAR_LENGTH(kode)=2 ORDER BY nama');
        $data['list_sumber_anggaran'] = DB::select('SELECT * FROM list_sumber_anggaran');
        return view('manajemen_jadwal.jadwal_asesmen', $data);
    }

    public function getJadwalAsesmen()
    {
        $data = DB::select("SELECT
                            `a`.`id` AS `id`,
                            `a`.`tanggal_uji` AS `tanggal_uji`,
                            `a`.`nama_jadwal` AS `nama_jadwal`,
                            `b`.`nama` AS `nama_tuk`,
                            `e`.`nama_kejuruan`,
                            `c`.`nama` AS `nama_skema`,
                            `c`.`nomor` AS `nomor_skema`,
                            `c`.`id_skema` AS `id_skema`,
                            `a`.`target_peserta` AS `target_peserta`,
                            `a`.`jam_uji` AS `jam_uji`,
                            `a`.`lokasi_uji` AS `lokasi_uji`,
                            `d`.`nama` AS `nama_sumber_anggaran`,
                            `a`.`anggaran_perasesi` AS `anggaran_perasesi`,
                            `a`.`nomor_spk` AS `nomor_spk`,
                            `a`.`tanggal_spk` AS `tanggal_spk`,
                            `a`.`tgl_mulai_pelatihan` AS `tgl_mulai_pelatihan`,
                            `a`.`tgl_akhir_pelatihan` AS `tgl_akhir_pelatihan`,
                            `a`.`durasi_pelatihan` AS `durasi_pelatihan`,
                            `a`.`nama_pelatihan` AS `nama_pelatihan`,
                            `a`.`nama_instansi` AS `nama_instansi`,
                            `a`.`nama_lsp` AS `nama_lsp`
                            FROM `jadwal_asesmen` `a`
                            LEFT JOIN `list_tuk` `b`
                            ON `a`.`tuk` = `b`.`id`
                            LEFT JOIN `list_skema` `c`
                            ON `a`.`skema` = `c`.`kode`
                            LEFT JOIN `list_sumber_anggaran` `d`
                            ON `a`.`sumber_anggaran` = `d`.`id`
                            LEFT JOIN `list_kejuruan` `e`
                            ON `a`.`tema` = `e`.`id_kejuruan`
                            ORDER BY `a`.`tanggal_uji` DESC");
        echo json_encode($data);
    }

    public function getSubTuk(Request $request)
    {
        $valTukKejuruan = $request->input('valTukKejuruan');
        $n = strlen($valTukKejuruan);
        $data = DB::select("SELECT * FROM list_tuk WHERE LEFT(kejuruan,$n)='$valTukKejuruan' ORDER BY nama ASC");
        echo json_encode($data);
    }

    public function getSubSkema(Request $request)
    {
        $valSkemaKejuruan = $request->input('valSkemaKejuruan');
        $n = strlen($valSkemaKejuruan);

        $data = DB::select("SELECT * FROM list_skema WHERE LEFT(kode,$n)='$valSkemaKejuruan' AND CHAR_LENGTH(kode)=5 ORDER BY nama");
        echo json_encode($data);
    }

    public function saveJadwalAsesmen(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        $nama_pelatihan = $request->input('nama_pelatihan');
        $tanggal_mulai_pelatihan = $request->input('tanggal_mulai_pelatihan');
        $tanggal_akhir_pelatihan = $request->input('tanggal_akhir_pelatihan');
        $nama_instansi = $request->input('nama_instansi');
        $durasi_pelatihan = $request->input('durasi_pelatihan');
        $kejuruan = $request->input('kejuruan');
        $tuk = $request->input('tuk');
        $skema_kejuruan = $request->input('skema_kejuruan');
        $skema = $request->input('skema');
        $sumber_anggaran = $request->input('sumber_anggaran');
        $anggaran_perasesi = $request->input('anggaran_perasesi');
        $nomor_spk = $request->input('nomor_spk');
        $tanggal_spk = $request->input('tanggal_spk');
        $nama_jadwal = $request->input('nama_jadwal');
        $tanggal_uji = $request->input('tanggal_uji');
        $target_peserta = $request->input('target_peserta');
        $jam_uji = $request->input('jam_uji');
        $lokasi_uji = $request->input('lokasi_uji');
        $waktu_sekarang     = Date('Y-m-d');


        $data = array(
            'tanggal_uji' => $tanggal_uji,
            'nama_jadwal' => $nama_jadwal,
            'tuk' => $tuk,
            'tema' => $kejuruan,
            'skema' => $skema,
            'nomor' => "",
            'id_skema' => "",
            'target_peserta' => $target_peserta,
            'jam_uji' => $jam_uji,
            'lokasi_uji' => $lokasi_uji,
            'sumber_anggaran' => $sumber_anggaran,
            'anggaran_perasesi' => $anggaran_perasesi,
            'nomor_spk' => $nomor_spk,
            'tanggal_spk' => $tanggal_spk,
            'tgl_mulai_pelatihan' => $tanggal_mulai_pelatihan,
            'tgl_akhir_pelatihan' => $tanggal_akhir_pelatihan,
            'durasi_pelatihan' => $durasi_pelatihan,
            'nama_pelatihan' => $nama_pelatihan,
            'nama_instansi' => $nama_instansi,
            'nama_lsp' => "BBPVP_Bandung"
        );

        DB::table('jadwal_asesmen')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([ManajemenJadwal::class, 'jadwal_asesmen']);
    }

    public function hapusJadwalAsesmen(Request $request)
    {
        $id = $request->input('id');

        DB::delete('DELETE FROM jadwal_asesmen WHERE id = ?', [$id]);
        echo json_encode($id);
    }

    public function jadwalLanjutTuk(Request $request, string $id)
    {
        $id_jadwal_asesmen = $id;
        //ambil tanggal dari jadwal asesmen
       
        //ambil tanggal uji dan nama_jadwal dari jadwal asesmen yg dipilih untuk nge group siapa aja asesornya
        $data['jadwal'] = DB::table('jadwal_asesor')
        ->where('id_jadwal','=',$id_jadwal_asesmen)->get();
        // dd('$jadwal');
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Manajemen Jadwal";
        $data['title_sub_menu'] = "Jadwal Asesor";
        
        return view('manajemen_jadwal.jadwal_lanjut_tuk', $data);
    }

    public function penugasan_asesor(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Manajemen Jadwal";
        $data['title_sub_menu'] = "Jadwal Asesor";

        $data['nama_jadwal'] = DB::select("SELECT id, nama_jadwal, tanggal_uji FROM jadwal_asesmen WHERE tanggal_uji>=CURRENT_DATE");
        $data['asesor'] = DB::select("SELECT 
                                        `a`.`id_asesor`,
                                        `a`.`id_akun`,
                                        `b`.`name`,
                                        `a`.`met_asesor`
                                    FROM `list_asesor` `a`
                                    JOIN `users` `b`
                                    ON `a`.`id_akun` = `b`.`id`
                                    ORDER BY `met_asesor` ASC");
        return view('manajemen_jadwal.penugasan_asesor', $data);
    }

    public function getTanggalUjiJadwalAsesmen(Request $request)
    {
        $id = $request->input('id');
        $data = DB::select("SELECT * FROM jadwal_asesmen WHERE id = '$id'");
        echo json_encode($data);
    }

    public function getNoregAsesor(Request $request)
    {
        $id = $request->input('id');
        $data = DB::select("SELECT * FROM `list_asesor` WHERE `id_akun` = '$id'");
        echo json_encode($data);
    }

    public function saveJadwalAsesor(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        $id_jadwal = $request->input('id_jadwal');
        $id_nama_jadwal = $request->input('nama_jadwal');
        $data_jadwal_asesmen = DB::table('jadwal_asesmen')->where('id', $id_nama_jadwal)->first();
        $nama_jadwal = $data_jadwal_asesmen->nama_jadwal;
        $tanggal_uji = $request->input('tanggal_uji');
        $id_asesor = $request->input('asesor');
        $data_users = DB::table('users')->where('id', $id_asesor)->first();
        $nama_asesor = $data_users->name;
        $no_reg = $request->input('met_asesor');
        $jumlah_asesi = $request->input('jumlah_asesi');
        $waktu_sekarang     = Date('Y-m-d');


        $data = array(
            'id_jadwal' => $id_jadwal,
            'nama_jadwal' => $nama_jadwal,
            'tanggal_uji' => $tanggal_uji,
            'asesor' => $nama_asesor,
            'no_reg' => "$no_reg",
            'jumlah_asesi' => $jumlah_asesi,
            'nama_lsp' => "BBPVP_Bandung"
        );

        DB::table('jadwal_asesor')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([ManajemenJadwal::class, 'penugasan_asesor']);
    }

    public function surat_tugas(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Manajemen Jadwal";
        $data['title_sub_menu'] = "Surat Tugas Asesor";

        $data['nama_jadwal'] = DB::select("SELECT id, nama_jadwal, tanggal_uji FROM jadwal_asesmen WHERE tanggal_uji>=CURRENT_DATE");
        // $data['asesor'] = DB::select("SELECT `id`, `name`, `no_reg` FROM `users` WHERE `role_id`=3 AND NOT `no_reg`='' ORDER BY `name`");
        return view('manajemen_jadwal.surat_tugas', $data);
    }

    public function getSuratTugas(Request $request)
    {
        $data = DB::select('SELECT * FROM view_surat_tugas ORDER BY id DESC');
        echo json_encode($data);
    }

    public function saveSuratTugas(Request $request)
    {
        $data_session = $request->session()->get('dataUser');
        $token = $data_session['token'];
        $user = DB::table('users')->where('token', $token)->first();

        $id_nama_jadwal = $request->input('nama_jadwal');
        $data_jadwal_asesmen = DB::table('jadwal_asesmen')->where('id', $id_nama_jadwal)->first();
        $nama_jadwal = $data_jadwal_asesmen->nama_jadwal;
        $skema = $request->input('skema');
        $sumber_anggaran = $request->input('sumber_dana');
        $tuk = $request->input('nama_tuk');
        $tanggal_uji = $request->input('tanggal_uji');
        $lokasi = $request->input('lokasi');
        $tanggal_surat = $request->input('tanggal_surat');
        $nomor_surat = $request->input('nomor_surat');
        $nomor_keputusan = $request->input('nomor_keputusan');
        $nomor_spk = $request->input('nomor_spk');
        $tanggal_spk = $request->input('tanggal_spk');
        $waktu_sekarang     = Date('Y-m-d');


        $data = array(
            'tanggal_uji' => $tanggal_uji,
            'nama_jadwal' => $nama_jadwal,
            'tuk' => $tuk,
            'skema' => $skema,
            'id_skema' => "",
            'sumber_anggaran' => $sumber_anggaran,
            'nomor_spk' => $nomor_spk,
            'tanggal_spk' => $tanggal_spk,
            'tanggal_surat' => "$tanggal_surat",
            'lokasi' => $lokasi,
            'nomor_surat' => "$nomor_surat",
            'nomor_keputusan' => "$nomor_keputusan"
        );

        DB::table('surat_tugas')->insert($data);
        Alert::success('Berhasil', 'Data Berhasil di Ditambahkan');
        return redirect()->action([ManajemenJadwal::class, 'surat_tugas']);
    }

    public function view_surat_tugas(Request $request, string $id, string $id_jadwal)
    {
        $data_session = $request->session()->get('dataUser');
        $data['role_id'] = $data_session['role_id'];
        $data['email'] = $data_session['email'];
        $data['user'] = DB::table('users')->where('email', $data['email'])->first();
        $data['title_menu'] = "Manajemen Jadwal";
        $data['title_sub_menu'] = "View Surat Tugas";

        $data['id_surat_tugas'] = $id;
        $data['surat_tugas'] = DB::table('view_surat_tugas')->where('id', $id)->first();
        $data['surat_tugas'] = DB::table('view_surat_tugas')->where('id_jadwal', $id_jadwal)->first();

        $data['res_jadwal_asesmen'] = DB::select("SELECT
                                                            `a`.`nama_jadwal` AS `nama_jadwal`,
                                                            `a`.`tanggal_spk` AS `tanggal_spk`,
                                                            `a`.`lokasi_uji` AS `lokasi_uji`,
                                                            `b`.`nama` AS `nama_skema`,
                                                            `b`.`nomor` AS `nomor_skema`,
                                                            `b`.`id_skema` AS `id_skema`,
                                                            `c`.`nama` AS `nama_tuk`
                                                            FROM `jadwal_asesmen` `a`
                                                            JOIN `list_skema` `b`
                                                            ON `a`.`skema` = `b`.`kode`
                                                            JOIN `list_tuk` `c`
                                                            ON `a`.`tuk` = `c`.`id`
                                                            WHERE `a`.`id` = $id_jadwal");
        // $data['list_tujuan_asesmen'] = DB::select("SELECT * from list_tujuan_asesmen");
        $id_skema = $data['res_jadwal_asesmen'][0]->id_skema;
        $data['data_skema'] = DB::select("SELECT id,induk,nama,
                max(case when (nama='1') then isi else '-' end) as kode,
                max(case when (nama='2') then isi else '-' end) as nama

                FROM (SELECT * FROM list_skema_full ORDER BY id ASC) a,
                (SELECT @pv := $id_skema) b
                WHERE find_in_set(induk, @pv)
                group by grup,induk order by id ASC");

        $data['direktur'] =  DB::table('struktur_jabatan_lsp')->where('jabatan', 1)->first();
        return view('manajemen_jadwal.view_surat_tugas', $data);
    }
}