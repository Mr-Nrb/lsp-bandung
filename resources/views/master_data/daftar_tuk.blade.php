@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>Daftar Tuk</h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <button type="button" class="btn btn-outline-primary block" data-bs-toggle="modal"
                                        data-bs-target="#default" onclick="cancel()">
                                        <i class="bi bi-plus-circle-fill"></i> Tambah Data
                                    </button>
                                    <div class="table-responsive">
                                        <table class="table center" id="tblDaftarTuk">
                                            <thead>
                                                <tr>
                                                    <th>Action Button</th>
                                                    <th>Nama TUK</th>
                                                    <th>Kejuruan</th>
                                                    <th>Instansi Satker</th>
                                                    <th>Alamat</th>
                                                    <th>Kota</th>
                                                    <th>Email</th>
                                                    <th>Nama Ketua</th>
                                                    <th>Nomor Ketua</th>
                                                    <th>Nama Pengelola</th>
                                                    <th>Nomor Penngelola</th>
                                                    <th>Persyaratan Teknis</th>
                                                    <th>Ruang Teori</th>
                                                    <th>Ruang Praktek</th>
                                                    <th>Perlengkapan I</th>
                                                    <th>Perlengkapan II</th>
                                                    <th>Perlengkapan III</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!--Basic Modal -->
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel1">Data TUK</h5>
                <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close"
                    onclick="cancel();">
                    <i data-feather="x">X</i>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-content">
                    <div class="card-body">
                        <form id="form_crud" class="form" action="/saveDaftarTuk" enctype="multipart/form-data"
                            method="POST">
                            @csrf
                            <input type="hidden" name="id" id="id">
                            <label for=""> <strong style="color:black; ">1. Data TUK </strong></label>
                            <div class="row mt-3">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nama">Nama TUK</label>
                                        <input style="color:black" type="text" id="nama" class="form-control"
                                            placeholder="Masukkan Nama TUK" name="nama" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="kejuruan">Kejuruan</label>
                                        <select id="kejuruan" name="kejuruan" class="form-select">
                                            <option value="">-- Pilih Kejuruan --</option>
                                            <?php foreach ($list_kejuruan as $row) { ?>
                                            <option value="<?= $row->id_kejuruan ?>"><?= $row->nama_kejuruan ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="instansi_satker">Instansi / Satker</label>
                                        <input style="color:black" type="text" id="instansi_satker" class="form-control"
                                            placeholder="Masukkan Instansi / Satker" name="instansi_satker" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="email">Email</label>
                                        <input style="color:black" type="text" id="email" class="form-control"
                                            name="email" placeholder="Masukkan Email" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="alamat">Alamat</label>
                                        <input style="color:black" type="text" id="alamat" class="form-control"
                                            placeholder="Masukkan Alamat" name="alamat" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="kota">Kota</label>
                                        <input style="color:black" type="text" id="kota" class="form-control"
                                            placeholder="Masukkan kota" name="kota" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nama_ketua">Nama Ketua</label>
                                        <input style="color:black" type="text" id="nama_ketua" class="form-control"
                                            name="nama_ketua" placeholder="Masukkan Nama Ketua" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nama_pengelola">Nama Pengelola</label>
                                        <input style="color:black" type="text" id="nama_pengelola" class="form-control"
                                            name="nama_pengelola" placeholder="Masukkan Nama Pengelola" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nomor_ketua">Nomor Telepon Ketua</label>
                                        <input style="color:black" type="number" id="nomor_ketua" class="form-control"
                                            name="nomor_ketua" placeholder="Masukkan Nomor Ketua" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nomor_pengelola">Nomor Telepon Pengelola</label>
                                        <input style="color:black" type="number" id="nomor_pengelola"
                                            class="form-control" name="nomor_pengelola"
                                            placeholder="Masukkan Nomor Pengelola" />
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <label for=""> <strong style="color:black; ">2. Data Dokumen TUK </strong></label>
                            <div class="row mt-3">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="persyaratan_teknis">Persyaratan
                                            Teknis</label>
                                        <br>
                                        <img id="riview_persyaratan_teknis" name="riview_persyaratan_teknis"
                                            width="100%">
                                        <input style="color:black" type="file" class="form-control"
                                            id="persyaratan_teknis" name="persyaratan_teknis">
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="ruang_teori">Ruang Teori</label>
                                        <br>
                                        <img id="riview_ruang_teori" name="riview_ruang_teori" width="100%">
                                        <input style="color:black" type="file" class="form-control" id="ruang_teori"
                                            name="ruang_teori">
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="ruang_praktek">Ruang Praktek</label>
                                        <br>
                                        <img id="riview_ruang_praktek" name="riview_ruang_praktek" width="100%">
                                        <input style="color:black" type="file" class="form-control" id="ruang_praktek"
                                            name="ruang_praktek">
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="perlengkapan_i">Perlengkapan I</label>
                                        <br>
                                        <img id="riview_perlengkapan_i" name="riview_perlengkapan_i" width="100%">
                                        <input style="color:black" type="file" class="form-control" id="perlengkapan_i"
                                            name="perlengkapan_i">
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="perlengkapan_ii">Perlengkapan II</label>
                                        <br>
                                        <img id="riview_perlengkapan_ii" name="riview_perlengkapan_ii" width="100%">
                                        <input style="color:black" type="file" class="form-control" id="perlengkapan_ii"
                                            name="perlengkapan_ii">
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="perlengkapan_iii">Perlengkapan III</label>
                                        <br>
                                        <img id="riview_perlengkapan_iii" name="riview_perlengkapan_iii" width="100%">
                                        <input style="color:black" type="file" class="form-control"
                                            id="perlengkapan_iii" name="perlengkapan_iii">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 d-flex justify-content-end mt-4" id="button_">
                                <button type="submit" class="btn btn-primary me-1 mb-1">
                                    Simpan
                                </button>
                            </div>

                        </form>


                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script src="{{ asset('assets/js/master_data/daftar_tuk.js') }}"></script>

@endsection