@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>Daftar Asesor</h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <button type="button" class="btn btn-outline-primary block" data-bs-toggle="modal"
                                        data-bs-target="#default" onclick="cancel()">
                                        <i class="bi bi-plus-circle-fill"></i> Tambah Data
                                    </button>
                                    <div class="table-responsive">
                                        <table class="table center" id="tblDaftarAsesor">
                                            <thead>
                                                <tr>
                                                    <th>ActionButton</th>
                                                    <th>Nama Asesor</th>
                                                    <th>No Reg</th>
                                                    <th>Akses</th>
                                                    <th>NIK</th>
                                                    <th>Tempat Lahir</th>
                                                    <th>Tanggal Lahir</th>
                                                    <th>Jenis Kelamin</th>
                                                    <th>Kebangsaan</th>
                                                    <th>Alamat</th>
                                                    <th>Kode Pos</th>
                                                    <th>Provinsi</th>
                                                    <th>Kabupaten / Kota</th>
                                                    <th>Nomor Telpon</th>
                                                    <th>Email</th>
                                                    <th>Kualifikasi Pendidikan</th>
                                                    <th>Pekerjaan</th>
                                                    <th>No Sertfikat</th>
                                                    <th>Tanggal Sertifikat</th>
                                                    <th>Tanggal Sertifikat Expired</th>
                                                    <th>Foto Sertifikat Asesor</th>
                                                    <th>Foto Sertifikat Teknik</th>
                                                    <th>No Blanko</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!--Basic Modal -->
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel1">Form Data</h5>
                <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close"
                    onclick="cancel();">
                    <i data-feather="x">X</i>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-content">
                    <div class="card-body">
                        <form id="form_crud" class="form" action="/saveDaftarAsesor" enctype="multipart/form-data"
                            method="POST">
                            @csrf
                            <input type="text" name="id" id="id" hidden>
                            <label for=""> <strong style="color:black; "> Data Asesor </strong></label>
                            <div class="row mt-3">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nama">Nama</label>
                                        <input style="color:black" type="text" id="nama" class="form-control"
                                            placeholder="Masukkan Nama Asesor" name="nama" />
                                        <input style="color:black" type="text" id="role_id" class="form-control"
                                            value="3" name="role_id" hidden />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="met_asesor">No. MET Asesor</label>
                                        <input style="color:black" type="text" id="met_asesor" class="form-control"
                                            placeholder="Masukkan Nomor MET Asesor" name="met_asesor" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nik">NIK</label>
                                        <input style="color:black" type="number" id="nik" class="form-control"
                                            placeholder="Masukkan NIK Asesor" name="nik" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="tempat_lahir">Tempat Lahir</label>
                                        <input style="color:black" type="text" id="tempat_lahir" class="form-control"
                                            placeholder="Masukkan Tempat Lahir Asesor" name="tempat_lahir" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="tanggal_lahir">Tanggal Lahir</label>
                                        <input style="color:black" type="date" id="tanggal_lahir" class="form-control"
                                            name="tanggal_lahir" placeholder="Masukkan Tanggal Lahir Asesor" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group" id="jenis_kelamin">
                                        <label style="color:black">Jenis Kelamin</label>
                                        <br>
                                        <input type="radio" name="jenis_kelamin" value="1" id="jenis_kelamin">
                                        <label for="laki"> Laki - laki</label>
                                        <br>
                                        <input type="radio" name="jenis_kelamin" value="2" id="jenis_kelamin">
                                        <label for="perempuan"> Perempuan</label>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="kebangsaan">Kebangsaan</label>
                                        <select id="kebangsaan" name="kebangsaan" class="form-select">
                                            <option value="">-- Pilih Kebangsaan --</option>
                                            <?php foreach ($list_kebangsaan as $row) { ?>
                                            <option value="<?= $row->id ?>"><?= $row->nama ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="email">Email</label>
                                        <input style="color:black" type="text" id="email" class="form-control"
                                            name="email" placeholder="Masukkan Email Asesor" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="password">Password</label>
                                        <input style="color:black" type="text" id="password" class="form-control"
                                            name="password" placeholder="Masukkan Password Asesor" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="kode_pos">Kode Pos</label>
                                        <input style="color:black" type="number" id="kode_pos" class="form-control"
                                            name="kode_pos" placeholder="Masukkan Kode Pos Asesor" />
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="alamat">Alamat</label>
                                        <input style="color:black" type="text" id="alamat" class="form-control"
                                            name="alamat" placeholder="Masukkan Alamat Asesor" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12" hidden>
                                    <div class="form-group">
                                        <label style="color:black" for="token">Token</label>
                                        <input style="color:black" type="text" id="token" class="form-control"
                                            name="token" />
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <label for=""> <strong style="color:black; "> 2. Data Dokumen TUK </strong></label>
                            <div class="row mt-3">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nomor_sertif">Nomor Sertifikat</label>
                                        <input style="color:black" type="text" id="nomor_sertif" class="form-control"
                                            placeholder="Masukkan Nomor Sertifikat" name="nomor_sertif" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nomor_blanko">Nomor Blanko</label>
                                        <input style="color:black" type="text" id="nomor_blanko" class="form-control"
                                            placeholder="Masukkan no. lisensi LSP" name="nomor_blanko" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="tanggal_sertif">Tanggal Sertifikat</label>
                                        <input style="color:black" type="date" id="tanggal_sertif" class="form-control"
                                            placeholder="Masukkan Nomor Sertifikat" name="tanggal_sertif" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="tanggal_sertif_exp">Tanggal Sertifikat
                                            Expired</label>
                                        <input style="color:black" type="date" id="tanggal_sertif_exp"
                                            class="form-control" placeholder="Masukkan Nomor Sertifikat"
                                            name="tanggal_sertif_exp" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="foto_sertif_asesor">Foto Sertifikat
                                            Asesor</label>
                                        <br>
                                        <img id="riview_foto_sertif_asesor" name="riview_foto_sertif_asesor"
                                            width="100%">
                                        <input style="color:black" type="file" class="form-control"
                                            id="foto_sertif_asesor" name="foto_sertif_asesor">
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="foto_sertif_teknik">Foto Sertifikat
                                            Teknik</label>
                                        <br>
                                        <img id="riview_foto_sertif_teknik" name="riview_foto_sertif_teknik"
                                            width="100%">
                                        <input style="color:black" type="file" class="form-control"
                                            id="foto_sertif_teknik" name="foto_sertif_teknik">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 d-flex justify-content-end mt-4" id="button_">
                                <button type="submit" class="btn btn-primary me-1 mb-1">
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script src="{{ asset('assets/js/master_data/daftar_asesor.js') }}"></script>


@endsection