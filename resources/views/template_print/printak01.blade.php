<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <!-- <meta name="viewport"content="width=device-width, initial-scale=1, maximum-scale=5"/> -->
    <meta name="viewport" content="width=768" />
    <title>FR.AK.01. PERSETUJUAN ASESMEN DAN KERAHASIAAN</title>
    <style>
    body {
        font-family: Arial, Helvetica, sans-serif;
        color: #000;
    }

    @media print {
        body {
            -webkit-print-color-adjust: exact;
        }

        .table1,
        .table1 td,
        .table2,
        .table2 tr th,
        .table2 tr td,
        .table4,
        .table5,
        .table6 {
            border: 1px solid black;
            border-collapse: collapse;
        }
    }


    @page {
        size: auto;
        margin: 0mm;
    }

    .box {
        margin: 5 10 10 5;
    }

    .invoice-box {
        width: 90%;
        margin: auto;
    }

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
    }

    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.item td {
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }

    .ab {
        border-collapse: collapse;
        border: 1px #000 solid;
    }

    .ab1 {
        border-collapse: collapse;
        border: 1px #000 solid;
        text-align: center;
    }

    .tab {
        text-align: center;
    }

    .yata {
        width: 10px;
    }

    textarea {
        width: 96%;
    }

    .ctt {
        width: 150px;
    }

    .kmntr {
        width: 100px;
        height: 100px;
    }

    .txtarea {
        width: 96%
    }

    /* table */
    .table {
        overflow: auto;
    }

    .table table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        color: #000;
    }

    .table tr {
        background: #f8f8f8;
    }

    .table tr:nth-child(even) {
        background: var(--pPage)
    }

    .table th {
        height: 37px;
    }

    .table th,
    .table td {
        border: none;
        text-align: left;
        padding: 8px;
    }

    textarea {
        resize: vertical;
    }

    a {
        text-decoration: none;
    }

    .kol1 {
        width: 18%;
    }

    .kol2 {
        width: 1%;
    }

    .kol3 {
        width: 50%;
    }

    .kol4 {
        width: 6%;
    }

    .kol5 {
        width: 20%;
    }

    .invoice-box {
        line-height: 21px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
    }

    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.item td {
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }

    .table {
        overflow: auto;
    }

    .table table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        color: #000;
    }

    .table tr {
        background: #f8f8f8;
    }

    .table tr:nth-child(even) {
        background: var(--pPage)
    }

    .table th {
        height: 37px;
    }

    .table th,
    .table td {
        border: none;
        text-align: left;
        padding: 8px;
    }

    .table1 {
        border: none;
    }

    .table1,
    .konten1 {
        width: 150px;
    }

    .table2 {
        border-collapse: collapse;
    }

    .table2 th {
        background-color: #fac090;
        width: auto;
        text-align: center;
        font-weight: bold;
    }

    .checkbox-btn {
        text-align: center;
    }

    .no-urut {
        width: 10px;
        text-align: center;
    }

    .no-urut-konten1 {
        text-align: center;
        width: 30px;
    }

    .table1 input {
        width: 97%;
    }

    .table2,
    .table2 th,
    .table2 td {
        border: 1px solid #000;
        border-collapse: collapse;
    }

    input[type="date"] {
        width: 220px !important;
    }

    * {
        text-decoration: none;
        font-family: arial;
    }

    table {
        border: 1px solid black;
        border-collapse: collapse;
    }

    th {
        padding: 10px;
        box-sizing: border-box;
        background-color: rgb(250, 180, 114);
    }

    input[type="file"],
    input[type="date"],
    div#el-tglttdasesor,
    div#el-tglttdasesi {
        display: inline;
    }

    input[type="date"],
    div#el-tglttdasesor,
    div#el-tglttdasesi {
        width: 220px !important;
    }

    #bukti {
        width: 200px;
    }

    select[multiple] {
        overflow-y: auto;
        resize: vertical;
    }

    .judulrow {
        width: 210px;
    }
    </style>
</head>

<body>
    <div id="content" class="invoice-box">
        <h3>FR.AK.01. PERSETUJUAN ASESMEN DAN KERAHASIAAN</h3>
        <div>
            <table>
                <tr>
                    <th colspan="6">
                        <p>
                            Persetujuan Asesmen ini untuk menjamin bahwa Asesi telah diberi
                            arahan secara rinci tentang perencanaan dan proses asesmen
                        </p>
                    </th>
                </tr>
                <tr>
                    <td rowspan="2" class="judulrow">
                        Skema Sertifikasi (KKNI/Okupasi/Klaster)
                    </td>
                    <td>
                        <label for="judul">Judul</label>
                    </td>
                    <td>:</td>
                    <td colspan="3">
                        <output name="judul" id="judul" disabled /></output>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="nomor">Nomor</label>
                    </td>
                    <td>:</td>
                    <td colspan="3">
                        <output name="nomor" id="nomor" disabled /></output>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label for="tuk">TUK</label>
                    </td>
                    <td>:</td>
                    <td colspan="3" id="el-tuk">
                        <output name="tuk" id="tuk" disabled /></output>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label for="namaasesor">Nama Asesor</label>
                    </td>
                    <td>:</td>
                    <td colspan="3">
                        <output name="namaasesor" id="namaasesor" disabled /></output>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label for="namaasesi">Nama Asesi</label>
                    </td>
                    <td>:</td>
                    <td colspan="3">
                        <output name="namaasesi" id="namaasesi" disabled /></output>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Bukti yang dikumpulkan</td>
                    <td>:</td>
                    <td colspan="3">
                        <select name="bukti" id="bukti" multiple>
                            <option value="TL:Verifikasi Portofolio">TL:Verifikasi Portofolio</option>
                            <option value="T:Hasil Tes Tulis">T:Hasil Tes Tulis</option>
                            <option value="T:Hasil Tes Lisan">T:Hasil Tes Lisan</option>
                            <option value="T:Hasil Wawancara">T:Hasil Wawancara</option>
                            <option value="L:Observasi Langsung">L:Observasi Langsung</option>
                        </select>
                    </td>
                </tr>
                <tr class="tip">
                    <td colspan="3"></td>
                    <td colspan="3">
                        <em>*Tahan tombol ctrl (windows) / command (MacOS) ketika
                            menggunakan laptop</em>
                    </td>
                </tr>
                <tr>
                    <td rowspan="2" class="judulrow">
                        Pelaksanaan asesmen disepakati pada
                    </td>
                    <td>
                        <label for="tanggal">Tanggal</label>
                    </td>
                    <td>:</td>
                    <td id="el-tanggal">
                        <output id="tanggal" disabled /></output>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="tuk2">Lokasi Uji</label>
                    </td>
                    <td>:</td>
                    <td>
                        <output id="tuk2" name="tuk2" disabled /></output>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <b>Asesi:</b> <br />
                        <p>
                            Bahwa Saya Sudah Mendapatkan Penjelasan Hak dan Prosedur Banding
                            Oleh Asesor.
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <b>Asesor:</b> <br />
                        <p>
                            Menyatakan tidak akan membuka hasil pekerjaan yang saya peroleh
                            karena penugasan saya sebagai Asesor dalam pekerjaan Asesmen
                            kepada siapapun atau organisasi apapun selain kepada pihak yang
                            berwenang sehubungan dengan kewajiban saya sebagai Asesor yang
                            ditugaskan oleh LSP.
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <b>Asesi:</b> <br />
                        <p>
                            Saya setuju mengikuti asesmen dengan pemahaman bahwa informasi
                            yang dikumpulkan hanya digunakan untuk pengembangan profesional
                            dan hanya dapat diakses oleh orang tertentu saja.
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="height: 100px;">
                        <p>Tanda tangan Asesor dan tanggal:</p>
                        <div id="el-ttdasesor">
                            <input type="text" id="ttdasesor" name="ttdasesor" disabled />
                        </div>
                        <input type="submit" value="Piih File" id="upload-ttdasesor" />
                        <div id="el-tglttdasesor"><input id="tglttdasesor" /></div>
                        <br />
                    </td>
                    <td colspan="3" style="height: 100px;">
                        <p>Tanda tangan Asesi dan tanggal:</p>
                        <div id="el-ttdasesi">
                            <img id="gambarTandaTangan" style="width: 350px"> <br>
                            <div id="el-ttdasesi"><input type="text" id="ttdasesi" name="ttdasesi" disabled /></div>
                            <label for="">Apakah ingin menggunakan tanda tangan</label>
                            <select name="validasittdasesi" id="validasittdasesi" class="input required">
                                <option value=""></option>
                                <option value="iya">Gunakan Tanda tangan</option>
                            </select>
                        </div>
                        <!-- <input type="submit" value="Piih File" id="upload-ttdasesi" /> -->
                        <div id="el-tglttdasesi"><input id="tglttdasesi" /></div>
                        <br />
                    </td>
                </tr>
            </table>
        </div>
        <br />
    </div>
</body>

<script type="text/javascript">
window.print();
</script>

</html>