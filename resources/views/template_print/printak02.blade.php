<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5" />
    <meta name="viewport" content="width=600">
    <title>FR.AK.02. FORMULIR REKAMAN ASESMEN KOMPETENSI</title>
    <style>
    body {
        font-family: Arial, Helvetica, sans-serif;
        color: #000;
    }

    @media print {
        body {
            -webkit-print-color-adjust: exact;
        }

        .table1,
        .table1 td,
        .table2,
        .table2 tr th,
        .table2 tr td,
        .table4,
        .table5,
        .table6 {
            border: 1px solid black;
            border-collapse: collapse;
        }
    }


    @page {
        size: auto;
        margin: 0mm;
    }

    .box {
        margin: 5 10 10 5;
    }

    .invoice-box {
        width: 90%;
        margin: auto;
    }

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
    }

    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.item td {
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }

    .ab {
        border-collapse: collapse;
        border: 1px #000 solid;
    }

    .ab1 {
        border-collapse: collapse;
        border: 1px #000 solid;
        text-align: center;
    }

    .tab {
        text-align: center;
    }

    .yata {
        width: 10px;
    }

    textarea {
        width: 96%;
    }

    .ctt {
        width: 150px;
    }

    .kmntr {
        width: 100px;
        height: 100px;
    }

    .txtarea {
        width: 96%
    }

    /* table */
    .table {
        overflow: auto;
    }

    .table table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        color: #000;
    }

    .table tr {
        background: #f8f8f8;
    }

    .table tr:nth-child(even) {
        background: var(--pPage)
    }

    .table th {
        height: 37px;
    }

    .table th,
    .table td {
        border: none;
        text-align: left;
        padding: 8px;
    }

    textarea {
        resize: vertical;
    }

    a {
        text-decoration: none;
    }

    .kol1 {
        width: 18%;
    }

    .kol2 {
        width: 1%;
    }

    .kol3 {
        width: 50%;
    }

    .kol4 {
        width: 6%;
    }

    .kol5 {
        width: 20%;
    }

    .invoice-box {
        line-height: 21px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
    }

    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.item td {
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }

    .table {
        overflow: auto;
    }

    .table table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        color: #000;
    }

    .table tr {
        background: #f8f8f8;
    }

    .table tr:nth-child(even) {
        background: var(--pPage)
    }

    .table th {
        height: 37px;
    }

    .table th,
    .table td {
        border: none;
        text-align: left;
        padding: 8px;
    }

    .table1 {
        border: none;
    }

    .table1,
    .konten1 {
        width: 150px;
    }

    .table2 {
        border-collapse: collapse;
    }

    .table2 th {
        background-color: #fac090;
        width: auto;
        text-align: center;
        font-weight: bold;
    }

    .checkbox-btn {
        text-align: center;
    }

    .no-urut {
        width: 10px;
        text-align: center;
    }

    .no-urut-konten1 {
        text-align: center;
        width: 30px;
    }

    .table1 input {
        width: 97%;
    }

    .table2,
    .table2 th,
    .table2 td {
        border: 1px solid #000;
        border-collapse: collapse;
    }

    input[type="date"] {
        width: 220px !important;
    }
    </style>
</head>

<body>
    <div id="content" class="invoice-box">
        <h3>FR.AK.02 FORMULIR REKAMAN ASESMEN KOMPETENSI</h3>
        <table class="table1">
            <tr>
                <td class="konten1">Nama Asesi</td>
                <td>

                </td>
            </tr>
            <tr>
                <td class="konten1">Nama Asesor</td>
                <td>

                </td>
            </tr>
            <tr>
                <td class="konten1">Skema Sertifikasi <br> (bila tersedia)</td>
                <td>

                </td>
            </tr>
            <tr>
                <td class="konten1">Unit Kompetensi</td>
                <td>

                </td>
            </tr>
            <tr>
                <td class="konten1">Tangal mulainya Asesmen</td>
                <td>

                </td>
            </tr>
            <tr>
                <td class="konten1">Tanggal Selesainya Asesmen</td>
                <td>

                </td>
            </tr>
        </table>
        <p>
            Beri tanda centang (√) di kolom yang sesuai untuk mencerminkan bukti
            yang diperoleh untuk menentukan Kompetensi asesi untuk setiap Unit
            Kompetensi.
        </p>
        <table class="table2">
            <tr>
                <th><label for="Unit-Kompetensi">Unit <br> Kompetensi</label></th>
                <th>
                    <label for="Unit-Observasi-Demonstrasi">Observasi <br> Demonstrasi</label>
                </th>
                <th><label for="Unit-Portofolio">Portofolio</label></th>
                <th>
                    <label for="Pernyataan-Pihak-Ketiga-Pertanyaan-Wawancara">Pernyataan Pihak Ketiga Pertanyaan
                        Wawancara</label>
                </th>
                <th><label for="Pertanyaan-Lisan">Pertanyaan <br> Lisan</label></th>
                <th>
                    <label for="Pertanyaan-Tertulis">Pertanyaan <br> Tertulis</label>
                </th>
                <th><label for="Proyek-Kerja">Proyek <br> Kerja</label></th>
                <th><label for="Lainnya">Lainnya</label></th>
            </tr>
            <tr>
                <td class="checkbox-btn">
                    ceklis
                </td>

                <td class="checkbox-btn">

                </td>

                <td class="checkbox-btn">

                </td>

                <td class="checkbox-btn">

                </td>

                <td class="checkbox-btn">

                </td>
                <td class="checkbox-btn">

                </td>
                <td class="checkbox-btn">

                </td>
                <td class="checkbox-btn">

                </td>
            </tr>
            <tr>
                <td colspan="2">Rekomendasi Hasil Asesmen</td>
                <td colspan="6">

                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Tindak lanjut yang dibutuhkan <br>
                    (Masukkan pekerjaan tambahan dan asesmen yang diperlukan untuk
                    mencapai kompetensi)
                </td>
                <td colspan="6">

                </td>
            </tr>
            <tr>
                <td colspan="2">Komentar/Observasi oleh Asesor</td>
                <td colspan="6">

                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <label for="tandatanganasesi">Tanda Tangan Asesi dan tanggal</label>
                </td>
                <td colspan="6">
                    <div id="el-ttdasesor">

                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <label for="tandatanganasesor">Tanda Tangan Asesor dan tanggal</label>
                </td>
                <td colspan="6">
                    <div id="el-ttdadmin">

                    </div>
                </td>
            </tr>
        </table>
        <h3>LAMPIRAN DOKUMEN</h3>
        <ol>
            <li>Dokumen APL 01 peserta.</li>
            <li>Dokumen APL 02 peserta.</li>
            <li>Bukti-bukti berkualitas peserta.</li>
            <li>Tinjauan proses asesmen.</li>
        </ol>
    </div>
</body>

<script type="text/javascript">
window.print();
</script>

</html>