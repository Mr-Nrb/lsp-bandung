<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5"> -->
    <meta name="viewport" content="width=768">
    <link rel="shortcut icon" href="../favicon.ico">
    <title>FR.AK.03. UMPAN BALIK DAN CATATAN ASESMEN</title>
    <link rel="stylesheet" type="text/css" href="printak03.css" async>

    <style>
    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    @media print {
        body {
            -webkit-print-color-adjust: exact;
        }

        .table1,
        .table1 td,
        .table2,
        .table2 tr th,
        .table2 tr td,
        .table4,
        .table5,
        .table6 {
            border: 1px solid black;
            border-collapse: collapse;
        }
    }


    @page {
        size: auto;
        margin: 0mm;
    }

    .box {
        margin: 5 10 10 5;
    }

    .invoice-box {
        width: 90%;
        margin: auto;
    }

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }

    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.item td {
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }

    .ab {
        border-collapse: collapse;
        border: 1px #000 solid;
    }

    .ab1 {
        border-collapse: collapse;
        border: 1px #000 solid;
        text-align: center;
    }

    /* .bg {
    background-color:  #f2dcdb;
    color: #31489b;
  } */
    .tab {
        text-align: center;
    }

    .yata {
        width: 10px;
    }

    textarea {
        width: 96%;
    }

    .ctt {
        width: 150px;
    }

    .kmntr {
        width: 100px;
        height: 100px;
    }

    .txtarea {
        width: 96%
    }

    /* table */
    .table {
        overflow: auto;
    }

    .table table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        color: #000;
    }

    .table tr {
        background: #f8f8f8;
    }

    .table tr:nth-child(even) {
        background: var(--pPage)
    }

    .table th {
        height: 37px;
    }

    .table th,
    .table td {
        border: none;
        text-align: left;
        padding: 8px;
    }

    .float {
        position: fixed;
        width: 60px;
        height: 60px;
        bottom: 40px;
        right: 40px;
        background-color: #355664;
        color: #FFF;
        border-radius: 50px;
        text-align: center;
        box-shadow: 2px 2px 3px #999;
    }

    .my-float {
        margin-top: 22px;
    }

    textarea {
        resize: vertical;
    }

    a {
        text-decoration: none;
    }

    .kol1 {
        width: 18%;
    }

    .kol2 {
        width: 1%;
    }

    .kol3 {
        width: 50%;
    }

    .kol4 {
        width: 6%;
    }

    .kol5 {
        width: 20%;
    }
    </style>
</head>

<body>
    <div>
        <div id="content" class="invoice-box">

            <h3>FR.AK.03 UMPAN BALIK DAN CATATAN ASESMEN</h3>

            <table class="a bg">
                <tr>
                    <td class="kol1"><label for="nama">Nama Asesi</label></td>
                    <td class="kol2">:</td>
                    <td class="kol3"></td>
                    <td class="kol4"><label for="tanggal">Tanggal</label></td>
                    <td class="kol2">:</td>
                    <td class="kol5"></td>
                </tr>
                <tr>
                    <td class="kol1"><label for="namasor">Nama Asesor</label></td>
                    <td class="kol2">:</td>
                    <td class="kol3"></td>
                    <td class="kol4"><label for="waktu">Waktu</label></td>
                    <td class="kol2">:</td>
                    <td class="kol5"></td>
                </tr>
            </table>

            <p>Umpan balik dari Asesi (diisi oleh Asesi setelah mengambil keputusan) :</p>

            <table class="ab">
                <tr class="tab ab">
                    <td class="ab" rowspan="2">KOMPONEN</td>
                    <td class="ab">Hasil</td>
                    <td class="ab ctt" rowspan="2">Catatan / komentar asesi</td>
                </tr>
                <tr class="ab tab">
                    <td class="ab yata">Ya/Tidak</td>
                </tr>
                <tr class="ab">
                    <td class="ab">Saya mendapatkan penjelasan yang cukup memadai mengenai proses asesmen/uji kompetensi
                    </td>
                    <td class="ab1">YA / TIDAK</td>
                    <td class="ab"></td>
                </tr>
                <tr class="ab">
                    <td class="ab">Saya diberikan kesempatan untuk mempelajari standar kompetensi yang akan diujikan dan
                        menilai diri sendiri terhadap pencapaiannya</td>
                    <td class="ab1">YA / TIDAK</td>
                    <td class="ab"></td>
                </tr>
                <tr class="ab">
                    <td class="ab">Asesor memberikan kesempatan untuk mendiskusikan/menegosiasikan metoda, instrumen dan
                        sumber asesmen serta jadwal asesmen </td>
                    <td class="ab1">YA / TIDAK</td>
                    <td class="ab"></td>
                </tr>
                <tr class="ab">
                    <td class="ab">Asesor berusaha menggali seluruh bukti pendukung yang sesuai dengan latar belakang
                        pelatihan dan pengalaman yang saya miliki</td>
                    <td class="ab1">YA / TIDAK</td>
                    <td class="ab"></td>
                </tr>
                <tr class="ab">
                    <td class="ab">Saya sepenuhnya diberikan kesempatan untuk mendemonstrasikan kompetensi yang saya
                        miliki
                        selama asesmen</td>
                    <td class="ab1">YA / TIDAK</td>
                    <td class="ab"></td>
                </tr>
                <tr class="ab">
                    <td class="ab">Saya mendapatkan penjelasan yang memadai mengenai keputusan asesmen</td>
                    <td class="ab1">YA / TIDAK</td>
                    <td class="ab"></td>
                </tr>
                <tr class="ab">
                    <td class="ab">Asesor memberikan umpan balik yang mendukung setelah asesmen serta tindak lanjutnya
                    </td>
                    <td class="ab1">YA / TIDAK</td>
                    <td class="ab"></td>
                </tr>
                <tr class="ab">
                    <td class="ab">Asesor bersama saya mempelajari semua dokumen asesmen serta menandatanganinya</td>
                    <td class="ab1">YA / TIDAK</td>
                    <td class="ab"></td>
                </tr>
                <tr class="ab">
                    <td class="ab">Saya mendapatkan jaminan kerahasiaan hasil asesmen serta penjelasan penanganan
                        dokumen
                        asesmen</td>
                    <td class="ab1">YA / TIDAK</td>
                    <td class="ab"></td>
                </tr>
                <tr class="ab">
                    <td class="ab">Asesor menggunakan keterampilan komunikasi yang efektif selama asesmen</td>
                    <td class="ab1">YA / TIDAK</td>
                    <td class="ab"></td>
                </tr>
                <tr class="ab">
                    <td class="ab kmntr">Catatan/komentar lainnya (apabila ada) :</td>
                    <td class="ab" colspan="3"></td>
                </tr>
            </table>
        </div>
    </div>
</body>

<script>
window.print()
</script>

</html>