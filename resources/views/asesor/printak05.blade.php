<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5"/> -->
    <meta name="viewport" content="width=600">
    <title>FR.AK.05. LAPORAN ASESMEN</title>
    <style>
    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    @media print {
        body {
            -webkit-print-color-adjust: exact;
        }
    }

    @page {
        size: auto;
        margin: 0mm;
    }

    .isi {
        width: 70%;
    }

    .box {
        margin: auto;
    }

    * {
        text-decoration: none;
        font-family: arial;
    }

    table {
        border: 1px solid black;
        border-collapse: collapse;
    }

    .table4 th,
    .table4 td,
    .table5 th,
    .table5 td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    .table1 {
        border-collapse: separate;
        height: 320px;
        width: 100%;
    }

    .table4 {
        border-collapse: separate;
        width: 100%;
    }

    .table5 {
        border-collapse: separate;
        width: 100%;
    }

    .table6 {
        border-collapse: separate;
        width: 100%;
    }

    .invoice-box {
        width: 90%;
        margin: auto;
    }

    .invoice-box .table1 td {
        vertical-align: middle;
    }

    th {
        padding: 7px;
        box-sizing: border-box;
        background-color: rgb(250, 180, 114);
    }

    .vCenter {
        vertical-align: middle !important;
    }

    input[type="text"],
    input[type="number"],
    input[type="date"],
    select {
        width: 95%;
        padding: 10px;
    }

    textarea {
        resize: vertical;
    }

    .align-center {
        text-align: center;
    }

    .display-block {
        display: block;
    }

    .label {
        width: 160px;
        color: black !important;
        padding-left: 5px;
        padding-right: 5px;
    }

    .label1 {
        width: 30%;
        height: 50px;
        color: black !important;
        padding-left: 5px;
        padding-right: 5px;
    }

    .label2 {
        width: 70%;
        color: black !important;
        padding-left: 5px;
        padding-right: 5px;
    }

    .label3 {
        width: 30%;
        color: black !important;
        padding-left: 5px;
        padding-right: 5px;
    }

    .label4 {
        width: 15%;
        height: 10px;
        color: black !important;
        padding-left: 5px;
        padding-right: 5px;
    }

    .divasesi {
        width: 100%;
    }

    .divaspek {
        width: 100%;
    }

    .divasesor {
        width: 100%;
    }

    .sublabel {
        width: 100px;
        text-align: center;
        padding-left: 5px;
    }

    [class*="l-"] {
        float: none;
    }

    .swal-button--confirm,
    .swal-button--cancel {
        background-color: #072d3a;
        color: #fff;
    }

    .swal-button--confirm:hover,
    .swal-button--cancel:hover {
        background-color: #072d3a !important;
    }

    @media print {

        .table1,
        .table1 td,
        .table2,
        .table2 tr th,
        .table2 tr td,
        .table4,
        .table5,
        .table6 {
            border: 1px solid black;
            border-collapse: collapse;
        }
    }

    .mid {
        text-align: center;
        padding-left: 2px;
        padding-right: 2px;
        width: 2px;
    }

    .catatan {
        width: 30%;
    }

    .catatan1 {
        width: 10%;
    }
    </style>
</head>

<body>
    <div class="box">
        <div id="content" class="invoice-box">
            <h3>FR.AK.05. LAPORAN ASESMEN</h3>
            <table id="induk" class="hide"></table>
            <table class="table1" border="1">
                <tr>
                    <td rowspan="2" class="label">Skema Sertifikasi <br> (KKNI/Okupasi/Klaster)</td>
                    <td>
                        <label for="judul" class="sublabel">Judul</label>
                    </td>
                    <td class="mid">:</td>
                    <td class="isi">

                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="nomor" class="sublabel">Nomor</label>
                    </td>
                    <td class="mid">:</td>
                    <td class="isi">

                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="label">TUK</td>
                    <td class="mid">:</td>
                    <td class="isi">

                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="label">
                        <label for="nama_asesor">Nama Asesor</label>
                    </td>
                    <td class="mid">:</td>
                    <td class="isi"></td>
                </tr>
                <tr>
                    <td colspan="2" class="label">
                        <label for="tanggal">Tanggal</label>
                    </td>
                    <td class="mid">:</td>
                    <td class="isi"></td>
                </tr>
                <tr>
                    <td colspan="2" class="label">
                        <label for="tanggal">Judul Jadwal</label>
                    </td>
                    <td class="mid">:</td>
                    <td class="isi"></td>
                </tr>
            </table>
            <br>

            <table class="table6" border="1">
                <tr>
                    <td rowspan="2" class="label3">Unit Kompetensi</td>
                    <td class="label4">
                        <label for="judul" class="sublabel">Kode Unit</label>
                    </td>
                    <td class="mid">:</td>
                    <td></td>
                </tr>
                <tr>
                    <td class="label4">
                        <label for="nomor" class="sublabel">Judul Unit</label>
                    </td>
                    <td class="mid">:</td>
                    <td></td>
                </tr>
            </table>

            <br>

            <div id="asesi" class="divasesi">
                <table class="table2 divasesi">
                    <tr class="align-center">
                        <th rowspan="2">No.</th>
                        <th rowspan="2">Nama Asesi</th>
                        <th>Rekomendasi</th>
                        <th rowspan="2">Keterangan</th>
                    </tr>
                    <tr class="align-center">
                        <th>K / BK</th>
                    </tr>
                    <tr>
                        <td colspan="4" class="align-center"></td>
                    </tr>
                </table>
            </div>
            <em>** tuliskan Kode dan Judul Unit Kompetensi yang dinyatakan BK bila mengakses satu skema</em>
            <br />
            <br>
            <div class="divaspek">
                <table class="table4">
                    <tr>
                        <td colspan="5" class="label1">
                            <label for="keterangan1">
                                Aspek Negatif dan Positif dalam Asesemen
                            </label>
                        </td>
                        <td class="label2"></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="label1">
                            <label for="keterangan2"> Pencatatan Penolakan Hasil Asesmen </label>
                        </td>
                        <td class="label2"></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="label1">
                            <label for="keterangan3">
                                Saran Perbaikan : <br> (Asesor/Personil Terkait)
                            </label>
                        </td>
                        <td class="label2"></td>
                    </tr>
                </table>
            </div>
            <br />
            <div class="divasesor">
                <table class="table5">
                    <tr>
                        <td class="catatan">
                            <label for="catatan" class="display-block">
                                <b>Catatan :</b>
                            </label>
                        </td>
                        <td colspan="2">
                            <label for="asesor">Asesor :</label>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="5"></td>
                    </tr>
                    <tr>
                        <td class="catatan1">
                            <label for="nama"> Nama </label>
                        </td>
                        <td>
                            <p>
                                <output name="nama" id="nama"></output>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="no_registrasi"> No. Reg </label>
                        </td>
                        <td>
                            <p>
                                <output name="no_registrasi" id="no_registrasi"
                                    placeholder="Masukkan Nomor Registrasi"></output>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>Tanda Tangan</td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td id="el-tanggalttd">
                        </td>
                    </tr>
                </table>
            </div>
            <br />
        </div>
    </div>
</body>

</html>