@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">

                                    <div class="table-responsive">
                                        <table id="ak05_jadwal" class="table">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Nama Jadwal</th>
                                                    <th>Tanggal Uji</th>
                                                    <th>Asesor</th>
                                                    <th>No Reg</th>
                                                    <th>Jumlah Asesi</th>
                                                    <th>Nama LSP</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($jadwal as $d)
                                                <td>
                                                    <a href="/lanjut_ak05">
                                                       <button type="submit">Lanjut ke AK05</button>
                                                    </a>
                                                </td>
                                                <td>{{$d -> nama_jadwal}}</td>
                                                <td>{{$d -> tanggal_uji}}</td>
                                                <td>{{$d -> asesor}}</td>
                                                <td>{{$d -> no_reg}}</td>
                                                <td>{{$d -> jumlah_asesi}}</td>
                                                <td>{{$d -> nama_lsp}}</td>
                                                @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!--Basic Modal -->

<!-- <script src="{{ asset('assets/js/laporan/ak05_pilih_jadwal.js') }}"></script> -->


@endsection