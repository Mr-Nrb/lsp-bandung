@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    <?= $title_sub_menu; ?>
                                </h4>
                                <hr>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <form class="form" action="/saveProfileAsesor" enctype="multipart/form-data" method="POST" id="myForm">
                                        @csrf
                                        <div class="row mt-3">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="email">Email</label>
                                                    <input style="color:black" type="text" id="email" class="form-control" placeholder="Masukkan Email" name="email" value="<?= $user->email ?>" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12 d-flex mt-4">
                                                <input type="button" class="btn btn-primary me-1 mb-1" onclick="location.href='ubahPassword';" value="Ubah Password" />
                                                
                                            </div>
                                        </div>
                                        <br>
                                        <label for=""> <strong style="color:black; "> 1. Data Pribadi </strong></label>
                                        <div class="row mt-3">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="name">Nama Lengkap</label>
                                                    <input style="color:black" type="text" id="name" class="form-control" placeholder="Masukkan Nama Lengkap" name="name" value="<?= $user->name ?>" oninput="this.value = this.value.toUpperCase()" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="nik">No. KTP/NIK/Paspor</label>
                                                    <input style="color:black" type="text" id="nik" class="form-control" placeholder="Masukkan NIK/no. paspor" name="nik" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" maxlength="16" pattern="[0-9]{16}" title="16 angka" value="<?= $user->nik ?>" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="tempat_lahir">Tempat Lahir</label>
                                                    <input style="color:black" type="text" id="tempat_lahir" class="form-control" placeholder="Masukkan Tempat Lahir" name="tempat_lahir" value="<?= $user->tempat_lahir ?>" oninput="this.value = this.value.toUpperCase()" required/>
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="tanggal_lahir">Tanggal Lahir</label>
                                                    <input style="color:black" type="date" id="tanggal_lahir" class="form-control" placeholder="Masukkan Tanggal Lahir" name="tanggal_lahir" value="<?= $user->tanggal_lahir ?>" required/>
                                                </div>
                                            </div>
                                            <!-- ambil datanya dari tabel list_jenis_kelamin -->
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="jenis_kelamin">Jenis Kelamin</label>
                                                    <fieldset class="form-group">
                                                        <select id="jenis_kelamin" name="jenis_kelamin" class="form-select" id="jenis_kelamin" required>
                                                            <option value="" disabled selected>-- Pilih Jenis Kelamin --</option>
                                                            <?php foreach ($jenis_kelamin as $row) { ?>
                                                                <option value="<?= $row->id ?>" <?= $user->jenis_kelamin == $row->id ? "selected" : "" ?>><?= $row->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <!-- ambil datanya dari tabel list_keabangsaan -->
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="kebangsaan">Kebangsaan</label>
                                                    <fieldset class="form-group">
                                                        <select id="kebangsaan" name="kebangsaan" class="form-select" id="kebangsaan" required>
                                                            <option value="" disabled selected>-- Pilih Kebangsaan --</option>
                                                            <?php foreach ($kebangsaan as $row) { ?>
                                                                <option value="<?= $row->id ?>" <?= $user->kebangsaan == $row->id ? "selected" : "" ?>><?= $row->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="alamat">Alamat</label>
                                                    <textarea style="color:black" class="form-control" id="alamat" name="alamat" rows="3" oninput="this.value = this.value.toUpperCase()" required><?= $user->alamat ?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="kode_pos">Kode Pos</label>
                                                    <input style="color:black" type="text" id="kode_pos" class="form-control" name="kode_pos" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" maxlength="5" pattern="[0-9]{5}" title="5 angka" value="<?= $user->kodepos ?>" required/>
                                                </div>
                                            </div>
                                            <!-- terapin konsep seelct pemilihan alamat -->
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="provinsi">Provinsi</label>
                                                    <fieldset class="form-group">
                                                        <select id="provinsi" name="provinsi" class="form-select" id="provinsi" required>
                                                            <option value="" disabled selected>-- Pilih Provinsi --</option>
                                                            <?php foreach ($provinsi as $row) { ?>
                                                                <option value="<?= $row->kode ?>" <?= $user->provinsi == $row->kode ? "selected" : "" ?>><?= $row->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="kab_kota">Kabupaten / Kota</label>
                                                    <fieldset class="form-group">
                                                        <select id="kab_kota" name="kab_kota" class="form-select" id="kab_kota" required>
                                                            <?php if (!$kabupaten_kota) { ?>
                                                                <option value="" disabled selected>-- Pilih Provinsi Terlebih dahulu --</option>
                                                                <?php } else {
                                                                foreach ($kabupaten_kota as $row) { ?>
                                                                    <option value="<?= $row->kode ?>"><?= $row->nama ?></option>
                                                            <?php }
                                                            } ?>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="no_hp">No. HP / WhatsApp</label>
                                                    <input style="color:black" type="text" id="no_hp" class="form-control" placeholder="Masukkan No. Telepon Anda" name="no_hp" value="<?= $user->no_hp ?>" required/>
                                                </div>
                                            </div>
                                            <!-- ambil datanya dari tabel list_pendidikan -->
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="pendidikan">Pendidikan Terakhir</label>
                                                    <fieldset class="form-group">
                                                        <select id="pendidikan" name="pendidikan" class="form-select" id="pendidikan" required>
                                                            <option value="" disabled selected>-- Pilih Pendidikan --</option>
                                                            <?php foreach ($pendidikan as $row) { ?>
                                                                <option value="<?= $row->id ?>" <?= $user->kualifikasi_pendidikan == $row->id ? "selected" : "" ?>><?= $row->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <!-- ambil datanya dari tabel list_pekerjaan -->
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="pekerjaan">Pekerjaan*</label>
                                                    <fieldset class="form-group">
                                                        <select id="pekerjaan" name="pekerjaan" class="form-select" id="pekerjaan" required>
                                                            <option value="" disabled selected>-- Pilih Pekerjaan --</option>
                                                            <?php foreach ($pekerjaan as $row) { ?>
                                                                <option value="<?= $row->id ?>" <?= $user->pekerjaan == $row->id ? "selected" : "" ?>><?= $row->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                        <small class="text-danger">*Jika belum bekerja, pilih Belum/Tidak Bekerja ATAU Pelajar/Mahasiswa</small>
                                        <br>

                                        <div class="row mt-3">
                                            
                                            <!-- <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    </div>
                                                </div> -->
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="npwp">No. NPWP</label>
                                                    <input style="color:black" type="text" class="form-control" id="npwp" name="npwp">
                                                    <label style="color:black" for="dokumen_npwp">Upload File NPWP</label>
                                                    <br>
                                                    <?php if ($user->dokumen_npwp != "") { ?>
                                                        <embed src="assets/document/npwp/asesor/<?= $user->dokumen_npwp  ?>" height="330" width="350">
                                                    <?php } ?>
                                                    <input style="color:black" type="file" class="form-control" id="dokumen_npwp" name="dokumen_npwp">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="dokumen_ktp">Upload File KTP</label>
                                                    <br>
                                                    <?php if ($user->dokumen_ktp != "") { ?>
                                                        <embed src="assets/document/ktp/asesor/<?= $user->dokumen_ktp  ?>" height="330" width="350">
                                                    <?php } ?>
                                                    <input style="color:black" type="file" class="form-control" id="dokumen_ktp" name="dokumen_ktp">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <br>
                                                    <label style="color:black" for="rekening_bank">No. Rekening</label>
                                                    <input style="color:black" type="text" class="form-control" id="rekening_bank" name="rekening_bank">
                                                    <label style="color:black" for="nama_bank">Nama Bank</label>
                                                        <select id="nama_bank" name="nama_bank" class="form-select" id="nama_bank" >
                                                            <option value="" disabled selected>-- Pilih Nama Bank --</option>
                                                            <?php foreach ($nama_bank as $row) { ?>
                                                                <option value="<?= $row->id ?>" <?= $user->nama_bank == $row->id ? "selected" : "" ?>><?= $row->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    <label style="color:black" for="pemilik_rekening">Nama Pemilik Rekening</label>
                                                    <input style="color:black" type="text" class="form-control" id="pemilik_rekening" name="pemilik_rekening" oninput="this.value = this.value.toUpperCase()" value="<?= $user->name ?> ">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <br>
                                                    <label style="color:black" for="tanda_tangan">Tanda Tangan Asesor</label>
                                                    <br>
                                                    <?php if ($user->tanda_tangan != "") { ?>
                                                        <embed src="assets/document/tanda_tangan/asesor/<?= $user->tanda_tangan  ?>" height="330" width="350">
                                                    <?php } ?>
                                                    <input style="color:black" type="file" class="form-control" id="tanda_tangan" name="tanda_tangan">
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <br>

                                        <label for=""> <strong style="color:black; "> 2. Data Pekerjaan</strong> </label>
                                        <div class="row mt-3">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="institusi_perusahaan">Nama Instansi</label>
                                                    <input style="color:black" type="text" id="institusi_perusahaan" class="form-control" placeholder="Masukkan Nama Instansi Anda" name="institusi_perusahaan" oninput="this.value = this.value.toUpperCase()" value="<?= $user->institusi_perusahaan ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="nip">NIP</label>
                                                    <input style="color:black" type="text" id="nip" class="form-control" placeholder="Masukkan nomor NIP Anda" name="nip" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" maxlength="18" pattern="[0-9]{18}" title="18 angka" value="<?= $user->nip ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="pangkat_golongan">Pangkat/Golongan</label>
                                                    <select id="pangkat_golongan" name="pangkat_golongan" class="form-select" id="pangkat_golongan" >
                                                            <option value="" disabled selected>-- Pilih Pangkat / Golongan --</option>
                                                            <?php foreach ($pangkat_golongan as $row) { ?>
                                                                <option value="<?= $row->id ?>" <?= $user->pangkat_golongan == $row->id ? "selected" : "" ?>><?= $row->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="jabatan">Jabatan</label>
                                                    <input style="color:black" type="text" id="jabatan" class="form-control" name="jabatan" oninput="this.value = this.value.toUpperCase()" value="<?= $user->jabatan ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <br>

                                        <label for=""> <strong style="color:black; "> 3. Data Sertifikat Kompetensi Asesor</strong></label>
                                        <div class="row mt-3">
                                            <div class="col-md-12 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="no_reg">Nomor Registrasi</label>
                                                    <input style="color:black" type="text" id="no_reg" class="form-control" placeholder="Masukkan Nomor Registrasi" name="no_reg" oninput="formatingNoReg(this)" value="<?= $user->no_reg ?>" />
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="no_sertif">Nomor Sertifikat</label>
                                                    <input style="color:black" type="text" id="no_sertif" class="form-control" placeholder="Masukkan Nomor Sertifikat" name="no_sertif" value="<?= $user->no_sertif ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="no_blanko">Nomor Blanko</label>
                                                    <input style="color:black" type="text" id="no_blanko" class="form-control" placeholder="Masukkan Nomor Blanko" name="no_blanko" value="<?= $user->no_blanko ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="tgl_sertif">Tanggal Sertifikat</label>
                                                    <input style="color:black" type="date" id="tgl_sertif" class="form-control" placeholder="Masukkan no. rekening bank LSP" name="tgl_sertif" value="<?= $user->tgl_sertif ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="tgl_expired">Tanggal Sertifikat Berakhir</label>
                                                    <input style="color:black" type="date" id="tgl_expired" class="form-control" placeholder="Masukkan no. rekening bank LSP" name="tgl_expired" value="<?= $user->tgl_expired ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="sertif_asesor">Dokumen Sertifikasi Asesor</label>
                                                    <br>
                                                    <?php if ($user->sertif_asesor != "") { ?>
                                                        <embed src="assets/document/sertif_asesor/<?= $user->sertif_asesor  ?>" height="330" width="350">
                                                    <?php } ?>
                                                    <input style="color:black" type="file" class="form-control" id="sertif_asesor" name="sertif_asesor">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="sertif_teknis">Dokumen Sertifikasi Teknis **</label>
                                                    <br>
                                                    <?php if ($user->sertif_teknis != "") { ?>
                                                        <embed src="assets/document/sertif_teknis/<?= $user->sertif_teknis  ?>" height="330" width="350">
                                                    <?php } ?>
                                                    <input style="color:black" type="file" class="form-control" id="sertif_teknis" name="sertif_teknis">
                                                </div>
                                            </div>
                                            <small class="text-danger">**Jadikan satu dokumen JPG / PDF untuk seluruh kompetensi teknis yang dimiliki.</small>
                                        </div>

                                        <div class="col-12 d-flex justify-content-end mt-4">
                                            <button type="submit" class="btn btn-primary me-1 mb-1">
                                                Simpan
                                            </button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>


<script>
    $("#provinsi").change(function(event) {
        let valProvinsi = document.getElementById("provinsi").value;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "getKabKota",
            type: "post",
            data: {
                valProvinsi: valProvinsi,
                panjang: 5
            },
            dataType: "json",
            success: function(data) {
                let valKabKota = "";
                data.forEach(element => {
                    valKabKota += `
                    <option value="${element.kode}">${element.nama}</option>
                    `
                });
                $("#kab_kota").html(valKabKota);
            },
        });
    });
</script>

<script>
        $(document).ready(function() {
            $('#no_hp').mask('+(62) 999-9999-9999');
            $('#nomor_npwp').mask('99.999.999.9-999.999');
            $('#no_reg').mask('MET.999.9999999 9999');
            $('#no_sertif').mask('No. 99999 9999 9999999 9999');
            $('#no_blanko').mask('9999999');

            var no_hp = $('no_hp').val();
            var nomor_npwp = $('nomor_npwp').val();
            var no_reg = $('no_reg').val();
            var no_sertif = $('no_sertif').val();
            var no_blanko = $('no_blanko').val();
        });
</script>
@endsection