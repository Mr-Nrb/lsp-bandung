@extends('template_dashboard.main')
@section('isiDashboard')
<link rel="stylesheet" href="{{ asset('assets/css') }}/ia11-lsp.css">
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <!-- ppppppppppppppppppppppppppppp -->
                                    <div id="content" class="invoice-box" style="color: black;">
                                    <div class="table-responsive">
                                        <h3>FR.IA.11. MENINJAU INSTRUMEN ASESMEN</h3>

                                        <form class="form" action="/savemia11" enctype="multipart/form-data" method="POST">
                                            @csrf
                                            <table class="table">
                                                <tr>
                                                    <input type="hidden" name="idJadwalAsesmen" id="idJadwalAsesmen" value="{{ $data_apl01->id_jadwal_asesmen }}">
                                                    <td><label for="judul">Judul</label></td>
                                                    <td>:</td>
                                                    <td style="padding-right:200px;"> <input type="hidden" name="juduljadwal" id="juduljadwal" value="{{ $data_apl01->skema }}"> {{ $data_apl01->skema }}</td>
                                                </tr>
                                                <tr>
                                                    <td><label for="nomor">Nomor</label></td>
                                                    <td>:</td>
                                                    <td> <input type="hidden" name="nomor" id="nomor" value="{{ $data_apl01->nomor }}"> {{ $data_apl01->nomor }}
                                                         <input type="hidden" name="no_reg" id="no_reg" value="{{ $data_apl01->no_Reg }}" hidden>       
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="nama">Nama Asesi</label></td>
                                                    <td>:</td>
                                                    <td> <input type="hidden" name="nama_asesi" id="nama_asesi" value="{{ $data_apl01->nama_asesi }}"> {{ $data_apl01->nama_asesi }} </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="namasor">Nama Asesor</label></td>
                                                    <td>:</td>
                                                    <td> <input type="hidden" name="namaasesor" id="namaasesor" value="{{ $data_apl01->namaasesor }}"> {{ $data_apl01->namaasesor }} </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="tanggalasesmen">Tanggal</label></td>
                                                    <td>:</td>
                                                    <td> <input type="hidden" name="tanggalasesmen" id="tanggalasesmen" value="{{ $data_apl01->tanggalasesmen }}"> {{ $data_apl01->tanggalasesmen }} </td>
                                                </tr>
                                                
                                            </table>


                                            <table class="table2" style="border: 1px solid black;  height:100px;" border-collapse="collapse">
                                                <tr  style="border: 1px solid black;" >
                                                    <th style="background:#fac090;" class="text-center">PANDUAN BAGI PENINJAU/PENYELIA</th>
                                                </tr>
                                                <tr>
                                                  <td>
                                              
                                                    <ul>
                                                      <li>Isilah tabel ini sesuai dengan informasi sesuai pertanyaan/pernyataan dalam table dibawah ini.</li>
                                                      <li>Beri tanda centang  pada hasil penilaian instrumen asesmen berdasarkan tinjauan anda dengan jastifikasi professional anda</li>
                                                      <li>Berikan komentar dengan jastifikasi profesional anda</li>
                                                    </ul>
                                              
                                                  </td>
                                                </tr>
                                              </table>
                                              <br>

                                              <div id="formsbs">
                                                <p><strong>Daftar Unit Kompetensi sesuai kemasan:</strong></p>
                                                <table class="table3" style="border: 1px solid black;" border-collapse="collapse">
                                                    <tr style="border: 1px solid black;" class="text-center">
                                                        <th style="background:#fac090;">No.</th>
                                                        <th style="background:#fac090;">Kode Unit</th>
                                                        <th style="background:#fac090;">Judul Unit</th>
                                                    </tr>
                                                    <?php $i = 1;
                                                    foreach ($get_skema as $row) : ?>
                                                        <tr style="border: 1px solid black;" class="table6">
                                                            <td style="border: 1px solid black;" class="text-center">
                                                                <label for="noUrut"><?= $i ?></label>
                                                            </td>
                                                            <td style="border: 1px solid black;" class="text-center">
                                                                <output id="no_uk" name="no_uk" class=" bg2" value="<?= $row->kode ?>"><?= $row->kode ?></output>
                                                            </td>
                                                            <td style="border: 1px solid black;">
                                                                <output value="<?= $row->nama ?>" id="uk" name="uk<?= $i ?>" class=" bg2"><?= $row->nama ?></output>
                                                            </td>
                                                        </tr>
                                                    <?php $i++;
                                                    endforeach; ?>
                                                </table>
                                            </div>
                                            <br>

                                            <table class="table4" style="border: 1px solid black;" border-collapse="collapse" >

                                                <tr class="text-center">
                                                  <th>Kegiatan Asesmen</th>
                                                  <th colspan="2">Ya/Tidak</th>
                                                  <th>Komentar</th>
                                                </tr>
                                                <tr>
                                                  <td style="width:500px;">Instruksi perangkat asesmen dan kondisi asesmen diidentifikasi dengan jelas</td>

                                                  <td style="border: 1px solid black;">
                                                  <div class="form-check">
                                                    <input class="form-check-input" type="radio" value="Ya" name="instruksi_jelas" id="instruksi_jelas1">
                                                    <label class="form-check-label" for="instruksi_jelas1">
                                                        Ya
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" value="Tidak" name="instruksi_jelas" id="instruksi_jelas2" >
                                                    <label class="form-check-label" for="instruksi_jelas2">
                                                        Tidak 
                                                    </label>
                                                </div>
                                                  </td>

                                                  <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>

                                                </tr>
                                                <tr>
                                                  <td>Informasi tertulis dituliskan secara tepat</td>

                                                   <td style="border: 1px solid black;">
                                                  <div class="form-check">
                                                    <input class="form-check-input" type="radio" value="Ya" name="informasi_tepat" id="informasi_tepat1">
                                                    <label class="form-check-label" for="informasi_tepat1">
                                                        Ya
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" value="Tidak" name="informasi_tepat" id="informasi_tepat2" >
                                                    <label class="form-check-label" for="informasi_tepat2">
                                                        Tidak 
                                                    </label>
                                                </div>
                                                  </td>

                                                  <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                </tr>
                                                <tr>
                                                  <td>Kegiatan asesmen membahas persyaratan bukti untuk kompetensi atau kompetensi yang diakses</td>
                                                  <td style="border: 1px solid black;">
                                                    <div class="form-check">
                                                      <input class="form-check-input" type="radio" value="Ya" name="kegiatan_asesmen" id="kegiatan_asesmen1">
                                                      <label class="form-check-label" for="kegiatan_asesmen1">
                                                          Ya
                                                      </label>
                                                  </div>
                                                  <div class="form-check">
                                                      <input class="form-check-input" type="radio" value="Tidak" name="kegiatan_asesmen" id="kegiatan_asesmen2" >
                                                      <label class="form-check-label" for="kegiatan_asesmen2">
                                                          Tidak 
                                                      </label>
                                                  </div>
                                                    </td>
  
                                                    <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                </tr>
                                                <tr>
                                                  <td>Tingkat kesulitan bahasa, literasi, dan berhitung sesuai dengan tingkat unit kompetensi yang dinilai</td>
                                                  <td style="border: 1px solid black;">
                                                    <div class="form-check">
                                                      <input class="form-check-input" type="radio" value="Ya" name="kesulitan_bahasa" id="kesulitan_bahasa1">
                                                      <label class="form-check-label" for="kesulitan_bahasa1">
                                                          Ya
                                                      </label>
                                                  </div>
                                                  <div class="form-check">
                                                      <input class="form-check-input" type="radio" value="Tidak" name="kesulitan_bahasa" id="kesulitan_bahasa2" >
                                                      <label class="form-check-label" for="kesulitan_bahasa2">
                                                          Tidak 
                                                      </label>
                                                  </div>
                                                    </td>
  
                                                    <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                </tr>
                                                <tr>
                                                  <td>Tingkat kesulitan kegiatan sesuai dengan kompetensi atau kompetensi yang diases.</td>
                                                  <td style="border: 1px solid black;">
                                                    <div class="form-check">
                                                      <input class="form-check-input" type="radio" value="Ya" name="kesulitan_kegiatan" id="kesulitan_kegiatan1">
                                                      <label class="form-check-label" for="kesulitan_kegiatan1">
                                                          Ya
                                                      </label>
                                                  </div>
                                                  <div class="form-check">
                                                      <input class="form-check-input" type="radio" value="Tidak" name="kesulitan_kegiatan" id="kesulitan_kegiatan2" >
                                                      <label class="form-check-label" for="kesulitan_kegiatan2">
                                                          Tidak 
                                                      </label>
                                                  </div>
                                                    </td>
  
                                                    <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                </tr>
                                                <tr>
                                                  <td>Contoh, benchmark dan / atau ceklis asesmen tersedia untuk digunakan dalam pengambilan keputusan asesmen.</td>
                                                  <td style="border: 1px solid black;">
                                                    <div class="form-check">
                                                      <input class="form-check-input" type="radio" value="Ya" name="ceklis_asesmen" id="ceklis_asesmen1">
                                                      <label class="form-check-label" for="ceklis_asesmen1">
                                                          Ya
                                                      </label>
                                                  </div>
                                                  <div class="form-check">
                                                      <input class="form-check-input" type="radio" value="Tidak" name="ceklis_asesmen" id="ceklis_asesmen2" >
                                                      <label class="form-check-label" for="ceklis_asesmen2">
                                                          Tidak 
                                                      </label>
                                                  </div>
                                                    </td>
  
                                                    <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                </tr>
                                                <tr>
                                                  <td>Diperlukan modifikasi (seperti yang diidentifikasi dalam Komentar)</td>
                                                  <td style="border: 1px solid black;">
                                                    <div class="form-check">
                                                      <input class="form-check-input" type="radio" value="Ya" name="modifikasi" id="modifikasi1">
                                                      <label class="form-check-label" for="modifikasi1">
                                                          Ya
                                                      </label>
                                                  </div>
                                                  <div class="form-check">
                                                      <input class="form-check-input" type="radio" value="Tidak" name="modifikasi" id="modifikasi2" >
                                                      <label class="form-check-label" for="modifikasi2">
                                                          Tidak 
                                                      </label>
                                                  </div>
                                                    </td>
  
                                                    <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                </tr>
                                                <tr>
                                                  <td>Tugas asesmen siap digunakan</td>
                                                  <td style="border: 1px solid black;">
                                                    <div class="form-check">
                                                      <input class="form-check-input" type="radio" value="Ya" name="tugas_asesmen" id="tugas_asesmen1">
                                                      <label class="form-check-label" for="tugas_asesmen1">
                                                          Ya
                                                      </label>
                                                  </div>
                                                  <div class="form-check">
                                                      <input class="form-check-input" type="radio" value="Tidak" name="tugas_asesmen" id="tugas_asesmen2" >
                                                      <label class="form-check-label" for="tugas_asesmen2">
                                                          Tidak 
                                                      </label>
                                                  </div>
                                                    </td>
  
                                                    <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                </tr>
                                              
                                              </table>
                                              <br>
                                              <br>

                                              <table class="table5" style="border: 1px solid black;" border-collapse="collapse">
                                                <tr style="border: 1px solid black;" class="text-center">
                                                  <th>Nama Peninjau</th>
                                                  <th>Tanda Tangan Peninjau</th>
                                                  <th>Komentar</th>
                                                </tr>
                                                <tr>
                                                  <td style="border: 1px solid black;"> <div class="text-center"> <input class="form-control"  type="text" name="peninjau" value="{{ $user->name }}" readonly> </div> </td>

                                                  <td style="width:38%; "><div class="text-center"><img id="gambarTandaTangan" src="{{ asset('assets/document/tanda_tangan/penyelia') }}/<?= $user->tanda_tangan ?>" style="width: 380px" height="380px"> <input type="hidden" id="ttdpenyelia" name="ttdpenyelia" value="<?= $user->tanda_tangan ?>"> </div> </td>

                                                  <td style="border: 1px solid black;" > <div class="text-center"> <input class="form-control-lg" type="text" name="komentar"> </div></td>
                                                </tr>
                                                </table>
                                                <br>
                                                <br>
                                                <input type="submit" class="btn btn-primary" value="Kirim" />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



@endsection