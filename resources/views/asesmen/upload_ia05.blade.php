@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    <?= $title_sub_menu; ?>
                                </h4>
                                <hr>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <button type="button" class="btn btn-outline-primary block" data-bs-toggle="modal" data-bs-target="#default">
                                        <i class="bi bi-plus-circle-fill"></i> Tambah Data
                                    </button>
                                    <div class="table-responsive">
                                        <table id="tbl_monitoring_apl01" class="table display">
                                            <thead>
                                                <tr>
                                                    <th>Action</th>
                                                    <th>Tanggal Uji</th>
                                                    <th>Nama Jadwal</th>
                                                    <th>Nama TUK</th>
                                                    <th>Skema</th>
                                                    <th>No. Registrasi Asesor</th>
                                                    <th>Nama Dokumen</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                <tr>
                                                    
                                                </tr>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!--Basic Modal -->
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel1">Upload Soal Praktik Demonstrasi</h5>
                <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close">
                    <i data-feather="x">X</i>
                </button>
            </div>
            <div class="modal-body">
            <div class="card-content">
                    <div class="card-body">
                        <form id="form_crud" class="form" action="/saveUpload_ia05" enctype="multipart/form-data" method="POST">
                            @csrf
                            <input type="hidden" name="id" id="id">
                            <div class="row mt-3">
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="nama_jadwal">Nama Jadwal</label>
                                        <select id="nama_jadwal" name="nama_jadwal" class="form-select">
                                            <option value="">Pilih Nama Jadwal</option>
                                            <?php foreach ($nama_jadwal as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->nama_jadwal ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12" hidden>
                                        <div class="form-group">
                                            <label style="color:black" for="id_jadwal_asesor">ID Jadwal Asesor</label>
                                            <input style="color:black" type="text" id="id_jadwal_asesor" class="form-control" name="id_jadwal_asesor" readonly />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label style="color:black" for="tanggal_uji">Tanggal Uji</label>
                                            <input style="color:black" type="text" id="tanggal_uji" class="form-control" name="tanggal_uji" readonly />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label style="color:black" for="tuk">Nama TUK</label>
                                            <input style="color:black" type="text" id="tuk" class="form-control" name="tuk" readonly />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label style="color:black" for="skema">Skema</label>
                                            <input style="color:black" type="text" id="skema" class="form-control" name="skema" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label style="color:black" for="nomor">Nomor Skema</label>
                                            <input style="color:black" type="text" id="nomor" class="form-control" name="nomor" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label style="color:black" for="nama_Asesor">Nama Asesor</label>
                                            <input style="color:black" type="text" id="nama_Asesor" class="form-control" name="nama_Asesor" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label style="color:black" for="no_reg">No. Registrasi Asesor</label>
                                            <input style="color:black" type="text" id="no_reg" class="form-control" name="no_reg" readonly/>
                                        </div>
                                    </div>
                            </div>
                            <br>
                            <div class="row mt-3">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label style="color:black" for="upload_ia05">Upload Dokumen</label>
                                        <br>
                                            @foreach($ia05 as $d) 
                                                <img src="assets/document/upload_ia05/asesor/{{$d->nama_dokumen}}" height="330" width="350">
                                            @endforeach
                                            <input style="color:black" type="file" class="form-control" id="nama_dokumen" name="nama_dokumen">
                                    </div>
                                </div>


                            </div>
                            
                            <div class="col-12 d-flex justify-content-end mt-4" id="button_">
                                <button type="submit" class="btn btn-primary me-1 mb-1">
                                    Simpan
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/asesmen/upload_ia05.js') }}"></script>


@endsection