@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">

                                    <div class="table-responsive">
                                        <label for=""> <strong>*Klik Data Jadwal Untuk Menuju Ke IA11</strong> </label>
                                        <br>
                                        <br>
                                        <table id="jadwalLanjut" class="table">
                                            <thead>
                                                <tr>
                                                    <th>ActionButton</th>
                                                    <th>Tanggal Asesmen</th>
                                                    <th>Nama Jadwal</th>
                                                    <th>Nama TUK</th>
                                                    <th>Nama Skema</th>
                                                    <th>Nomor Skema</th>
                                                    <th>Nama Asesor</th>
                                                    <th>Nama Asesi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                               @foreach ($data_apl01 as $row)
                                                    <tr>
                                                        <td>
                                                            <div style="margin-left:15%">
                                                                <a href="/mia011/{{ $row->id }}/{{ $row->id_jadwal_asesmen }}" class="btn btn-outline-primary btn-sm block">Pilih</a>
                                                            </div>
                                                        </td>
                                                        <td> {{ $row->tanggalasesmen }}</td>
                                                        <td>{{ $row->juduljadwal }}</td>
                                                        <td>{{ $row->tuk }}</td>
                                                        <td>{{ $row->skema }}</td>
                                                        <td> {{ $row->nomor }}</td>
                                                        <td>{{ $row->namaasesor}} </td>
                                                        <td>{{ $row->nama_asesi}} </td>
                                                    </tr>
                                              @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>




@endsection