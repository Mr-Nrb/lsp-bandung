<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>BLK Bandung</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link rel="stylesheet" href="{{ asset('assets/template/mazer-main/dist') }}/assets/css/bootstrap.css">
    <link href="{{ asset('assets/assetsHomePage/assets') }}/img/favicon.png" rel="icon">
    <link href="{{ asset('assets/assetsHomePage/assets') }}/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/assetsHomePage/assets') }}/vendor/aos/aos.css" rel="stylesheet">
    <link href="{{ asset('assets/assetsHomePage/assets') }}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('assets/assetsHomePage/assets') }}/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="{{ asset('assets/assetsHomePage/assets') }}/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="{{ asset('assets/assetsHomePage/assets') }}/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="{{ asset('assets/assetsHomePage/assets') }}/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/assetsHomePage/assets') }}/css/style.css" rel="stylesheet">

</head>

<body>

    <!-- ======= Top Bar ======= -->
    <section id="topbar" class="d-flex align-items-center">
        <div class="container d-flex justify-content-center justify-content-md-between">
            <div class="contact-info d-flex align-items-center">
                <i class="bi bi-envelope d-flex align-items-center"><a href="mailto:pbk.blkbandung@gmail.com">pbk.blkbandung@gmail.com</a></i>
                <i class="bi bi-phone d-flex align-items-center ms-4"><span>(022) 731 2564</span></i>
            </div>
            <div class="social-links d-none d-md-flex align-items-center">
                <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
                <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
                <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
                <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></i></a>
            </div>
        </div>
    </section>

    <!-- ======= Header ======= -->
    <header id="header" class="d-flex align-items-center" style="background-color: #D0CBCB;">
        <div class="container d-flex justify-content-between">

            <div class="logo">
                <a href="https://demo.blk.web.id/view/web/index.php">
                    <img src="https://demo.blk.web.id/view/web/images/logo-bbplk.png" alt="logo-bbplk.png">
                    <span class="font-weight-bold" style="color:black;">LSP BBPVP Bandung</span>
                </a>
            </div>

            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto active" style="color:black;" href="<?= asset('') ?>">Beranda</a></li>
                    <li><a class="nav-link scrollto" style="color:black;" href="<?= asset('/galeri') ?>">Galeri</a></li>
                    <li class="dropdown"><a href="#" style="color:black;"><span>Informasi</span> <i class="bi bi-chevron-down"></i></a>
                        <ul>
                            <li><a href="<?= asset('/artikel') ?>">Artikel</a></li>
                            <li><a href="<?= asset('/berita') ?>">Berita</a></li>
                            <li><a href="#">Alumni</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#" style="color:black;"><span>Download</span> <i class="bi bi-chevron-down"></i></a>
                        <ul>
                            <li><a href="#">SKKNI</a></li>
                            <li><a href="#">Skema Sertifikasi</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#" style="color:black;"><span>Tentang Kami</span> <i class="bi bi-chevron-down"></i></a>
                        <ul>
                            <li><a href="#">Profile LSP</a></li>
                            <li><a href="#">Asesor Kompetensi</a></li>
                            <li><a href="#">Tempat Uji Kompetensi</a></li>
                            <li><a href="#">Rencana Kerja</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-link scrollto" style="color:black;" href="<?= asset('/login') ?>">Masuk/Daftar</a>
                    </li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav><!-- .navbar -->

        </div>
    </header><!-- End Header -->
    @yield('isiHomePage')
    <!-- ======= Footer ======= -->

    <hr>
    <footer>
        <div class="container">
            <div class="copyright">
                <p class="text-center"> Copyright &copy; <?php date('Y'); ?> - <strong><span>PT. QIA Solution.</span></strong> </p>
            </div>
        </div>
    </footer><!-- End Footer -->

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/assetsHomePage/assets') }}/vendor/purecounter/purecounter_vanilla.js"></script>
    <script src="{{ asset('assets/assetsHomePage/assets') }}/vendor/aos/aos.js"></script>
    <script src="{{ asset('assets/assetsHomePage/assets') }}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('assets/assetsHomePage/assets') }}/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="{{ asset('assets/assetsHomePage/assets') }}/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="{{ asset('assets/assetsHomePage/assets') }}/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="{{ asset('assets/assetsHomePage/assets') }}/vendor/php-email-form/validate.js"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('assets/assetsHomePage/assets') }}/js/main.js"></script>

</body>

</html>