@extends('template_home_page.main')
@section('isiHomePage')
<main id="main">
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container">

            <div class="section-title" data-aos="zoom-in">
                <h3>Galeri <span>Kami</span></h3>
                <p>Definisi Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p>
            </div>

            <div class="card-group">
                <?php foreach ($data_galeri as $row) : ?>
                    <div class="card p-1" style="cursor: pointer;">
                        <h5 class="card-title text-center"><?= $row->judul_galeri ?></h5>
                        <img class="card-img-top" style="width:100%;height:220px;" src="{{ asset('assets/document/home_page/galeri')}}/<?= $row->gambar ?>" alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text"><small class="text-muted">Tanggal : <?= $row->tanggal_kegiatan ?></small></p>
                            <p class="card-text"> <?= $row->keterangan ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

        </div>
    </section><!-- End Contact Section -->

</main><!-- End #main -->
@endsection