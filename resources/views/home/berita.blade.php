@extends('template_home_page.main')
@section('isiHomePage')
<main id="main">
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container">

            <div class="section-title" data-aos="zoom-in">
                <h3>Informasi /<span> Berita</span></h3>
                <p>Definisi Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p>
            </div>


            <div class="col-lg-12">
                <?php foreach ($data_berita as $row) : ?>
                    <div class="card">
                        <div class="card-body">
                            <div style="text-align: center;">
                                <h2 class="card-title"><?= $row->judul_berita ?></h2>

                            </div>
                            <br>

                            <img style=" display: block; margin-left: auto; margin-right: auto;" src="{{ asset('assets/document/home_page/berita')}}/<?= $row->gambar ?>" width="60%" class="img-fluid" alt="Responsive image">
                            <br>
                            <br>
                            <p> Tanggal : <?= $row->tanggal_berita ?> </p>
                            <p style="text-align: justify; text-justify: inter-word;">
                                <?= $row->keterangan ?>
                            </p>
                        </div>
                    </div>
                    <br>
                <?php endforeach; ?>
            </div>


        </div>
    </section><!-- End Contact Section -->

</main><!-- End #main -->
@endsection