@extends('template_home_page.main')
@section('isiHomePage')
<main id="main">
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container">

            <div class="section-title" data-aos="zoom-in">
                <h3>Informasi /<span> Artikel</span></h3>
                <p>Definisi Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p>
            </div>


            <div class="row col-lg-12">
                <div class="col-lg-3">
                    <div class="card" style="border: none;">
                        <label class="card-header">Tema</label>
                        <div class="card-body" style="cursor: pointer;">
                            <h5 class="card-title">JUDUL ARTIKEL</h5>
                            <p> <small class="text-muted">Tanggal : 10 Agustus 2022</small> </p>
                            <p> <small class="text-muted">Oleh : Dr. Budi Santoso</small> </p>
                        </div>
                    </div>
                    <br>
                    <div class="card" style="border: none;">
                        <label class="card-header">Tema</label>
                        <div class="card-body" style="cursor: pointer;">
                            <h5 class="card-title">JUDUL ARTIKEL</h5>
                            <p> <small class="text-muted">Tanggal : 10 Agustus 2022</small> </p>
                            <p> <small class="text-muted">Oleh : Dr. Budi Santoso</small> </p>
                        </div>
                    </div>
                    <br>
                    <div class="card" style="border: none;">
                        <label class="card-header">Tema</label>
                        <div class="card-body" style="cursor: pointer;">
                            <h5 class="card-title">JUDUL ARTIKEL</h5>
                            <p> <small class="text-muted">Tanggal : 10 Agustus 2022</small> </p>
                            <p> <small class="text-muted">Oleh : Dr. Budi Santoso</small> </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-body">
                            <div style="text-align: center;">
                                <h4 class="card-title">JUDUL ARTIKEL DILETAKKAN DISINI</h4>
                                <p> <small class="text-muted">Oleh : Dr. Budi Santoso</small> </p>
                            </div>
                            <br>
                            <p> <small class="text-muted">Bandung, 12/09/2022</small> </p>
                            <img src="{{ asset('assets/assetsHomePage/assets') }}/img/mesin.jpg" class="img-fluid" alt="Responsive image">
                            <br>
                            <br>

                            <p style="text-align: justify; text-justify: inter-word;">
                                <?php $text = "Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini.
                            Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini
                            Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini
                            Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini
                            Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini
                            Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini Isi artikel lengkap disiniIsi artikel lengkap disini";

                                $string = $text;
                                if (strlen($string) > 500) {

                                    // truncate string
                                    $stringCut = substr($string, 0, 500);
                                    $endPoint = strrpos($stringCut, ' ');

                                    //if the string doesn't contain any space then it will cut without word basis.
                                    $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                    $string .= '... <p class="text-right" ><a href="" class="text-primary"> Lanjut Membaca >></a></p>';
                                }
                                echo $string; ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section><!-- End Contact Section -->

</main><!-- End #main -->
@endsection