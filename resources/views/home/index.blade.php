@extends('template_home_page.main')
@section('isiHomePage')
<!-- ======= Hero Section ======= -->
<section id="hero">
    <div class="hero-container" data-aos="fade-up">
        <h1>LSP BBPVP Bandung</h1>
        <h2>Bersama Meraih SDM Kompeten</h2>
        <a href="<?= asset('/login') ?>" class="btn-get-started scrollto">Masuk / Daftar</a>
    </div>
</section><!-- End Hero -->

<main id="main">


    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us">
        <div class="container">
            <div class="section-title" data-aos="zoom-in">
                <h3>Apa itu <span>(LSP-P2) BBPLK Bandung</span> ?</h3>
                <p>(LSP-P2) BBPLK Bandung adalah lembaga pendukung BNSP (Badan Nasional Sertifikasi Profesi) yang bertanggung jawab melaksanakan sertifikasi kompetensi profesi.</p>
            </div>
        </div>
    </section><!-- End Why Us Section -->

    <!-- ======= Pricing Section ======= -->


    <div class="container">

        <div class="card-group">
            <?php foreach ($data_galeri as $row) : ?>
                <div class="card">
                    <img class="card-img-top" style="width:100%;height:260px;" src="{{ asset('assets/document/home_page/galeri')}}/<?= $row->gambar ?>" alt="Card image cap">
                </div>
            <?php endforeach; ?>

        </div>


    </div>



    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us">
        <div class="container">
            <div class="section-title mt-3" data-aos="zoom-in">
                <h3>Skema Sertifikasi</h3>
            </div>
            <div class="row">


                <?php $i = 1;
                foreach ($data_kejuruan as $row) : ?>
                    <div class="col-lg-3 mt-4 mt-lg-0" style="cursor:pointer" data-aos="fade-up" data-aos-delay="200">
                        <div class="box">
                            <span><?= $i ?></span>
                            <h4> <?= $row->nama ?></h4>
                            <a href="<?= asset("/detail_skema") ?>/<?= $row->id ?>" class="btn btn-secondary">Pilih Kejuruan</a>
                        </div>
                    </div>
                <?php $i++;
                endforeach; ?>



            </div>

        </div>
    </section><!-- End Why Us Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
        <div class="container">

            <div class="section-title" data-aos="zoom-in">
                <h3>Galeri & <span>Informasi</span></h3>

            </div>

            <div class="row">
                <div class="col-lg-6">
                    <h3 class="font-weight-bold"> Galeri</h3>
                    <hr>
                    <?php foreach ($data_galeri as $row) : ?>
                        <div class="card-group">
                            <div class="card" style="border: none;">
                                <div class="row">
                                    <div class="col-lg-4 mt-4">
                                        <img style=" display: block; margin-left: auto; margin-right: auto;" src="{{ asset('assets/document/home_page/galeri')}}/<?= $row->gambar ?>" width="100%" class="img-fluid" alt="Responsive image">
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="card-body">
                                            <h5 class="card-title"><?= $row->judul_galeri ?></h5>
                                            <p class="card-text"><?= $row->keterangan ?></p>
                                            <p class="card-text"><small class="text-muted">Tangaal : <?= $row->tanggal_kegiatan ?></small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>



                </div>
                <div class="col-lg-6">

                    <!-- ======= F.A.Q Section ======= -->
                    <section id="faq" class="faq">

                        <div class="section-title" data-aos="zoom-in">
                            <h3><span>Informasi</span></h3>

                        </div>

                        <div class="faq-list">
                            <ul>

                                <?php $i = 1;
                                foreach ($data_informasi as $row) : ?>
                                    <li data-aos="fade-up" data-aos-delay="100">
                                        <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-<?php $i ?>" class="collapsed"><?= $row->judul_informasi ?> <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                                        <div id="faq-list-<?php $i ?>" class="collapse" data-bs-parent=".faq-list">
                                            <p>
                                                <?= $row->keterangan ?>
                                            </p>
                                        </div>
                                    </li>
                                <?php $i++;
                                endforeach; ?>




                            </ul>
                        </div>

                    </section><!-- End F.A.Q Section -->


                </div>
            </div>

        </div>
    </section><!-- End Portfolio Section -->







    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container">

            <div class="section-title" data-aos="zoom-in">
                <h3>Kontak <span>Kami</span></h3>
                <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p>
            </div>

            <div class="row mt-5">
                <div class="col-lg-4" data-aos="fade-right">
                    <div class="info">
                        <div class="address">
                            <i class="bi bi-geo-alt"></i>
                            <h4>Location:</h4>
                            <p>Alamat: Jl. Jenderal Gatot Subroto No. 170 Bandung</p>
                        </div>

                        <div class="email">
                            <i class="bi bi-envelope"></i>
                            <h4>Email:</h4>
                            <p>pbk.blkbandung@gmail.com</p>
                        </div>

                        <div class="phone">
                            <i class="bi bi-phone"></i>
                            <h4>Call:</h4>
                            <p>(022) 731 2564 / 0856-5928-0995 (Yusnita)</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 mt-5 mt-lg-0" data-aos="fade-left">
                    <div>
                        <iframe style="border:0; width: 100%; height: 270px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.681739473354!2d107.63558041384326!3d-6.928592394993961!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e876408ef877%3A0x5e629876fdb42c9!2sJl.%20Gatot%20Subroto%20No.170%2C%20Maleer%2C%20Kec.%20Batununggal%2C%20Kota%20Bandung%2C%20Jawa%20Barat%2040275!5e0!3m2!1sen!2sid!4v1662996733819!5m2!1sen!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                        <!-- <iframe style="border:0; width: 100%; height: 270px;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" allowfullscreen></iframe> -->
                    </div>
                </div>
            </div>

        </div>
    </section><!-- End Contact Section -->

</main><!-- End #main -->
@endsection