@extends('template_home_page.main')
@section('isiHomePage')
<main id="main">
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container">

            <div class="section-title" data-aos="zoom-in">
                <h3>List Kejuruan<span> <?= $nama_kejuruan ?> </span></h3>
            </div>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Nomor Skema</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1;
                    foreach ($data_skema as $row) : ?>
                        <tr>
                            <th scope="row"><?= $i ?></th>
                            <td><?= $row->nama ?></td>
                            <td><?= $row->nomor ?></td>
                            <td>
                                <a href="" class="btn btn-outline-success btn-sm block"><i class="bi bi-file-earmark-arrow-down"></i> Unduh</a>
                            </td>
                        </tr>
                    <?php $i++;
                    endforeach; ?>

                </tbody>
            </table>


        </div>
    </section><!-- End Contact Section -->

</main><!-- End #main -->
@endsection