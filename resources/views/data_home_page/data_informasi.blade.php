@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <button type="button" class="btn btn-outline-primary block" data-bs-toggle="modal" data-bs-target="#default">
                                        <i class="bi bi-plus-circle-fill"></i> Tambah Data
                                    </button>
                                    <div class="table-responsive">
                                        <table id="tblDataInformasi" class="table">
                                            <thead>
                                                <tr>
                                                    <th>ActionButton</th>
                                                    <th>Judul Informasi</th>
                                                    <th>Keterangan</th>
                                                    <th>Status Aktif</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!-- Basic Modal -->
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel1"> Form Data</h5>
                <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close" onclick="cancel();">
                    <i data-feather="x">X</i>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-content">
                    <div class="card-body">
                        <form id="form_crud" class="form" action="/saveDataInformasi" enctype="multipart/form-data" method="POST">
                            @csrf
                            <input type="hidden" id="id" name="id">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="judul_informasi">Judul Informasi</label>
                                        <input style="color:black" type="text" id="judul_informasi" class="form-control" name="judul_informasi" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="keterangan">Keterangan</label>
                                        <textarea style="color:black" type="text" id="keterangan" class="form-control" placeholder="Masukkan Keterangan Informasi" name="keterangan" rows="6"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="status_aktif">Status Aktif</label>
                                        <fieldset class="form-group">
                                            <select id="status_aktif" name="status_aktif" class="form-select">
                                                <?php foreach ($list_status_aktif as $row) { ?>
                                                    <option value="<?= $row->id ?>"><?= $row->nama_status_aktif ?></option>
                                                <?php } ?>
                                            </select>
                                        </fieldset>
                                    </div>
                                </div>

                            </div>


                            <div class="col-12 d-flex justify-content-end mt-4" id="button_">
                                <button type="submit" class="btn btn-primary me-1 mb-1">
                                    Simpan
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset('assets/js/data_home_page/data_informasi.js') }}"></script>


@endsection