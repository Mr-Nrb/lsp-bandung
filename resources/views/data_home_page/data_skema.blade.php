@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <button type="button" class="btn btn-outline-primary block" data-bs-toggle="modal"
                                        data-bs-target="#default">
                                        <i class="bi bi-plus-circle-fill"></i> Tambah Data
                                    </button>
                                    <div class="table-responsive">
                                        <table id="tblDataSkema" class="table">
                                            <thead>
                                                <tr>
                                                    <th>ActionButton</th>
                                                    <th>Nama Kejuruan</th>
                                                    <th>Nama Skema</th>
                                                    <th>Dokuemn</th>
                                                    <th>Status Aktif</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!-- Basic Modal -->
<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel1"> Form Data</h5>
                <button type="button" class="close rounded-pill" data-bs-dismiss="modal" aria-label="Close"
                    onclick="cancel();">
                    <i data-feather="x">X</i>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-content">
                    <div class="card-body">
                        <form id="form_crud" class="form" action="/saveDataSkema" enctype="multipart/form-data"
                            method="POST">
                            @csrf
                            <input type="hidden" id="id" name="id">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="skema_kejuruan">Kejuruan</label>
                                        <fieldset class="form-group">
                                            <select id="skema_kejuruan" name="skema_kejuruan" class="form-select">
                                                <option value="">-- Pilih Kejuruan --</option>
                                                <?php foreach ($list_kejuruan as $row) { ?>
                                                <option value="<?= $row->kode ?>"><?= $row->nama_kejuruan ?></option>
                                                <?php } ?>
                                            </select>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="skema">Skema</label>
                                        <fieldset class="form-group">
                                            <select id="skema" name="skema" class="form-select">
                                                <option value="">-- Pilih Skema --</option>
                                            </select>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="gambar">Dokumen Skema</label>
                                        <img name="riview_gambar" id="riview_gambar" width="100%">
                                        <input style="color:black" type="file" class="form-control" id="gambar"
                                            name="gambar">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="color:black" for="status_aktif">Status Aktif</label>
                                        <fieldset class="form-group">
                                            <select id="status_aktif" name="status_aktif" class="form-select">
                                                <?php foreach ($list_status_aktif as $row) { ?>
                                                <option value="<?= $row->id ?>"><?= $row->nama_status_aktif ?></option>
                                                <?php } ?>
                                            </select>
                                        </fieldset>
                                    </div>
                                </div>

                            </div>


                            <div class="col-12 d-flex justify-content-end mt-4" id="button_">
                                <button type="submit" class="btn btn-primary me-1 mb-1">
                                    Simpan
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset('assets/js/data_home_page/data_skema.js') }}"></script>


@endsection