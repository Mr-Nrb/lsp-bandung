@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <label for=""> <strong> *Pilih Asesi</strong></label>
                                        <br>
                                        <br>

                                        <input type="text" id="id" value="<?= $data_apl01[0]->id?>" hidden>
                                        <input type="text" id="id_jadwal" value="<?= $id_jadwal?>" hidden>
                                        <input type="text" id="token_asesor" value="<?= $token_asesor?>" hidden>
                                        <table id="tblFrakak02_lanjut" class="table">
                                            <thead>
                                                <tr>
                                                    <th>ActionButton</th>
                                                    <th>Tanggal Asesmen</th>
                                                    <th>Nama Asesi</th>
                                                    <th>Nama Asesor</th>
                                                    <th>Nama Jadwal</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($ambil_asesi as $key)
                                                <tr>
                                                    <td>
                                                        <div style="margin-left:15%">
                                                            <a href="/formFrakak02/{{ $key->id_jadwal_asesmen }}/{{$key->id}}"
                                                                class="btn btn-outline-primary btn-sm block">Pilih
                                                            </a>
                                                        </div>
                                                    </td>
                                                    <td>{{$key->tanggalasesmen}}</td>
                                                    <td>{{$key->nama_asesi}}</td>
                                                    <td>{{$key->namaasesor}}</td>
                                                    <td>{{$key->juduljadwal}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>


<!-- <script src="{{ asset('assets/js/uji_kompetensi/frakak02_lanjut.js') }}"></script> -->


@endsection