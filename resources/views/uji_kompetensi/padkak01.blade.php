@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>  
        </a>
    </header>

    <div class="page-heading">  
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <input type="hidden" id="idJadwalAsesmen" name="idJadwalAsesmen" value="<?= $data_apl01[0]->id_jadwal_asesmen ?>">
                                    <div class="table-responsive">
                                        <label for=""> <strong> *Pilih Jadwal Asesmen</strong></label>
                                        <br>
                                        <br>
                                        <table id="tblPadkak01" class="table">
                                            <thead>
                                                <tr>
                                                    <th>ActionButton</th>
                                                    <th>Tanggal Uji</th>
                                                    <th>Nama Jadwal</th>
                                                    <th>TUK</th>
                                                    <th>Tema</th>
                                                    <th>Skema</th>
                                                    <th>Nomor</th>
                                                    <th>Lokasi Pengujian</th>
                                                    <th>Tanggal Mulai Pelatihan</th>
                                                    <th>Tanggal Akhir Pelatihan</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>


<script src="{{ asset('assets/js/uji_kompetensi/padkak01.js') }}"></script>


@endsection