@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <div id="content" class="invoice-box">
                                        <h3>FR.AK.01. PERSETUJUAN ASESMEN DAN KERAHASIAAN</h3>
                                        <div>
                                            <form class="form" action="/savePadkak01" enctype="multipart/form-data"
                                                method="POST">
                                                @csrf
                                                <table class="table">
                                                    <tr>
                                                        <th colspan="6"
                                                            style="background:#fac090; border: 1px solid grey;">
                                                            <p>
                                                                Persetujuan Asesmen ini untuk menjamin bahwa Asesi telah
                                                                diberi
                                                                arahan secara rinci tentang perencanaan dan proses
                                                                asesmen
                                                            </p>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" rowspan="2" class="judulrow">
                                                            Skema Sertifikasi (KKNI/Okupasi/Klaster)
                                                        </td>
                                                        <td style="border: 0px">
                                                            <label for="judul">Judul</label>
                                                        </td>
                                                        <td style="border: 0px">:</td>
                                                        <td colspan="3" style="border: 0px">
                                                            <input style="border:none" name="judul" id="judul"
                                                                value="<?= $data_apl01[0]->skema; ?>" readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px">
                                                            <label for="nomor">Nomor</label>
                                                        </td>
                                                        <td style="border: 0px">:</td>
                                                        <td style="border: 0px" colspan="3">
                                                            <input style="border:none" name="nomor" id="nomor"
                                                                value="<?= $data_apl01[0]->nomor; ?>" readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="border: 0px">
                                                            <label for="tuk">TUK</label>
                                                        </td>
                                                        <td style="border: 0px">:</td>
                                                        <td style="border: 0px" colspan="3" id="el-tuk">
                                                            <input style="border:none" name="tuk" id="tuk"
                                                                value="<?= $data_apl01[0]->tuk; ?>" readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" colspan="2">
                                                            <label for="namaasesor">Nama Asesor</label>
                                                        </td>
                                                        <td style="border: 0px">:</td>
                                                        <td style="border: 0px" colspan="3">
                                                            <input style="border:none" value="<?= $data_apl01[0]->namaasesor; ?>" disabled readonly />
                                                            <input type="hidden" name="nama_asesor" id="nama_asesor" value="<?= $data_apl01[0]->no_Reg; ?>">
                                                            <input type="hidden" name="id_jadwal" id="id_jadwal" value="<?= $data_apl01[0]->id_jadwal; ?>">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" colspan="2">
                                                            <label for="namaasesi">Nama Asesi</label>
                                                        </td>
                                                        <td style="border: 0px">:</td>
                                                        <td style="border: 0px" colspan="3">
                                                            <input style="border:none" name="nama_asesi" id="nama_asesi"
                                                                value="<?= $data_apl01[0]->nama_asesi; ?>" readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" colspan="2">Bukti yang dikumpulkan</td>
                                                        <td style="border: 0px">:</td>
                                                        <td style="border: 0px" colspan="3">
                                                            <ul class="list-unstyled mb-0">
                                                                <?php foreach ($list_bukti as $row) : ?>
                                                                <li class="d-inline-block me-2 mb-1">
                                                                    <div class="form-check">
                                                                        <div class="checkbox">
                                                                            <input type="checkbox"
                                                                                value="<?= $row->id ?>" name="bukti[]"
                                                                                id="bukti<?= $row->id ?>"
                                                                                class="form-check-input">
                                                                            <label
                                                                                for="checkbox1"><?= $row->nama_bukti ?></label>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <?php endforeach; ?>

                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <tr class="tip">
                                                        <td style="border: 0px" colspan="3"></td>
                                                        <td style="border: 0px" colspan="3">

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" rowspan="2" class="judulrow">
                                                            Pelaksanaan asesmen disepakati pada
                                                        </td>
                                                        <td style="border: 0px">
                                                            <label for="tanggal">Tanggal</label>
                                                        </td>
                                                        <td style="border: 0px">:</td>
                                                        <td style="border: 0px" id="el-tanggal">
                                                            <input id="tanggal" name="tanggal" style="border:none"
                                                                value="<?= $data_apl01[0]->tanggalasesmen; ?>"
                                                                readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px">
                                                            <label for="tuk2">Lokasi Uji</label>
                                                        </td>
                                                        <td style="border: 0px">:</td>
                                                        <td style="border: 0px">
                                                            <input id="lokasi_uji" name="lokasi_uji" style="border:none"
                                                                value="<?= $data_apl01[0]->lokasi_uji; ?>" readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" colspan="6">
                                                            <b>Asesi:</b> <br />
                                                            <p>
                                                                Bahwa Saya Sudah Mendapatkan Penjelasan Hak dan Prosedur
                                                                Banding
                                                                Oleh Asesor.
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" colspan="6">
                                                            <b>Asesor:</b> <br />
                                                            <p>
                                                                Menyatakan tidak akan membuka hasil pekerjaan yang saya
                                                                peroleh
                                                                karena penugasan saya sebagai Asesor dalam pekerjaan
                                                                Asesmen
                                                                kepada siapapun atau organisasi apapun selain kepada
                                                                pihak yang
                                                                berwenang sehubungan dengan kewajiban saya sebagai
                                                                Asesor yang
                                                                ditugaskan oleh LSP.
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" colspan="6">
                                                            <b>Asesi:</b> <br />
                                                            <p>
                                                                Saya setuju mengikuti asesmen dengan pemahaman bahwa
                                                                informasi
                                                                yang dikumpulkan hanya digunakan untuk pengembangan
                                                                profesional
                                                                dan hanya dapat diakses oleh orang tertentu saja.
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" colspan="3" style="height: 100px;">
                                                            <p>Tanda tangan Asesor dan tanggal:</p>
                                                            <input type="file" value="Piih File" id="ttd_asesor"
                                                                name="ttd_asesor"
                                                                <?= $role_id != 2  ? "disabled" : "" ?> />
                                                            <div id="el-tglttdasesor">
                                                                <input id="tgl_ttd_asesor" name="tgl_ttd_asesor"
                                                                    type="date" class="form-control"
                                                                    <?= $role_id != 2  ? "readonly" : "" ?> />
                                                            </div>
                                                            <br />
                                                        </td>
                                                        <td style="border: 0px" colspan="3" style="height: 100px;">
                                                            <p>Tanda tangan Asesi dan tanggal:</p>
                                                            <label for="ttd">Tanda Tangan Asesi:</label>
                                                            <div id="el-ttdasesi">
                                                                <img id="gambarTandaTangan"
                                                                    src="{{ asset('assets/document/tanda_tangan/asesi') }}/<?= $user->tanda_tangan ?>"
                                                                    style="width: 280px; display: block; margin-left: auto;margin-right: auto;">
                                                                <input type="hidden" id="ttd_asesi" name="ttd_asesi"
                                                                    value="<?= $user->tanda_tangan ?>">
                                                            </div>

                                                            <div id="el-tglttdasesi"><input id="tgl_ttd_asesi"
                                                                    name="tgl_ttd_asesi" type="text"
                                                                    class="form-control" value=" <?= Date('Y-m-d'); ?>"
                                                                    readonly /></div>
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                        </div>
                                        <br />
                                        <input type="submit" class="btn btn-primary" value="Kirim" id="kirim" />
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>
<!--Basic Modal -->

<!-- <script src="{{ asset('assets/js/uji_kompetensi/jadwal_lanjut.js') }}"></script> -->


@endsection