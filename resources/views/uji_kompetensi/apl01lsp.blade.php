@extends('template_dashboard.main')
@section('isiDashboard')
<link rel="stylesheet" href="{{ asset('assets/css') }}/apl01-lsp.css">  
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <!-- ppppppppppppppppppppppppppppp -->
                                    <div id="content" class="invoice-box" style="color: black;">


                                        <!-- <form enctype="multipart/form-data"> -->
                                        <h3>FR.APL.01. PERMOHONAN SERTIFIKASI KOMPETENSI</h3>

                                        <h4>Bagian 1 : Rincian Data Pemohon Sertifikasi</h4>
                                        <p>Pada bagian ini, cantumlah data pribadi, data pendidikan formal serta data pekerjaan anda pada saat ini</p>

                                        <h4>a. Data Pribadi</h4>
                                        <p class="wd text-danger">*WAJIB DIISI <br> FORM INPUT TIDAK MENGGUNAKAN SIMBOL KUTIP ( ' ) </p>
                                        <form class="form" action="/saveApl01Lsp" enctype="multipart/form-data" method="POST">
                                            @csrf
                                            <table class="table1">
                                                <tr>
    
                                                    <td><label for="nama">Nama lengkap</label></td>
                                                    <td>:</td>
                                                    <td><input id="nama_asesi" name="nama_asesi" style="border:none" value="<?= $user[0]->nama_lengkap ?>"> </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="nik">No. KTP/NIK/Paspor</label></td>
                                                    <td>:</td>
                                                    <td><input id="nik" name="nik" style="border:none" value="<?= $user[0]->nik ?>"></td>
                                                </tr>
                                                <tr>
                                                    <td><label for="tempatlahir">Tempat Lahir</label></td>
                                                    <td>:</td>
                                                    <td><input id="tempat_lahir" name="tempat_lahir" style="border:none" value="<?= $user[0]->tempat_lahir ?>"></td>
                                                </tr>
                                                <tr>
                                                    <td><label for="tanggallahir">Tanggal Lahir</label></td>
                                                    <td>:</td>
                                                    <td><input id="tanggal_lahir" name="tanggal_lahir" style="border:none" value="<?= $user[0]->tanggal_lahir ?>"> </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="jeniskelamin">Jenis Kelamin</label></td>
                                                    <td>:</td>
                                                    <td><input id="nama_jenis_kelamin" name="nama_jenis_kelamin" style="border:none" value="<?= $user[0]->nama_jenis_kelamin ?>"> </td>
                                                <tr>
                                                    <td><label for="kebangsaan">Kebangsaan</label></td>
                                                    <td>:</td>
                                                    <td><input id="nama_kebangsaan" name="nama_kebangsaan" style="border:none" value="<?= $user[0]->nama_kebangsaan ?>"> </td>
                                                <tr>
                                                    <td><label for="alamat">Alamat</label></td>
                                                    <td>:</td>
                                                    <td><input id="alamat" name="alamat" style="border:none" value="<?= $user[0]->alamat ?>"> </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="kodepos">Kode pos</label></td>
                                                    <td>:</td>
                                                    <td><input id="kodepos" name="kodepos" style="border:none" value="<?= $user[0]->kodepos ?>"> </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="prvns">Provinsi</label></td>
                                                    <td>:</td>
                                                    <td><input id="nama_provinsi" name="nama_provinsi" style="border:none" value="<?= $user[0]->nama_provinsi ?>"> </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="kabupatenkota">Kabupaten / Kota</label></td>
                                                    <td>:</td>
                                                    <td><input id="nama_kab_kota" name="nama_kab_kota" style="border:none" value="<?= $kab_kota_user[0]->nama_kab_kota ?>"> </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="tlp">No. Telepon / Hp</label></td>
                                                    <td>:</td>
                                                    <td><input id="no_hp" name="no_hp" style="border:none" value="<?= $user[0]->no_hp ?>"> </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="email">Email</label></td>
                                                    <td>:</td>
                                                    <td><input id="email" name="email" style="border:none" value="<?= $user[0]->email ?>"> </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="kualifikasipendidikan">kualifikasi Pendidikan</label></td>
                                                    <td>:</td>
                                                    <td><input id="nama_kualifikasi_pendidikan" name="nama_kualifikasi_pendidikan" style="border:none" value="<?= $user[0]->nama_kualifikasi_pendidikan ?>"> </td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="pekerjaan">Pekerjaan</label></td>
                                                    <td>:</td>
                                                    <td><input id="nama_pekerjaan" name="nama_pekerjaan" style="border:none" value="<?= $user[0]->nama_pekerjaan ?>"> </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" class="text-danger">*Pilih belum/tidak bekerja apabila belum bekerja atau pelajar/mahasiswa apabila masih pelajar.</td>
                                                </tr>
                                            </table>
                                            <br>
                                            <h4>b. Data Pekerjaan Sekarang</h4>
                                            <p class="wd text-danger">*JIKA BELUM BEKERJA, TIDAK PERLU DIISI <br> FORM INPUT TIDAK MENGGUNAKAN SIMBOL KUTIP ( ' )</p>
                                            <table class="table2">
                                                <tr>
                                                    <td><label for="namainstitusiperusahaan">Nama Insitusi/Perusahaan</label></td>
                                                    <td>:</td>
                                                    <td><input id="institusi_perusahaan" name="institusi_perusahaan" style="border:none" value="<?= $user[0]->institusi_perusahaan ?>"> </td>
                                                    <!-- <td colspan="3"><input onkeypress="return event.charCode != 34 && event.charCode != 39 && event.charCode != 96" class="input" type="text" id="namainstitusiperusahaan" name="namainstitusiperusahaan" placeholder="Masukan nama Insitusi / Perusahaan anda"></td> -->
                                                </tr>
                                                <tr>
                                                    <td><label for="jabatan">Jabatan</label></td>
                                                    <td>:</td>
                                                    <td><input id="jabatan" name="jabatan" style="border:none" value="<?= $user[0]->jabatan ?>"> </td>
                                                    <!-- <td colspan="3"><input onkeypress="return event.charCode != 34 && event.charCode != 39 && event.charCode != 96" class="input" type="text" id="jabatan" name="jabatan" placeholder="Masukan nama jabatan anda"></td> -->
                                                </tr>
                                                <tr>
                                                    <td><label for="alamatkantor">Alamat kantor</label></td>
                                                    <td>:</td>
                                                    <td><input id="alamat_kantor" name="alamat_kantor" style="border:none" value="<?= $user[0]->alamat_kantor ?>"> </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="kodepos2">Kode pos</label></td>
                                                    <td>:</td>
                                                    <td><input id="kodepos_kantor" name="kodepos_kantor" style="border:none" value="<?= $user[0]->kodepos_kantor ?>"> </td>
                                                    <!-- <td colspan="3"><input type="email" id="email2" name="email2" class="input" placeholder="Masukan alamat email kantor"></td> -->
                                                </tr>
                                                <tr>
                                                    <td><label for="telepon2">No. Telepon Kantor </label></td>
                                                    <td>:</td>
                                                    <td><input id="no_hp_kantor" name="no_hp_kantor" style="border:none" value="<?= $user[0]->no_hp_kantor ?>"> </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="fax2">Fax</label></td>
                                                    <td>:</td>
                                                    <td><input id="fax_kantor" name="fax_kantor" style="border:none" value="<?= $user[0]->fax_kantor ?>"> </td>
                                                    <!-- <td colspan="3"><input type="email" id="email2" name="email2" class="input" placeholder="Masukan alamat email kantor"></td> -->
                                                </tr>
                                                <tr>
                                                    <td><label for="email2">Email Kantor</label></td>
                                                    <td>:</td>
                                                    <td><input id="email_kantor" name="email_kantor" style="border:none" value="<?= $user[0]->email_kantor ?>"> </td>
                                                    <!-- <td colspan="3"><input type="email" id="email2" name="email2" class="input" placeholder="Masukan alamat email kantor"></td> -->
                                                </tr>
                                                <tr>
                                                    <td colspan="3" class="text-danger" style="opacity: 0;">*Pilih belum/tidak bekerja apabila belum bekerja atau pelajar/mahasiswa apabila masih pelajar.</td>
                                                </tr>
                                            </table>

                                            <h3>Bagian 2 : Data Sertifikasi</h3>
                                            <p>Tuliskan Judul dan Nomor Skema Sertifikasi yang anda ajukan berikut Daftar Unit Kompetensi sesuai kemasan pada skema sertifikasi untuk mendapatkan pengakuan sesuai dengan latar belakang pendidikan, pelatihan serta pengalaman kerja yang anda miliki.</p>

                                            <input type="hidden" name="idJadwalAsesmen" id="idJadwalAsesmen" value="<?= $id_jadwal_asesmen; ?>">
                                            <input type="hidden" name="idJadwalAsesor" id="idJadwalAsesor">

                                            <table class="table" style="border: 1px solid black;">
                                                <tr class="hide">
                                                    <td>TUK</td>
                                                    <td>:</td>
                                                    <td id="el-tuk">
                                                        <input type="text" name="tuk" id="tuk" value="<?= $res_jadwal_asesmen[0]->nama_tuk  ?>" readonly />
                                                        <input type="text" name="lokasi_uji" id="lokasi_uji" value="<?= $res_jadwal_asesmen[0]->lokasi_uji  ?>" readonly />
                                                    </td>
                                                </tr>
                                                <tr style="border: 1px solid black;">
                                                    <td style="border: 1px solid black;" colspan="2">Nama Jadwal</td>
                                                    <td style="border: 1px solid black;"><input id="judul_jadwal" name="judul_jadwal" style="border:none" value="<?= $res_jadwal_asesmen[0]->nama_jadwal  ?>"></td>

                                                </tr>
                                                <tr style="border: 1px solid black;">
                                                    <td style="border: 1px solid black;" rowspan="2">Skema Sertifikasi (KKNI/Okupasi/Klaster)</td>
                                                    <td style="border: 1px solid black;"><label for="jdl">Judul </label></td>
                                                    <td style="border: 1px solid black;">
                                                        <!-- <div class="el-skema"> -->
                                                        <input id="skema" name="skema" style="border:none" value="<?= $res_jadwal_asesmen[0]->nama_skema  ?>">
                                                    </td>
                                                    <!-- </div> -->

                                                </tr>
                                                <tr style="border: 1px solid black;">
                                                    <td style="border: 1px solid black;"><label for="nmr">Nomor </label></td>
                                                    <td style="border: 1px solid black;"><input id="nomor" name="nomor" style="border:none" value="<?= $res_jadwal_asesmen[0]->nomor_skema  ?>"></td>
                                                </tr>
                                                <tr style="border: 1px solid black;">
                                                    <td style="border: 1px solid black;" colspan="2">Nama Asesor</td>
                                                    <td style="border: 1px solid black;"><input id="asesor" name="asesor" style="border:none" value="<?= $res_jadwal_asesor[0]->asesor  ?>"></td>
                                                </tr>
                                                <tr class="hide">
                                                    <td>No. Reg. Asesor</td>
                                                    <td>:</td>
                                                    <td id="el-no_reg">
                                                        <input type="text" name="no_reg" id="no_reg" readonly value="<?= $res_jadwal_asesor[0]->no_reg ?>" />
                                                    </td>
                                                </tr>
                                                <tr class="hide">
                                                    <td>No. Reg. Asesor</td>
                                                    <td>:</td>
                                                    <td id="el-token">
                                                        <input type="text" name="token_asesor" id="token_asesor" readonly value="<?= $res_jadwal_asesor[0]->token ?>" />
                                                    </td>
                                                </tr>
                                                <tr style="border: 1px solid black;">
                                                    <td style="border: 1px solid black;" colspan="2"><label>Tujuan Asesmen</label></td>
                                                    <td style="border: 1px solid black;">
                                                        <select name="tujuan_asesmen" id="tujuan_asesmen" class="form-select required">
                                                            <option value="">Pilih Tujuan Asesmen</option>
                                                            <?php foreach ($list_tujuan_asesmen as $row) : ?>
                                                                <option value="<?= $row->nama_tujuan_asesmen ?>"><?= $row->nama_tujuan_asesmen ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr style="border: 1px solid black;">
                                                    <td style="border: 1px solid black;" colspan="2">Tanggal Asesmen</td>
                                                    <!-- <td> -->
                                                    <td style="border: 1px solid black;"><input id="tanggal_asesmen" name="tanggal_asesmen" style="border:none" value="<?= $res_jadwal_asesmen[0]->tanggal_spk  ?>"></td>

                                                </tr>
                                            </table>

                                            <br>
                                            <div id="formsbs">
                                                <p><strong>Daftar Unit Kompetensi sesuai kemasan:</strong></p>
                                                <table class="table" style="border: 1px solid black;" border-collapse="collapse">
                                                    <tr style="border: 1px solid black;" class="align-center">
                                                        <th style="background:#fac090;">No.</th>
                                                        <th style="background:#fac090;">Kode Unit</th>
                                                        <th style="background:#fac090;">Judul Unit</th>
                                                    </tr>
                                                    <?php $i = 1;
                                                    foreach ($get_skema as $row) : ?>
                                                        <tr style="border: 1px solid black;" class="table6">
                                                            <td style="border: 1px solid black;" class="align-center">
                                                                <label for="noUrut"><?= $i ?></label>
                                                            </td>
                                                            <td style="border: 1px solid black;" class="align-center">
                                                                <output id="no_uk" name="no_uk<?= $i ?>" class=" bg2" value="<?= $row->kode ?>"><?= $row->kode ?></output>
                                                            </td>
                                                            <td style="border: 1px solid black;">
                                                                <output value="<?= $row->nama ?>" id="uk" name="uk<?= $i ?>" class=" bg2"><?= $row->nama ?></output>
                                                            </td>
                                                        </tr>
                                                    <?php $i++;
                                                    endforeach; ?>
                                                </table>
                                            </div>
                                            <br>

                                            <h3>Bagian 3 : Bukti Kelengkapan Pemohon</h3>
                                            <p>Bukti Persyaratan Dasar Pemohon</p>
                                            <p class="wd">*FORMAT FILE HARUS BERBENTUK PDF ATAU GAMBAR <br> NAMA FILE TIDAK MENGGUNAKAN SIMBOL KUTIP ( ' )</p>

                                            <table class="table">
                                                <tr>
                                                    <th style="background:#fac090;" rowspan="2">No.</th>
                                                    <th style="background:#fac090;" rowspan="2">Bukti Persyaratan Dasar</th>
                                                    <th style="background:#fac090;" colspanrequired>Ada</th>
                                                    <th style="background:#fac090;" width="300px" rowspan="2">Tidak ada</th>
                                                </tr>
                                                <tr>
                                                    <th style="background:#fac090;"><label>Memenuhi Syarat / Tidak Memenuhi Syarat</label></th>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid black;" class="no">1.</td>
                                                    <td style="border: 1px solid black;">Pas Foto 3x4 :
                                                        <div id="el-pasFoto">
                                                            <!-- <input type="text" class="form-input" id="pasFoto" name="pasFoto" value="" disabled> -->
                                                            <img id="riviePasFoto" name="riviePasFoto" style="width: 280px">
                                                        </div>

                                                        <input type="file" class="form-control" value="Pilih File" id="pasFoto" name="pasFoto">
                                                    </td>
                                                    <td style="border: 1px solid black;">

                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="Memenuhi" <?= $role_id != 2  ? "disabled" : "" ?> name="pasFoto_ada" id="pasFoto_ada1">
                                                            <label class="form-check-label" for="pasFoto_ada1">
                                                                Memenuhi Syarat
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="Tidak Memenuhi" <?= $role_id != 2  ? "disabled" : "" ?> name="pasFoto_ada" id="pasFoto_ada2" checked>
                                                            <label class="form-check-label" for="pasFoto_ada2">
                                                                Tidak Memenuhi Syarat
                                                            </label>
                                                        </div>

                                                    </td>
                                                    <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text" <?= $role_id != 2  ? "readonly" : "" ?>></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid black;" class="no">2.</td>
                                                    <td style="border: 1px solid black;">KTP :
                                                        <div id="el-ktp">
                                                            <img id="riviewKtp" name="riviewKtp" style="width: 280px">
                                                        </div>
                                                        <input type="file" class="form-control" value="Pilih File" id="ktp" name="ktp">
                                                    </td>
                                                    <td style="border: 1px solid black;">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="Memenuhi" <?= $role_id != 2  ? "disabled" : "" ?> name="ktp_ada" id="ktp_ada1">
                                                            <label class="form-check-label" for="ktp_ada1">
                                                                Memenuhi Syarat
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="Tidak Memenuhi" <?= $role_id != 2  ? "disabled" : "" ?> name="ktp_ada" id="ktp_ada2" checked>
                                                            <label class="form-check-label" for="ktp_ada2">
                                                                Tidak Memenuhi Syarat
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text" <?= $role_id != 2  ? "readonly" : "" ?>></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid black;" class="no">3.</td>
                                                    <td style="border: 1px solid black;">Ijazah :
                                                        <div id="el-ijazah">
                                                            <img id="riviewIjazah" name="riviewIjazah" style="width: 280px">
                                                        </div>
                                                        <input type="file" class="form-control" value="Pilih File" id="ijazah" name="ijazah">
                                                    </td>
                                                    <td style="border: 1px solid black;">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="Memenuhi" <?= $role_id != 2  ? "disabled" : "" ?> name="ijazah_ada" id="ijazah_ada1">
                                                            <label class="form-check-label" for="ijazah_ada1">
                                                                Memenuhi Syarat
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="Tidak Memenuhi" <?= $role_id != 2  ? "disabled" : "" ?> name="ijazah_ada" id="ijazah_ada2" checked>
                                                            <label class="form-check-label" for="ijazah_ada2">
                                                                Tidak Memenuhi Syarat
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text" <?= $role_id != 2  ? "readonly" : "" ?>></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid black;" class="no">4.</td>
                                                    <td style="border: 1px solid black;">Sertifikasi Pelatihan :
                                                        <div id="el-sertifikasipelatihan">
                                                            <img id="riviewSertifikasiPelatihan" name="riviewSertifikasiPelatihan" style="width: 280px">
                                                        </div>
                                                        <input type="file" class="form-control" value="Pilih File" id="sertifikasi_pelatihan" name="sertifikasi_pelatihan">
                                                    </td>
                                                    <td style="border: 1px solid black;">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="Memenuhi" <?= $role_id != 2  ? "disabled" : "" ?> name="sertifikasipelatihan_ada" id="sertifikasipelatihan_ada1">
                                                            <label class="form-check-label" for="sertifikasipelatihan_ada1">
                                                                Memenuhi Syarat
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="Tidak Memenuhi" <?= $role_id != 2  ? "disabled" : "" ?> name="sertifikasipelatihan_ada" id="sertifikasipelatihan_ada2" checked>
                                                            <label class="form-check-label" for="sertifikasipelatihan_ada2">
                                                                Tidak Memenuhi Syarat
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text" <?= $role_id != 2  ? "readonly" : "" ?>></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid black;" class="no">5.</td>
                                                    <td style="border: 1px solid black;">CV :
                                                        <div id="el-cv">
                                                            <img id="riviewCv" name="riviewCv" style="width: 280px">
                                                        </div>
                                                        <input type="file" class="form-control" value="Pilih File" id="cv" name="cv">
                                                    </td>
                                                    <td style="border: 1px solid black;">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="Memenuhi" <?= $role_id != 2  ? "disabled" : "" ?> name="cv_ada" id="cv_ada1">
                                                            <label class="form-check-label" for="cv_ada1">
                                                                Memenuhi Syarat
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="Tidak Memenuhi" <?= $role_id != 2  ? "disabled" : "" ?> name="cv_ada" id="cv_ada2" checked>
                                                            <label class="form-check-label" for="cv_ada2">
                                                                Tidak Memenuhi Syarat
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text" <?= $role_id != 2  ? "readonly" : "" ?>></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid black;" class="no">6.</td>
                                                    <td style="border: 1px solid black;">Portofolio :
                                                        <div id="el-portofolio">
                                                            <img id="riviewPortofolio" name="riviewPortofolio" style="width: 280px">
                                                        </div>
                                                        <input type="file" class="form-control" value="Pilih File" id="portofolio" name="portofolio">
                                                    </td>
                                                    <td style="border: 1px solid black;">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="Memenuhi" <?= $role_id != 2  ? "disabled" : "" ?> name="portofolio_ada" id="portofolio_ada1">
                                                            <label class="form-check-label" for="portofolio_ada1">
                                                                Memenuhi Syarat
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="Tidak Memenuhi" <?= $role_id != 2  ? "disabled" : "" ?> name="portofolio_ada" id="portofolio_ada2" checked>
                                                            <label class="form-check-label" for="portofolio_ada2">
                                                                Tidak Memenuhi Syarat
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text" <?= $role_id != 2  ? "readonly" : "" ?>></td>
                                                </tr>
                                            </table>

                                            <br>

                                            <table class="table">
                                                <tr>
                                                    <td style="border: 1px solid black;" rowspan="3" class="rekomendasi">Rekomendasi (diisi oleh LSP): <br> Berdasarkan ketentuan persyaratan dasar, maka pemohon : <br> (

                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="Diterima" <?= $role_id != 2  ? "disabled" : "" ?> name="rekomendasi" id="rekomendasi1">
                                                            <label class="form-check-label" for="rekomendasi1">
                                                                Diterima
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="Tidak Diterima" <?= $role_id != 2  ? "disabled" : "" ?> name="rekomendasi" id="rekomendasi2">
                                                            <label class="form-check-label" for="rekomendasi2">
                                                                Tidak Diterima
                                                            </label>
                                                        </div>

                                                        ) sebagai peserta sertifikasi
                                                    </td>
                                                    <td style="border: 1px solid black;" colspan="2">Pemohon/Kandidat:</td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid black;"><label for="namapk">Nama</label></td>
                                                    <td style="border: 1px solid black;">
                                                        <p>
                                                            <input id="nama_pemohon" name="nama_pemohon" style="border:none" value="<?= $user[0]->nama_lengkap  ?>">
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid black;"><label for="ttdpk">Tanda tangan/</label><label for="tanggalpk">Tanggal</label></td>
                                                    <td style="border: 1px solid black;">
                                                        <div id="el-ttdasesi">
                                                            <img id="gambarTandaTangan" src="{{ asset('assets/document/tanda_tangan/asesi') }}/<?= $user[0]->tanda_tangan ?>" style="width: 280px">
                                                            <input type="hidden" id="ttdasesi" name="ttdasesi" value="<?= $user[0]->tanda_tangan ?>">
                                                        </div>

                                                        <!-- <input type = "submit" value = "Pilih File" id="upload-ttd"> -->
                                                        <br>
                                                        <div id="el-tanggal_pemohon">
                                                            <input type="text" name="tanggal_pemohon" id="tanggal_pemohon" class="input form-control" value="<?= date('Y-m-d') ?>" readonly>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid black;" rowspan="4">Catatan : <br>
                                                        <textarea <?= $role_id != 2  ? "readonly" : "" ?> id="catatanadmin" name="catatanadmin" class="catmin form-control"></textarea>
                                                    </td>
                                                    <td style="border: 1px solid black;" colspan="2">Admin LSP :</td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid black;"><label for="namaadmn">Nama</label></td>
                                                    <td style="border: 1px solid black;" id="el-namaadmin"><input name="namaadmn" id="namaadmin" <?= $role_id != 2  ? "readonly" : "" ?> class="form-control" type="text" placeholder="Masukan nama admin"></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid black;"><label for="noreg">No. Pegawai</label></td>
                                                    <td style="border: 1px solid black;"><input type="text" id="noreg" <?= $role_id != 2  ? "readonly" : "" ?> class="form-control" placeholder="Masukan nomor pegawai admin"></output></p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid black;"><label for="ttdpkadmn">Tanda tangan/</label><label for="tanggalpkadmn">Tanggal</label></td>
                                                    <td style="border: 1px solid black;">
                                                        <div id="el-ttdadmin">
                                                            <img id="ttdadmin" name="ttdadmin" src="" style="width: 350px">
                                                        </div>
                                                        <input type="file" class="form-control" <?= $role_id != 2  ? "disabled" : "" ?> value="Pilih File" id="upload-ttd-admin">
                                                        <div id="el-tanggaladmin">
                                                            <input type="text" class="form-control" name="tanggal" id="tanggaladmin" value="<?= date('Y-m-d') ?>" disabled>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>

                                            <br>

                                            <input type="submit" class="btn btn-primary" value="Kirim" />
                                        </form>


                                    </div>
                                    <!-- ppppppppppppppppppppppppppppp -->

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!--Basic Modal -->

<!-- <script src="{{ asset('assets/js/uji_kompetensi/jadwal_lanjut.js') }}"></script> -->


@endsection