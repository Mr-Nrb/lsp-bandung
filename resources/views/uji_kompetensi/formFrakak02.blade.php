@extends('template_dashboard.main')
@section('isiDashboard')
<style>
td {
    color: black;
}

p {
    color: black;
}
</style>
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <div id="content" class="invoice-box">
                                        <h3>FR.AK.02. FORMULIR REKAMAN ASESMEN KOMPETENSI</h3>
                                        <div>
                                            <form class="form" action="/saveFrakak02" enctype="multipart/form-data"
                                                method="POST">
                                                @csrf
                                                <input type="text" name="id_jadwal" id="id_jadwal"
                                                    value="<?= $data_jadwal_asesmen[0]->id; ?>" hidden>
                                                <table class="table" border="1" style="border-color: black;">
                                                    <tr>
                                                        <td colspan="2">
                                                            <label for="nama_asesi">Nama
                                                                Asesi</label>
                                                        </td>
                                                        <td colspan="3">
                                                            <input style="border:none" name="nama_asesi" id="nama_asesi"
                                                                value="<?= $data_apl01[0]->nama_asesi; ?>" readonly />
                                                            <input style="border:none" name="email_asesi"
                                                                id="email_asesi" value="<?= $data_apl01[0]->email; ?>"
                                                                readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <label for="namaasesor">Nama Asesor</label>
                                                        </td>
                                                        <td colspan="3">
                                                            <input style="border:none" name="nama_asesor"
                                                                id="nama_asesor"
                                                                value="<?= $data_apl01[0]->namaasesor; ?>" readonly />
                                                            <input style="border:none" name="met_asesor" id="met_asesor"
                                                                value="<?= $data_apl01[0]->no_Reg; ?>" readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <label for="skema">Skema Sertifikasi <br> ( bila
                                                                tersedia )</label>
                                                        </td>
                                                        <td colspan="3">
                                                            <input style="border:none" name="skema" id="skema"
                                                                value="<?= $data_apl01[0]->skema; ?>" readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <label for="tanggal_mulai_asesmen">Tanggal mulainya
                                                                asesmen</label>
                                                        </td>
                                                        <td colspan="3">
                                                            <input style="border:none" name="tanggal_mulai_asesmen"
                                                                id="tanggal_mulai_asesmen"
                                                                value="<?= $data_jadwal_asesmen[0]->tgl_mulai_pelatihan; ?>"
                                                                readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <label for="tanggal_akhir_asesmen">Tanggal selesainya
                                                                asesmen</label>
                                                        </td>
                                                        <td colspan="3">
                                                            <input style="border:none" name="tanggal_akhir_asesmen"
                                                                id="tanggal_akhir_asesmen"
                                                                value="<?= $data_jadwal_asesmen[0]->tgl_akhir_pelatihan; ?>"
                                                                readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    </tr>
                                                </table>
                                                <div id="rUnit">
                                                    <table class="table" border="1"
                                                        style="border-color: black; width: 100%;">
                                                        <tr>
                                                            <td colspan="2" style="text-align: center;">Unit Kompetensi
                                                            </td>
                                                        </tr>
                                                        <?php foreach ($data_skema as $row) : ?>
                                                        <tr>
                                                            <td>
                                                                <output style="font-size:15px" id="no_uk" class=" bg2"
                                                                    value="<?= $row->kode ?>"><?= $row->kode ?></output>
                                                            </td>
                                                            <td>
                                                                <output style="font-size:15px" value="<?= $row->nama ?>"
                                                                    id="uk" class="bg2"><?= $row->nama ?></output>
                                                            </td>
                                                        </tr>
                                                        <?php endforeach; ?>
                                                    </table>
                                                </div>
                                                <p>
                                                    Beri tanda centang (√) di kolom yang sesuai untuk
                                                    mencerminkan bukti yang diperoleh untuk menentukan
                                                    Kompetensi asesi untuk setiap Unit Kompetensi.
                                                </p>
                                                <table class="table" border="1" style="border-color: black;">
                                                    <tr>
                                                        <td style="border-color: black; border: 1px;">Bukti
                                                            yang
                                                            dikumpulkan</td>
                                                        <td style="border-color: black; border: 1px;">
                                                            <ul class="list-unstyled mb-0">
                                                                <?php foreach ($list_buktiak02 as $row) : ?>
                                                                <li class="d-inline-block me-2 mb-1">
                                                                    <div class="form-check">
                                                                        <div class="checkbox">
                                                                            <input type="checkbox"
                                                                                value="<?= $row->id ?>" name="bukti[]"
                                                                                id="bukti<?= $row->id ?> <"
                                                                                class="form-check-input">
                                                                            <label
                                                                                for="checkbox1"><?= $row->nama_bukti ?></label>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border-color: black; border: 1px;">
                                                            Rekomendasi hasil asesmen</td>
                                                        <td style="border-color: black; border: 1px;">
                                                            <input type="radio" name="rekomendasi" value="Kompeten"
                                                                id="rekomendasi">
                                                            <label> Kompeten</label>
                                                            <input type="radio" name="rekomendasi"
                                                                value="Belum Kompeten" id="rekomendasi">
                                                            <label for="perempuan"> Belum Kompeten</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border-color: black; border: 1px;">
                                                            Tindak lanjut yang dibutuhkan
                                                            (Masukkan pekerjaan tambahan dan asesmen yang diperlukan
                                                            untuk mencapai kompetensi)
                                                        </td>
                                                        <td style="border-color: black; border: 1px;">
                                                            <textarea type="text" id="tindak_lanjut"
                                                                name="tindak_lanjut"></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border-color: black; border: 1px;">
                                                            Komentar/ Observasi oleh asesor
                                                        </td>
                                                        <td style="border-color: black; border: 1px;">
                                                            <textarea type="text" id="komentar_asesor"
                                                                name="komentar_asesor"></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 100px;">
                                                            <p>Tanda tangan Asesor dan tanggal:</p>
                                                            <img id="gambarTandaTangan"
                                                                src="{{ asset('assets/document/tanda_tangan/asesor') }}/<?= $user->tanda_tangan ?>"
                                                                style="width: 280px; display: block; margin-left: auto;margin-right: auto;">
                                                            <input type="hidden" id="ttdasesor" name="ttdasesor"
                                                                value="<?= $user->tanda_tangan ?>">
                                                            <br>
                                                            <div id="el-tglttdasesor">
                                                                <input id="tanggal_asesor" name="tanggal_asesor"
                                                                    type="date" class="form-control" />
                                                            </div>
                                                            <br />
                                                        </td>
                                                        <td style="height: 100px;">
                                                            <p>Tanda tangan Asesi dan tanggal:</p>
                                                            <label for="ttd">Tanda Tangan Asesi:</label>
                                                            <div id="el-ttdasesi">
                                                                <img id="gambarTandaTangan"
                                                                    src="{{ asset('assets/document/tanda_tangan/asesi') }}/<?= $user->tanda_tangan ?>"
                                                                    style="width: 280px; display: block; margin-left: auto;margin-right: auto;">
                                                                <input type="file" id="ttdasesi" name="ttdasesi"
                                                                    <?= $user->role_id != 4 ? "hidden" : "" ?>
                                                                    <?= $user->role_id != 4 ? "value='$user->tanda_tangan' " : "" ?> />
                                                            </div>
                                                            <div id="el-tglttdasesi"><input id="tanggal_asesi"
                                                                    name="tanggal_asesi" type="text"
                                                                    class="form-control" value=" <?= Date('Y-m-d'); ?>"
                                                                    readonly
                                                                    <?= $user->role_id != 4 ? "readonly" : "" ?> />
                                                            </div>
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                        </div>
                                        <br />
                                        <input type="submit" class="btn btn-primary" value="Kirim" id="kirim" />
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>
<!--Basic Modal -->

<!-- <script src="{{ asset('assets/js/uji_kompetensi/jadwal_lanjut.js') }}"></script> -->


@endsection