@extends('template_dashboard.main')
@section('isiDashboard')
<link rel="stylesheet" href="{{ asset('assets/css') }}/ak03-lsp.css">
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <h3>FR.AK.03 UMPAN BALIK DAN CATATAN ASESMEN</h3>

                                        <form class="form" action="/saveUbdcaak03" enctype="multipart/form-data" method="POST">
                                            @csrf
                                            <table class="table">
                                                <tr>
                                                    <td><label for="nama">Nama Asesi</label></td>
                                                    <td>:</td>
                                                    <td><input style="border: none; outline: none;" type="text" name="namaasesi" id="namaasesi" placeholder="Masukkan nama anda" value="<?= $user->name; ?>" readonly /> </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="namasor">Nama Asesor</label></td>
                                                    <td>:</td>
                                                    <td><input style="border: none; outline: none;" type="text" name="namaasesor" id="namaasesor" readonly value="<?= $data_apl01[0]->namaasesor; ?>" /></td>
                                                </tr>
                                                <tr>
                                                    <td><label for="tanggal">Tanggal</label></td>
                                                    <td>:</td>
                                                    <td><input style="border: none; outline: none;" type="text" name="tanggal" id="tanggal" value="<?= $data_apl01[0]->tanggalasesmen; ?>" readonly /> </td>
                                                </tr>
                                            </table>

                                            <p>Umpan balik dari Asesi (diisi oleh Asesi setelah mengambil keputusan) :</p>

                                            <table class="table">
                                                <tr class="tab">
                                                    <td style="border: 1px solid gray; text-align:center;" rowspan="2">KOMPONEN</td>
                                                    <td style="border: 1px solid gray; text-align:center;">Hasil</td>
                                                    <td style="border: 1px solid gray; text-align:center;" class="ab ctt" rowspan="2">Catatan / komentar asesi</td>
                                                </tr>
                                                <tr class="tab">
                                                    <td style="border: 1px solid gray; text-align:center;" class="ab yata">Ya/Tidak</td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid gray;">Saya mendapatkan penjelasan yang cukup memadai mengenai proses asesmen/uji kompetensi</td>
                                                    <td style="border: 1px solid gray;">

                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="YA" name="yata1" id="yata11">
                                                            <label class="form-check-label" for="yata11">
                                                                Ya
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="TIDAK" name="yata1" id="yata12">
                                                            <label class="form-check-label" for="yata12">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td style="border: 1px solid gray;"><textarea class="form-control" name="ket1" id="ket1"></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid gray;">Saya diberikan kesempatan untuk mempelajari standar kompetensi yang akan diujikan dan menilai diri sendiri terhadap pencapaiannya</td>
                                                    <td style="border: 1px solid gray;">

                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="YA" name="yata2" id="yata21">
                                                            <label class="form-check-label" for="yata21">
                                                                Ya
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="TIDAK" name="yata2" id="yata22">
                                                            <label class="form-check-label" for="yata22">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td style="border: 1px solid gray;"><textarea class="form-control" name="ket2" id="ket2"></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid gray;">Asesor memberikan kesempatan untuk mendiskusikan/menegosiasikan metoda, instrumen dan sumber asesmen serta jadwal asesmen </td>
                                                    <td style="border: 1px solid gray;">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="YA" name="yata3" id="yata31">
                                                            <label class="form-check-label" for="yata31">
                                                                Ya
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="TIDAK" name="yata3" id="yata32">
                                                            <label class="form-check-label" for="yata32">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td style="border: 1px solid gray;"><textarea class="form-control" name="ket3" id="ket3"></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid gray;">Asesor berusaha menggali seluruh bukti pendukung yang sesuai dengan latar belakang pelatihan dan pengalaman yang saya miliki</td>
                                                    <td style="border: 1px solid gray;">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="YA" name="yata4" id="yata41">
                                                            <label class="form-check-label" for="yata41">
                                                                Ya
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="TIDAK" name="yata4" id="yata42">
                                                            <label class="form-check-label" for="yata42">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td style="border: 1px solid gray;"><textarea class="form-control" name="ket4" id="ket4"></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid gray;">Saya sepenuhnya diberikan kesempatan untuk mendemonstrasikan kompetensi yang saya miliki selama asesmen</td>
                                                    <td style="border: 1px solid gray;">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="YA" name="yata5" id="yata51">
                                                            <label class="form-check-label" for="yata51">
                                                                Ya
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="TIDAK" name="yata5" id="yata52">
                                                            <label class="form-check-label" for="yata52">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td style="border: 1px solid gray;"><textarea class="form-control" name="ket5" id="ket5"></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid gray;">Saya mendapatkan penjelasan yang memadai mengenai keputusan asesmen</td>
                                                    <td style="border: 1px solid gray;">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="YA" name="yata6" id="yata61">
                                                            <label class="form-check-label" for="yata61">
                                                                Ya
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="TIDAK" name="yata6" id="yata62">
                                                            <label class="form-check-label" for="yata62">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td style="border: 1px solid gray;"><textarea class="form-control" name="ket6" id="ket6"></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid gray;">Asesor memberikan umpan balik yang mendukung setelah asesmen serta tindak lanjutnya</td>
                                                    <td style="border: 1px solid gray;">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="YA" name="yata7" id="yata71">
                                                            <label class="form-check-label" for="yata71">
                                                                Ya
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="TIDAK" name="yata7" id="yata72">
                                                            <label class="form-check-label" for="yata72">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td style="border: 1px solid gray;"><textarea class="form-control" name="ket7" id="ket7"></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid gray;">Asesor bersama saya mempelajari semua dokumen asesmen serta menandatanganinya</td>
                                                    <td style="border: 1px solid gray;">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="YA" name="yata8" id="yata81">
                                                            <label class="form-check-label" for="yata81">
                                                                Ya
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="TIDAK" name="yata8" id="yata82">
                                                            <label class="form-check-label" for="yata82">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td style="border: 1px solid gray;"><textarea class="form-control" name="ket8" id="ket8"></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid gray;">Saya mendapatkan jaminan kerahasiaan hasil asesmen serta penjelasan penanganan dokumen asesmen</td>
                                                    <td style="border: 1px solid gray;">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="YA" name="yata9" id="yata91">
                                                            <label class="form-check-label" for="yata91">
                                                                Ya
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="TIDAK" name="yata9" id="yata92">
                                                            <label class="form-check-label" for="yata92">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td style="border: 1px solid gray;"><textarea class="form-control" name="ket9" id="ket9"></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid gray;">Asesor menggunakan keterampilan komunikasi yang efektif selama asesmen</td>
                                                    <td style="border: 1px solid gray;">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="YA" name="yata10" id="yata101">
                                                            <label class="form-check-label" for="yata101">
                                                                Ya
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" value="TIDAK" name="yata10" id="yata102">
                                                            <label class="form-check-label" for="yata102">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td style="border: 1px solid gray;"><textarea class="form-control" name="ket10" id="ket10"></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td style="border: 1px solid gray;" class="ab kmntr">Catatan/komentar lainnya (apabila ada) :</td>
                                                    <td style="border: 1px solid gray;" colspan="3"><textarea class="form-control" name="keterangan" id="keterangan"></textarea></td>
                                                </tr>
                                            </table>
                                            <br>
                                            <br>
                                            <button type="submit" value="Kirim" class="btn btn-primary">Kirim</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>


<!-- <script src="{{ asset('assets/js/uji_kompetensi/ba04.js') }}"></script> -->


@endsection