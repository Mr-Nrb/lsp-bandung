@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    <?= $title_sub_menu; ?>
                                </h4>
                                <hr>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <form class="form" action="/saveProfileLsp" enctype="multipart/form-data" method="POST">
                                        @csrf
                                        <label for=""> <strong style="color:black; "> 1. Data LSP </strong></label>
                                        <div class="row mt-3">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="jenis_lsp">Jenis LSP</label>
                                                    <fieldset class="form-group">
                                                        <select id="jenis_lsp" name="jenis_lsp" class="form-select" id="basicSelect">
                                                            <option value="">-- Pilih Jenis LSP --</option>
                                                            <option {{old('jenis_lsp',$user->jenis_lsp)=="Pihak Pertama"? 'selected':''}} value="Pihak Pertama">Pihak Pertama (P1)</option>
                                                            <option {{old('jenis_lsp',$user->jenis_lsp)=="Pihak Kedua Kerja"? 'selected':''}} value="Pihak Kedua Kerja">Pihak Kedua (P2)</option>
                                                            <option {{old('jenis_lsp',$user->jenis_lsp)=="Pihak Ketiga"? 'selected':''}} value="Pihak Ketiga">Pihak Ketiga (P3)</option>

                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="no_hp">No. HP</label>
                                                    <input style="color:black" type="number" id="no_hp" class="form-control" placeholder="Masukkan No. HP / WhatsApp" name="no_hp" value="<?= $user->no_hp ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="no_hp_kantor">No. Telepon</label>
                                                    <input style="color:black" type="number" id="no_hp_kantor" class="form-control" placeholder="Masukkan No. Telepon Kantor" name="no_hp_kantor" value="<?= $user->no_hp_kantor ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="no_fax">No. Fax</label>
                                                    <input style="color:black" type="number" id="no_fax" class="form-control" placeholder="Masukkan No. Fax Kantor" name="no_fax" value="<?= $user->fax_kantor ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="email">Email</label>
                                                    <input style="color:black" type="text" id="email" class="form-control" name="email" placeholder="Masukkan email LSP" value="<?= $user->email ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="password">Password</label>
                                                    <input style="color:black" type="text" id="password" class="form-control" name="password" value="<?= $user->password ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="website">Website</label>
                                                    <input style="color:black" type="text" id="website" class="form-control" name="website" placeholder="Masukkan website LSP" value="<?= $user->website ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="kode_pos">Kode Pos</label>
                                                    <input style="color:black" type="number" id="kode_pos" class="form-control" name="kode_pos" value="<?= $user->kodepos ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="alamat_kantor">Alamat Kantor</label>
                                                    <textarea style="color:black" class="form-control" id="alamat_kantor" name="alamat_kantor" rows="3"><?= $user->alamat_kantor ?></textarea>
                                                </div>
                                            </div>

                                        </div>
                                        <br>
                                        <br>
                                        <label for=""> <strong style="color:black; "> 2. Data Dokumen LSP </strong></label>
                                        <div class="row mt-3">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="no_sk_lsp">No. SK</label>
                                                    <input style="color:black" type="text" id="no_sk_lsp" class="form-control" placeholder="Masukkan nomor SK LSP" name="no_sk_lsp" value="<?= $user->no_sk_lsp ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="dokumen_sk_lsp">Dokumen SK</label>
                                                    <br>
                                                    <?php if ($user->dokumen_sk_lsp != "") { ?>
                                                        <embed src="assets/document/sk_lsp/<?= $user->dokumen_sk_lsp  ?>" height="330" width="350">
                                                    <?php } ?>
                                                    <input style="color:black" type="file" class="form-control" id="dokumen_sk_lsp" name="dokumen_sk_lsp">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="no_lisensi_lsp">No. Lisensi</label>
                                                    <input style="color:black" type="text" id="no_lisensi_lsp" class="form-control" placeholder="Masukkan no. lisensi LSP" name="no_lisensi_lsp" value="<?= $user->no_lisensi_lsp ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="dokumen_lisensi_lsp">Dokumen Lisensi</label>
                                                    <br>
                                                    <?php if ($user->dokumen_lisensi_lsp != "") { ?>
                                                        <embed src="assets/document/lisensi_lsp/<?= $user->dokumen_lisensi_lsp  ?>" height="330" width="350">
                                                    <?php } ?>
                                                    <input style="color:black" type="file" class="form-control" id="dokumen_lisensi_lsp" name="dokumen_lisensi_lsp">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="npwp">No. NPWP</label>
                                                    <input style="color:black" type="text" id="npwp" class="form-control" placeholder="Masukkan no. NPWP LSP" name="npwp" value="<?= $user->npwp ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="dokumen_npwp">Dokumen NPWP</label>
                                                    <br>
                                                    <?php if ($user->dokumen_npwp != "") { ?>
                                                        <embed src="assets/document/npwp/<?= $user->dokumen_npwp  ?>" height="330" width="350">
                                                    <?php } ?>
                                                    <input style="color:black" type="file" class="form-control" id="dokumen_npwp" name="dokumen_npwp">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="rekening_bank">No. Rekening Bank</label>
                                                    <input style="color:black" type="number" id="rekening_bank" class="form-control" placeholder="Masukkan no. rekening bank LSP" name="rekening_bank" value="<?= $user->rekening_bank ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label style="color:black" for="dokumen_rek_bank">Dokumen Rekening Bank</label>
                                                    <br>
                                                    <?php if ($user->dokumen_rek_bank != "") { ?>
                                                        <embed src="assets/document/rek_bank/<?= $user->dokumen_rek_bank  ?>" height="330" width="350">
                                                    <?php } ?>
                                                    <input style="color:black" type="file" class="form-control" id="dokumen_rek_bank" name="dokumen_rek_bank">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-12 d-flex justify-content-end mt-4">
                                            <button type="submit" class="btn btn-primary me-1 mb-1">
                                                Simpan
                                            </button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
</div>
@endsection