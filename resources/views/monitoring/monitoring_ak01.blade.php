@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>Data Asesi AK01</h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="tbl_monitoring_ak01" class="table display">
                                            <thead>
                                                <tr>
                                                    <th>Action</th>
                                                    <th>Judul jadwal</th>
                                                    <th>Tanggal asesmen</th>
                                                    <th>Nama asesi</th>
                                                    <th>Skema</th>
                                                    <th>Nomor skema</th>
                                                    <th>Tuk</th>
                                                    <th>Asesor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 @foreach ($data_ak01 as $row) 
                                                    <tr>
                                                        <td><button><a href="/detail_ak01/{{$row->id}}">Detail</a></button></td>
                                                        
                                                        <td> {{$row->nama_jadwal}}</td>
                                                        <td> {{$row->tanggal}}</td>
                                                        <td> {{$row->nama_asesi}}</td>
                                                        <td> {{$row->skema}}</td>
                                                        <td> {{$row->nomor}}</td>
                                                        <td> {{$row->tuk}}</td>
                                                        <td> {{$row->name}}</td>
                                                    </tr>
                                                 @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>


<!-- <script>
    $(document).ready(function() {
        $('#tbl_monitoring_apl01').DataTable({
            dom: "Bfrtip"

        });
    });
</script> -->


@endsection