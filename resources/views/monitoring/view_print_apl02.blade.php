

<link rel="stylesheet" href="{{ asset('assets/css') }}/apl02-lsp.css">
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
        <style>
            .noprint:hover{
                background-color: blue;
            }

            .noprint{
                background-color: #072d3a;
                display: inline-block;
                border-radius: 50%;
                width: 70px;
                height: 70px;
                text-align: center;
                color: white;
                float: right;
            }

            .apl02 {
    border-collapse: collapse;
}

.l4bel {
    width: 60px;
}

.pam1 {
    height: 5px;
    background-color: #fac090;
}

.pam1 h5 {
    text-align: center;
}

.uk {
    width: 25%;
}

.elemen {
    border-bottom: 0px;
}

.bg1 {
    background-color: #fac090;
}

a {
    text-decoration: none;
}

select {
    width: 100% !important;
}

input[type="date"] {
    width: 220px !important;
}

.element {
    width: 100%;
}

.kbk {
    width: 180px;
}

.hide {
    display: none;
}

[class*="l-"] {
    float: none;
}

.swal-button--confirm,
.swal-button--cancel {
    background-color: #072d3a;
    color: #fff;
}

.swal-button--confirm:hover,
.swal-button--cancel:hover {
    background-color: #072d3a !important;
}

output {
    display: block;
}

        </style>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <div>
                                        <h3>FR.APL.02. ASESMEN MANDIRI</h3>
                                        <form class="form" action="/saveAm02" enctype="multipart/form-data" method="POST">
                                            @csrf
                                            <table class="table" border="2" id="tabel-skema"></table>
                                            <table class="table" border="2">
                                                <tr>
                                                    <th class="pam1" style="background:#fac090; border: 1px solid grey;">
                                                        <h5>PANDUAN ASESMEN MANDIRI</h5>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td class="pam2" style=" border: 1px solid grey;">
                                                        <h5>Instruksi:</h5>
                                                        <ul>
                                                            <li class="f10">
                                                                <p>Baca setiap pernyataan di kolom sebelah kiri</p>
                                                            </li>
                                                            <li class="f10">Pilih rekomendasi Kompeten pada kotak pilihan K / BK jika Anda yakin dapat melakukan tugas yang dijelaskan</li>
                                                        </ul>
                                                    </td>
                                                </tr>

                                            </table>

                                            <br>
                                            <div class="p" id="sskema">
                                                <table name="tabel 1" class="apl02 pam2" border="2">
                                                    <tr>
                                                        <td>
                                                            <p style="text-align: center;"> Gagal mengambil data skema. <br> Pastikan anda sudah memilih jadwal untuk menampilkan skema</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <br>


                                            <table class="table" style=" border: 1px solid grey;">
                                                <tr>
                                                    <td class="label3 bg2" colspan="2">
                                                        <label for="nasi">Nama Asesi:</label>
                                                        <input class="form-control" id="nama_asesi" name="nama_asesi" value="<?= $data_apl02[0]->nama_asesi; ?>" readonly />
                                                        <input class="form-control" id="id_jadwal_asesmen" name="id_jadwal_asesmen" value="<?= $data_apl02[0]->id_jadwal_asesmen; ?>" readonly />
                                                        <input class="form-control" id="no_Reg" name="no_Reg" value="<?= $data_apl02[0]->no_Reg; ?>" readonly />
                                                    </td>
                                                    <td class="label3 bg2" colspan="2">
                                                        <label for="ttd">Tanda Tangan Asesi:</label>
                                                        <div id="el-ttd_asesi">
                                                            <img id="gambarTandaTangan" src="{{ asset('assets/document/tanda_tangan/asesi') }}/<?= $user->tanda_tangan ?>" style="width: 280px; display: block;margin-left: auto;margin-right: auto;">
                                                            <input type="hidden" id="ttd_asesi" name="ttd_asesi" value="<?= $user->tanda_tangan ?>">
                                                        </div>

                                                        <input type="text" name="tanggal_asesi" id="tanggal_asesi" value="<?= date('Y-m-d') ?>" class="form-control" readonly>
                                                    </td>
                                                    <td class="label3 bg2" colspan="1">

                                                    </td>
                                                </tr>
                                                <tr style="background:#fac090; border: 1px solid grey;">
                                                    <td class="" colspan="5">
                                                        <h5>Ditinjau oleh Asesor:</h5>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label3 bg2" colspan="2">
                                                        <label for="nasor">Nama Asesor:</label>
                                                        <input type="text" name="nama_asesor" style="border:none" id="nama_asesor" <?= $role_id != 2  ? "readonly" : "" ?>>
                                                        <br>
                                                        <label for="rekomendasi">Rekomendasi:</label>
                                                        <select id="rekomendasi" disabled class="form-select">
                                                            <option value="">Rekomendasi diisi oleh asesor</option>
                                                            <option value="Asesment dapat dilanjut">Asesment dapat dilanjut</option>
                                                            <option value="Asesment tidak dapat dilanjut">Asesment tidak dapat dilanjut</option>
                                                        </select>
                                                    </td>
                                                    <td class="label3 bg2" colspan="2">
                                                        <label for="ttd">Tanda Tangan dan Tanggal:</label>
                                                        <div id="el-ttdasesor">
                                                            <input class="form-select" type="file" id="ttd_asesor" name="ttd_asesor" <?= $role_id != 2  ? "disabled" : "" ?>>
                                                        </div>
                                                        <input class="form-control" type="date" id="tanggal_asesor" name="tanggal_asesor" <?= $role_id != 2  ? "readonly" : "" ?>>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br>
                                            <button class="noprint" onclick="printApl01()" type="button" class="btn btn-outline-primary block">
                                            Print
                                            </button>
                                        </form>
                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>




<script src="{{ asset('assets/js/uji_kompetensi/formAm02.js') }}"></script>
<script src=""></script>

