@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>Monitoring APL01</h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    Data Asesi APL01
                                </h4>
                                <hr>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="tbl_monitoring_apl01" class="table display">
                                            <thead>
                                                <tr>
                                                    <th style="width: 50px;">ActionButton</th>
                                                    <th>Status Asesmen</th>
                                                    <th>judul jadwal</th>
                                                    <th>tanggal asesmen</th>
                                                    <th>nama asesi</th>
                                                    <th>nik</th>
                                                    <th>tempat lahir</th>
                                                    <th>tanggal lahir</th>
                                                    <th>jenis kelamin</th>
                                                    <th>kebangsaan</th>
                                                    <th>alamat</th>
                                                    <th>kode pos</th>
                                                    <th>provinsi</th>
                                                    <th>kabupaten kota</th>
                                                    <th>telepon</th>
                                                    <th>email</th>
                                                    <th>skema</th>
                                                    <th>nomor</th>
                                                    <th>nama asesor</th>
                                                    <th>no Reg</th>
                                                    <th>tujuan asesmen</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 @foreach ($data_apl01 as $row) 
                                                    <tr>
                                                        <td style="width: 50px;">
                                                            <div>
                                                            <a href="/detail_apl01/{{$row->id}}/{{$row->id_jadwal_asesmen}}" class="btn btn-outline-dark btn-sm block"><i class="bi bi-eye"></i></a>
                                                            @if($user->role_id == 2)
                                                            <a href="/Edit_apl01/{{$row->id}}/{{$row->id_jadwal_asesmen}}" class="btn btn-primary btn-sm block"><i class="bi bi-pencil-square"></i></a>
                                                            @else
                                                                <sub>Hanya Admin yang dapat mengedit APL01</sub>
                                                            @endif
                                                            </div>
                                                        </td>
                                                        @if($row->rekomendasi == "direkomendasikan")
                                                        <td><p class="text-success " style="padding: 5px;">Asesmen Diterima</p></td>
                                                        @elseif($row->rekomendasi == "tidak direkomendasikan")
                                                        <td><p class="text-danger " style="padding: 5px;">Asesmen Ditolak</p></td>
                                                        @else
                                                        <td><p class=".text-blue-50" style="padding: 5px;">Belum Diproses</p></td>
                                                        @endif
                                                        <td> {{$row->juduljadwal}}</td>
                                                        <td> {{$row->tanggalasesmen}}</td>
                                                        <td> {{$row->nama_asesi}}</td>
                                                        <td> {{$row->nik}}</td>
                                                        <td> {{$row->tempatlahir}}</td>
                                                        <td> {{$row->tanggallahir}}</td>
                                                        <td> {{$row->jeniskelamin}}</td>
                                                        <td> {{$row->kebangsaan}}</td>
                                                        <td> {{$row->alamat}}</td>
                                                        <td> {{$row->kodepos}}</td>
                                                        <td> {{$row->provinsi}}</td>
                                                        <td> {{$row->kabupatenkota}}</td>
                                                        <td> {{$row->telepon}}</td>
                                                        <td> {{$row->email}}</td>
                                                        <td> {{$row->skema}}</td>
                                                        <td> {{$row->nomor}}</td>
                                                        <td> {{$row->namaasesor}}</td>
                                                        <td> {{$row->no_Reg}}</td>
                                                        <td> {{$row->tujuanasesmen}}</td>

                                                    </tr>
                                                 @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>


<!-- <script>
    $(document).ready(function() {
        $('#tbl_monitoring_apl01').DataTable({
            dom: "Bfrtip"

        });
    });
</script> -->


@endsection