<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet">
<div id="main">
        <style>
           * {
    font-family: arial;
}

.rekomendasi {
    width: 200px;
}

.rincian_data_content{
    margin-left: 0px;
    margin-right: 0px;
}

/* data sertifikat content */
.table_datasertif{
    width: 100%;
    border-collapse: collapse;    
}

.table_datasertif, td{
    padding: 7px;
}

.data_sertif_content{
    margin-left: 0px;
    margin-right: 0px;
}

/* data unit kompetensi content */
.table_unitkompetensi{
    width: 100%;
    border-collapse: collapse; 
}

.table_unitkompetensi, td{
    padding: 7px;
}

.daftar_unitkompetensi_content{
    margin-left: 0px;
    margin-right: 0px;
}

/* data pekerjaan sekarang content */
.data_pekerjaan{

    margin-left: 0px;
}

/* data bukti kelengkapan content */
.bukti_kelengkapan_content{
    margin-left: 0px;
    margin-right: 0px;
}

.table_bukti_kelengkapan{
    width: 100%;
    border-collapse: collapse; 
}

.table_bukti_kelengkapan, td{
    padding: 7px;
}






table.ab {
    border: 2 #000 solid;
}

.wd {
    color: red;
}

th {
    background-color: #fac090;
    text-align: center;
}

.no {
    padding: 10px;
    text-align: center;
}

.bpd {
    width: 96%;
}

.ta {
    width: 92%;
}

.tatd {
    width: 60px;
}
.noprint:hover{
    background-color: blue;
}

.noprint{
    background-color: #072d3a;
    display: inline-block;
    border-radius: 50%;
    width: 50px;
    height: 50px;
    text-align: center;
    color: white;
    float: right;
}

.back-btn{
    background-color: #072d3a;
    display: inline-block;
    border-radius: 50%;
    width: 70px;
    height: 70px;
    text-align: center;
    color: white;
    float: left;
}

@media print {
               .noprint {
                  visibility: hidden;
               }
               .btn-back-noprint {
                  visibility: hidden;
               }
               
            }

.catmin {
    width: 200px;
    height: 100px;
}

textarea {
    resize: vertical;
}

.table3 {
    height: 350px;
}

.table3 input,
.table3 .input,
.table3 select {
    margin: 0;
}

.invoice-box .table3 td {
    vertical-align: middle;
}

a {
    text-decoration: none;
}

.hide {
    display: none;
}

.white-placeholder::placeholder {
    color: #f0f8ff;
}

[class*="l-"] {
    float: none;
}

.swal-button--confirm,
.swal-button--cancel {
    background-color: #072d3a;
    color: #fff;
}

.swal-button--confirm:hover,
.swal-button--cancel:hover {
    background-color: #072d3a !important;
}

output {
    display: block;
}

.table6 {
    border: 1px solid black;
    border-collapse: collapse;
}

.table6 tr td {
    border: 1px solid black;
    border-collapse: collapse;
}
.align-center {
    text-align: center;
}

.card{
    background-color: #f0f8ff;
    align-items: center;
    margin-left: 50px;
    padding: 10px;
}

        </style>
   
</head>
<body>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card" id="viewapl01">

                            <div class="card-content">
                                <div class="card-body">
                                <!--  -->
                                
                                    <!-- ppppppppppppppppppppppppppppp -->
                                    <div id="content" class="invoice-box" style="color: black;">

                                        <div class="rincian_data_content">
                                        <!-- <form enctype="multipart/form-data"> -->
                                        <h3>FR.APL.01. PERMOHONAN SERTIFIKASI KOMPETENSI</h3>

                                        <h4>Bagian 1 : Rincian Data Pemohon Sertifikasi</h4>
                                        <p>Pada bagian ini, cantumlah data pribadi, data pendidikan formal serta data pekerjaan anda pada saat ini</p>

                                        <h4>a. Data Pribadi</h4>
                                        <p class="wd text-danger">*WAJIB DIISI <br> FORM INPUT TIDAK MENGGUNAKAN SIMBOL KUTIP ( ' ) </p>
                                            
                                        <table class="table1">
                                            <tr>
                                                <td><label for="nama">Nama lengkap</label></td>
                                                <td>:</td>
                                                <td>{{$data_apl01->nama_asesi}}</td>
                                            </tr>
                                            
                                            <tr>
                                                <td><label for="nik">No. KTP/NIK/Paspor</label></td>
                                                <td>:</td>
                                                <td><output id="nik">{{$data_apl01->nik}}</output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="tempatlahir">Tempat Lahir</label></td>
                                                <td>:</td>
                                                <td><output id="tempatlahir">{{$data_apl01->tempatlahir}}</output> </td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Lahir</td>
                                                <td>:</td>
                                                <td><span type="text" disabled> </span> <span type="text" name="bulanz" id="bulanz" disabled> </span>
                                                 {{ \Carbon\Carbon::parse($data_apl01->tanggallahir)->format('d-m-Y')}} </td>
                                            </tr>
                                            <tr>
                                                <td><label for="jeniskelamin">Jenis Kelamin</label></td>
                                                <td>:</td>
                                                <td><output id="jeniskelamin">{{$data_apl01->jeniskelamin}}</output> </td>
                                            <tr>
                                                <td><label for="kebangsaan">Kebangsaan</label></td>
                                                <td>:</td> 
                                                <td><output id="kebangsaan">{{$data_apl01->kebangsaan}}</output> </td>
                                            <tr>
                                                <td><label for="alamat">Alamat</label></td>
                                                <td>:</td>
                                                <td><output id="alamat">{{$data_apl01->alamat}}</output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="kodepos">Kode pos</label></td>
                                                <td>:</td>
                                                <td><output id="kodepos">{{$data_apl01->kodepos}}</output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="prvns">Provinsi</label></td>
                                                <td>:</td>
                                                <td><output id="provinsi">{{$data_apl01->provinsi}}</output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="kabupatenkota">Kabupaten / Kota</label></td>
                                                <td>:</td>
                                                <td><output id="kabupatenkota"> {{$data_apl01->kabupatenkota}} </output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="tlp">No. Telepon / Hp</label></td>
                                                <td>:</td>
                                                <td><output id="telepon">{{$data_apl01->telepon}}</output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="email">Email</label></td>
                                                <td>:</td>
                                                <td><output id="email">{{$data_apl01->email}} </output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="kualifikasipendidikan">kualifikasi Pendidikan</label></td>
                                                <td>:</td>
                                                <td><output id="kualifikasipendidikan">{{$data_apl01->kualifikasipendidikan}}</output> </td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><label for="pekerjaan">Pekerjaan</label></td>
                                                <td>:</td>
                                                <td><output id="pekerjaan">{{$data_apl01->pekerjaan}} </output> </td>
                                            </tr>
                                        </table>
                                        </div>
                                        <!-- ======================================================================================================================================= -->
                                        <br>
                                        <div class="data_pekerjaan">
                                        <h4>b. Data Pekerjaan Sekarang</h4>
                                        <p class="wd text-danger">*JIKA BELUM BEKERJA, TIDAK PERLU DIISI <br> FORM INPUT TIDAK MENGGUNAKAN SIMBOL KUTIP ( ' )</p>
                                        <table class="table_data_pekerjaan">
                                            <tr>
                                                <td><label for="namainstitusiperusahaan">Nama Insitusi/Perusahaan</label></td>
                                                <td>:</td>
                                                <td><output id="namainstitusiperusahaan">{{$data_apl01->namainstitusiperusahaan}} </output> </td>
                                                <!-- <td colspan="3"><input onkeypress="return event.charCode != 34 && event.charCode != 39 && event.charCode != 96" class="input" type="text" id="namainstitusiperusahaan" name="namainstitusiperusahaan" placeholder="Masukan nama Insitusi / Perusahaan anda"></td> -->
                                            </tr>
                                            <tr>
                                                <td><label for="jabatan">Jabatan</label></td>
                                                <td>:</td>
                                                <td><output id="jabatan">{{$data_apl01->jabatan}} </output> </td>
                                                <!-- <td colspan="3"><input onkeypress="return event.charCode != 34 && event.charCode != 39 && event.charCode != 96" class="input" type="text" id="jabatan" name="jabatan" placeholder="Masukan nama jabatan anda"></td> -->
                                            </tr>
                                            <tr>
                                                <td><label for="alamatkantor">Alamat kantor</label></td>
                                                <td>:</td>
                                                <td><output id="alamatkantor">{{$data_apl01->alamatkantor}} </output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="kodepos2">Kode pos</label></td>
                                                <td>:</td>
                                                <td><output id="kodepos2">{{$data_apl01->kodepos2}} </output> </td>
                                                <!-- <td colspan="3"><input type="email" id="email2" name="email2" class="input" placeholder="Masukan alamat email kantor"></td> -->
                                            </tr>
                                            <tr>
                                                <td><label for="telepon2">No. Telepon Kantor </label></td>
                                                <td>:</td>
                                                <td><output id="telepon2">{{$data_apl01->telepon2}} </output> </td>
                                            </tr>
                                            <tr>
                                                <td><label for="fax2">Fax</label></td>
                                                <td>:</td>
                                                <td><output id="fax2">{{$data_apl01->fax2}} </output> </td>
                                                <!-- <td colspan="3"><input type="email" id="email2" name="email2" class="input" placeholder="Masukan alamat email kantor"></td> -->
                                            </tr>
                                            <tr>
                                                <td><label for="email2">Email Kantor</label></td>
                                                <td>:</td>
                                                <td><output id="email2">{{$data_apl01->email2}} </output> </td>
                                                <!-- <td colspan="3"><input type="email" id="email2" name="email2" class="input" placeholder="Masukan alamat email kantor"></td> -->
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-danger" style="opacity: 0;">*Pilih belum/tidak bekerja apabila belum bekerja atau pelajar/mahasiswa apabila masih pelajar.</td>
                                            </tr>
                                        </table>
                                        </div>

                                        <!-- ========================================================================================================================== -->

                                        <div class="data_sertif_content">
                                        <h3>Bagian 2 : Data Sertifikasi</h3>
                                        <p>Tuliskan Judul dan Nomor Skema Sertifikasi yang anda ajukan berikut Daftar Unit Kompetensi sesuai kemasan pada skema sertifikasi untuk mendapatkan pengakuan sesuai dengan latar belakang pendidikan, pelatihan serta pengalaman kerja yang anda miliki.</p>

                                        <input type="hidden" name="idJadwalAsesor" id="idJadwalAsesor">
                                  
                                        <table class="table_datasertif">
                                            <tr class="hide">
                                                <td>TUK</td>
                                                <td>:</td>
                                                <td id="el-tuk">
                                                {{$data_apl01->tuk}}
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid black;">
                                                <td style="border: 1px solid black;" colspan="2">Nama Jadwal</td>
                                                <td style="border: 1px solid black; width:70%;"><output id="juduljadwal"> {{$data_apl01->juduljadwal}} </output></td>

                                            </tr>
                                            <tr style="border: 1px solid black;">
                                                <td style="border: 1px solid black;" rowspan="2">Skema Sertifikasi (KKNI/Okupasi/Klaster)</td>
                                                <td style="border: 1px solid black;"><label for="jdl">Judul </label></td>
                                                <td style="border: 1px solid black;">
                                                    <!-- <div class="el-skema"> -->
                                                    <output id="skema"> {{$data_apl01->skema}} </output>
                                                </td>
                                                <!-- </div> -->

                                            </tr>
                                            <tr style="border: 1px solid black;">
                                                <td style="border: 1px solid black;"><label for="nmr">Nomor </label></td>
                                                <td style="border: 1px solid black;"><output id="nomor"> {{$data_apl01->nomor}} </output></td>
                                            </tr>
                                            <tr style="border: 1px solid black;">
                                                <td style="border: 1px solid black;" colspan="2">Nama Asesor</td>
                                                <td style="border: 1px solid black;"><output id="namaasesor"> {{$data_apl01->namaasesor}} </output></td>
                                            </tr>
                                            <tr class="hide">
                                                <td>No. Reg. Asesor</td>
                                                <td>:</td>
                                                <td id="el-no_Reg">
                                                {{$data_apl01->no_Reg}}
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid black;">
                                                <td style="border: 1px solid black;" colspan="2"><label>Tujuan Asesmen</label></td>
                                                <td style="border: 1px solid black;">{{$data_apl01->tujuanasesmen}}</td>
                                                
                                            </tr>
                                            <tr style="border: 1px solid black;">
                                                <td style="border: 1px solid black;" colspan="2">Tanggal Asesmen</td>
                                                <!-- <td> -->
                                                <td style="border: 1px solid black;"><output id="tanggalasesmen">
                                                     {{ \Carbon\Carbon::parse($data_apl01->tanggalasesmen)->format('d-m-Y')}} 
                                                    </output> </td>
                                            </tr>
                                        </table>
                                       
                                        </div>
                                        <br>
                                        <div class="daftar_unitkompetensi_content">
                                        <form id="formsbs">
                                            <p><strong>Daftar Unit Kompetensi sesuai kemasan:</strong></p>
                                            <table class="table_unitkompetensi" style="border: 1px solid black;" border-collapse="collapse">
                                                <tr style="border: 1px solid black;" class="align-center">
                                                    <th style="background:#fac090;">No.</th>
                                                    <th style="background:#fac090;">Kode Unit</th>
                                                    <th style="background:#fac090;">Judul Unit</th>
                                                </tr>
                                                <?php $i = 1;
                                                    foreach ($get_skema as $row) : ?>
                                                        <tr style="border: 1px solid black;" class="table6">
                                                            <td style="border: 1px solid black;" class="align-center">
                                                                <label for="noUrut"><?= $i ?></label>
                                                            </td>
                                                            <td style="border: 1px solid black;" class="align-center">
                                                                <output id="no_uk" name="no_uk<?= $i ?>" class=" bg2" value="<?= $row->kode ?>"><?= $row->kode ?></output>
                                                            </td>
                                                            <td style="border: 1px solid black;">
                                                                <output value="<?= $row->nama ?>" id="uk" name="uk<?= $i ?>" class=" bg2"><?= $row->nama ?></output>
                                                            </td>
                                                        </tr>
                                                    <?php $i++;
                                                    endforeach; ?>
                                            </table>
                                        </form>
                                        </div>
                                        <br>
                                        <div class="bukti_kelengkapan_content">
                                        <form action="/UpdateApl01/{{$data_apl01->id}}" method="post">
                                        @csrf
                                        <h3>Bagian 3 : Bukti Kelengkapan Pemohon</h3>
                                        <p>Bukti Persyaratan Dasar Pemohon</p>
                                        <p class="wd">*FORMAT FILE HARUS BERBENTUK PDF ATAU GAMBAR <br> NAMA FILE TIDAK MENGGUNAKAN SIMBOL KUTIP ( ' )</p>

                                        <table class="table_bukti_kelengkapan">
                                            <tr>
                                                <th style="background:#fac090;" rowspan="2">No.</th>
                                                <th style="background:#fac090;" rowspan="2">Bukti Persyaratan Dasar</th>
                                                <th style="background:#fac090;" colspanrequired>Ada</th>
                                                <th style="background:#fac090;" rowspan="2">Tidak ada</th>
                                            </tr>
                                            <tr>
                                                <th style="background:#fac090;"><label>Memenuhi Syarat / Tidak Memenuhi Syarat</label></th>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;" class="no">1.</td>
                                                <td style="border: 1px solid black;">Pas Foto 3x4 :
                                                    <div id="el-pasFoto">
                                                        <!-- <input type="text" class="form-input" id="pasFoto" name="pasFoto" value="" disabled> -->
                                                        <img id="ttdadmin" name="ttdadmin" src="{{ asset("assets/document/pas_foto/{$data_apl01->pasFoto}") }}" style="width: 280px">
                                                    </div>
                                                </td>
                                                <td style="border: 1px solid black;"><select disabled class="form-select" id="pasFoto_ada">
                                                        <option value=""></option>
                                                        <option value="Memenuhi">Memenuhi Syarat</option>
                                                        <option value="Tidak Memenuhi">Tidak Memenuhi Syarat</option>
                                                    </select></td>
                                                <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text"></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;" class="no">2.</td>
                                                <td style="border: 1px solid black;">KTP :
                                                    <div id="el-ktp">
                                                    <img id="ttdadmin" name="ttdadmin" src="{{ asset("assets/document/ktp/{$data_apl01->ktp}") }}" style="width: 280px">

                                                    </div>
                                                </td>
                                                <td style="border: 1px solid black;"><select disabled class="form-select" id="ktp_ada">
                                                        <option value=""></option>
                                                        <option value="Memenuhi">Memenuhi Syarat</option>
                                                        <option value="Tidak Memenuhi">Tidak Memenuhi Syarat</option>
                                                    </select></td>
                                                <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text"></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;" class="no">3.</td>
                                                <td style="border: 1px solid black;">Ijazah :
                                                    <div id="el-ijazah">
                                                    <img id="ttdadmin" name="ttdadmin" src="{{ asset("assets/document/ijazah/{$data_apl01->ijazah}") }}" style="width: 280px">

                                                    </div>
                                                </td>
                                                <td style="border: 1px solid black;"><select disabled class="form-select" id="ijazah_ada">
                                                        <option value=""></option>
                                                        <option value="Memenuhi">Memenuhi Syarat</option>
                                                        <option value="Tidak Memenuhi">Tidak Memenuhi Syarat</option>
                                                    </select></td>
                                                <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text"></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;" class="no">4.</td>
                                                <td style="border: 1px solid black;">Sertifikasi Pelatihan :
                                                    <div id="el-sertifikasipelatihan">
                                                        <<img id="ttdadmin" name="ttdadmin" src="{{ asset("assets/document/ijazah/{$data_apl01->ijazah}") }}" style="width: 280px">
                                                    </div>
                                                    
                                                </td>
                                                <td style="border: 1px solid black;"><select disabled id="sertifikasipelatihan_ada" class="input form-select">
                                                        <option value=""></option>
                                                        <option value="Memenuhi">Memenuhi Syarat</option>
                                                        <option value="Tidak Memenuhi">Tidak Memenuhi Syarat</option>
                                                    </select></td>
                                                <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text"></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;" class="no">5.</td>
                                                <td style="border: 1px solid black;">CV :
                                                    <div id="el-cv">
                                                    <img id="ttdadmin" name="ttdadmin" src="{{ asset("assets/document/ijazah/{$data_apl01->ijazah}") }}" style="width: 280px">
                                                    </div>
                                                   
                                                </td>
                                                <td style="border: 1px solid black;"><select disabled class="form-select" id="cv_ada">
                                                        <option value=""></option>
                                                        <option value="Memenuhi">Memenuhi Syarat</option>
                                                        <option value="Tidak Memenuhi">Tidak Memenuhi Syarat</option>
                                                    </select></td>
                                                <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text"></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;" class="no">6.</td>
                                                <td style="border: 1px solid black;">Portofolio :
                                                    <div id="el-portofolio">
                                                    <img id="ttdadmin" name="ttdadmin" src="{{ asset("assets/document/ijazah/{$data_apl01->ijazah}") }}" style="width: 280px">
                                                    </div>
                                                    
                                                </td>
                                                <td style="border: 1px solid black;"><select disabled class="form-select" id="portofolio_ada">
                                                        <option value=""></option>
                                                        <option value="Memenuhi">Memenuhi Syarat</option>
                                                        <option value="Tidak Memenuhi">Tidak Memenuhi Syarat</option>
                                                    </select></td>
                                                <td style="border: 1px solid black;" class="tatd"><input class="ta form-control" type="text"></td>
                                            </tr>
                                        </table>
                                        
                                        <br>

                                        <table class="table_bukti_kelengkapan">
                                            <tr>
                                                <td style="border: 1px solid black;" rowspan="3" class="rekomendasi">
                                                Rekomendasi (diisi oleh LSP): <br> Berdasarkan ketentuan persyaratan dasar, 
                                                maka pemohon : <br> ({{$data_apl01->rekomendasi}}) sebagai peserta sertifikasi
                                                </td>
                                                <td style="border: 1px solid black;" colspan="2">Pemohon/Kandidat:</td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;"><label for="namapk">Nama</label></td>
                                                <td style="border: 1px solid black;">{{$data_apl01->nama_asesi}}</td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;"><label for="ttdpk">Tanda tangan/</label><label for="tanggalpk">Tanggal</label></td>
                                                <td style="border: 1px solid black;">
                                                    <div id="el-ttdasesi">
                                                        <img id="gambarTandaTangan"
                                                        src="{{ asset("assets/document/tanda_tangan/asesi/{$data_apl01->ttdpemohon}") }}" style="width:250px;">
                                                    </div>

                                                    <!-- <input type = "submit" value = "Pilih File" id="upload-ttd"> -->
                                                    <br>
                                                    <div id="el-tanggalpemohon">
                                                    {{ \Carbon\Carbon::parse($data_apl01->tanggalpemohon)->format('d-m-Y')}} 
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;" rowspan="4">Catatan : <br>
                                                {{$data_apl01->catatanadmin}}
                                                </td>
                                                <td style="border: 1px solid black;" colspan="2">Admin LSP :</td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;"><label for="namaadmn">Nama</label></td>
                                                <td style="border: 1px solid black;" id="el-namaadmin">
                                                {{$data_apl01->namaadmin}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;"><label for="noreg">No. Pegawai</label></td>
                                                <td style="border: 1px solid black;"> {{$data_apl01->no_Reg}}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid black;"><label for="ttdpkadmn">Tanda tangan/</label><label for="tanggalpkadmn">Tanggal</label></td>
                                                <td style="border: 1px solid black;">
                                                    <div id="el-ttdadmin">
                                                        <img id="ttdadmin" name="ttdadmin" src="{{ asset("assets/document/tanda_tangan/admin/{$data_apl01->ttdadmin}") }}" style="width: 350px">
                                                    </div>
                                                    
                                                    <div id="el-tanggaladmin">
                                                    {{ \Carbon\Carbon::parse($data_apl01->tanggaladmin)->format('d-m-Y')}} 
                                                    </div>
                                                </td>
                                            </tr>
                                           
                                        </table>
                                        <br>
                                        </form>
                                        <div class="btn-back-noprint">
                                        <a href="/pilih_asesi_monitoring_apl01/{{$data_apl01->no_Reg}}/{{$data_apl01->id_jadwal_asesmen}}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" fill="" class="bi bi-arrow-left-square-fill" viewBox="0 0 16 16">
                                                <path d="M16 14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12zm-4.5-6.5H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5a.5.5 0 0 0 0-1z"/>
                                            </svg>
                                        </a>
                                        </div>
                                        <button class="noprint" onclick="printApl01()" type="button" class="btn btn-outline-primary block">
                                            Print
                                        </button>
                                        </div>
                                        <br>

                                        <!-- </form> -->

                                       
                                    </div>
                                    <!-- ppppppppppppppppppppppppppppp -->

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!--Basic Modal -->

<!-- <script src="{{ asset('assets/js/monitoring/print_apl01.js') }}"></script> -->
<script src="{{ asset('assets/js/monitoring/print_apl01.js') }}"></script>


</body>
</html>



