
<link rel="stylesheet" href="{{ asset('assets/css') }}/ak01-lsp.css">
<div id="main">
    <header class="mb-3">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
        <style>
            .table_ak01_content{
                padding: 10px;
            }
            .page-content{
                margin-left: 50px;
                margin-right: 20px;
            }

            .page-heading{
                margin-left: 50px;
                margin-right: 20px;
            }

            th, tr, td{
                padding: 10px;
            }

            .noprint{
                background-color: #072d3a;
                display: inline-block;
                border-radius: 50%;
                width: 50px;
                height: 50px;
                text-align: center;
                color: white;
                /* float: right; */
            }

            @media print {
               .noprint {
                  visibility: hidden;
               }
               
            }
            ul.list_bukti{
                list-style-type: none;
                margin: 0;
                padding: 0;
            }
        </style>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <div id="content" class="invoice-box">
                                        <h3>FR.AK.01. PERSETUJUAN ASESMEN DAN KERAHASIAAN</h3>
                                        <div>
                                            <form class="form" action="/savePadkak01" enctype="multipart/form-data" method="POST">
                                                @csrf
                                                <table class="table">
                                                    <div class="table_ak01_content">
                                                    <tr>
                                                        <th colspan="6" style="background:#fac090; border: 1px solid grey;">
                                                            <p>
                                                                Persetujuan Asesmen ini untuk menjamin bahwa Asesi telah diberi
                                                                arahan secara rinci tentang perencanaan dan proses asesmen
                                                            </p>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" rowspan="2" class="judulrow">
                                                            Skema Sertifikasi (KKNI/Okupasi/Klaster)
                                                        </td>
                                                        <td style="border: 0px">
                                                            <label for="judul">Judul</label>
                                                        </td>
                                                        <td style="border: 0px">:</td>
                                                        <td colspan="3" style="border: 0px">
                                                            <input style="border:none" name="judul" id="judul" value="<?= $data_ak01[0]->skema; ?>" readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px">
                                                            <label for="nomor">Nomor</label>
                                                        </td>
                                                        <td style="border: 0px">:</td>
                                                        <td style="border: 0px" colspan="3">
                                                            <input style="border:none" name="nomor" id="nomor" value="<?= $data_ak01[0]->nomor; ?>" readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="border: 0px">
                                                            <label for="tuk">TUK</label>
                                                        </td>
                                                        <td style="border: 0px">:</td>
                                                        <td style="border: 0px" colspan="3" id="el-tuk">
                                                            <input style="border:none" name="tuk" id="tuk" value="<?= $data_ak01[0]->tuk; ?>" readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" colspan="2">
                                                            <label for="namaasesor">Nama Asesor</label>
                                                        </td>
                                                        <td style="border: 0px">:</td>
                                                        <td style="border: 0px" colspan="3">
                                                            <input style="border:none" value="<?= $data_ak01[0]->namaasesor; ?>" disabled readonly />
                                                           
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" colspan="2">
                                                            <label for="namaasesi">Nama Asesi</label>
                                                        </td>
                                                        <td style="border: 0px">:</td>
                                                        <td style="border: 0px" colspan="3">
                                                            <input style="border:none" name="nama_asesi" id="nama_asesi" value="<?= $data_ak01[0]->nama_asesi; ?>" readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" colspan="2">Bukti yang dikumpulkan</td>
                                                        <td style="border: 0px">:</td>
                                                        <td style="border: 0px" colspan="3">
                                                            <ul class="list_bukti">
                                                            <?php foreach ($list_bukti_ak01 as $row) : ?>
                                                                <li class="d-inline-block me-2 mb-1">
                                                                    <div class="form-check">
                                                                        <div class="checkbox">
                                                                            <?php
                                                                                $buktimantap = explode(';', $bukti2);
                                                                                $isChecked = in_array($row->id, $buktimantap);
                                                                                ?>
                                                                            <input type="checkbox"
                                                                                value="<?= $row->id ?>" name="bukti[]"
                                                                                id="bukti<?= $row->id ?>"
                                                                                <?= $isChecked ? 'checked' : '' ?>
                                                                                class="form-check-input" id="flexCheckCheckedDisabled" disabled>
                                                                            <label
                                                                                for="flexCheckCheckedDisabled" class="form-check-label"><?= $row->nama_bukti ?></label>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <tr class="tip">
                                                        <td style="border: 0px" colspan="3"></td>
                                                        <td style="border: 0px" colspan="3">

                                                        </td>
                                                    </tr>
                                                    <br />
                                                    <tr>
                                                        <td style="border: 0px" rowspan="2" class="judulrow">
                                                            Pelaksanaan asesmen disepakati pada
                                                        </td>
                                                        <td style="border: 0px">
                                                            <label for="tanggal">Tanggal</label>
                                                        </td>
                                                        <td style="border: 0px">:</td>
                                                        <td style="border: 0px" id="el-tanggal">
                                                        {{ \Carbon\Carbon::parse($data_ak01[0]->tanggalasesmen)->format('d-m-Y')}}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px">
                                                            <label for="tuk2">Lokasi Uji</label>
                                                        </td>
                                                        <td style="border: 0px">:</td>
                                                        <td style="border: 0px">
                                                            <input id="lokasi_uji" name="lokasi_uji" style="border:none" value="<?= $data_ak01[0]->lokasi_uji; ?>" readonly />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" colspan="6">
                                                            <b>Asesi:</b> <br />
                                                            <p>
                                                                Bahwa Saya Sudah Mendapatkan Penjelasan Hak dan Prosedur Banding
                                                                Oleh Asesor.
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" colspan="6">
                                                            <b>Asesor:</b> <br />
                                                            <p>
                                                                Menyatakan tidak akan membuka hasil pekerjaan yang saya peroleh
                                                                karena penugasan saya sebagai Asesor dalam pekerjaan Asesmen
                                                                kepada siapapun atau organisasi apapun selain kepada pihak yang
                                                                berwenang sehubungan dengan kewajiban saya sebagai Asesor yang
                                                                ditugaskan oleh LSP.
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" colspan="6">
                                                            <b>Asesi:</b> <br />
                                                            <p>
                                                                Saya setuju mengikuti asesmen dengan pemahaman bahwa informasi
                                                                yang dikumpulkan hanya digunakan untuk pengembangan profesional
                                                                dan hanya dapat diakses oleh orang tertentu saja.
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 0px" colspan="3" style="height: 100px;">
                                                            <p>Tanda tangan Asesor dan tanggal:</p>
                                                           
                                                            <img id="gambarTandaTangan" src="{{ asset('assets/document/tanda_tangan/asesor') }}/<?= $ttd_asesor[0]->tanda_tangan ?>" style="width: 280px; display: block; margin-left: auto;margin-right: auto;">
                                                            <div id="el-tglttdasesor" style="width:100%; float:right;">
                                                                <br>
                                                            {{ \Carbon\Carbon::parse($data_ak01[0]->tgl_ttd_asesor)->format('d-m-Y')}}
                                                            </div>
                                                            <br />
                                                        </td>
                                                        <td style="border: 0px" colspan="3" style="height: 100px;">
                                                            <p>Tanda tangan Asesi dan tanggal:</p>
                                                            <label for="ttd">Tanda Tangan Asesi:</label>
                                                            <div id="el-ttdasesi">
                                                                <img id="gambarTandaTangan" src="{{ asset('assets/document/tanda_tangan/asesi') }}/<?= $user->tanda_tangan ?>" style="width: 280px; display: block; margin-left: auto;margin-right: auto;">
                                                                <input type="hidden" id="ttd_asesi" name="ttd_asesi" value="<?= $user->tanda_tangan ?>">
                                                            </div>
                                                                <br>
                                                            <div id="el-tglttdasesi" style="width:100%;"><p style="width: 280px; display: block; margin-left: auto;margin-right: auto;">{{ \Carbon\Carbon::parse($data_ak01[0]->tanggalpemohon)->format('d-m-Y')}}</p></div>
                                                            <br />

                                                            
                                                        </td>
                                                    </tr>
                                                    </div>
                                                </table>
                                        </div>
                                        <br />
                                        <button class="noprint" onclick="printAk01()" type="button" class="btn btn-outline-primary block">
                                            Print
                                        </button>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>
<!--Basic Modal -->

<script src="{{ asset('assets/js/monitoring/print_ak01.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>



