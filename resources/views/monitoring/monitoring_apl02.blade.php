@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3><?= $title_sub_menu; ?></h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                   
                                    <input type="hidden" id="idJadwalAsesmen" name="idJadwalAsesmen" value="<?= $data_apl02[0]->id_jadwal ?>">
                                    <input type="hidden" id="no_reg" name="no_reg" value="<?= $data_apl02[0]->no_Reg ?>">
                                    <div class="table-responsive">
                                        <label for=""> <strong> *Pilih Jadwal Asesmen</strong></label>
                                        <br>
                                        <br>
                                        <table id="tblAm02" class="table">
                                            <thead>
                                                <tr>
                                                    <th>ActionButton</th>
                                                    <th>Tanggal Uji</th>
                                                    <th>Nama Jadwal</th>
                                                    <th>Skema</th>
                                                    <th>Nomor</th>
                                                    <th>Nama Asesi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>


<script src="{{ asset('assets/js/monitoring/data_asesi_apl02.js') }}"></script>

@endsection