@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>Pilih Asesor</h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">

                                    <div class="table-responsive">
                                        <table id="jadwalLanjutTuk" class="table">
                                            <thead>
                                                <tr>
                                                    <th>Action</th>
                                                    <th>Nama Jadwal</th>
                                                    <th>Tanggal Uji</th>
                                                    <th>Asesor</th>
                                                    <th>No Reg</th>
                                                    <th>Jumlah Asesi</th>
                                                    <th>Nama LSP</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($jadwal as $d)
                                                <tr>
                                                    <td><a href="/pilih_asesi_monitoring_ia11/{{$d->no_reg}}/{{$d->id_jadwal}}"><button class="btn btn-outline-primary btn-sm block">Lanjut Pilih Asesi</button></a></td>
                                                    <td>{{$d -> nama_jadwal}}</td>
                                                    <td>{{$d -> tanggal_uji}}</td>
                                                    <td>{{$d -> asesor}}</td>
                                                    <td>{{$d -> no_reg}}</td>
                                                    <td>{{$d -> jumlah_asesi}}</td>
                                                    <td>{{$d -> nama_lsp}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>



<!--Basic Modal -->

<!-- <script src="{{ asset('assets/js/manajemen_jadwal/jadwal_asesmen.js') }}"></script> -->


@endsection