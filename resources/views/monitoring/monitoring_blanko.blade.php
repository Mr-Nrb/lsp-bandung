@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                </h4>
                                <hr>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                   
                                    <div class="table-responsive">
                                        <table class="table display">
                                            <thead>
                                                <tr>
                                                    <th>Judul Skema</th>
                                                    <th>Nomor</th>
                                                    <th>TUK</th>
                                                    <th>Asesor</th>
                                                    <th>No. Registrasi</th>
                                                    <th>Tanggal Uji</th>
                                                    <th>Judul Jadwal</th>
                                                    <th>Tanda Tangan Asesor</th>
                                                    <th>Tanggal Tanda Tangan</th>
                                                    <th>Keterangan 1</th>
                                                    <th>Keterangan 2 </th>
                                                    <th>Keterangan 3</th>
                                                    <th>Catatan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                               
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>


<!-- <script>
    $(document).ready(function() {
        $('#tbl_monitoring_apl01').DataTable({
            dom: "Bfrtip"

        });
    });
</script> -->


@endsection