@extends('template_dashboard.main')
@section('isiDashboard')
<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <h3>Data Asesi AK01</h3>
    </div>

    <div class="page-content">
        <section class="row">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12 col-xl-12">

                        <div class="card">

                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="tbl_monitoring_ia11" class="table display">
                                            <thead>
                                                <tr>
                                                    <th>Action</th>
                                                    <th>Judul jadwal</th>
                                                    <th>Tanggal asesmen</th>
                                                    <th>Nama asesi</th>
                                                    <th>Nomor Skema</th>
                                                    <th>Peninjau</th>
                                                    <th>Asesor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data_ia11 as $row)
                                                <tr>
                                                    <td><button class="btn btn-outline-primary btn-sm block"><a href="/detail_ia11/{{$row->id}}/{{$row->id_jadwal_asesmen}}">Detail</a></button></td>

                                                    <td> {{$row->juduljadwal}}</td>
                                                    <td> {{$row->tanggalasesmen}}</td>
                                                    <td> {{$row->nama_asesi}}</td>
                                                    <td> {{$row->nomor}}</td>
                                                    <td> {{$row->peninjau}}</td>
                                                    <td> {{$row->namaasesor}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
</div>


<!-- <script>
    $(document).ready(function() {
        $('#tbl_monitoring_apl01').DataTable({
            dom: "Bfrtip"

        });
    });
</script> -->


@endsection