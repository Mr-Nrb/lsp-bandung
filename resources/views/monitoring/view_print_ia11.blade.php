<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('assets/css') }}/ak01-lsp.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        td {
            border-collapse: collapse;
            border: solid 1px #000;
        }

        .align-center {
            text-align: center;
        }



        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        td {
            padding: 5px;
        }


        .page-content {
            margin-left: 50px;
            margin-right: 20px;
        }

        .page-heading {
            margin-left: 50px;
            margin-right: 20px;
        }

        .table5 {
            margin-right: 100px;
        }

        .noprint {
            background-color: #072d3a;
            display: inline-block;
            border-radius: 50%;
            width: 50px;
            height: 50px;
            text-align: center;
            color: white;
            /* float: right; */
        }

        @media print {
            .noprint {
                visibility: hidden;
            }
        }
    </style>
</head>

<body>
    <div id="main">


        <div class="page-heading">
            <h3><?= $title_sub_menu; ?></h3>
        </div>

        <div class="page-content">
            <section class="row">
                <div class="col-12 col-lg-12">
                    <div class="row">
                        <div class="col-12 col-xl-12">

                            <div class="card">

                                <div class="card-content">
                                    <div class="card-body">
                                        <!-- ppppppppppppppppppppppppppppp -->
                                        <div id="content" class="invoice-box" style="color: black;">
                                            <div class="table-responsive">
                                                <h3>FR.IA.11. MENINJAU INSTRUMEN ASESMEN</h3>
                                                <?php if ($data_ia11 == []) { ?>
                                                    <div class="text-center" style="margin-top:150px;margin-bottom:150px">
                                                        <h5><svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                                                            </svg> <strong> Tidak Ada data</strong></h5>
                                                    </div>
                                                <?php } else { ?>
                                                    <form class="form" action="/savemia11" enctype="multipart/form-data" method="POST">
                                                        @csrf
                                                        <table class="table">
                                                            <tr>

                                                                <input type="hidden" name="idJadwalAsesmen" id="idJadwalAsesmen" value="{{ $data_ia11[0]->id_apl01 }}">
                                                                <td><label for="judul">Judul</label></td>
                                                                <td>:</td>
                                                                <td style="padding-right:200px;"> <input type="hidden" name="juduljadwal" id="juduljadwal" value="{{ $data_ia11[0]->skema }}"> {{ $data_ia11[0]->skema }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><label for="nomor">Nomor</label></td>
                                                                <td>:</td>
                                                                <td> <input type="hidden" name="nomor" id="nomor" value="{{ $data_ia11[0]->nomor }}"> {{ $data_ia11[0]->nomor }}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><label for="nama">Nama Asesi</label></td>
                                                                <td>:</td>
                                                                <td> <input type="hidden" name="nama_asesi" id="nama_asesi" value="{{ $data_ia11[0]->nama_asesi }}"> {{ $data_ia11[0]->nama_asesi }} </td>
                                                            </tr>
                                                            <tr>
                                                                <td><label for="namasor">Nama Asesor</label></td>
                                                                <td>:</td>
                                                                <td> <input type="hidden" name="namaasesor" id="namaasesor" value="{{ $data_ia11[0]->namaasesor }}"> {{ $data_ia11[0]->namaasesor }} </td>
                                                            </tr>
                                                            <tr>
                                                                <td><label for="tanggalasesmen">Tanggal</label></td>
                                                                <td>:</td>
                                                                <td> <input type="hidden" name="tanggalasesmen" id="tanggalasesmen" value="{{ $data_ia11[0]->tanggalasesmen }}"> {{ $data_ia11[0]->tanggalasesmen }} </td>
                                                            </tr>

                                                        </table>

                                                        <br>
                                                        <br>

                                                        <table class="table2" style="border: 1px solid black;  height:100px;" border-collapse="collapse">
                                                            <tr style="border: 1px solid black;">
                                                                <th style="background:#fac090;" class="text-center">PANDUAN BAGI PENINJAU/PENYELIA</th>
                                                            </tr>
                                                            <tr>
                                                                <td>

                                                                    <ul>
                                                                        <li>Isilah tabel ini sesuai dengan informasi sesuai pertanyaan/pernyataan dalam table dibawah ini.</li>
                                                                        <li>Beri tanda centang pada hasil penilaian instrumen asesmen berdasarkan tinjauan anda dengan jastifikasi professional anda</li>
                                                                        <li>Berikan komentar dengan jastifikasi profesional anda</li>
                                                                    </ul>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br>

                                                        <div id="formsbs">
                                                            <p><strong>Daftar Unit Kompetensi sesuai kemasan:</strong></p>
                                                            <table class="table3" style="border: 1px solid black;" border-collapse="collapse">
                                                                <tr style="border: 1px solid black;" class="text-center">
                                                                    <th style="background:#fac090;">No.</th>
                                                                    <th style="background:#fac090;">Kode Unit</th>
                                                                    <th style="background:#fac090;">Judul Unit</th>
                                                                </tr>
                                                                <?php $i = 1;
                                                                foreach ($get_skema as $row) : ?>
                                                                    <tr style="border: 1px solid black;" class="table6">
                                                                        <td style="border: 1px solid black;" class="text-center">
                                                                            <label for="noUrut"><?= $i ?></label>
                                                                        </td>
                                                                        <td style="border: 1px solid black;" class="text-center">
                                                                            <output id="no_uk" name="no_uk" class=" bg2" value="<?= $row->kode ?>"><?= $row->kode ?></output>
                                                                        </td>
                                                                        <td style="border: 1px solid black;">
                                                                            <output value="<?= $row->nama ?>" id="uk" name="uk<?= $i ?>" class=" bg2"><?= $row->nama ?></output>
                                                                        </td>
                                                                    </tr>
                                                                <?php $i++;
                                                                endforeach; ?>
                                                            </table>
                                                        </div>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <table class="table4" style="border: 1px solid black;" border-collapse="collapse">

                                                            <tr class="text-center">
                                                                <th>Kegiatan Asesmen</th>
                                                                <th colspan="2">Ya/Tidak</th>
                                                                <th>Komentar</th>
                                                            </tr>
                                                            <tr>
                                                                @if($data_ia11[0]->instruksi_jelas == "Ya")
                                                                <td style="width:500px;">Instruksi perangkat asesmen dan kondisi asesmen diidentifikasi dengan jelas</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input input class="form-check-input" type="radio" name="flexRadioDisabled" id="flexRadioCheckedDisabled" checked disabled>
                                                                        <label class="form-check-label" for="flexRadioCheckedDisabled">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input input class="form-check-input" type="radio" name="flexRadioDisabled" id="flexRadioCheckedDisabled" disabled>
                                                                        <label class="form-check-label" for="instruksi_jelas2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                @elseif($data_ia11[0]->instruksi_jelas == "Tidak")
                                                                <td style="width:500px;">Instruksi perangkat asesmen dan kondisi asesmen diidentifikasi dengan jelas</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input input class="form-check-input" type="radio" name="flexRadioDisabled" id="flexRadioCheckedDisabled">
                                                                        <label class="form-check-label" for="instruksi_jelas1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input input class="form-check-input" type="radio" name="flexRadioDisabled" id="flexRadioCheckedDisabled" checked>
                                                                        <label class="form-check-label" for="instruksi_jelas2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                @else
                                                                <td style="width:500px;">Instruksi perangkat asesmen dan kondisi asesmen diidentifikasi dengan jelas</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="instruksi_jelas" id="instruksi_jelas1">
                                                                        <label class="form-check-label" for="instruksi_jelas1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="instruksi_jelas" id="instruksi_jelas2">
                                                                        <label class="form-check-label" for="instruksi_jelas2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                @endif
                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                            </tr>

                                                            <tr>
                                                                @if($data_ia11[0]->informasi_tepat == "Ya")
                                                                <td>Informasi tertulis dituliskan secara tepat</td>

                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="informasi_tepat" id="informasi_tepat1" checked disabled>
                                                                        <label class="form-check-label" for="informasi_tepat1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="informasi_tepat" id="informasi_tepat2" disabled>
                                                                        <label class="form-check-label" for="informasi_tepat2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @elseif($data_ia11[0]->informasi_tepat == "Tidak")
                                                                <td>Informasi tertulis dituliskan secara tepat</td>

                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="informasi_tepat" id="informasi_tepat1" disabled>
                                                                        <label class="form-check-label" for="informasi_tepat1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="informasi_tepat" id="informasi_tepat2" checked disabled>
                                                                        <label class="form-check-label" for="informasi_tepat2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @else
                                                                <td>Informasi tertulis dituliskan secara tepat</td>

                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="informasi_tepat" id="informasi_tepat1" disabled>
                                                                        <label class="form-check-label" for="informasi_tepat1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="informasi_tepat" id="informasi_tepat2" disabled>
                                                                        <label class="form-check-label" for="informasi_tepat2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @endif
                                                            </tr>
                                                            <tr>
                                                                @if($data_ia11[0]->kegiatan_asesmen == "Ya")
                                                                <td>Kegiatan asesmen membahas persyaratan bukti untuk kompetensi atau kompetensi yang diakses</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="kegiatan_asesmen" id="kegiatan_asesmen1" checked disabled>
                                                                        <label class="form-check-label" for="kegiatan_asesmen1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="kegiatan_asesmen" id="kegiatan_asesmen2" disabled>
                                                                        <label class="form-check-label" for="kegiatan_asesmen2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @elseif($data_ia11[0]->kegiatan_asesmen == "Tidak")
                                                                <td>Kegiatan asesmen membahas persyaratan bukti untuk kompetensi atau kompetensi yang diakses</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="kegiatan_asesmen" id="kegiatan_asesmen1" disabled>
                                                                        <label class="form-check-label" for="kegiatan_asesmen1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="kegiatan_asesmen" id="kegiatan_asesmen2" checked disabled>
                                                                        <label class="form-check-label" for="kegiatan_asesmen2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @else
                                                                <td>Kegiatan asesmen membahas persyaratan bukti untuk kompetensi atau kompetensi yang diakses</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="kegiatan_asesmen" id="kegiatan_asesmen1">
                                                                        <label class="form-check-label" for="kegiatan_asesmen1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="kegiatan_asesmen" id="kegiatan_asesmen2">
                                                                        <label class="form-check-label" for="kegiatan_asesmen2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @endif
                                                            </tr>
                                                            <tr>
                                                                <!--  -->
                                                                @if($data_ia11[0]->kesulitan_bahasa == "Ya")
                                                                <td>Tingkat kesulitan bahasa, literasi, dan berhitung sesuai dengan tingkat unit kompetensi yang dinilai</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="kesulitan_bahasa" id="kesulitan_bahasa1" checked disabled>
                                                                        <label class="form-check-label" for="kesulitan_bahasa1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="kesulitan_bahasa" id="kesulitan_bahasa2">
                                                                        <label class="form-check-label" for="kesulitan_bahasa2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @elseif($data_ia11[0]->kesulitan_bahasa == "Tidak")
                                                                <td>Tingkat kesulitan bahasa, literasi, dan berhitung sesuai dengan tingkat unit kompetensi yang dinilai</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="kesulitan_bahasa" id="kesulitan_bahasa1" disabled>
                                                                        <label class="form-check-label" for="kesulitan_bahasa1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="kesulitan_bahasa" id="kesulitan_bahasa2" checked disabled>
                                                                        <label class="form-check-label" for="kesulitan_bahasa2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @else
                                                                <td>Tingkat kesulitan bahasa, literasi, dan berhitung sesuai dengan tingkat unit kompetensi yang dinilai</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="kesulitan_bahasa" id="kesulitan_bahasa1" disabled>
                                                                        <label class="form-check-label" for="kesulitan_bahasa1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="kesulitan_bahasa" id="kesulitan_bahasa2" disabled>
                                                                        <label class="form-check-label" for="kesulitan_bahasa2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @endif
                                                                <!--  -->
                                                            </tr>

                                                            <tr>
                                                                <!--  -->
                                                                @if($data_ia11[0]->kesulitan_kegiatan == "Ya")
                                                                <td>Tingkat kesulitan kegiatan sesuai dengan kompetensi atau kompetensi yang diases.</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="kesulitan_kegiatan" id="kesulitan_kegiatan1" checked disabled>
                                                                        <label class="form-check-label" for="kesulitan_kegiatan1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="kesulitan_kegiatan" id="kesulitan_kegiatan2">
                                                                        <label class="form-check-label" for="kesulitan_kegiatan2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @elseif($data_ia11[0]->kesulitan_kegiatan == "Tidak")
                                                                <td>Tingkat kesulitan kegiatan sesuai dengan kompetensi atau kompetensi yang diases.</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="kesulitan_kegiatan" id="kesulitan_kegiatan1" disabled>
                                                                        <label class="form-check-label" for="kesulitan_kegiatan1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="kesulitan_kegiatan" id="kesulitan_kegiatan2" checked disabled>
                                                                        <label class="form-check-label" for="kesulitan_kegiatan2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @else
                                                                <td>Tingkat kesulitan kegiatan sesuai dengan kompetensi atau kompetensi yang diases.</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="kesulitan_kegiatan" id="kesulitan_kegiatan1" disabled>
                                                                        <label class="form-check-label" for="kesulitan_kegiatan1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="kesulitan_kegiatan" id="kesulitan_kegiatan2" disabled>
                                                                        <label class="form-check-label" for="kesulitan_kegiatan2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @endif
                                                                <!--  -->
                                                            </tr>

                                                            <tr>
                                                                <!--  -->
                                                                @if($data_ia11[0]->ceklis_asesmen == "Ya")
                                                                <td>Contoh, benchmark dan / atau ceklis asesmen tersedia untuk digunakan dalam pengambilan keputusan asesmen.</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="ceklis_asesmen" id="ceklis_asesmen1" disabled checked>
                                                                        <label class="form-check-label" for="ceklis_asesmen1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="ceklis_asesmen" id="ceklis_asesmen2" disabled>
                                                                        <label class="form-check-label" for="ceklis_asesmen2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @elseif($data_ia11[0]->ceklis_asesmen == "Tidak")
                                                                <td>Contoh, benchmark dan / atau ceklis asesmen tersedia untuk digunakan dalam pengambilan keputusan asesmen.</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="ceklis_asesmen" id="ceklis_asesmen1" disabled>
                                                                        <label class="form-check-label" for="ceklis_asesmen1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="ceklis_asesmen" id="ceklis_asesmen2" disabled checked>
                                                                        <label class="form-check-label" for="ceklis_asesmen2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @else
                                                                <td>Contoh, benchmark dan / atau ceklis asesmen tersedia untuk digunakan dalam pengambilan keputusan asesmen.</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="ceklis_asesmen" id="ceklis_asesmen1" disabled>
                                                                        <label class="form-check-label" for="ceklis_asesmen1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="ceklis_asesmen" id="ceklis_asesmen2" disabled>
                                                                        <label class="form-check-label" for="ceklis_asesmen2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @endif
                                                                <!--  -->
                                                            </tr>
                                                            <tr>
                                                                <!--  -->
                                                                @if($data_ia11[0]->modifikasi == "Ya")
                                                                <td>Diperlukan modifikasi (seperti yang diidentifikasi dalam Komentar)</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="modifikasi" id="modifikasi1" disabled checked>
                                                                        <label class="form-check-label" for="modifikasi1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="modifikasi" id="modifikasi2" disabled>
                                                                        <label class="form-check-label" for="modifikasi2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @elseif($data_ia11[0]->modifikasi == "Tidak")
                                                                <td>Diperlukan modifikasi (seperti yang diidentifikasi dalam Komentar)</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="modifikasi" id="modifikasi1" disabled>
                                                                        <label class="form-check-label" for="modifikasi1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="modifikasi" id="modifikasi2" disabled checked>
                                                                        <label class="form-check-label" for="modifikasi2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @else
                                                                <td>Diperlukan modifikasi (seperti yang diidentifikasi dalam Komentar)</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="modifikasi" id="modifikasi1" disabled>
                                                                        <label class="form-check-label" for="modifikasi1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="modifikasi" id="modifikasi2" disabled>
                                                                        <label class="form-check-label" for="modifikasi2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @endif
                                                                <!--  -->
                                                            </tr>
                                                            <tr>
                                                                <!--  -->
                                                                @if($data_ia11[0]->tugas_asesmen == "Ya")
                                                                <td>Tugas asesmen siap digunakan</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="tugas_asesmen" id="tugas_asesmen1" disabled checked>
                                                                        <label class="form-check-label" for="tugas_asesmen1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="tugas_asesmen" id="tugas_asesmen2" disabled>
                                                                        <label class="form-check-label" for="tugas_asesmen2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @elseif($data_ia11[0]->tugas_asesmen == "Tidak")
                                                                <td>Tugas asesmen siap digunakan</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="tugas_asesmen" id="tugas_asesmen1" disabled checked>
                                                                        <label class="form-check-label" for="tugas_asesmen1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="tugas_asesmen" id="tugas_asesmen2" disabled>
                                                                        <label class="form-check-label" for="tugas_asesmen2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @else
                                                                <td>Tugas asesmen siap digunakan</td>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Ya" name="tugas_asesmen" id="tugas_asesmen1" disabled checked>
                                                                        <label class="form-check-label" for="tugas_asesmen1">
                                                                            Ya
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" value="Tidak" name="tugas_asesmen" id="tugas_asesmen2" disabled>
                                                                        <label class="form-check-label" for="tugas_asesmen2">
                                                                            Tidak
                                                                        </label>
                                                                    </div>
                                                                </td>

                                                                <td style="border: 1px solid black;" class="tatd" colspan="2"><input class="ta form-control" type="text" readonly></td>
                                                                @endif
                                                                <!--  -->
                                                            </tr>

                                                        </table>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <table class="table-ttd" style="border: 1px solid black;" border-collapse="collapse">
                                                            <tr style="border: 1px solid black;" class="text-center">
                                                                <th>Nama Peninjau</th>
                                                                <th>Tanda Tangan Peninjau</th>
                                                                <th>Komentar</th>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: 1px solid black;">
                                                                    <div class="text-center"> <input class="form-control" type="text" name="peninjau" value="{{ $data_ia11[0]->peninjau }}" readonly> </div>
                                                                </td>

                                                                <td style="width:38%; ">
                                                                    <div class="text-center"><img id="gambarTandaTangan" src="{{ asset('assets/document/tanda_tangan/penyelia') }}/<?= $user->tanda_tangan ?>" style="width: 380px" height="380px"> <input type="hidden" id="ttdpenyelia" name="ttdpenyelia" value="<?= $user->tanda_tangan ?>"> </div>
                                                                </td>

                                                                <td style="border: 1px solid black;">
                                                                    <div class="text-center"> <input class="form-control" type="text" name="komentar"> </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br>
                                                        <br>
                                                        <button class="noprint" onclick="printIa11()" type="button" class="btn btn-outline-primary block">
                                                            Print
                                                        </button>
                                                    </form>
                                                <?php } ?>


                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

            </section>
        </div>
    </div>
    <script src="{{ asset('assets/js/monitoring/print_ia11.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>