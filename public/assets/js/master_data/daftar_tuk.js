$(document).ready(function () {
    datatabel = $("#tblDaftarTuk").DataTable({
        dom: "Bfrtip",
        ajax: {
            type: "GET",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/getDaftarTuk",
            dataSrc: "",
            dataType: "json",
        },
        columns: [
            {
                render: function (full, type, data, meta) {
                    return `
                    <div style="margin-left:15%" >
                    <button type="button" class="btn btn-outline-warning btn-sm block" data-bs-toggle="modal" data-bs-target="#default" onclick="edit(${data.id})">
                    <i class="bi bi-pencil-square"></i>
                    </button>
                    <button type="button" id="btnHapus" class="btn btn-outline-danger btn-sm block" onclick="hapus(${data.id})">
                    <i class="bi bi-trash"></i>
                    </button>
                    <div>
                `;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_kejuruan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.instansi_satker;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.alamat;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.kota;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.email;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_ketua;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nomor_ketua;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_pengelola;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nomor_pengelola;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.persyaratan_teknis;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.ruang_teori;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.ruang_praktek;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.perlengkapan_i;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.perlengkapan_ii;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.perlengkapan_iii;
                },
            },
        ],
    });
});

function hapus(id) {
    alert(id);
    if (confirm("Yakin Ingin Menghapus?")) {
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/hapusDaftarTuk",
            type: "POST",
            data: {
                id: id,
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                Swal.fire({
                    icon: "success",
                    title: "Berhasil",
                    text: "Data Berhasil Dihapus",
                });
                reload_table(datatabel);
            },
        });
    } else {
        return false;
    }
}

function reload_table(table) {
    table.ajax.reload();
}

function edit(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "/getDataDaftarTuk",
        type: "GET",
        data: {
            id: id,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#id").val(id);
            $("#nama").val(data.dataTuk.nama);
            $("#kejuruan").val(data.dataTuk.kejuruan);
            $("#instansi_satker").val(data.dataTuk.instansi_satker);
            $("#alamat").val(data.dataTuk.alamat);
            $("#kota").val(data.dataTuk.kota);
            $("#email").val(data.dataTuk.email);
            $("#nama_ketua").val(data.dataTuk.nama_ketua);
            $("#nomor_ketua").val(data.dataTuk.nomor_ketua);
            $("#nama_pengelola").val(data.dataTuk.nama_pengelola);
            $("#nomor_pengelola").val(data.dataTuk.nomor_pengelola);
            if (data.dataTuk.persyaratan_teknis != "") {
                $("#riview_persyaratan_teknis").attr(
                    "src",
                    "assets/document/persyaratan_teknis/" +
                        data.dataTuk.persyaratan_teknis
                );
            } else {
                $("#riview_persyaratan_teknis").attr("src", "");
            }
            if (data.dataTuk.ruang_teori != "") {
                $("#riview_ruang_teori").attr(
                    "src",
                    "assets/document/ruang_teori/" + data.dataTuk.ruang_teori
                );
            } else {
                $("#riview_ruang_teori").attr("src", "");
            }
            if (data.dataTuk.ruang_praktek != "") {
                $("#riview_ruang_praktek").attr(
                    "src",
                    "assets/document/ruang_praktek/" +
                        data.dataTuk.ruang_praktek
                );
            } else {
                $("#riview_ruang_praktek").attr("src", "");
            }
            if (data.dataTuk.perlengkapan_i != "") {
                $("#riview_perlengkapan_i").attr(
                    "src",
                    "assets/document/perlengkapan/" +
                        data.dataTuk.perlengkapan_i
                );
            } else {
                $("#riview_perlengkapan_i").attr("src", "");
            }
            if (data.dataTuk.perlengkapan_ii != "") {
                $("#riview_perlengkapan_ii").attr(
                    "src",
                    "assets/document/perlengkapan/" +
                        data.dataTuk.perlengkapan_ii
                );
            } else {
                $("#riview_perlengkapan_ii").attr("src", "");
            }
            if (data.dataTuk.perlengkapan_iii != "") {
                $("#riview_perlengkapan_iii").attr(
                    "src",
                    "assets/document/perlengkapan/" +
                        data.dataTuk.perlengkapan_iii
                );
            } else {
                $("#riview_perlengkapan_iii").attr("src", "");
            }

            let buttonedit = `
            <button type="button" id="btnedit"  class="btn btn-warning me-1 mb-1">
                Update
            </button>
            `;
            $("#button_").html(buttonedit);

            $("#btnedit").click(function (event) {
                event.preventDefault();

                let new_form = $("#form_crud")[0];
                let data = new FormData(new_form);

                $.ajax({
                    url: "updateDataDaftarTuk",
                    enctype: "multipart/form-data",
                    processData: false,
                    contentType: false,
                    cache: false,
                    type: "POST",
                    data: data,
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil",
                            text: "Data Berhasil Diubah",
                        });

                        reload_table(datatabel);
                        cancel();
                    },
                });
            });
        },
    });
}

function cancel() {
    $("#default").modal("hide");
    $("#id").val("");
    $("#nama").val("");
    $("#kejuruan").val("");
    $("#instansi_satker").val("");
    $("#alamat").val("");
    $("#kota").val("");
    $("#email").val("");
    $("#nama_ketua").val("");
    $("#nomor_ketua").val("");
    $("#nama_pengelola").val("");
    $("#nomor_pengelola").val("");
    $("#riview_persyaratan_teknis").attr("src", "");
    $("#riview_ruang_teori").attr("src", "");
    $("#riview_ruang_praktek").attr("src", "");
    $("#riview_perlengkapan_i").attr("src", "");
    $("#riview_perlengkapan_ii").attr("src", "");
    $("#riview_perlengkapan_iii").attr("src", "");

    let buttontambah = `
    <button type="submit" class="btn btn-primary me-1 mb-1">
        Simpan
    </button>
                `;
    $("#button_").html(buttontambah);
}
