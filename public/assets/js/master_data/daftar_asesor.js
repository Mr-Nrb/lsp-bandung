$(document).ready(function () {
    datatabel = $("#tblDaftarAsesor").DataTable({
        dom: "Bfrtip",
        ajax: {
            type: "GET",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/getDaftarAsesor",
            dataSrc: "",
            dataType: "json",
        },
        columns: [
            {
                render: function (full, type, data, meta) {
                    return `
                    <div style="margin-left:15%" >
                    <button type="button" class="btn btn-outline-warning btn-sm block" data-bs-toggle="modal" data-bs-target="#default" onclick="edit(${data.id})">
                    <i class="bi bi-pencil-square"></i>
                    </button>
                    <button type="button" id="btnHapus" class="btn btn-outline-danger btn-sm block" onclick="hapus(${data.id})">
                    <i class="bi bi-trash"></i>
                    </button>
                    <div>
                `;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.name;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.met_asesor;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_role;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nik;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tempat_lahir;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tanggal_lahir;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_kelamin;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_kebangsaan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.alamat;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.kodepos;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_provinsi;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_kabupatenkota;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.no_hp;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.email;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_pendidikan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_pekerjaan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nomor_sertif;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tanggal_sertif;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tanggal_sertif_exp;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.foto_sertif_asesor;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.foto_sertif_teknik;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nomor_blanko;
                },
            },
        ],
    });
});

function hapus(id) {
    alert(id);
    if (confirm("Yakin Ingin Menghapus?")) {
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/hapusDaftarAsesor",
            type: "POST",
            data: {
                id: id,
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                Swal.fire({
                    icon: "success",
                    title: "Berhasil",
                    text: "Data Berhasil Dihapus",
                });
                reload_table(datatabel);
            },
        });
    } else {
        return false;
    }
}

function reload_table(table) {
    table.ajax.reload();
}

function edit(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "/getDataDaftarAsesor",
        type: "POST",
        data: {
            id: id,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#id").val(id);
            $("#nama").val(data.dataUsers.name);
            $("#role_id").val(data.dataUsers.role_id);
            $("#met_asesor").val(data.dataAsesor.met_asesor);
            $("#nik").val(data.dataUsers.nik);
            $("#tempat_lahir").val(data.dataUsers.tempat_lahir);
            $("#tanggal_lahir").val(data.dataUsers.tanggal_lahir);
            // $("#jenis_kelamin").val(data.dataUsers.jenis_kelamin);
            if (data.dataUsers.jenis_kelamin == 1) {
                let jenis_kelamin = `
                <label style="color:black">Jenis Kelamin</label>
                <br>
                <input type="radio" name="jenis_kelamin" value="1" id="jenis_kelamin" checked><label for="laki"> Laki - laki</label>
                <br>
                <input type="radio" name="jenis_kelamin" value="2" id="jenis_kelamin"><label for="perempuan"> Perempuan</label>
                `;
                $("#jenis_kelamin").html(jenis_kelamin);
            } else if (data.dataUsers.jenis_kelamin == 2) {
                let jenis_kelamin = `
                <label style="color:black">Jenis Kelamin</label>
                <br>
                <input type="radio" name="jenis_kelamin" value="1" id="jenis_kelamin"><label for="laki"> Laki - laki</label>
                <br>
                <input type="radio" name="jenis_kelamin" value="2" id="jenis_kelamin" checked><label for="perempuan"> Perempuan</label>
                `;
                $("#jenis_kelamin").html(jenis_kelamin);
            }
            $("#kebangsaan").val(data.dataUsers.kebangsaan);
            $("#email").val(data.dataUsers.email);
            $("#password").val(data.dataUsers.password);
            $("#alamat").val(data.dataUsers.alamat);
            $("#kode_pos").val(data.dataUsers.kodepos);
            $("#nomor_sertif").val(data.dataAsesor.nomor_sertif);
            $("#tanggal_sertif").val(data.dataAsesor.tanggal_sertif);
            $("#tanggal_sertif_exp").val(data.dataAsesor.tanggal_sertif_exp);
            $("#nomor_blanko").val(data.dataAsesor.nomor_blanko);
            $("#token").val(data.dataUsers.token);
            if (data.dataAsesor.foto_sertif_asesor != "") {
                $("#riview_foto_sertif_asesor").attr(
                    "src",
                    "assets/document/foto_sertif_asesor/" +
                        data.dataAsesor.foto_sertif_asesor
                );
            } else {
                $("#riview_foto_sertif_asesor").attr("src", "");
            }
            if (data.dataAsesor.foto_sertif_teknik != "") {
                $("#riview_foto_sertif_teknik").attr(
                    "src",
                    "assets/document/foto_sertif_teknik/" +
                        data.dataAsesor.foto_sertif_teknik
                );
            } else {
                $("#riview_foto_sertif_teknik").attr("src", "");
            }

            let buttonedit = `
            <button type="button" id="btnedit"  class="btn btn-warning me-1 mb-1">
                Update
            </button>
            `;
            $("#button_").html(buttonedit);

            $("#btnedit").click(function (event) {
                event.preventDefault();

                let new_form = $("#form_crud")[0];
                let data = new FormData(new_form);

                $.ajax({
                    url: "/updateDataDaftarAsesor",
                    enctype: "multipart/form-data",
                    processData: false,
                    contentType: false,
                    cache: true,
                    type: "POST",
                    data: data,
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil",
                            text: "Data Berhasil Diubah",
                        });

                        reload_table(datatabel);
                        cancel();
                    },
                });
            });
        },
    });

    function cancel() {
        $("#default").modal("hide");
        $("#id").val("");
        $("#nama").val("");
        $("#role_id").val("");
        $("#met_asesor").val("");
        $("#nik").val("");
        $("#tempat_lahir").val("");
        $("#tanggal_lahir").val("");
        $("#jenis_kelamin").val("");
        let jenis_kelamin = `
                    <label style="color:black">Jenis Kelamin</label>
                    <br>
                    <input type="radio" name="jenis_kelamin" value="1" id="laki"><label for="laki"> Laki - laki</label>
                    <br>
                    <input type="radio" name="jenis_kelamin" value="2" id="perempuan"><label for="perempuan"> Perempuan</label>
                    `;
        $("#jenis_kelamin").html(jenis_kelamin);
        $("#kebangsaan").val("");
        $("#email").val("");
        $("#password").val("");
        $("#nama").val("");
        $("#akses").val("");
        $("#alamat").val("");
        $("#kode_pos").val("");
        $("#nomor_sertif").val("");
        $("#tanggal_sertif").val("");
        $("#tanggal_sertif_exp").val("");
        $("#foto_sertif_asesor").val("");
        $("#foto_sertif_teknik").val("");
        $("#nomor_blanko").val("");
        $("#riview_foto_sertif_asesor").attr("src", "");
        $("#riview_foto_sertif_teknik").attr("src", "");
        $("#token").val("");

        let buttontambah = `
        <button type="submit" class="btn btn-primary me-1 mb-1">
            Simpan
        </button>
                    `;
        $("#button_").html(buttontambah);
    }
}
