$(document).ready(function () {
    datatabel = $("#jadwalAsesmen").DataTable({
        dom: "Bfrtip",
        ajax: {
            type: "GET",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        url: "/getJadwalMonitoring_ak01",
            dataSrc: "",
            dataType: "json",
        },
        columns: [
            {
                render: function (full, type, data, meta) {
                    return `
                    <div style="margin-left:15%" >
                        <a href="jadwalLanjut_ak01_monitoring/${data.id}" class="btn btn-outline-primary btn-sm block" >Pilih Jadwal Asesmen
                        </a>
                    </div>
                `;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tanggal_uji;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_jadwal;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_tuk;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tema;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_skema;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nomor_skema;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.id_skema;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.target_peserta;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.jam_uji;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.lokasi_uji;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_sumber_anggaran;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.anggaran_perasesi;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nomor_spk;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tanggal_spk;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tgl_mulai_pelatihan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tgl_akhir_pelatihan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.durasi_pelatihan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_pelatihan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_instansi;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_lsp;
                },
            },
        ],
    });
});

function reload_table(table) {
    table.ajax.reload();
}
