$.ajax({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
    url: "/GetSkema_full/{id_jadwal_asesmen}",
    type: "GET",
    dataType: "json",
    success: function (data) {
        console.log(data);
        console.log(data.id_skema);
        id = data.id_skema;
        let menu = data.data_skema;
        out = laso1(id, "nama");
        document.getElementById("sskema").innerHTML = out;

        function laso1(induk, nama) {
            arr1 = filter(menu, { induk: induk });
            console.log(arr1);
            out = "";
            if (arr1.length) {
                for (var i1 = 0; i1 < arr1.length; i1++) {
                    node1 = arr1[i1];
                    noUrut = i1 + 1;

                    out +=
                        '<table name="tabel 1" class="table apl02 pam2" border="2">\
                            <tr>\
                            <td style="border: 1px solid gray;" class="uk">\
                              <label for="uk">Unit Kompetensi:</label>\
                            </td>\
                            <td style="border: 1px solid gray;" colspan="3">\
                              <p><input value="' +
                        node1.nama +
                        '" id="unit_kompetensi' +
                        noUrut +
                        '" name="unit_kompetensi[]" class="bg2" style="border:none" size="70" /></p>\
                            </td>\
                          </tr>\
                        <tr>\
                        <td style="border: 1px solid gray;" class="uk">\
                            <label for="uk">Kode Unit:</label>\
                        </td>\
                        <td style="border: 1px solid gray;" colspan="3">\
                            <p><input id="no_unit_kompetensi' +
                        noUrut +
                        '" name="no_unit_kompetensi[]" class=" bg2" value="' +
                        node1.kode +
                        '" style="border:none" /></p>\
                        </td>\
                        </tr>\
                        <tr>\
                        <td style="border: 1px solid gray;" colspan="3">\
                            <label class="f10 ds" for="ds">Dapatkah Saya ?</label>\
                        </td>\
                        </tr>';
                    out += laso(node1.id, "elemen", noUrut);

                    out +=
                        "</table>\
        <br>";
                }
            } else {
                out +=
                    '<table name="tabel 1" class="table apl02 pam2" border="2">\
                <tr>\
                  <td style="border: 1px solid gray;" >\
                    <p style="text-align: center;"> Gagal mengambil data skema. <br> Pastikan anda sudah memilih jadwal untuk menampilkan skema</p>\
                  </td>\
                </tr>\
              </table>';
            }
            return out;
        }

        function filter(arr, criteria) {
            return arr.filter(function (obj) {
                return Object.keys(criteria).every(function (c) {
                    return obj[c] == criteria[c];
                });
            });
        }

        function laso2(induk, nama) {
            arr2 = filter(menu, { induk: induk });
            console.log(menu);
            console.log(arr2);
            console.log(induk);

            out = "<ol>";
            for (var i2 = 0; i2 < arr2.length; i2++) {
                node2 = arr2[i2];
                out += "<li>" + node2[nama] + "</li>";
            }
            out += "</ol>";
            return out;
        }

        function laso(induk, nama, noUrut) {
            arr = filter(menu, { induk: induk });
            out = "<ol>";
            for (var i = 0; i < arr.length; i++) {
                console.log(i);
                node = arr[i];
                noUrut2 = i + 1;
                out += "<tr>";
                out +=
                    '<td style="border: 1px solid gray;" colspan="2" class="elemen">';
                out += "<li>";
                out +=
                    "Elemen " +
                    noUrut2 +
                    ':<p><input style="border:none" size="70"  name="elemen' +
                    noUrut +
                    '[]" id="elemen' +
                    noUrut +
                    "-" +
                    noUrut2 +
                    '" value="' +
                    node[nama] +
                    '" class="">';
                ("</p>");
                out += laso2(node.id, "kuk");
                out += "</li>";
                out += "</td>";
                // out+='<div id="kanan">coba</div>\';
                out +=
                    '<td style="border: 1px solid gray;" class="kbk">\
              K / BK :\
              <select  name="kbk' +
                    noUrut +
                    noUrut2 +
                    '[]" id="kbk' +
                    noUrut +
                    "-" +
                    noUrut2 +
                    '" class="form-select">\
                <option value="" disabled selected></option>\
                <option value="Kompeten">Kompeten</option>\
                <option value="Tidak Berkompeten">Tidak Berkompeten</option>\
              </select>\
              Bukti :\
              <select name="bukti' +
                    noUrut +
                    noUrut2 +
                    '[]" id="bukti' +
                    noUrut +
                    "-" +
                    noUrut2 +
                    '" class="form-select">\
                <option value="" disabled selected></option>\
                <option value="Ijazah">Ijazah</option>\
                <option value="Sertifikasi Pelatihan">Sertifikasi Pelatihan</option>\
                <option value="CV">CV</option>\
                <option value="Portofolio">Portofolio</option>\
              </select>\
              </td>';
                out += "</tr>";
            }
            out += "</ol>";
            return out;
        }

        out2 = `<tr>
                  <td style="border: 1px solid gray;" class="tabel" rowspan="2" ><label>Skema Sertifikasi (KKNI/Okupasi/Klaster)</label></td>
                  <td style="border: 1px solid gray;" class="tabel"><label for="skema">Skema</label></td>
                  <td style="border: 1px solid gray;" colspan="3"><p><input class name="skema" readonly id="skema" style="border: none; outline: none;"><p></td>
                </tr>
                <tr>
                  <td style="border: 1px solid gray;" class="tabel"><label for="no">Nomor</label></td>
                  <td style="border: 1px solid gray;" colspan="3"><p><input name="nomor" id="nomor" readonly style="border: none; outline: none;"></p></td>
                </tr>`;
        document.getElementById("tabel-skema").innerHTML = out2;
        let data_jadwal_asesmen = data.data_jadwal_asesmen;
        console.log(data_jadwal_asesmen);
        document.getElementById("skema").value =
            data_jadwal_asesmen[0].nama_skema;
        document.getElementById("nomor").value =
            data_jadwal_asesmen[0].nomor_skema;
    },
});
