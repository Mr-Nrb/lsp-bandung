$(document).ready(function () {
    datatabel = $("#tblDataInformasi").DataTable({
        dom: "Bfrtip",
        ajax: {
            type: "GET",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/getDataInformasi",
            dataSrc: "",
            dataType: "json",
        },
        columns: [
            {
                render: function (full, type, data, meta) {
                    return `
                    <button type="button" class="btn btn-outline-warning btn-sm block" title="Edit" data-bs-toggle="modal" data-bs-target="#default" onclick="edit(${data.id})">
                        <i class="bi bi-pencil-square"></i>
                    </button>
                    <button type="button" id="btnHapus" class="btn btn-outline-danger btn-sm block" title="Edit" onclick="hapus(${data.id})">
                        <i class="bi bi-trash"></i>
                    </button>
                `;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.judul_informasi;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.keterangan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.is_aktif;
                },
            },
        ],
    });
});

function hapus(id) {
    if (confirm("Yakin Ingin Menghapus?")) {
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/hapusDataInformasi",
            type: "POST",
            data: {
                id: id,
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                Swal.fire({
                    icon: "success",
                    title: "Berhasil",
                    text: "Data Berhasil Dihapus",
                });
                reload_table(datatabel);
            },
        });
    } else {
        return false;
    }
}

function reload_table(table) {
    table.ajax.reload();
}

function edit(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "/getDataInformasiById",
        type: "POST",
        data: {
            id: id,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#id").val(id);
            $("#judul_informasi").val(data[0].judul_informasi);
            $("#status_aktif").val(data[0].is_aktif);
            $("#keterangan").val(data[0].keterangan);
            let buttonedit = `
            <button type="button" id="btnedit"  class="btn btn-warning me-1 mb-1">
                Update
            </button>
            `;
            $("#button_").html(buttonedit);

            $("#btnedit").click(function (event) {
                event.preventDefault();

                let new_form = $("#form_crud")[0];
                let data = new FormData(new_form);

                $.ajax({
                    url: "/updateDataInformasi",
                    enctype: "multipart/form-data",
                    processData: false,
                    contentType: false,
                    cache: false,
                    type: "POST",
                    data: data,
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil",
                            text: "Data Berhasil Diubah",
                        });

                        reload_table(datatabel);
                        cancel();
                    },
                });
            });
        },
    });
}

function cancel() {
    $("#default").modal("hide");
    $("#id").val("");
    $("#judul_informasi").val("");
    $("#keterangan").val("");
    let buttontambah = `
    <button type="submit" class="btn btn-primary me-1 mb-1">
        Simpan
    </button>
                `;
    $("#button_").html(buttontambah);
}
