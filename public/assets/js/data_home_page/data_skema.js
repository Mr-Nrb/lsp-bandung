$(document).ready(function () {
    datatabel = $("#tblDataSkema").DataTable({
        dom: "Bfrtip",
        ajax: {
            type: "GET",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/getDataSkema",
            dataSrc: "",
            dataType: "json",
        },
        columns: [
            {
                render: function (full, type, data, meta) {
                    return `
                    <button type="button" class="btn btn-outline-warning btn-sm block" title="Edit" data-bs-toggle="modal" data-bs-target="#default" onclick="edit(${data.id})">
                        <i class="bi bi-pencil-square"></i>
                    </button>
                    <button type="button" id="btnHapus" class="btn btn-outline-danger btn-sm block" title="Hapus" onclick="hapus(${data.id})">
                        <i class="bi bi-trash"></i>
                    </button>
                `;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.kejuruan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.skema;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.dokumen;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.is_aktif;
                },
            },
        ],
    });
});

$("#skema_kejuruan").change(function (event) {
    let valSkemaKejuruan = document.getElementById("skema_kejuruan").value;
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "getSubSkema",
        type: "post",
        data: {
            valSkemaKejuruan: valSkemaKejuruan,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            let skema = "";
            data.forEach((element) => {
                skema += `
                <option value="${element.kode}">${element.nama}</option>
                `;
            });
            $("#skema").html(skema);
        },
    });
});

function hapus(id) {
    if (confirm("Yakin Ingin Menghapus?")) {
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/hapusDataSkema",
            type: "POST",
            data: {
                id: id,
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                Swal.fire({
                    icon: "success",
                    title: "Berhasil",
                    text: "Data Berhasil Dihapus",
                });
                reload_table(datatabel);
            },
        });
    } else {
        return false;
    }
}

function reload_table(table) {
    table.ajax.reload();
}

function edit(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "/getDataSkemaById",
        type: "POST",
        data: {
            id: id,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#id").val(id);
            $("#skema_kejuruan").val(data[0].id_kejuruan);
            $("#skema").val(data[0].id_skema);
            $("#status_aktif").val(data[0].is_aktif);
            $("#keterangan").val(data[0].keterangan);
            if (data[0].gambar != "") {
                $("#riview_gambar").attr(
                    "src",
                    "assets/document/home_page/skema/" + data[0].dokumen
                );
            } else {
                $("#riview_gambar").attr("src", "");
            }

            let buttonedit = `
            <button type="button" id="btnedit"  class="btn btn-warning me-1 mb-1">
                Update
            </button>
            `;
            $("#button_").html(buttonedit);

            $("#btnedit").click(function (event) {
                event.preventDefault();

                let new_form = $("#form_crud")[0];
                let data = new FormData(new_form);

                $.ajax({
                    url: "/updateDataSkema",
                    enctype: "multipart/form-data",
                    processData: false,
                    contentType: false,
                    cache: false,
                    type: "POST",
                    data: data,
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil",
                            text: "Data Berhasil Diubah",
                        });

                        reload_table(datatabel);
                        cancel();
                    },
                });
            });
        },
    });
}

function cancel() {
    $("#default").modal("hide");
    // $("#id").val("");
    $("#skema_kejuruan").val("");
    $("#skema").val("");
    $("#riview_gambar").attr("src", "");
    $("#gambar").val("");
    $("#status_aktif").val(1);
    let buttontambah = `
    <button type="submit" class="btn btn-primary me-1 mb-1">
        Simpan
    </button>
                `;
    $("#button_").html(buttontambah);
}
