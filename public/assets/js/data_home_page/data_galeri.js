$(document).ready(function () {
    datatabel = $("#tblDataGaleri").DataTable({
        dom: "Bfrtip",
        ajax: {
            type: "GET",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/getDataGaleri",
            dataSrc: "",
            dataType: "json",
        },
        columns: [
            {
                render: function (full, type, data, meta) {
                    return `
                    <button type="button" class="btn btn-outline-warning btn-sm block" title="Edit" data-bs-toggle="modal" data-bs-target="#default" onclick="edit(${data.id})">
                        <i class="bi bi-pencil-square"></i>
                    </button>
                    <button type="button" id="btnHapus" class="btn btn-outline-danger btn-sm block" title="Hapus" onclick="hapus(${data.id})">
                        <i class="bi bi-trash"></i>
                    </button>
                `;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.judul_galeri;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tanggal_kegiatan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.keterangan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.is_aktif;
                },
            },
        ],
    });
});

function hapus(id) {
    if (confirm("Yakin Ingin Menghapus?")) {
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "/hapusDataGaleri",
            type: "POST",
            data: {
                id: id,
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                Swal.fire({
                    icon: "success",
                    title: "Berhasil",
                    text: "Data Berhasil Dihapus",
                });
                reload_table(datatabel);
            },
        });
    } else {
        return false;
    }
}

function reload_table(table) {
    table.ajax.reload();
}

function edit(id) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "/getDataGaleriById",
        type: "POST",
        data: {
            id: id,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#id").val(id);
            $("#judul_galeri").val(data[0].judul_galeri);
            $("#tanggal_kegiatan").val(data[0].tanggal_kegiatan);
            $("#keterangan").val(data[0].keterangan);
            if (data[0].gambar != "") {
                $("#riview_gambar").attr(
                    "src",
                    "assets/document/home_page/galeri/" + data[0].gambar
                );
            } else {
                $("#riview_gambar").attr("src", "");
            }

            let buttonedit = `
            <button type="button" id="btnedit"  class="btn btn-warning me-1 mb-1">
                Update
            </button>
            `;
            $("#button_").html(buttonedit);

            $("#btnedit").click(function (event) {
                event.preventDefault();

                let new_form = $("#form_crud")[0];
                let data = new FormData(new_form);

                $.ajax({
                    url: "/updateDataGaleri",
                    enctype: "multipart/form-data",
                    processData: false,
                    contentType: false,
                    cache: false,
                    type: "POST",
                    data: data,
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil",
                            text: "Data Berhasil Diubah",
                        });

                        reload_table(datatabel);
                        cancel();
                    },
                });
            });
        },
    });
}

function cancel() {
    $("#default").modal("hide");
    // $("#id").val("");
    $("#judul_galeri").val("");
    $("#tanggal_kegiatan").val("");
    $("#keterangan").val("");
    $("#riview_gambar").attr("src", "");
    $("#gambar").val("");
    // $("#status_aktif").val(1);
    let buttontambah = `
    <button type="submit" class="btn btn-primary me-1 mb-1">
        Simpan
    </button>
                `;
    $("#button_").html(buttontambah);
}
