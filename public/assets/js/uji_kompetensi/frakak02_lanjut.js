$(document).ready(function () {
    datatabel = $("#tblFrakak02_lanjut").DataTable({
        dom: "Bfrtip",
        ajax: {
            type: "GET",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            data: {
                id_jadwal: id_jadwal,
                id: id,
            },
            url: "/getAsesi_ak02",
            dataSrc: "",
            dataType: "json",
        },
        columns: [
            {
                render: function (full, type, data, meta) {
                    return `
                    <div style="margin-left:15%" >
                        <a href="formFrakak01/${data.id_jadwal}/${data.id}" class="btn btn-outline-primary btn-sm block" >Pilih Jadwal Asesmen
                        </a>
                    </div>
                `;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tanggalasesmen;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_asesi;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.namaasesor;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.juduljadwal;
                },
            },
        ],
    });
});

function reload_table(table) {
    table.ajax.reload();
}
