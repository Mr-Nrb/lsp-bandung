$(document).ready(function () {
    let idJadwalAsesmen = document.getElementById("idJadwalAsesmen").value;
    datatabel = $("#tblPadkak01").DataTable({
        dom: "Bfrtip",
        ajax: {
            type: "GET",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            data: {
                idJadwalAsesmen: idJadwalAsesmen,
            },
            url: "/getJadwalAsesmen_ak01",
            dataSrc: "",
            dataType: "json",
        },
        columns: [
            {
                render: function (full, type, data, meta) {
                    return `
                    <div style="margin-left:15%" >
                        <a href="formPadkak01/${data.id}" class="btn btn-outline-primary btn-sm block" >Pilih Jadwal Asesmen
                        </a>
                    </div>
                `;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tanggal_uji;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_jadwal;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_tuk;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tema;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nama_skema;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.nomor_skema;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.lokasi_uji;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tgl_mulai_pelatihan;
                },
            },
            {
                render: function (full, type, data, meta) {
                    return data.tgl_akhir_pelatihan;
                },
            },
        ],
    });
});

function reload_table(table) {
    table.ajax.reload();
}
