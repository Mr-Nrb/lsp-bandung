$("#nama_jadwal").change(function (event) {
    let id = document.getElementById("nama_jadwal").value;
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "getTanggalUjiJadwalAsesmen",
        type: "post",
        data: {
            id: id,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (data.length == 1) {
                $("#tanggal_uji").val(data[0].tanggal_uji);
                $("#id_jadwal").val(data[0].id);
            } else {
                $("#tanggal_uji").val("");
                $("#id_jadwal").val("");
            }
        },
    });
});

$("#asesor").change(function (event) {
    let id = document.getElementById("asesor").value;
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        url: "getNoregAsesor",
        type: "post",
        data: {
            id: id,
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (data.length == 1) {
                $("#met_asesor").val(data[0].met_asesor);
            } else {
                $("#met_asesor").val("");
            }
        },
    });
});

// $("#nama_jadwal").change(function (event) {
//     let id = document.getElementById("nama_jadwal").value;
//     $.ajax({
//         headers: {
//             "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
//         },
//         url: "getIdJadwalAsesmen",
//         type: "post",
//         data: {
//             id: id,
//         },
//         dataType: "json",
//         success: function (data) {
//             console.log(data);
//             if (data.length == 1) {
//                 $("#id_jadwal").val(data[0].id);
//             } else {
//                 $("#id_jadwal").val("");
//             }
//         },
//     });
// });

